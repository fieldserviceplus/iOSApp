//
//  ResponseObjects.swift
//  Field Service Pulse
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper


//MARK: Login and SignUP
class SignUpResponse : Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:SignUpData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
         Result <- map["Result"]
         ResponseMsg <- map["ResponseMsg"]
         data <- map["data"]
    }
}

class LoginResponse : Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:LoginData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}


//MARK: Account
class accountDetailsResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:accountDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class accountRelatedListResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:accountRelatedListData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class ContactResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class LocationResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[LocationData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class WorkOrderResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[WorkOrderData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class CreateAccountResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var AccountID:Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        AccountID <- map["AccountID"]
    }
    
}

//MARK: Work Order
class MyOrdersResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[MyOrdersData]?
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class AllOrdersResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AllOrdersData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class OrderDetailsResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:OrderDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class OrderRelatedListResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:OrderRelatedListData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class EventResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[EventData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class TaskResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[TaskData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class FileResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[FileData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class LineItemsResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[LineItemsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ChemicalResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ChemicalData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class NoteResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[NoteData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class InvoiceResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class GetAllUsersResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var UsersData:[UsersData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        UsersData <- map["data"]
    }
    
}

class AccountTypeResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var TypeData:[AccountTypeData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        TypeData <- map["data"]
    }
    
}

class ContactsResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var ContactsData:[ContactsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        ContactsData <- map["data"]
    }
    
}

class AccountRelatedContactsResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedContactsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}


class WorkOrderTypesResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var WorkOrderTypesData:[WorkOrderTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        WorkOrderTypesData <- map["data"]
    }
    
}

class PriorityTypesResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var PriorityTypesData:[PriorityTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        PriorityTypesData <- map["data"]
    }
    
}

class StatusTypesResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var StatusTypesData:[StatusTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        StatusTypesData <- map["data"]
    }
    
}

class CategoryTypesResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var CategoryTypesData:[CategoryTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        CategoryTypesData <- map["data"]
    }
    
}

class GetAccountTypesResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var GetAccountTypesData:[GetAccountTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        GetAccountTypesData <- map["data"]
    }
    
}
class CreateWOResponse : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var WorkOrderID:Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        WorkOrderID <- map["WorkOrderID"]
    }
    
}

class GetAccountViews : Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetAccountViewsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class AccountListView: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class RecentAccounts: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentAccountsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedTasks: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedTasksData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedEstimate: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedEstimateData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedEvent: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedEventData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedFile: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedFileData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedInvoice: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedInvoiceData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedChemical: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedChemicalData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountRelatedProduct: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountRelatedProductData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountFields: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[AccountFieldsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class AccountFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class WOGetViews: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[WOGetViewsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class ViewWOList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:Any]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class WOFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetWOViewFields: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetWOViewFieldsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class RecentWO: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentWOData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetProducts: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetProductsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetChemicals: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetChemicalsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetProductFamily: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetProductFamilyData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetUnitOfMeasurement: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetUnitOfMeasurementData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetEventTaskByDates: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEventTaskByDatesData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class CalendarFilterData: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:CalendarFilterDataDict?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetWorkOrderByDate: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetWorkOrderByDateData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class RecentEstimates: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentEstimatesData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class CreateEstimate: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var EstimateID:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        EstimateID <- map["EstimateID"]
    }
}

class EstimateDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:EstimateDetailsData?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class EstimateRelatedList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:EstimateRelatedListData?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetEstimateViews: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEstimateViewsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateListView: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetEstimateStatus: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEstimateStatusData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateRelatedLineItem: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[EstimateRelatedLineItemData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateRelatedEvent: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[EstimateRelatedEventData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateRelatedTask: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[EstimateRelatedTaskData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateRelatedFile: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[EstimateRelatedFileData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateRelatedNote: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[EstimateRelatedNoteData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetEstimateViewFields: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEstimateViewFieldsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class EstimateFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetEstimates: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEstimatesData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class CreateContact: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var ContactID:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        ContactID <- map["ContactID"]
    }
}

class RecentContacts: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentContactsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class ContactDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:ContactDetailsData?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class ContactRelatedList : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:ContactRelatedListData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactGetViews : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactGetViewsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class GetLeadSources : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetLeadSourcesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class GetSalutations : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetSalutationsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetTitlesOfContact : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetTitlesOfContactData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedWorkOrder : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedWorkOrderData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedEstimate : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedEstimateData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedInvoice : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedInvoiceData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedEvent : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedEventData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedTask : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedTaskData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedFile : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedFileData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class ContactRelatedNote : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[ContactRelatedNoteData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class ContactFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class RecentInvoices: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentInvoicesData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class CreateInvoice: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var InvoiceID:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        InvoiceID <- map["InvoiceID"]
    }
}

class InvoiceDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:InvoiceDetailsDict?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class InvoiceRelatedListForInvoice: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:InvoiceRelatedList?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class InvoiceGetViews: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceGetViewsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetInvoiceStatus: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetInvoiceStatusData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class GetParentWorkOrders: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetParentWorkOrdersData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class ViewInvoiceList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:Any]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetInvoicePaymentTerms: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetInvoicePaymentTermsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class InvoiceRelatedLineItem: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceRelatedLineItemData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class InvoiceRelatedEvent: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceRelatedEventData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class InvoiceRelatedTask: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceRelatedTaskData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class InvoiceRelatedNote: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceRelatedNoteData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class InvoiceRelatedFile: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[InvoiceRelatedFileData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class InvoiceFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class GetInvoices: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetInvoicesData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}


class CreateTask: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var TaskID:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        TaskID <- map["TaskID"]
    }
}
class RecentTasks: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentTasksData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class TaskDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:TaskDetailsDict?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class GetTaskType: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetTaskTypeData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class GetTaskStatus: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetTaskStatusData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetTaskPriority: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetTaskPriorityData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetRelatedObjList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetRelatedObjListData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetRelatedToList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetRelatedToListData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class TaskGetViews: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[TaskGetViewsData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class ViewTaskList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:Any]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class TaskFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetViewsFile: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetViewsFileData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class CreateFile : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var FileID:Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        FileID <- map["FileID"]
    }
    
}

class RecentFiles: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentFilesData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class ViewFileList: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class FileFilter: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class FileDetails : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:FileDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class CreateLocation : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var LocationID:Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        LocationID <- map["LocationID"]
    }
    
}
class LocationDetails : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:LocationDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class RecentLocations : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[RecentLocationsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class EditLocation : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
    }
    
}

class GetLocationTypes : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetLocationTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetParentLocations : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetParentLocationsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetViewsLocations : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetViewsLocationsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}


class ViewLocationList : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:Any]]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}



class LocationFilter : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[[String:String]]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetEmailTemplates : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEmailTemplatesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class CreateEvent : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var EventID:Int?
    var Subject:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        EventID <- map["EventID"]
        Subject <- map["Subject"]
    }
    
}
class GetEventPriorities : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEventPrioritiesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}
class GetEventTypes : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetEventTypesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetSchedule : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetScheduleData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetTasks : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetTasksData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetRecents : Mappable{
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetRecentsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
    
}

class GetCustomViewDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:GetCustomViewDetailsDict?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}


class EventDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:EventDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class NoteDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:NoteDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class UserDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:UserDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}
class ProductDetails: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:ProductDetailsData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class ConvertToWO: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var WorkOrderID:Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        WorkOrderID <- map["WorkOrderID"]
    }
}
class ConvertToInvoice: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var InvoiceID:Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        InvoiceID <- map["InvoiceID"]
    }
}
class GenerateDocument: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:GenerateDocumentData?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class GetGenDocTemplates: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetGenDocTemplatesData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

class ResponseDict: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
    }
}
class GetCustomFields: Mappable {
    
    var Result:String?
    var ResponseMsg:String?
    var data:[GetCustomFieldsData]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Result <- map["Result"]
        ResponseMsg <- map["ResponseMsg"]
        data <- map["data"]
    }
}

