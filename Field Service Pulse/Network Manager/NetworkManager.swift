//
//  NetworkManager.swift
//  Field Service Pulse
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD




class NetworkManager
{
    
    static let sharedInstance = NetworkManager()
    
    func signUp(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (SignUpResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
        print(parameters!)
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<SignUpResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
    }
    
    func login(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (LoginResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<LoginResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountDetails(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (accountDetailsResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<accountDetailsResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedList(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (accountRelatedListResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<accountRelatedListResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
    }
    
    func contacts(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func locations(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (LocationResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<LocationResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func workOrders(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (WorkOrderResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<WorkOrderResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func createAccount(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateAccountResponse) -> Void){
        print(parameters)
        print(urlString)
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateAccountResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func myOrders(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (MyOrdersResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<MyOrdersResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func allOrders(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AllOrdersResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AllOrdersResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func orderDetails(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (OrderDetailsResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<OrderDetailsResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func orderRelatedList(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (OrderRelatedListResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<OrderRelatedListResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func events(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EventResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EventResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func file(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (FileResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<FileResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func task(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (TaskResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<TaskResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func lineItems(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (LineItemsResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<LineItemsResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func chemical(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ChemicalResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ChemicalResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func note(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (NoteResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<NoteResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoice(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getAllUsers(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetAllUsersResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetAllUsersResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountType(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountTypeResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountTypeResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func contacts(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactsResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactsResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedContacts(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedContactsResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedContactsResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func workOrderTypes(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (WorkOrderTypesResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<WorkOrderTypesResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func priorityTypes(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (PriorityTypesResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<PriorityTypesResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func statusTypes(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (StatusTypesResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<StatusTypesResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func categoryTypes(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CategoryTypesResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CategoryTypesResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getAccountTypes(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetAccountTypesResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetAccountTypesResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }

    func createWO(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateWOResponse) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateWOResponse>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getAccountView(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetAccountViews) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetAccountViews>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountListView(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountListView) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountListView>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func recentAccounts(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentAccounts) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentAccounts>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedTasks(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedTasks) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedTasks>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedEstimate(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedEstimate) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedEstimate>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedEvent(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedEvent) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedEvent>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedInvoice(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedInvoice) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedInvoice>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedFile(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedFile) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedFile>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedChemical(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedChemical) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedChemical>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountRelatedProduct(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountRelatedProduct) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountRelatedProduct>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func accountFields(urlString: String, parameters: [String: String]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountFields) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountFields>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func accountFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (AccountFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<AccountFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func woFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (WOFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<WOFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func woGetViews(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (WOGetViews) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<WOGetViews>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func viewWOList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ViewWOList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ViewWOList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getWOViewFields(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetWOViewFields) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetWOViewFields>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func recentWO(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentWO) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentWO>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getProducts(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetProducts) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetProducts>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getChemicals(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetChemicals) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetChemicals>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getProductFamily(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetProductFamily) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetProductFamily>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getUnitOfMeasurement(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetUnitOfMeasurement) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetUnitOfMeasurement>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getEventTaskByDates(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEventTaskByDates) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEventTaskByDates>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func calendarFilterData(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CalendarFilterData) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CalendarFilterData>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getWorkOrderByDate(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetWorkOrderByDate) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetWorkOrderByDate>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func viewEstimateList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateListView) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateListView>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func createEstimate(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateEstimate) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateEstimate>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func recentEstimates(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentEstimates) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentEstimates>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateRelatedList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateRelatedList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateRelatedList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getEstimateViews(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEstimateViews) -> Void) {
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEstimateViews>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateListViews(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateListView) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateListView>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getEstimateStatus(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEstimateStatus) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEstimateStatus>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateRelatedLineItem(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateRelatedLineItem) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateRelatedLineItem>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateRelatedEvent(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateRelatedEvent) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateRelatedEvent>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateRelatedTask(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateRelatedTask) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateRelatedTask>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateRelatedFile(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateRelatedFile) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateRelatedFile>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateRelatedNote(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateRelatedNote) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateRelatedNote>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getEstimateViewFields(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEstimateViewFields) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEstimateViewFields>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func estimateFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EstimateFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EstimateFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getEstimateProducts(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEstimates) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEstimates>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func createContact(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateContact) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateContact>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func recentContacts(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentContacts) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentContacts>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
    }
    func contactGetViews(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactGetViews) -> Void){
            
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactGetViews>) in
                    SVProgressHUD.dismiss()
                    let userData = response.result.value
                    print(userData)
                guard let _ = userData else {
                    return
                }
                    completionHandler(userData!)
                }
            }
            else{
                SVProgressHUD.dismiss()
                Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
            }
        
    }
    func getLeadSources(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetLeadSources) -> Void){
            
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetLeadSources>) in
                    SVProgressHUD.dismiss()
                    let userData = response.result.value
                    print(userData)
                guard let _ = userData else {
                    return
                }
                    completionHandler(userData!)
                }
            }
            else{
                SVProgressHUD.dismiss()
                Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
            }
            
    }
    func getSalutations(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetSalutations) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetSalutations>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getTitlesOfContact(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetTitlesOfContact) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetTitlesOfContact>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedWorkOrder(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedWorkOrder) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedWorkOrder>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedEstimate(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedEstimate) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedEstimate>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedInvoice(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedInvoice) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedInvoice>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedEvent(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedEvent) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedEvent>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedTask(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedTask) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedTask>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedFile(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedFile) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedFile>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactRelatedNote(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactRelatedNote) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactRelatedNote>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func contactFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ContactFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ContactFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func createInvoice(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateInvoice) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateInvoice>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func recentInvoices(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentInvoices) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentInvoices>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceRelatedList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceRelatedListForInvoice) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceRelatedListForInvoice>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getInvoiceViews(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceGetViews) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceGetViews>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getInvoiceStatus(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetInvoiceStatus) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetInvoiceStatus>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getParentWorkOrders(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetParentWorkOrders) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetParentWorkOrders>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func viewInvoiceList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ViewInvoiceList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ViewInvoiceList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getInvoicePaymentTerms(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetInvoicePaymentTerms) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetInvoicePaymentTerms>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceRelatedLineItem(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceRelatedLineItem) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceRelatedLineItem>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceRelatedEvent(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceRelatedEvent) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceRelatedEvent>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceRelatedTask(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceRelatedTask) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceRelatedTask>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceRelatedNote(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceRelatedNote) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceRelatedNote>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceRelatedFile(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceRelatedFile) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceRelatedFile>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func invoiceFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (InvoiceFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<InvoiceFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getInvoiceProducts(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetInvoices) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetInvoices>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func createTask(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateTask) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateTask>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func recentTasks(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentTasks) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentTasks>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func taskDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (TaskDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<TaskDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getTaskType(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetTaskType) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetTaskType>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getTaskStatus(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetTaskStatus) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetTaskStatus>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getTaskPriority(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetTaskPriority) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetTaskPriority>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getRelatedObjList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetRelatedObjList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetRelatedObjList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getRelatedToList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetRelatedToList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetRelatedToList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func taskGetViews(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (TaskGetViews) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<TaskGetViews>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func viewTaskList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ViewTaskList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ViewTaskList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func taskFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (TaskFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<TaskFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getViewsFile(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetViewsFile) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetViewsFile>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func recentFiles(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentFiles) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentFiles>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func viewFileList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ViewFileList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ViewFileList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func fileFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (FileFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<FileFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func fileDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (FileDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<FileDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func createLocation(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateLocation) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateLocation>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func recentLocations(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (RecentLocations) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<RecentLocations>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func locationDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (LocationDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<LocationDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func editLocation(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EditLocation) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EditLocation>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getLocationTypes(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetLocationTypes) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetLocationTypes>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getParentLocations(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetParentLocations) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetParentLocations>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getViewsLocations(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetViewsLocations) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetViewsLocations>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func viewLocationList(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ViewLocationList) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ViewLocationList>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func locationFilter(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (LocationFilter) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<LocationFilter>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getEmailTemplates(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEmailTemplates) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEmailTemplates>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func createEvent(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (CreateEvent) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<CreateEvent>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getEventPriorities(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEventPriorities) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEventPriorities>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getEventTypes(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetEventTypes) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetEventTypes>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getSchedules(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetSchedule) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetSchedule>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getTasks(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetTasks) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetTasks>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func getRecents(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetRecents) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetRecents>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
   
    func getCustomViewDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetCustomViewDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetCustomViewDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func eventDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (EventDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<EventDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    
    func noteDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (NoteDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<NoteDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func userDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (UserDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<UserDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func productDetails(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ProductDetails) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ProductDetails>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func convertToWO(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ConvertToWO) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ConvertToWO>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func convertToInvoice(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ConvertToInvoice) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ConvertToInvoice>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getGenDocTemplates(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetGenDocTemplates) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetGenDocTemplates>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func generateDocument(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GenerateDocument) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GenerateDocument>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func getCustomFields(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (GetCustomFields) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<GetCustomFields>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
        
    }
    func webserviceCall(urlString: String, parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ResponseDict) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            Alamofire.request(urlString, method: .post, parameters: parameters!, encoding: URLEncoding.default, headers: headers).responseObject { (response :DataResponse<ResponseDict>) in
                SVProgressHUD.dismiss()
                let userData = response.result.value
                print(userData)
                guard let _ = userData else {
                    return
                }
                completionHandler(userData!)
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
    }
    
    func createNewFileAll(urlString: String, pickedImages:[UIImage], parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ResponseDict) -> Void){
        
        if Helper.instance.connectedToNetwork() {
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                for i in 0..<pickedImages.count {
                    let imgData = pickedImages[i].jpegData(compressionQuality: 0.2)!
                    multipartFormData.append(imgData, withName: "FileName[]",fileName: "file.jpg", mimeType: "image/jpg")
                }
                
                for (key, value) in parameters! {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                } //Optional for extra parameters
            },
                             to:urlString, method: .post, headers: headers)
            
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseObject(completionHandler: { (response:DataResponse<ResponseDict>) in
                        SVProgressHUD.dismiss()
                        let userData = response.result.value
                        completionHandler(userData!)
                        
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    SVProgressHUD.dismiss()
                }
            }
        }
        else{
            SVProgressHUD.dismiss()
            Helper.instance.showAlertNotification(message: Message.NoInternet, vc: vc)
        }
    }
    

    func uploadDocs(urlString: String,fileName: String, URLs: [NSURL], parameters: [String: Any]?, headers: [String:String], vc:UIViewController,completionHandler: @escaping (ResponseDict) -> Void)
    {
        Alamofire.upload(
            multipartFormData: {
                multipartFormData in
                
                for i in 0..<URLs.count {
                    
                    do {
                        let pdfData =  try Data(contentsOf: URLs[i].absoluteURL!)
                    
                        multipartFormData.append(pdfData, withName: "\(fileName)[]", fileName: URLs[i].lastPathComponent!, mimeType:"application/pdf")
                }catch{}
                
                    for (key, value) in parameters! {
                        multipartFormData.append(((value as? String)?.data(using: .utf8))!, withName: key)
                    }
                }
        },
            to: urlString,
            method: .post,
            headers: headers,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseObject(completionHandler: { (response:DataResponse<ResponseDict>) in
                        SVProgressHUD.dismiss()
                        let userData = response.result.value
                        completionHandler(userData!)
                        print("SUCCESS")
                    })
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
    
    
}








    
    

