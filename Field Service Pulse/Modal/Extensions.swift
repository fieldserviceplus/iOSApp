//
//  Extensions.swift
//  Field Service Pulse
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

//MARK: Extensions

//UIButton

extension UIButton {
    
    func giveCornerRadius() {
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            self.layer.cornerRadius = 20
        } else { //IPAD
            self.layer.cornerRadius = 30
        }
    }
    
    func giveBorderToButton()
    {
        self.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        self.layer.borderWidth = 2
    }
    
    func leftImage(image: UIImage, renderMode: UIImage.RenderingMode) {
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        
        self.contentHorizontalAlignment = .left
        self.imageView?.contentMode = .scaleAspectFit
        self.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            self.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: image.size.width / 2)
            self.titleEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
        } else { //IPAD
            self.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: image.size.width / 2)
            self.titleEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        }
    }
    
    func setRightImage()
    {
        self.imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
    }
    
}

//UILabel

extension UILabel {
    
    func giveBorderToLabel()
    {
        self.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        self.layer.borderWidth = 2
    }
}

//UIView

extension UIView {
    
    func dropShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 2
    }
    
    func giveBorderToView()
    {
        self.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        self.layer.borderWidth = 2
    }
}

//Textfield

extension UITextField{
    
    func setRightImage(name:String, placeholder:String)
    {
        let arrow = UIImageView(image: UIImage(named: name))
        if let size = arrow.image?.size {
            arrow.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        arrow.contentMode = UIView.ContentMode.center
        self.rightView = arrow
        self.rightViewMode = UITextField.ViewMode.always
        
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.layer.borderWidth = 1.5
        self.layer.borderColor = UIColor.darkGray.cgColor
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 4, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func removeRightImage()
    {
        let arrow = UIImageView(image: UIImage(named: ""))
        if let size = arrow.image?.size {
            arrow.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        arrow.contentMode = UIView.ContentMode.center
        self.rightView = arrow
        self.rightViewMode = UITextField.ViewMode.always
        self.attributedPlaceholder = NSAttributedString(string: "--None--", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.layer.borderWidth = 1.5
        self.layer.borderColor = UIColor.darkGray.cgColor
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 4, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}


//UITextview

extension UITextView {
    
    func giveBorder()
    {
        self.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        self.layer.borderWidth = 2
    }
}

//UIApplication
extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

//CGFloat
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

//String

extension String{
    
    func isValidEmail() -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func isPasswordValid() -> Bool {
        if self.count >= 5 &&  self.count <= 15
        {
            return true
        }
        else{
            return false
        }
    }
    
    func isBlank() -> Bool {
        let trimmedString = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString.isEmpty
    }
    
   func camelCaseToWords() -> String {
            
            return unicodeScalars.reduce("") {
                if CharacterSet.uppercaseLetters.contains($1) == true {
                    return ($0 + " " + String($1))
                }
                else {
                    return $0 + String($1)
                }
            }
    }
    
    func convertToCurrencyFormat() -> String
    {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current
        // We'll force unwrap with the !, if you've got defined data you may need more error checking
        return currencyFormatter.string(from: NSNumber(value: Double(self) ?? 0))!
    }
    
    func convertFromCurrencyFormat() -> String
    {
        var str = self
        let charset = CharacterSet(charactersIn: "$")
        if self.rangeOfCharacter(from: charset) != nil {
            print("yes")
        }
        else{
            str = "$" + self
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        if let number = formatter.number(from: str) {
            let amount = number.doubleValue
           return String(amount)
        }
        
        return "0.0"
    }
    
    func removeSpecialCharacters() -> String {
        
        return self.components(separatedBy:CharacterSet.decimalDigits.inverted)
            .joined(separator: "")
    }
    
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.endIndex.encodedOffset
        } else {
            return false
        }
    }
    
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.characters.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.characters.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

//UIViewController
extension UIViewController{
    
    func showHUD()
    {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setForegroundColor(UIColor.black)           //Ring Color
        SVProgressHUD.setBackgroundColor(UIColor.white)        //HUD Color
        SVProgressHUD.setBackgroundLayerColor(UIColor.darkGray.withAlphaComponent(0.5))
        SVProgressHUD.show(withStatus: "Please Wait...")
    }
    
    //Hide Keyboard
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


//UIColor

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

//Array
extension Array {
    
    mutating func remove(at indexs: [Int]) {
        guard !isEmpty else { return }
        let newIndexs = Set(indexs).sorted(by: >)
        newIndexs.forEach {
            guard $0 < count, $0 >= 0 else { return }
            remove(at: $0)
        }
    }
}

//UIimage

extension UIImage {
    
    func resizeImage(newWidth: CGFloat) -> UIImage {
        
        let scale = CGFloat(55.0 / 349.0)
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        self.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension Date {
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
}
