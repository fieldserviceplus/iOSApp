//
//  Helper.swift
//  Field Service Pulse
//
//  Created by Apple on 01/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import SystemConfiguration


class Helper: NSObject {
    
    static let instance = Helper()
    
    func showAlertNotification(message:String, vc:UIViewController)
    {
        // the alert view
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        vc.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlertNotificationWithBack(message:String, vc:UIViewController)
    {
        // the alert view
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        vc.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
            vc.navigationController?.popViewController(animated: true)
        }
    }
    
    func showAlertView(message:String, vc:UIViewController) {
        let alert = UIAlertController(title: "Reminder", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    func convertDateFormat(date: String, fromFormat:String, toFormat:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = toFormat
        return  dateFormatter.string(from: date!)
    }
    
    func saveToUserDefaults(key:String, value:Any)
    {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func getUserDefaultsValue(key:String) -> Any
    {
        return UserDefaults.standard.value(forKey: key) as Any 
    }
    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags : SCNetworkReachabilityFlags = []
        
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func addLabel(viewBackground:UIView, upperView:UIView) {
        
        var constant = 0
        let customLabel = UILabel()
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            constant = 20
            customLabel.font = UIFont(name: "System-Semibold", size: 17)
            
        } else { //IPAD
            constant = 40
            customLabel.font = UIFont(name: "System-Semibold", size: 22)
        }
        
        customLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        customLabel.text = "Extra Fields"
        
        viewBackground.addSubview(customLabel)
        
        customLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: customLabel, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: upperView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
        let leadingConstraint = NSLayoutConstraint(item: customLabel, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: upperView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 5)
        let heightConstraint = NSLayoutConstraint(item: customLabel, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 15)
        viewBackground.addConstraints([topConstraint,leadingConstraint,heightConstraint])
    }
}

@IBDesignable class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 5.0
    @IBInspectable var rightInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}



