//
//  Constants.swift
//  Field Service Pulse
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class User: NSObject {
    
    static let instance = User()
    
    let screenSize = UIScreen.main.bounds
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    let deviceID = UIDevice.current.identifierForVendor!.uuidString
    let key = "03d612662bf82f471741773cf9bd5a79"
    var UserID = ""
    var token = ""
    var OrganizationID = ""
    let googleApiKey = "AIzaSyDIsBxYlPTfCfpir0vxd5YdVD6cZ9Wf7KY"
    
    //ID
    lazy var accountID = ""
    lazy var workorderID =  ""
    lazy var estimateID = ""
    lazy var allOrdersArr:[AllOrdersData]? = []
    lazy var acID = ""
    lazy var contactID = ""
    lazy var invoiceID = ""
    lazy var taskID = ""
    lazy var fileID = ""
    lazy var locationID = ""
    lazy var eventID = ""
    lazy var noteID = ""
    
    //Object
    lazy var accountName = ""
    lazy var workorderSubject =  ""
    lazy var estimateName = ""
    lazy var contactName = ""
    lazy var invoiceNo = ""
    
    lazy var accountNameForProductUI = ""
    
    lazy var ShowTasks:String = "1"
    lazy var AssignedToArr:[String] = []
    
    lazy var eventTitleArr:[String] = []
    lazy var eventTimeArr:[String] = []
    lazy var eventColorArr:[String] = []
    lazy var eventIDArr:[String] = []
    lazy var eventTypeArr:[String] = []
    lazy var flag = "1"
    lazy var flagForEditChemical = "1"
    lazy var flagForGetUsers = "1"
    lazy var flagSelectAll = "1"
    lazy var flagSelectAllMap = "1"
    lazy var flagType = "1"
    lazy var flagStatus = "1"
    lazy var flagPriority = "1"
    
    lazy var productIDArr:[String] = []
    lazy var productNameArr:[String] = []
    lazy var quantityArr:[String] = []
    lazy var listPriceArr:[String] = []
    lazy var taxableArr:[String] = []
    lazy var taxArr:[String] = []
    lazy var listPriceEditableArr:[String] = []
    lazy var quantityEditableArr:[String] = []
    lazy var discountArr:[String] = []
    
    lazy var chemicalArr:[String] = []
    lazy var tested1Arr:[String] = []
    lazy var tested2Arr:[String] = []
    lazy var app1Arr:[String] = []
    lazy var app2Arr:[String] = []
    lazy var appAreaArr:[String] = []
    lazy var appNoteArr:[String] = []
    
    lazy var allUsersArr:[String] = []
    
    lazy var checkboxArr:[String] = []
    lazy var selectedUsersArr:[String] = []
    lazy var selectedUsersIDArr:[String] = []
    
    lazy var checkboxTypeArr:[String] = []
    lazy var checkboxStatusArr:[String] = []
    lazy var checkboxPriorityArr:[String] = []
    lazy var typeIDArr:[String] = []
    lazy var statusIDArr:[String] = []
    lazy var priorityIDArr:[String] = []
    
    //MAP
    lazy var checkboxMapArr:[String] = []
    lazy var selectedUsersMapArr:[String] = []
    lazy var selectedUsersIDMapArr:[String] = []
    lazy var checkboxTypeMapArr:[String] = []
    lazy var checkboxStatusMapArr:[String] = []
    lazy var checkboxPriorityMapArr:[String] = []
    lazy var typeIDMapArr:[String] = []
    lazy var statusIDMapArr:[String] = []
    lazy var priorityIDMapArr:[String] = []
    
    lazy var allUsersMapArr:[String] = []
    
    lazy var flagMap = "1"
    lazy var flagForGetUsersMap = "1"
    lazy var flagTypeMap = "1"
    lazy var flagStatusMap = "1"
    lazy var flagPriorityMap = "1"
    lazy var flagMapWO = "1"
    
    lazy var workOrderByDateArr:[GetEventTaskByDatesData] = []
    
    lazy var flagAddProducts:String = "1"
    
    //Edit Lines(WO)
    
    lazy var listPriceArr1:[String] = []
    lazy var discountArr1:[String] = []
    lazy var quantityArr1:[String] = []
    lazy var taxableArr1:[String] = []
    lazy var taxArr1:[String] = []
    lazy var listPriceEditableArr1:[String] = []
    lazy var quantityEditableArr1:[String] = []
    lazy var productIDArr1:[String] = []
    lazy var productArr1:[String] = []
    
    lazy var flagSkipProduct = "0"
    
    //Edit Lines(Estimate)
    
    lazy var listPriceArr2:[String] = []
    lazy var discountArr2:[String] = []
    lazy var quantityArr2:[String] = []
    lazy var taxableArr2:[String] = []
    lazy var taxArr2:[String] = []
    lazy var listPriceEditableArr2:[String] = []
    lazy var quantityEditableArr2:[String] = []
    lazy var productIDArr2:[String] = []
    lazy var productArr2:[String] = []
    
    lazy var flagSkipEstimateProduct = "0"
    lazy var flagAddEstimateProducts:String = "1"
    
    lazy var productIDArr3:[String] = []
    lazy var productNameArr3:[String] = []
    lazy var quantityArr3:[String] = []
    lazy var listPriceArr3:[String] = []
    lazy var taxableArr3:[String] = []
    lazy var taxArr3:[String] = []
    lazy var listPriceEditableArr3:[String] = []
    lazy var quantityEditableArr3:[String] = []
    lazy var discountArr3:[String] = []
    
    //Edit Lines(Invoice)
    
    lazy var listPriceArr4:[String] = []
    lazy var discountArr4:[String] = []
    lazy var quantityArr4:[String] = []
    lazy var taxableArr4:[String] = []
    lazy var taxArr4:[String] = []
    lazy var listPriceEditableArr4:[String] = []
    lazy var quantityEditableArr4:[String] = []
    lazy var productIDArr4:[String] = []
    lazy var productArr4:[String] = []
    
    lazy var flagSkipInvoiceProduct = "0"
    lazy var flagAddInvoiceProducts:String = "1"
    
    lazy var productIDArr5:[String] = []
    lazy var productNameArr5:[String] = []
    lazy var quantityArr5:[String] = []
    lazy var listPriceArr5:[String] = []
    lazy var taxableArr5:[String] = []
    lazy var taxArr5:[String] = []
    lazy var listPriceEditableArr5:[String] = []
    lazy var quantityEditableArr5:[String] = []
    lazy var discountArr5:[String] = []
}
struct API {
    
    static let baseURL = "http://beforesubmit.com/fieldserviceplus/API/"
    
    static let signUpURL = API.baseURL + "User/Auth/Signup/"
    static let loginURL = API.baseURL + "User/Auth/Login/"
    static let accountDetailsURL = API.baseURL + "Account/AcActions/AccountDetails/"
    static let accountRelatedListURL = API.baseURL + "Account/AcActions/AccountRelatedList/"
    static let contactsURL = API.baseURL + "Account/AcActions/AccountRelatedContact/"
    static let locationURL = API.baseURL + "Account/AcActions/AccountRelatedLocation/"
    static let workOrdersURL = API.baseURL + "Account/AcActions/AccountRelatedWorkOrder/"
    static let createAccountURL = API.baseURL + "Account/AcActions/CreateAccount/"
    static let myOrdersURL = API.baseURL + "WorkOrder/WoActions/RecentMyWorkOrders/"
    static let allOrdersURL = API.baseURL + "WorkOrder/WoActions/RecentAllWorkOrders/"
    static let orderDetailsURL = API.baseURL + "WorkOrder/WoActions/WorkOrderDetails/"
    static let editAccountURL = API.baseURL + "Account/AcActions/EditAccount/"
    static let editOrderURL = API.baseURL + "WorkOrder/WoActions/EditWorkOrder/"
    static let orderRelatedListURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedList/"
    static let eventURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedEvent/"
    static let taskURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedTask/"
    static let fileURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedFile/"
    static let forgotPasswordURL = API.baseURL + "User/Auth/ForgotPassword/"
    static let getAllUsersURL = API.baseURL + "User/Data/GetAllUsers/"
    static let accountTypeURL = API.baseURL + "Account/Data/GetAccountType/"
    static let allContactsURL = API.baseURL + "Contact/Data/GetContacts/"
    static let woTypesURL = API.baseURL + "WorkOrder/Data/GetWorkOrderTypes/"
    static let priorityTypesURL = API.baseURL + "WorkOrder/Data/GetWorkOrderPriorities/"
    static let statusTypesURL = API.baseURL + "WorkOrder/Data/GetWorkOrderStatus/"
    static let categoryTypesURL = API.baseURL + "WorkOrder/Data/GetWorkOrderCategories/"
    static let getAccountTypesURL = API.baseURL + "Account/Data/GetAccounts/"
    static let createWOURL = API.baseURL + "WorkOrder/WoActions/CreateWorkOrder/"
    static let getAccountViewsURL = API.baseURL + "Account/View/GetViews"
    static let accountListViewsURL = API.baseURL + "Account/View/ViewAccountList"
    static let accountRelatedTaskURL = API.baseURL + "Account/AcActions/AccountRelatedTask/"
    static let accountRelatedEstimateURL = API.baseURL + "Account/AcActions/AccountRelatedEstimate/"
    static let accountRelatedEventURL = API.baseURL + "Account/AcActions/AccountRelatedEvent/"
    static let accountRelatedInvoiceURL = API.baseURL + "Account/AcActions/AccountRelatedInvoice/"
    static let accountRelatedFileURL = API.baseURL + "Account/AcActions/AccountRelatedFile/"
    static let accountRelatedChemicalURL = API.baseURL + "Account/AcActions/AccountRelatedChemical/"
    static let accountRelatedProductURL = API.baseURL + "Account/AcActions/AccountRelatedProduct/"
    static let recentAccountsURL = API.baseURL + "Account/AcActions/RecentAccounts/"
    static let accountFieldsURL = API.baseURL +  "Account/View/GetAccountViewFields"
    static let accountFilterURL = API.baseURL + "Account/View/AccountFilter/"
    static let recentWOURL = API.baseURL + "WorkOrder/WoActions/RecentWorkOrders/"
    static let getWOViewFieldsURL = API.baseURL + "WorkOrder/View/GetWorkOrderViewFields/"
    static let woFilterURL = API.baseURL + "WorkOrder/View/WorkOrderFilter/"
    static let viewWOListURL = API.baseURL + "WorkOrder/View/ViewWorkOrderList"
    static let woGetViewsURL = API.baseURL + "WorkOrder/View/GetViews"
    static let lineItemsURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedLineItem/"
    static let chemicalURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedChemical/"
    static let noteURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedNote/"
    static let invoiceURL = API.baseURL + "WorkOrder/WoActions/WorkOrderRelatedInvoice/"
    static let getProducts = API.baseURL + "WorkOrder/WoActions/GetProducts/"
    static let getChemicals = API.baseURL + "WorkOrder/WoActions/GetChemicals/"
    static let accountRelatedContactURL = API.baseURL + "Account/AcActions/AccountRelatedContact/"
    static let saveSignatureURL = API.baseURL + "WorkOrder/WoActions/SaveWorkOrderSignature/"
    static let getProductFamilyURL = API.baseURL + "WorkOrder/WoActions/GetProductFamily/"
    static let getUnitOfMeasurementURL = API.baseURL + "WorkOrder/WoActions/GetUnitOfMeasurement/"
    static let saveWOChemicalsURL = API.baseURL + "WorkOrder/WoActions/SaveWorkorderChemicals/"
    static let saveWOLineItemsURL = API.baseURL + "WorkOrder/WoActions/SaveWorkorderLineItems/"
    static let getEventTaskByDatesURL = API.baseURL + "Calendar/CalendarView/GetEventTaskByDates/"
    static let calendarFilterDataURL = API.baseURL + "Calendar/CalendarView/CalendarFilterData/"
    static let getWorkOrderByDateURL = API.baseURL + "Calendar/CalendarView/GetWorkOrderByDate/"
    static let RecentEstimatesURL = API.baseURL + "Estimate/ETActions/RecentEstimates/"
    static let createEstimatesURL = API.baseURL + "Estimate/ETActions/CreateEstimate"
    static let estimateDetailsURL = API.baseURL + "Estimate/ETActions/EstimateDetails/"
    static let estimateRelatedListURL = API.baseURL + "Estimate/ETActions/EstimateRelatedList/"
    static let editEstimateURL = API.baseURL + "Estimate/ETActions/EditEstimate"
    static let viewEstimateListURL = API.baseURL + "Estimate/View/ViewEstimateList"
    static let getEstimateViewsURL = API.baseURL + "Estimate/View/GetViews"
    static let estimateStatusURL = API.baseURL + "Estimate/Data/GetEstimateStatus/"
    static let estimateRelatedLineItemURL = API.baseURL + "Estimate/ETActions/EstimateRelatedLineItem/"
    static let estimateRelatedEventURL = API.baseURL + "Estimate/ETActions/EstimateRelatedEvent/"
    static let estimateRelatedTaskURL = API.baseURL + "Estimate/ETActions/EstimateRelatedTask/"
    static let estimateRelatedFileURL = API.baseURL + "Estimate/ETActions/EstimateRelatedFile/"
    static let estimateRelatedNoteURL = API.baseURL + "Estimate/ETActions/EstimateRelatedNote/"
    static let saveEstimateSignatureURL = API.baseURL + "Estimate/ETActions/SaveEstimateSignature/"
    static let getEstimateViewFieldsURL = API.baseURL + "Estimate/View/GetEstimateViewFields/"
    static let estimateFilterURL = API.baseURL + "Estimate/View/EstimateFilter/"
    static let getEstimateProductsURL = API.baseURL + "Estimate/ETActions/GetProducts/"
    static let saveEstimateLineItemsURL = API.baseURL + "Estimate/ETActions/SaveEstimateLineItems/"
    static let recentContactsURL = API.baseURL + "Contact/CTActions/RecentContacts"
    static let createContactURL = API.baseURL + "Contact/CTActions/CreateContact"
    static let contactDetailsURL = API.baseURL + "Contact/CTActions/ContactDetails"
    static let editContactURL = API.baseURL + "Contact/CTActions/EditContact"
    static let contactRelatedListURL = API.baseURL + "Contact/CTActions/ContactRelatedList"
    static let contactGetViewsURL = API.baseURL + "Contact/View/GetViews"
    static let getLeadSourcesURL = API.baseURL + "Contact/Data/GetLeadSources"
    static let getSalutationsURL = API.baseURL + "Contact/Data/GetSalutations"
    static let getTitlesOfContactURL = API.baseURL + "Contact/Data/GetTitlesOfContact"
    static let contactRelatedWorkOrderURL = API.baseURL + "Contact/CTActions/ContactRelatedWorkOrder"
    static let contactRelatedEstimateURL = API.baseURL + "Contact/CTActions/ContactRelatedEstimate"
    static let contactRelatedInvoiceURL = API.baseURL + "Contact/CTActions/ContactRelatedInvoice"
    static let contactRelatedEventURL = API.baseURL + "Contact/CTActions/ContactRelatedEvent"
    static let contactRelatedTaskURL = API.baseURL + "Contact/CTActions/ContactRelatedTask"
    static let contactRelatedFileURL = API.baseURL + "Contact/CTActions/ContactRelatedFile"
    static let contactRelatedNoteURL = API.baseURL + "Contact/CTActions/ContactRelatedNote"
    static let getContactViewFieldsURL = API.baseURL + "Contact/View/GetContactViewFields"
    static let contactFilterURL = API.baseURL + "Contact/View/ContactFilter/"
    static let viewContactListURL = API.baseURL + "Contact/View/ViewContactList"
    static let recentInvoicesURL = API.baseURL + "Invoice/INActions/RecentInvoices"
    static let createInvoiceURL = API.baseURL + "Invoice/INActions/CreateInvoice"
    static let invoiceDetailsURL = API.baseURL + "Invoice/INActions/InvoiceDetails"
    static let invoiceRelatedListURL = API.baseURL + "Invoice/INActions/InvoiceRelatedList"
    static let getViewsInvoiceURL = API.baseURL + "Invoice/View/GetViews/"
    static let getInvoiceStatusURL = API.baseURL + "Invoice/Data/GetInvoiceStatus/"
    static let getParentWorkOrdersURL = API.baseURL + "WorkOrder/Data/GetParentWorkOrders"
    static let getInvoiceViewFieldsURL = API.baseURL + "Invoice/View/GetInvoiceViewFields/"
    static let editInvoiceURL = API.baseURL + "Invoice/INActions/EditInvoice"
    static let viewInvoiceListURL = API.baseURL + "Invoice/View/ViewInvoiceList/"
    static let getInvoicePaymentTermsURL = API.baseURL + "Invoice/Data/GetInvoicePaymentTerms/"
    static let invoiceRelatedLineItemURL = API.baseURL + "Invoice/INActions/InvoiceRelatedLineItem"
    static let invoiceRelatedEventURL = API.baseURL + "Invoice/INActions/InvoiceRelatedEvent"
    static let invoiceRelatedTaskURL = API.baseURL + "Invoice/INActions/InvoiceRelatedTask"
    static let invoiceRelatedNoteURL = API.baseURL + "Invoice/INActions/InvoiceRelatedNote"
    static let invoiceRelatedFileURL = API.baseURL + "Invoice/INActions/InvoiceRelatedFile"
    static let invoiceFilterURL = API.baseURL + "Invoice/View/InvoiceFilter/"
    static let invoiceGetProductsURL = API.baseURL + "Invoice/INActions/GetProducts"
    static let saveInvoiceLineItemsURL = API.baseURL + "Invoice/INActions/SaveInvoiceLineItems/"
    static let createTaskURL = API.baseURL + "Task/TKActions/CreateTask"
    static let editTaskURL = API.baseURL + "Task/TKActions/EditTask"
    static let recentTasksURL = API.baseURL + "Task/TKActions/RecentTasks"
    static let taskDetailsURL = API.baseURL + "Task/TKActions/TaskDetails"
    static let getTaskTypeURL = API.baseURL + "Task/Data/GetTaskType"
    static let getTaskStatusURL = API.baseURL + "Task/Data/GetTaskStatus"
    static let getTaskPriorityURL = API.baseURL + "Task/Data/GetTaskPriority"
    static let getRelatedObjListURL = API.baseURL + "Task/Data/GetRelatedObjList"
    static let getRelatedToListURL = API.baseURL + "Task/Data/GetRelatedToList"
    static let taskGetViewsURL = API.baseURL + "Task/View/GetViews"
    static let viewTaskListURL = API.baseURL + "Task/View/ViewTaskList"
    static let taskFilterURL = API.baseURL + "Task/View/TaskFilter"
    static let getTaskViewFieldsURL = API.baseURL + "Task/View/GetTaskViewFields/"
    
    static let createFileURL = API.baseURL + "File/FLActions/CreateFile"
    static let editFileURL = API.baseURL + "File/FLActions/EditFile"
    static let getViewsFileURL = API.baseURL + "File/View/GetViews"
    static let getRelatedObjListFileURL = API.baseURL + "File/Data/GetRelatedObjList"
    static let getRelatedToListFileURL = API.baseURL + "File/Data/GetRelatedToList"
    static let getFileViewFieldsURL = API.baseURL + "File/View/GetFileViewFields/"
    static let recentFilesURL = API.baseURL + "File/FLActions/RecentFiles"
    static let viewFileListURL = API.baseURL + "File/View/ViewFileList"
    static let fileDetailsURL = API.baseURL + "File/FLActions/FileDetails"
    static let fileFilterURL = API.baseURL + "File/View/FileFilter"
    static let createLocationURL = API.baseURL + "Location/LCActions/CreateLocation"
    static let recentLocationsURL = API.baseURL + "Location/LCActions/RecentLocations"
    static let locationDetailsURL = API.baseURL + "Location/LCActions/LocationDetails"
    static let editLocationURL = API.baseURL + "Location/LCActions/EditLocation"
    static let getLocationTypesURL = API.baseURL + "Location/Data/GetLocationTypes"
    static let getParentLocationsURL = API.baseURL + "Location/Data/GetParentLocations"
    static let getViewsLocationsURL = API.baseURL + "Location/View/GetViews"
    static let createNewFileAllURL = API.baseURL + "Common/Actions/CreateNewFile/"
    static let viewLocationListURL = API.baseURL + "Location/View/ViewLocationList"
    static let getLocationViewFieldsURL = API.baseURL + "Location/View/GetLocationViewFields/"
    static let locationFilterURL = API.baseURL + "Location/View/LocationFilter"
    static let getEmailTemplatesURL = API.baseURL + "Common/Data/GetEmailTemplates"
    static let sendEmailURL = API.baseURL + "Common/Actions/SendEmail"
    static let deleteObjectURL = API.baseURL + "Common/Actions/DeleteObject"
    static let createEventURL = API.baseURL + "Common/Actions/CreateEvent"
    static let getEventPrioritiesURL = API.baseURL + "Common/Data/GetEventPriorities"
    static let getEventTypesURL = API.baseURL + "Common/Data/GetEventTypes"
    static let getScheduleURL = API.baseURL + "Home/Data/GetSchedule"
    static let getTasksURL = API.baseURL + "Home/Data/GetTasks"
    static let getRecentsURL = API.baseURL + "Home/Data/GetRecents"
    static let markATaskURL = API.baseURL + "Task/TKActions/MarkATask"
    static let createNewViewURL = API.baseURL + "Common/View/CreateNewView"
    static let copyCustomViewURL = API.baseURL + "Common/View/CopyCustomView"
    static let deleteCustomViewURL = API.baseURL + "Common/View/DeleteCustomView"
    static let editDisplayedColumnsURL = API.baseURL + "Common/View/EditDisplayedColumnsCustomView"
    static let renameCustomViewURL = API.baseURL + "Common/View/RenameCustomView"
    static let getCustomViewDetailsURL = API.baseURL + "Common/View/GetCustomViewDetails"
    static let editSharingCustomViewURL = API.baseURL + "Common/View/EditSharingCustomView"
    static let eventDetailsURL = API.baseURL + "Common/Actions/EventDetails/"
    static let createNoteURL = API.baseURL + "Common/Actions/CreateNote/"
    static let noteDetailsURL = API.baseURL + "Common/Actions/NoteDetails/"
    static let userDetailsURL = API.baseURL + "Common/Actions/UserDetails/"
    static let productDetailsURL = API.baseURL + "Common/Actions/ProductDetails/"
    static let convertToInvoiceURL = API.baseURL + "WorkOrder/WoActions/ConvertToInvoice/"
    static let convertToWorkOrderURL = API.baseURL + "Estimate/ETActions/ConvertToWorkOrder/"
    static let editFiltersCustomViewURL = API.baseURL + "Common/View/EditFiltersCustomView"
    static let getGenDocTemplatesURL = API.baseURL + "Common/Data/GetGenDocTemplates"
    static let generateDocumentURL = API.baseURL + "Common/Actions/GenerateDocument"
    static let sortCustomViewURL = API.baseURL + "Common/View/SortCustomView"
    static let editNoteURL = API.baseURL + "Common/Actions/EditNote/"
    static let editEventURL = API.baseURL + "Common/Actions/EditEvent"
    static let getCustomFieldsURL = API.baseURL + "Common/Data/GetCustomFields"
}

struct Message {
    
    static let NoInternet = "Please check your internet connection"
    static let validEmail = "Please enter valid email address"
    static let validPassword = "Password should be alphanumeric and 8-15 characters long"
    static let validName = "Please enter valid name"
    static let confirmPasswordDifferent = "Password and Confirm Password should be same"
    static let validPhone = "Please enter valid phone number"
    static let fillNecessaryField = "Please fill required fields"
    static let selectProduct = "Please select Product"
    static let enterChemicalName = "Please select Chemical Name"
    static let enterRelatedTo = "Please select Related To"
    static let enterTitle = "Please Enter Title"
    static let enterValidUrl = "Please enter Valid Url"
    static let noContactAvailable = "Contact doesn't exist"
    static let allFieldsRequired = "All fields are required"
}
