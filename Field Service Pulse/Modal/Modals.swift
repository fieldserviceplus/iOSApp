//
//  Modals.swift
//  Field Service Pulse
//
//  Created by Apple on 02/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper



class SignUpData: Mappable {
    
    var FirstName:String?
    var LastName:String?
    var Email:String?
    var UserID:String?
    
    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map)
    {
        FirstName <- map["FirstName"]
        LastName <- map["LastName"]
        Email <- map["Email"]
        UserID <- map["UserID"]
       
        
    }
}

class LoginData: Mappable {
    
    var FirstName:String?
    var LastName:String?
    var Email:String?
    var UserID:String?
    var token:String?
    var OrganizationID:String?
    
    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map)
    {
        FirstName <- map["FirstName"]
        LastName <- map["LastName"]
        Email <- map["Email"]
        UserID <- map["UserID"]
        token <- map["token"]
        OrganizationID <- map["OrganizationID"]
    }
}



class accountDetailsData: Mappable {
    
    var AccountName:String?
    var AccountType:String?
    var AccountID:String?
    var PrimaryContact:String?
    var BillingAddress:String?
    var BillingStreet:String?
    var BillingCity:String?
    var BillingPostalCode:String?
    var Notes:String?
    var AccessType:String?
    var AccessNotes:String?
    var PopUpReminder:String?
    var Website:String?
    var LastActivityDate:String?
    var PhoneNo:String?
    var LastServiceDate:String?
    var AssignedToName:String?
    var AccountTypeName:String?
    var AssignedTo:String?
    
    var AccountNo:String?
    var BillingState:String?
    var BillingCountry:String?
    var ShippingCity:String?
    var ShippingCountry:String?
    var ShippingLatitude:String?
    var ShippingLongitude:String?
    var ShippingAddress:String?
    var ShippingStreet:String?
    var ShippingPostalCode:String?
    var IsActive:String?
    var BillingLatitude:String?
    var BillingLongitude:String?
    var PreferredTechnician:String?
    var ShippingState:String?
    var PrimaryContactName:String?
    var PreferredTechnicianName:String?
    var CreatedDate:String?
    var CreatedBy:String?
    var LastModifiedDate:String?
    var LastModifiedBy:String?
    
    
    
    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map)
    {
        AccountName <- map["AccountName"]
        AccountType <- map["AccountType"]
        AccountID <- map["AccountID"]
        PrimaryContact <- map["PrimaryContact"]
        BillingAddress <- map["BillingAddress"]
        BillingStreet <- map["BillingStreet"]
        BillingCity <- map["BillingCity"]
        BillingPostalCode <- map["BillingPostalCode"]
        Notes <- map["Notes"]
        AccessType <- map["AccessType"]
        AccessNotes <- map["AccessNotes"]
        PopUpReminder <- map["PopUpReminder"]
        Website <- map["Website"]
        LastActivityDate <- map["LastActivityDate"]
        PhoneNo <- map["PhoneNo"]
        LastServiceDate <- map["LastServiceDate"]
        AssignedToName <- map["AssignedToName"]
        AccountTypeName <- map["AccountTypeName"]
        AssignedTo <- map["AssignedTo"]
        
        AccountNo <- map["AccountNo"]
        BillingState <- map["BillingState"]
        BillingCountry <- map["BillingCountry"]
        ShippingCity <- map["ShippingCity"]
        ShippingCountry <- map["ShippingCountry"]
        ShippingLatitude <- map["ShippingLatitude"]
        ShippingLongitude <- map["ShippingLongitude"]
        ShippingAddress <- map["ShippingAddress"]
        ShippingStreet <- map["ShippingStreet"]
        ShippingPostalCode <- map["ShippingPostalCode"]
        ShippingState <- map["ShippingState"]
        IsActive <- map["IsActive"]
        BillingLatitude <- map["BillingLatitude"]
        BillingLongitude <- map["BillingLongitude"]
        PreferredTechnician <- map["PreferredTechnician"]
        PreferredTechnicianName <- map["PreferredTechnicianName"]
        PrimaryContactName <- map["PrimaryContactName"]
        CreatedDate <- map["CreatedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedDate <- map["LastModifiedDate"]
        LastModifiedBy <- map["LastModifiedBy"]
        
    }
    
}



class accountRelatedListData: Mappable {
    
    
    
    var AccountContact:AccountRelated?
    var AccountWorkOrder:AccountRelated?
    var AccountEstimate:AccountRelated?
    var AccountInvoice:AccountRelated?
    var AccountFile:AccountRelated?
    var AccountEvent:AccountRelated?
    var AccountTask:AccountRelated?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        AccountContact <- map["Contact"]
        AccountWorkOrder <- map["WorkOrder"]
        AccountEstimate <- map["Estimate"]
        AccountInvoice <- map["Invoice"]
        AccountFile <- map["File"]
        AccountEvent <- map["Event"]
        AccountTask <- map["Task"]
    }
}

class AccountRelated: Mappable {
    
    var title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        title <- map["title"]
    }
    
}



class ContactData: Mappable {
    
    var ContactID:String?
    var FirstName:String?
    var LastName:String?
    var Title:String?
    var PhoneNo:String?
    var Email:String?
    var FullName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        ContactID <- map["ContactID"]
        FirstName <- map["FirstName"]
        LastName <- map["LastName"]
        Title <- map["Title"]
        PhoneNo <- map["PhoneNo"]
        Email <- map["Email"]
        FullName <- map["FullName"]
    }
    
}

class LocationData: Mappable {
    
    var LocationID:String?
    var Name:String?
    var Address:String?
    var City:String?
    var State:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        LocationID <- map["LocationID"]
        Name <- map["Name"]
        Address <- map["Address"]
        City <- map["City"]
        State <- map["State"]
        
    }
    
}

class WorkOrderData: Mappable {
    
    var WorkOrderID:String?
    var WorkOrderNo:String?
    var Subject:String?
    var Priority:String?
    var Status:String?
    var CategoryName:String?
    var StartDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        WorkOrderID <- map["WorkOrderID"]
        WorkOrderNo <- map["WorkOrderNo"]
        Subject <- map["Subject"]
        Priority <- map["Priority"]
        Status <- map["Status"]
        CategoryName <- map["CategoryName"]
        StartDate <- map["StartDate"]
    }
    
}

class MyOrdersData: Mappable {
    
    var WorkOrderID:String?
    var WorkOrderNo:String?
    var Subject:String?
    var Priority:String?
    var Status:String?
    
    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map)
    {
        WorkOrderID <- map["WorkOrderID"]
        WorkOrderNo <- map["WorkOrderNo"]
        Subject <- map["Subject"]
        Priority <- map["Priority"]
        Status <- map["Status"]
    }
    
}

class AllOrdersData: Mappable {
    
    var WorkOrderID:String?
    var WorkOrderNo:String?
    var Subject:String?
    var Priority:String?
    var Status:String?
    
    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map)
    {
        WorkOrderID <- map["WorkOrderID"]
        WorkOrderNo <- map["WorkOrderNo"]
        Subject <- map["Subject"]
        Priority <- map["Priority"]
        Status <- map["Status"]
    }
    
}


class OrderDetailsData: Mappable {
    
    var WorkOrderID:String?
    var WorkOrderNo:String?
    var PrimaryContact:String?
    var WorkOrderType:String?
    var Address:String?
    var Street:String?
    var City:String?
    var State:String?
    var Country:String?
    var PostalCode:String?
    var Subject:String?
    var PopUpReminder:String?
    var LastModifiedDate:String?
    var LastModifiedBy:String?
    var WOStatus:String?
    var CreatedBy:String?
    var WOPriority:String?
    var WOCategory:String?
    var Description:String?
    var Priority:String?
    var AssignedToName:String?
    var WorkOrderTypeName:String?
    var WORecurrenceID:String?
    var Account:String?
    var AssignedTo:String?
    var CreatedDate:String?
    
    var StartDate:String?
    var EndDate:String?
    var EndTime:String?
    var StartTime:String?
    var IsRecurring:String?
    var RepeatEvery:String?
    var IntervalEvery:String?
    var RepeatOn:String?
    var Ends:String?
    var StartOn:String?
    var EndsOnDate:String?
    var EndsAfterOccurrences:String?
    var PrimaryContactName:String?
    var Status:String?
    var CategoryName:String?
    var AccountName:String?
    
    var Discount:String?
    var GrandTotal:String?
    var LineItemCount:String?
    var SubTotal:String?
    var Tax:String?
    var TotalPrice:String?
    var Signature:String?
    var ParentWorkOrder:String?
    var ParentWorkOrderName:String?
    var Latitude:String?
    var Longitude:String?
    var MobileNo:String?
    
    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map)
    {
        WorkOrderID <- map["WorkOrderID"]
        WorkOrderNo <- map["WorkOrderNo"]
        PrimaryContact <- map["PrimaryContact"]
        WorkOrderType <- map["WorkOrderType"]
        Address <- map["Address"]
        Street <- map["Street"]
        City <- map["City"]
        State <- map["State"]
        Country <- map["Country"]
        PostalCode <- map["PostalCode"]
        Subject <- map["Subject"]
        LastModifiedDate <- map["LastModifiedDate"]
        PopUpReminder <- map["PopUpReminder"]
        LastModifiedBy <- map["LastModifiedBy"]
        CreatedBy <- map["CreatedBy"]
        WOStatus <- map["WOStatus"]
        WOPriority <- map["WOPriority"]
        WOCategory <- map["WOCategory"]
        Description <- map["Description"]
        Priority <- map["Priority"]
        AssignedToName <- map["AssignedToName"]
        WorkOrderTypeName <- map["WorkOrderTypeName"]
        WORecurrenceID <- map["WORecurrenceID"]
        
        StartDate <- map["StartDate"]
        EndDate <- map["EndDate"]
        EndTime <- map["EndTime"]
        StartTime <- map["StartTime"]
        IsRecurring <- map["IsRecurring"]
        RepeatEvery <- map["RepeatEvery"]
        IntervalEvery <- map["IntervalEvery"]
        RepeatOn <- map["RepeatOn"]
        Ends <- map["Ends"]
        StartOn <- map["StartOn"]
        EndsOnDate <- map["EndsOnDate"]
        PrimaryContactName <- map["PrimaryContactName"]
        EndsAfterOccurrences <- map["EndsAfterOccurrences"]
        Status <- map["Status"]
        CategoryName <- map["CategoryName"]
        Account <- map["Account"]
        AccountName <- map["AccountName"]
        AssignedTo <- map["AssignedTo"]
        
        Discount <- map["Discount"]
        GrandTotal <- map["GrandTotal"]
        LineItemCount <- map["LineItemCount"]
        SubTotal <- map["SubTotal"]
        Tax <- map["Tax"]
        TotalPrice <- map["TotalPrice"]
        Signature <- map["Signature"]
        ParentWorkOrder <- map["ParentWorkOrder"]
        ParentWorkOrderName <- map["ParentWorkOrderName"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        CreatedDate <- map["CreatedDate"]
        MobileNo <- map["MobileNo"]
    }
    
}

class OrderRelatedListData: Mappable {
    
    var Event:OrderRelatedListDict?
    var Task:OrderRelatedListDict?
    var File:OrderRelatedListDict?
    var WOLineItem:OrderRelatedListDict?
    var Chemical:OrderRelatedListDict?
    var Note:OrderRelatedListDict?
    var Invoice:OrderRelatedListDict?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        Event <- map["Event"]
        Task <- map["Task"]
        File <- map["File"]
        WOLineItem <- map["WOLineItem"]
        Chemical <- map["Chemical"]
        Note <- map["Note"]
        Invoice <- map["Invoice"]
    }
}

class OrderRelatedListDict: Mappable {
    
    var title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        title <- map["title"]
    }
    
}


class EventData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var EventStatus:String?
    var EventTypeName:String?
    var EventStartDate:String?
    var EventEndDate:String?
    var EventStartTime:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        EventStatus <- map["EventStatus"]
        EventTypeName <- map["EventTypeName"]
        EventStartDate <- map["EventStartDate"]
        EventEndDate <- map["EventEndDate"]
        EventStartTime <- map["EventStartTime"]
    }
    
}

class TaskData: Mappable {
    
    var TaskID:String?
    var Subject:String?
    var CallDisposition:String?
    var TaskType:String?
    var Priority:String?
    var TaskStatus:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        TaskID <- map["TaskID"]
        Subject <- map["Subject"]
        CallDisposition <- map["CallDisposition"]
        TaskType <- map["TaskType"]
        Priority <- map["Priority"]
        TaskStatus <- map["TaskStatus"]
    }
    
}

class FileData: Mappable {
    
    var FileID:String?
    var FileName:String?
    var Subject:String?
    var ContentType:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        FileID <- map["FileID"]
        FileName <- map["FileName"]
        Subject <- map["Subject"]
        ContentType <- map["ContentType"]
        
        
    }
    
}

class LineItemsData: Mappable {
    
    var ProductID:String?
    var LineItemNo:String?
    var ProductName:String?
    var UnitPrice:String?
    var NetTotal:String?
    
    var Product:String?
    var Discount:String?
    var ListPrice:String?
    var Quantity:String?
    var Taxable:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Tax:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductID <- map["ProductID"]
        LineItemNo <- map["LineItemNo"]
        ProductName <- map["ProductName"]
        UnitPrice <- map["UnitPrice"]
        NetTotal <- map["NetTotal"]
        
        Quantity <- map["Quantity"]
        Product <- map["Product"]
        ListPrice <- map["ListPrice"]
        Taxable <- map["Taxable"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Tax <- map["Tax"]
        Discount <- map["Discount"]
        
    }
    
}
class ChemicalData: Mappable {
    
    var ProductID:String?
    var ChemicalID:String?
    var ChemicalNo:String?
    var TestConcentration:String?
    var TestedUnitOfMeasure:String?
    var ApplicationAmount:String?
    var ApplicationUnitOfMeasure:String?
    var ProductName:String?
    var Product:String?
    var ApplicationArea:String?
    var AdditionalNotes:String?
    var CreatedDate:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductID <- map["ProductID"]
        ChemicalID <- map["ChemicalID"]
        ChemicalNo <- map["ChemicalNo"]
        TestConcentration <- map["TestConcentration"]
        TestedUnitOfMeasure <- map["TestedUnitOfMeasure"]
        ApplicationAmount <- map["ApplicationAmount"]
        ApplicationUnitOfMeasure <- map["ApplicationUnitOfMeasure"]
        ProductName <- map["ProductName"]
        Product <- map["Product"]
        ApplicationArea <- map["ApplicationArea"]
        AdditionalNotes <- map["AdditionalNotes"]
        CreatedDate <- map["CreatedDate"]
    }
    
}
class NoteData: Mappable {
    
    var NoteID:String?
    var Subject:String?
    var CreatedDate:String?
    var OwnerName:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        NoteID <- map["NoteID"]
        Subject <- map["Subject"]
        CreatedDate <- map["CreatedDate"]
        OwnerName <- map["OwnerName"]
        
        
    }
    
}
class InvoiceData: Mappable {
    
    var InvoiceID:String?
    var InvoiceNumber:String?
    var TotalPrice:String?
    var SubTotal:String?
    var InvoiceStatus:String?
    var InvoiceDate:String?
    var DueDate:String?
    var WorkOrderName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        InvoiceID <- map["InvoiceID"]
        InvoiceNumber <- map["InvoiceNumber"]
        TotalPrice <- map["TotalPrice"]
        SubTotal <- map["SubTotal"]
        InvoiceStatus <- map["InvoiceStatus"]
        InvoiceDate <- map["InvoiceDate"]
        DueDate <- map["DueDate"]
        WorkOrderName <- map["WorkOrderName"]
        
    }
    
}
///////////////


class UsersData: Mappable {
    
    var UserID:String?
    var FullName:String?
    var Email:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        UserID <- map["UserID"]
        FullName <- map["FullName"]
        Email <- map["Email"]
    }
}

class AccountTypeData: Mappable {
    
    var AccountTypeID:String?
    var AccountType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        AccountTypeID <- map["AccountTypeID"]
        AccountType <- map["AccountType"]
        
    }
}

class ContactsData: Mappable {
    
    var ContactID:String?
    var FullName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        ContactID <- map["ContactID"]
        FullName <- map["FullName"]
        
    }
}

class AccountRelatedContactsData: Mappable {
    
    var ContactID:String?
    var FullName:String?
    var Title:String?
    var PhoneNo:String?
    var Email:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        ContactID <- map["ContactID"]
        FullName <- map["FullName"]
        Title <- map["Title"]
        PhoneNo <- map["PhoneNo"]
        Email <- map["Email"]
        
    }
}

class WorkOrderTypesData: Mappable {
    
    var WorkOrderTypeID:String?
    var WorkOrderType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        WorkOrderTypeID <- map["WorkOrderTypeID"]
        WorkOrderType <- map["WorkOrderType"]
        
    }
}

class PriorityTypesData: Mappable {
    
    var WOPriorityID:String?
    var Priority:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        WOPriorityID <- map["WOPriorityID"]
        Priority <- map["Priority"]
        
    }
}

class StatusTypesData: Mappable {
    
    var WOStatusID:String?
    var Status:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        WOStatusID <- map["WOStatusID"]
        Status <- map["Status"]
        
    }
}

class CategoryTypesData: Mappable {
    
    var WOCategoryID:String?
    var CategoryName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        WOCategoryID <- map["WOCategoryID"]
        CategoryName <- map["CategoryName"]
        
    }
}

class GetAccountTypesData: Mappable {
    
    var AccountID:String?
    var AccountName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        AccountID <- map["AccountID"]
        AccountName <- map["AccountName"]
        
    }
}

class GetAccountViewsData : Mappable {
    
    var AccountViewID:String?
    var AccountViewName:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        AccountViewID <- map["AccountViewID"]
        AccountViewName <- map["AccountViewName"]
    }
    
}



class AccountRelatedTasksData: Mappable {
    
    var TaskID:String?
    var Subject:String?
    var Name:String?
    var TaskType:String?
    var AssignedTo:String?
    var TaskStatus:String?
    var Priority:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskID <- map["TaskID"]
        Subject <- map["Subject"]
        Name <- map["Name"]
        TaskType <- map["TaskType"]
        AssignedTo <- map["AssignedTo"]
        TaskStatus <- map["TaskStatus"]
        Priority <- map["Priority"]
    }
}


class AccountRelatedEstimateData: Mappable {
    
    var EstimateID:String?
    var EstimateNo:String?
    var EstimateName:String?
    var ExpirationDate:String?
    var OwnerName:String?
    var Status:String?
    var GrandTotal:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateID <- map["EstimateID"]
        EstimateNo <- map["EstimateNo"]
        EstimateName <- map["EstimateName"]
        ExpirationDate <- map["ExpirationDate"]
        OwnerName <- map["OwnerName"]
        Status <- map["Status"]
        GrandTotal <- map["GrandTotal"]
        CreatedDate <- map["CreatedDate"]
    }
}

class AccountRelatedEventData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var Name:String?
    var EventStartDate:String?
    var EventEndDate:String?
    var AssignedTo:String?
    var CreatedBy:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        Name <- map["Name"]
        EventStartDate <- map["EventStartDate"]
        EventEndDate <- map["EventEndDate"]
        CreatedBy <- map["CreatedBy"]
        AssignedTo <- map["AssignedTo"]
        CreatedDate <- map["CreatedDate"]
    }
}

class AccountRelatedInvoiceData: Mappable {
    
    var InvoiceID:String?
    var InvoiceNumber:String?
    var DueDate:String?
    var InvoiceStatus:String?
    var SubTotal:String?
    var TotalPrice:String?
    var Subject:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceID <- map["InvoiceID"]
        InvoiceNumber <- map["InvoiceNumber"]
        DueDate <- map["DueDate"]
        InvoiceStatus <- map["InvoiceStatus"]
        SubTotal <- map["SubTotal"]
        TotalPrice <- map["TotalPrice"]
        Subject <- map["Subject"]
    }
}

class AccountRelatedFileData: Mappable {
    
    var FileID:String?
    var FileName:String?
    var ContentType:String?
    var Subject:String?
    var CreatedBy:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FileID <- map["FileID"]
        FileName <- map["FileName"]
        ContentType <- map["ContentType"]
        Subject <- map["Subject"]
        CreatedBy <- map["CreatedBy"]
        CreatedDate <- map["CreatedDate"]
    }
}


class AccountRelatedChemicalData: Mappable {
    
    var ChemicalNo:String?
    var ProductName:String?
    var Account:String?
    var WorkOrder:String?
    var CreatedBy:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ChemicalNo <- map["ChemicalNo"]
        ProductName <- map["ProductName"]
        Account <- map["Account"]
        WorkOrder <- map["WorkOrder"]
        CreatedBy <- map["CreatedBy"]
        CreatedDate <- map["CreatedDate"]
    }
}

class AccountRelatedProductData: Mappable {
    
    var ProductCode:String?
    var ProductName:String?
    var DefaultQuantity:String?
    var ListPrice:String?
    var DatePurchased:String?
    var CreatedBy:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductCode <- map["ProductCode"]
        ProductName <- map["ProductName"]
        DefaultQuantity <- map["DefaultQuantity"]
        DatePurchased <- map["DatePurchased"]
        CreatedBy <- map["CreatedBy"]
        CreatedDate <- map["CreatedDate"]
    }
}

class RecentAccountsData: Mappable {
    
    var AccountID:String?
    var AccountName:String?
    var PhoneNo:String?
    var BillingCity:String?
    var BillingState:String?
    var AssignedTo:String?
    var AccountType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        AccountID <- map["AccountID"]
        AccountName <- map["AccountName"]
        PhoneNo <- map["PhoneNo"]
        BillingCity <- map["City"]
        BillingState <- map["State"]
        AssignedTo <- map["AssignedTo"]
        AccountType <- map["Type"]
    }
}

class AccountFieldsData: Mappable {
    
    var FieldName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FieldName <- map["FieldName"]
    }
}

class WOGetViewsData: Mappable {
    
    var WorkOrderViewID:String?
    var WorkOrderViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrderViewID <- map["WorkOrderViewID"]
        WorkOrderViewName <- map["WorkOrderViewName"]
    }
}

class GetWOViewFieldsData: Mappable {
    
    var FieldName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FieldName <- map["FieldName"]
    }
}

class RecentWOData: Mappable {
    
    var WorkOrderID:String?
    var WorkOrderNo:String?
    var Subject:String?
    var Priority:String?
    var Status:String?
    var CreatedDate:String?
    var AccountName:String?
    var AssignedTo:String?
    var AccountType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrderNo <- map["WO#"]
        WorkOrderID <- map["WorkOrderID"]
        Subject <- map["Subject"]
        Priority <- map["Priority"]
        Status <- map["Status"]
        CreatedDate <- map["CreatedDate"]
        AccountName <- map["AccountName"]
        AssignedTo <- map["AssignedTo"]
        AccountType <- map["Type"]
    }
}

class GetProductsData: Mappable {
    
    var ProductID:String?
    var ProductCode:String?
    var ProductName:String?
    var Description:String?
    var ListPrice:String?
    var DefaultQuantity:String?
    var Taxable:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Tax:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductID <- map["ProductID"]
        ProductCode <- map["ProductCode"]
        ProductName <- map["ProductName"]
        Description <- map["Description"]
        ListPrice <- map["ListPrice"]
        DefaultQuantity <- map["DefaultQuantity"]
        Taxable <- map["Taxable"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Tax <- map["Tax"]
    }
}

class GetChemicalsData: Mappable {
    
    var ProductID:String?
    var ProductCode:String?
    var ProductName:String?
    var Description:String?
    var ListPrice:String?
    var DefaultQuantity:String?
    var Taxable:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductID <- map["ProductID"]
        ProductCode <- map["ProductCode"]
        ProductName <- map["ProductName"]
        Description <- map["Description"]
        ListPrice <- map["ListPrice"]
        DefaultQuantity <- map["DefaultQuantity"]
        Taxable <- map["Taxable"]
    }
}


class GetProductFamilyData: Mappable {
    
    var ProductFamilyID:String?
    var ProductFamily:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductFamilyID <- map["ProductFamilyID"]
        ProductFamily <- map["ProductFamily"]
    }
}
class GetUnitOfMeasurementData: Mappable {
    
    var UnitOfMeasurementID:String?
    var UnitOfMeasurement:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        UnitOfMeasurementID <- map["UnitOfMeasurementID"]
        UnitOfMeasurement <- map["UnitOfMeasurement"]
    }
}

class GetEventTaskByDatesData: Mappable {
    
    var ID:String?
    var type:String?
    var Title:String?
    var Start:String?
    var End:String?
    var Time:String?
    var ColorCode:String?
    var Latitude:String?
    var Longitude:String?
    var ShapeType:String?
    var StartTime:String?
    var EndTime:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ID <- map["ID"]
        type <- map["Type"]
        Title <- map["Title"]
        Start <- map["Start"]
        End <- map["End"]
        Time <- map["Time"]
        ColorCode <- map["ColorCode"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        ShapeType <- map["ShapeType"]
        StartTime <- map["StartTime"]
        EndTime <- map["EndTime"]
    }
}

class CalendarFilterDataDict: Mappable {
    
    var WOType:[WOTypeData]?
    var WOStatus:[WOStatusData]?
    var WOPriority:[WOPriorityData]?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WOType <- map["WOType"]
        WOStatus <- map["WOStatus"]
        WOPriority <- map["WOPriority"]
    }
}

class WOTypeData: Mappable {
    
    var WorkOrderTypeID:String?
    var WorkOrderType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrderTypeID <- map["WorkOrderTypeID"]
        WorkOrderType <- map["WorkOrderType"]
    }
}

class WOStatusData: Mappable {
    
    var WOStatusID:String?
    var Status:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WOStatusID <- map["WOStatusID"]
        Status <- map["Status"]
    }
}

class WOPriorityData: Mappable {
    
    var WOPriorityID:String?
    var Priority:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WOPriorityID <- map["WOPriorityID"]
        Priority <- map["Priority"]
    }
}

class GetWorkOrderByDateData: Mappable {
    
    var WorkOrderID:String?
    var Title:String?
    var Start:String?
    var End:String?
    var Latitude:String?
    var Longitude:String?
    var ShapeType:String?
    var ColorCode:String?
    var StartTime:String?
    var EndTime:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrderID <- map["WorkOrderID"]
        Title <- map["Title"]
        Start <- map["Start"]
        End <- map["End"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        ShapeType <- map["ShapeType"]
        ColorCode <- map["ColorCode"]
        StartTime <- map["StartTime"]
        EndTime <- map["EndTime"]
    }
}

class RecentEstimatesData: Mappable {
    
    var EstimateID:String?
    var ET:String?
    var EstimateName:String?
    var AccountName:String?
    var Status:String?
    var Owner:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateID <- map["EstimateID"]
        ET <- map["ET#"]
        EstimateName <- map["EstimateName"]
        AccountName <- map["AccountName"]
        Status <- map["Status"]
        Owner <- map["Owner"]
    }
}

class EstimateDetailsData: Mappable {
    
    var EstimateID:String?
    var EstimateNo:String?
    var EstimateName:String?
    var Contact:String?
    var OrganizationID:String?
    var Account:String?
    var Owner:String?
    var BillingAddress:String?
    var BillingCity:String?
    var BillingState:String?
    var BillingCountry:String?
    var BillingPostalCode:String?
    var BillingLatitude:String?
    var BillingLongitude:String?
    var BillingName:String?
    var Description:String?
    var ExpirationDate:String?
    var Phone:String?
    var Email:String?
    var EstimateToAddress:String?
    var EstimateToName:String?
    var ShippingAddress:String?
    var ShippingCity:String?
    var ShippingState:String?
    var ShippingCountry:String?
    var ShippingPostalCode:String?
    var ShippingLatitude:String?
    var ShippingLongitude:String?
    var ShippingName:String?
    var EstimateStatus:String?
    var ShippingHandling:String?
    var Tax:String?
    var Signature:String?
    var IsDeleted:String?
    var LastModifiedBy:String?
    var CreatedBy:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var PrimaryContactName:String?
    var AccountName:String?
    var Status:String?
    var OwnerName:String?
    var SubTotal:String?
    var Discount:String?
    var GrandTotal:String?
    var LineItemCount:String?
    var TotalPrice:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        CreatedDate <- map["CreatedDate"]
        EstimateID <- map["EstimateID"]
        EstimateNo <- map["EstimateNo"]
        EstimateName <- map["EstimateName"]
        Contact <- map["Contact"]
        OrganizationID <- map["OrganizationID"]
        Account <- map["Account"]
        Owner <- map["Owner"]
        BillingAddress <- map["BillingAddress"]
        BillingCity <- map["BillingCity"]
        BillingState <- map["BillingState"]
        BillingCountry <- map["BillingCountry"]
        BillingPostalCode <- map["BillingPostalCode"]
        BillingLatitude <- map["BillingLatitude"]
        BillingLongitude <- map["BillingLongitude"]
        BillingName <- map["BillingName"]
        Description <- map["Description"]
        ExpirationDate <- map["ExpirationDate"]
        Phone <- map["Phone"]
        Email <- map["Email"]
        EstimateToAddress <- map["EstimateToAddress"]
        EstimateToName <- map["EstimateToName"]
        ShippingAddress <- map["ShippingAddress"]
        ShippingCity <- map["ShippingCity"]
        ShippingState <- map["ShippingState"]
        ShippingCountry <- map["ShippingCountry"]
        ShippingPostalCode <- map["ShippingPostalCode"]
        ShippingLatitude <- map["ShippingLatitude"]
        ShippingLongitude <- map["ShippingLongitude"]
        ShippingName <- map["ShippingName"]
        EstimateStatus <- map["EstimateStatus"]
        ShippingHandling <- map["ShippingHandling"]
        Tax <- map["Tax"]
        Signature <- map["Signature"]
        IsDeleted <- map["IsDeleted"]
        LastModifiedBy <- map["LastModifiedBy"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedDate <- map["LastModifiedDate"]
        PrimaryContactName <- map["PrimaryContactName"]
        AccountName <- map["AccountName"]
        Status <- map["Status"]
        SubTotal <- map["SubTotal"]
        Discount <- map["Discount"]
        TotalPrice <- map["TotalPrice"]
        GrandTotal <- map["GrandTotal"]
        LineItemCount <- map["LineItemCount"]
        OwnerName <- map["OwnerName"]
    }
}

class EstimateRelatedListData: Mappable {
    
    var EstimateLine:EstimateRelatedListDict?
    var Event:EstimateRelatedListDict?
    var Task:EstimateRelatedListDict?
    var Note:EstimateRelatedListDict?
    var File:EstimateRelatedListDict?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateLine <- map["EstimateLine"]
        Event <- map["Event"]
        Task <- map["Task"]
        Note <- map["Note"]
        File <- map["File"]
    }
}
class EstimateRelatedListDict: Mappable {
    
    var title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        title <- map["title"]
    }
}

class GetEstimateViewsData: Mappable {
    
    var EstimateViewID:String?
    var EstimateViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateViewID <- map["EstimateViewID"]
        EstimateViewName <- map["EstimateViewName"]
    }
}
class GetEstimateStatusData: Mappable {
    
    var EstimateStatusID:String?
    var Status:String?
    var IsDefault:String?
    var IsClosed:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateStatusID <- map["EstimateStatusID"]
        Status <- map["Status"]
        IsDefault <- map["IsDefault"]
        IsClosed <- map["IsClosed"]
    }
}

class EstimateRelatedLineItemData: Mappable {
    
    var EstimateLineID:String?
    var EstimateLineNo:String?
    var OrganizationID:String?
    var Estimate:String?
    var Product:String?
    var Discount:String?
    var Description:String?
    var LineNumber:String?
    var ListPrice:String?
    var Quantity:String?
    var UnitPrice:String?
    var SubTotal:String?
    var TotalPrice:String?
    var Taxable:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    var ProductName:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Tax:String?
    var ShippingHandling:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateLineID <- map["EstimateLineID"]
        EstimateLineNo <- map["EstimateLineNo"]
        OrganizationID <- map["OrganizationID"]
        Estimate <- map["Estimate"]
        Product <- map["Product"]
        Discount <- map["Discount"]
        Description <- map["Description"]
        LineNumber <- map["LineNumber"]
        ListPrice <- map["ListPrice"]
        Quantity <- map["Quantity"]
        UnitPrice <- map["UnitPrice"]
        SubTotal <- map["SubTotal"]
        TotalPrice <- map["TotalPrice"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        ProductName <- map["ProductName"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Tax <- map["Tax"]
        Taxable <- map["Taxable"]
        ShippingHandling <- map["ShippingHandling"]
    }
}

class EstimateRelatedEventData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var EventStatus:String?
    var EventTypeName:String?
    var EventStartDate:String?
    var EventEndDate:String?
    var EventStartTime:String?
    var EventEndTime:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        EventStatus <- map["EventStatus"]
        EventTypeName <- map["EventTypeName"]
        EventStartDate <- map["EventStartDate"]
        EventEndDate <- map["EventEndDate"]
        EventStartTime <- map["EventStartTime"]
        EventEndTime <- map["EventEndTime"]
    }
}
class EstimateRelatedTaskData: Mappable {
    
    var TaskID:String?
    var Subject:String?
    var CallDisposition:String?
    var TaskType:String?
    var Priority:String?
    var TaskStatus:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskID <- map["TaskID"]
        Subject <- map["Subject"]
        CallDisposition <- map["CallDisposition"]
        TaskType <- map["TaskType"]
        Priority <- map["Priority"]
        TaskStatus <- map["TaskStatus"]
    }
}
class EstimateRelatedFileData: Mappable {
    
    var FileID:String?
    var FileName:String?
    var Subject:String?
    var ContentType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FileID <- map["FileID"]
        FileName <- map["FileName"]
        Subject <- map["Subject"]
        ContentType <- map["ContentType"]
    }
}

class EstimateRelatedNoteData: Mappable {
    
    var NoteID:String?
    var Subject:String?
    var CreatedDate:String?
    var OwnerName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        NoteID <- map["NoteID"]
        Subject <- map["Subject"]
        CreatedDate <- map["CreatedDate"]
        OwnerName <- map["OwnerName"]
        
    }
}
class GetEstimateViewFieldsData: Mappable {
    
    var FieldName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FieldName <- map["FieldName"]
    }
}
class GetEstimatesData: Mappable {
    
    var ProductID:String?
    var ProductCode:String?
    var ProductName:String?
    var Description:String?
    var ListPrice:String?
    var DefaultQuantity:String?
    var Taxable:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Tax:String?
    var ShippingHandling:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductID <- map["ProductID"]
        ProductCode <- map["ProductCode"]
        ProductName <- map["ProductName"]
        Description <- map["Description"]
        ListPrice <- map["ListPrice"]
        DefaultQuantity <- map["DefaultQuantity"]
        Taxable <- map["Taxable"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Tax <- map["Tax"]
        ShippingHandling <- map["ShippingHandling"]
    }
}

class RecentContactsData: Mappable {
    
    var ContactID:String?
    var ContactName:String?
    var PhoneNo:String?
    var AccountName:String?
    var MailingCity:String?
    var MailingState:String?
    var AssignedTo:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ContactID <- map["ContactID"]
        ContactName <- map["ContactName"]
        PhoneNo <- map["PhoneNo"]
        AccountName <- map["AccountName"]
        MailingCity <- map["MailingCity"]
        MailingState <- map["MailingState"]
        AssignedTo <- map["AssignedTo"]
    }
}

class ContactDetailsData: Mappable {
    
    var ContactID:String?
    var ContactNo:String?
    var OrganizationID:String?
    var Title:String?
    var Salutation:String?
    var Name:String?
    var FirstName:String?
    var LastName:String?
    var Email:String?
    var EmailOptOut:String?
    var PhoneNo:String?
    var MobileNo:String?
    var HomePhoneNo:String?
    var FaxNo:String?
    var AssignedTo:String?
    var Account:String?
    var BirthDate:String?
    var Description:String?
    var DoNotCall:String?
    var HasOptedOutOfEmail:String?
    var MailingAddress:String?
    var MailingCity:String?
    var MailingState:String?
    var MailingCountry:String?
    var MailingLatitude:String?
    var MailingLongitude:String?
    var MailingPostalCode:String?
    var LeadSource:String?
    var Notes:String?
    var IsActive:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    var AccountName:String?
    var ContactName:String?
    var AssignedToName:String?
    var LeadSourceName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ContactID <- map["ContactID"]
        ContactNo <- map["ContactNo"]
        OrganizationID <- map["OrganizationID"]
        Title <- map["Title"]
        Salutation <- map["Salutation"]
        Name <- map["Name"]
        FirstName <- map["FirstName"]
        LastName <- map["LastName"]
        Email <- map["Email"]
        EmailOptOut <- map["EmailOptOut"]
        PhoneNo <- map["PhoneNo"]
        MobileNo <- map["MobileNo"]
        HomePhoneNo <- map["HomePhoneNo"]
        FaxNo <- map["FaxNo"]
        AssignedTo <- map["AssignedTo"]
        Account <- map["Account"]
        BirthDate <- map["BirthDate"]
        Description <- map["Description"]
        DoNotCall <- map["DoNotCall"]
        HasOptedOutOfEmail <- map["HasOptedOutOfEmail"]
        MailingAddress <- map["MailingAddress"]
        MailingCity <- map["MailingCity"]
        MailingState <- map["MailingState"]
        MailingCountry <- map["MailingCountry"]
        MailingLatitude <- map["MailingLatitude"]
        MailingLongitude <- map["MailingLongitude"]
        MailingPostalCode <- map["MailingPostalCode"]
        LeadSource <- map["LeadSource"]
        Notes <- map["Notes"]
        IsActive <- map["IsActive"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        AccountName <- map["AccountName"]
        ContactName <- map["ContactName"]
        AssignedToName <- map["AssignedToName"]
        LeadSourceName <- map["LeadSourceName"]
    }
}

class ContactRelatedListData: Mappable {
    
    var WorkOrder:ContactRelatedListDict?
    var Estimate:ContactRelatedListDict?
    var Invoice:ContactRelatedListDict?
    var Event:ContactRelatedListDict?
    var File:ContactRelatedListDict?
    var Task:ContactRelatedListDict?
    var Note:ContactRelatedListDict?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrder <- map["WorkOrder"]
        Estimate <- map["Estimate"]
        Invoice <- map["Invoice"]
        Event <- map["Event"]
        File <- map["File"]
        Task <- map["Task"]
        Note <- map["Note"]
    }
}
class ContactRelatedListDict: Mappable {
    
    var title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        title <- map["title"]
    }
}

class ContactGetViewsData: Mappable {
    
    var ContactViewID:String?
    var ContactViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ContactViewID <- map["ContactViewID"]
        ContactViewName <- map["ContactViewName"]
    }
}

class GetLeadSourcesData: Mappable {
    
    var LeadSourceID:String?
    var LeadSource:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        LeadSourceID <- map["LeadSourceID"]
        LeadSource <- map["LeadSource"]
    }
}

class GetSalutationsData: Mappable {
    
    var SalutationID:String?
    var Salutation:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        SalutationID <- map["SalutationID"]
        Salutation <- map["Salutation"]
    }
}
class GetTitlesOfContactData: Mappable {
    
    var TitleOfPeopleID:String?
    var Title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TitleOfPeopleID <- map["TitleOfPeopleID"]
        Title <- map["Title"]
    }
}
class ContactRelatedWorkOrderData: Mappable {
    
    var WorkOrderID:String?
    var WorkOrderNo:String?
    var Subject:String?
    var Priority:String?
    var Status:String?
    var CategoryName:String?
    var StartDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrderID <- map["WorkOrderID"]
        WorkOrderNo <- map["WorkOrderNo"]
        Subject <- map["Subject"]
        Priority <- map["Priority"]
        Status <- map["Status"]
        CategoryName <- map["CategoryName"]
        StartDate <- map["StartDate"]
        
    }
}
class ContactRelatedEstimateData: Mappable {
    
    var EstimateID:String?
    var EstimateNo:String?
    var EstimateName:String?
    var ExpirationDate:String?
    var OwnerName:String?
    var Status:String?
    var GrandTotal:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EstimateID <- map["EstimateID"]
        EstimateNo <- map["EstimateNo"]
        EstimateName <- map["EstimateName"]
        ExpirationDate <- map["ExpirationDate"]
        OwnerName <- map["OwnerName"]
        Status <- map["Status"]
        GrandTotal <- map["GrandTotal"]
        CreatedDate <- map["CreatedDate"]
    }
}
class ContactRelatedInvoiceData: Mappable {
    
    var InvoiceID:String?
    var InvoiceNumber:String?
    var DueDate:String?
    var InvoiceStatus:String?
    var TotalPrice:String?
    var SubTotal:String?
    var Subject:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceID <- map["InvoiceID"]
        InvoiceNumber <- map["InvoiceNumber"]
        DueDate <- map["DueDate"]
        InvoiceStatus <- map["InvoiceStatus"]
        TotalPrice <- map["TotalPrice"]
        SubTotal <- map["SubTotal"]
        Subject <- map["Subject"]
    }
}

class ContactRelatedEventData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var Name:String?
    var EventStartDate:String?
    var EventEndDate:String?
    var AssignedTo:String?
    var CreatedBy:String?
    var CreatedDate:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        Name <- map["Name"]
        EventStartDate <- map["EventStartDate"]
        EventEndDate <- map["EventEndDate"]
        AssignedTo <- map["AssignedTo"]
        CreatedBy <- map["CreatedBy"]
        CreatedDate <- map["CreatedDate"]
    }
}
class ContactRelatedTaskData: Mappable {
    
    var TaskID:String?
    var Subject:String?
    var Name:String?
    var TaskType:String?
    var Date:String?
    var AssignedTo:String?
    var TaskStatus:String?
    var Priority:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskID <- map["TaskID"]
        Subject <- map["Subject"]
        Name <- map["Name"]
        TaskType <- map["TaskType"]
        Date <- map["Date"]
        AssignedTo <- map["AssignedTo"]
        TaskStatus <- map["TaskStatus"]
        Priority <- map["Priority"]
    }
}
class ContactRelatedFileData: Mappable {
    
    var FileID:String?
    var FileName:String?
    var Subject:String?
    var ContentType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FileID <- map["FileID"]
        FileName <- map["FileName"]
        Subject <- map["Subject"]
        ContentType <- map["ContentType"]
    }
}
class ContactRelatedNoteData: Mappable {
    
    var NoteID:String?
    var CreatedDate:String?
    var OwnerName:String?
    var Subject:String?
    var Umang:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        NoteID <- map["NoteID"]
        CreatedDate <- map["CreatedDate"]
        OwnerName <- map["OwnerName"]
        Subject <- map["Subject"]
    }
}

class RecentInvoicesData: Mappable {
    
    var InvoiceID:String?
    var IN:String?
    var AccountName:String?
    var InvoiceStatus:String?
    var AssignedToName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceID <- map["InvoiceID"]
        IN <- map["IN#"]
        AccountName <- map["AccountName"]
        InvoiceStatus <- map["InvoiceStatus"]
        AssignedToName <- map["AssignedToName"]
    }
}

class InvoiceDetailsDict: Mappable {
    
    var InvoiceID:String?
    var InvoiceNo:String?
    var OrganizationID:String?
    var WorkOrder:String?
    var Account:String?
    var Contact:String?
    var AssignedTo:String?
    var Address:String?
    var City:String?
    var State:String?
    var Country:String?
    var PostalCode:String?
    var Latitude:String?
    var Longitude:String?
    var Description:String?
    var InvoiceStatus:String?
    var InvoiceDate:String?
    var DueDate:String?
    var PaymentTerms:String?
    var AdditionalInformation:String?
    var Tax:String?
    var IsClosed:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    var AccountName:String?
    var ContactName:String?
    var AssignedToName:String?
    var SubTotal:String?
    var Discount:String?
    var TotalPrice:String?
    var GrandTotal:String?
    var LineItemCount:String?
    var WorkOrderSubject:String?
    var InvoiceStatusID:String?
    var InvoicePaymentTermID:String?
    var PhoneNo:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceID <- map["InvoiceID"]
        InvoiceNo <- map["InvoiceNo"]
        OrganizationID <- map["OrganizationID"]
        WorkOrder <- map["WorkOrder"]
        Account <- map["Account"]
        Contact <- map["Contact"]
        AssignedTo <- map["AssignedTo"]
        Address <- map["Address"]
        City <- map["City"]
        State <- map["State"]
        Country <- map["Country"]
        PostalCode <- map["PostalCode"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        Description <- map["Description"]
        InvoiceStatus <- map["InvoiceStatus"]
        InvoiceDate <- map["InvoiceDate"]
        DueDate <- map["DueDate"]
        PaymentTerms <- map["PaymentTerms"]
        AdditionalInformation <- map["AdditionalInformation"]
        Tax <- map["Tax"]
        IsClosed <- map["IsClosed"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        AccountName <- map["AccountName"]
        ContactName <- map["ContactName"]
        AssignedToName <- map["AssignedToName"]
        SubTotal <- map["SubTotal"]
        Discount <- map["Discount"]
        TotalPrice <- map["TotalPrice"]
        GrandTotal <- map["GrandTotal"]
        LineItemCount <- map["LineItemCount"]
        WorkOrderSubject <- map["WorkOrderSubject"]
        InvoiceStatusID <- map["InvoiceStatusID"]
        InvoicePaymentTermID <- map["InvoicePaymentTermID"]
        PhoneNo <- map["PhoneNo"]
    }
}

class InvoiceRelatedList: Mappable {
    
    var InvoiceLineItems:InvoiceRelatedListDict?
    var Event:InvoiceRelatedListDict?
    var Task:InvoiceRelatedListDict?
    var File:InvoiceRelatedListDict?
    var Note:InvoiceRelatedListDict?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceLineItems <- map["InvoiceLineItems"]
        Event <- map["Event"]
        Task <- map["Task"]
        File <- map["File"]
        Note <- map["Note"]
    }
}
class InvoiceRelatedListDict: Mappable {
    
    var title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        title <- map["title"]
    }
}

class InvoiceGetViewsData: Mappable {
    
    var InvoiceViewID:String?
    var InvoiceViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceViewID <- map["InvoiceViewID"]
        InvoiceViewName <- map["InvoiceViewName"]
    }
}

class GetInvoiceStatusData: Mappable {
    
    var InvoiceStatusID:String?
    var InvoiceStatus:String?
    var IsDefault:String?
    var IsClosed:String?
    var IsPaid:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceStatusID <- map["InvoiceStatusID"]
        InvoiceStatus <- map["InvoiceStatus"]
        IsDefault <- map["IsDefault"]
        IsClosed <- map["IsClosed"]
        IsPaid <- map["IsPaid"]
    }
}

class GetParentWorkOrdersData: Mappable {
    
    var WorkOrderID:String?
    var Subject:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        WorkOrderID <- map["WorkOrderID"]
        Subject <- map["Subject"]
    }
}

class GetInvoicePaymentTermsData: Mappable {
    
    var InvoicePaymentTermID:String?
    var PaymentTerms:String?
    var IsDefault:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoicePaymentTermID <- map["InvoicePaymentTermID"]
        PaymentTerms <- map["PaymentTerms"]
        IsDefault <- map["IsDefault"]
    }
}
class InvoiceRelatedLineItemData: Mappable {
    
    var InvoiceLineNo:String?
    var Invoice:String?
    var OrganizationID:String?
    var Product:String?
    var AssignedTo:String?
    var Discount:String?
    var ListPrice:String?
    var Taxable:String?
    var Quantity:String?
    var UnitPrice:String?
    var SubTotal:String?
    var TotalPrice:String?
    var ProductName:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Tax:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        InvoiceLineNo <- map["InvoiceLineNo"]
        Invoice <- map["Invoice"]
        OrganizationID <- map["OrganizationID"]
        Product <- map["Product"]
        AssignedTo <- map["AssignedTo"]
        Discount <- map["Discount"]
        ListPrice <- map["ListPrice"]
        Taxable <- map["Taxable"]
        Quantity <- map["Quantity"]
        UnitPrice <- map["UnitPrice"]
        SubTotal <- map["SubTotal"]
        TotalPrice <- map["TotalPrice"]
        ProductName <- map["ProductName"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Tax <- map["Tax"]
    }
}

class InvoiceRelatedEventData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var EventStatus:String?
    var EventTypeName:String?
    var EventStartDate:String?
    var EventEndDate:String?
    var EventStartTime:String?
    var EventEndTime:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        EventStatus <- map["EventStatus"]
        EventTypeName <- map["EventTypeName"]
        EventStartDate <- map["EventStartDate"]
        EventEndDate <- map["EventEndDate"]
        EventStartTime <- map["EventStartTime"]
        EventEndTime <- map["EventEndTime"]
    }
}
class InvoiceRelatedTaskData: Mappable {
    
    var TaskID:String?
    var Subject:String?
    var CallDisposition:String?
    var TaskType:String?
    var Priority:String?
    var TaskStatus:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskID <- map["TaskID"]
        Subject <- map["Subject"]
        CallDisposition <- map["CallDisposition"]
        TaskType <- map["TaskType"]
        Priority <- map["Priority"]
        TaskStatus <- map["TaskStatus"]
    }
}
class InvoiceRelatedNoteData: Mappable {
    
    var NoteID:String?
    var Subject:String?
    var CreatedDate:String?
    var OwnerName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        NoteID <- map["NoteID"]
        Subject <- map["Subject"]
        CreatedDate <- map["CreatedDate"]
        OwnerName <- map["OwnerName"]
    }
}
class InvoiceRelatedFileData: Mappable {
    
    var FileID:String?
    var FileName:String?
    var Subject:String?
    var ContentType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FileID <- map["FileID"]
        FileName <- map["FileName"]
        Subject <- map["Subject"]
        ContentType <- map["ContentType"]
    }
}
class GetInvoicesData: Mappable {
    
    var ProductID:String?
    var ProductCode:String?
    var ProductName:String?
    var Description:String?
    var ListPrice:String?
    var DefaultQuantity:String?
    var Taxable:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Tax:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ProductID <- map["ProductID"]
        ProductCode <- map["ProductCode"]
        ProductName <- map["ProductName"]
        Description <- map["Description"]
        ListPrice <- map["ListPrice"]
        DefaultQuantity <- map["DefaultQuantity"]
        Taxable <- map["Taxable"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Tax <- map["Tax"]
    }
}

class RecentTasksData: Mappable {
    
    var TaskID:String?
    var RelatedTo:String?
    var Subject:String?
    var ContactName:String?
    var DueDate:String?
    var TaskStatus:String?
    var TaskPriority:String?
    var TaskType:String?
    var IsQuantityEditable:String?
    var Tax:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskID <- map["TaskID"]
        RelatedTo <- map["RelatedTo"]
        Subject <- map["Subject"]
        ContactName <- map["ContactName"]
        DueDate <- map["DueDate"]
        TaskStatus <- map["TaskStatus"]
        TaskPriority <- map["TaskPriority"]
        TaskType <- map["TaskType"]
    }
}

class TaskDetailsDict: Mappable {
    
    var Subject:String?
    var RelatedTo:String?
    var What:String?
    var AssignedTo:String?
    var Who:String?
    var CallDisposition:String?
    var Description:String?
    var ActivityDate:String?
    var Address:String?
    var City:String?
    var State:String?
    var Country:String?
    var PostalCode:String?
    var Latitude:String?
    var Longitude:String?
    var Email:String?
    var TaskStatus:String?
    var Phone:String?
    var TaskPriority:String?
    var RecurrenceInterval:String?
    var IsReminderSet:String?
    var TaskType:String?
    var RecurrenceRegeneratedType:String?
    var IsRecurrence:String?
    var RecurrenceID:String?
    var DueDate:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedBy:String?
    var CreatedBy:String?
    var LastModifiedDate:String?
    var ContactName:String?
    var TaskStatusID:String?
    var TaskTypeID:String?
    var TaskPriorityID:String?
    var RelatedToName:String?
    var WhatName:String?
    var AssignedToName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        Subject <- map["Subject"]
        RelatedTo <- map["RelatedTo"]
        What <- map["What"]
        AssignedTo <- map["AssignedTo"]
        Who <- map["Who"]
        CallDisposition <- map["CallDisposition"]
        Description <- map["Description"]
        ActivityDate <- map["ActivityDate"]
        Address <- map["Address"]
        City <- map["City"]
        State <- map["State"]
        Country <- map["Country"]
        PostalCode <- map["PostalCode"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        Email <- map["Email"]
        TaskStatus <- map["TaskStatus"]
        Phone <- map["Phone"]
        TaskPriority <- map["TaskPriority"]
        RecurrenceInterval <- map["RecurrenceInterval"]
        IsReminderSet <- map["IsReminderSet"]
        TaskType <- map["TaskType"]
        RecurrenceRegeneratedType <- map["RecurrenceRegeneratedType"]
        IsRecurrence <- map["IsRecurrence"]
        RecurrenceID <- map["RecurrenceID"]
        DueDate <- map["DueDate"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedBy <- map["LastModifiedBy"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedDate <- map["LastModifiedDate"]
        ContactName <- map["ContactName"]
        TaskStatusID <- map["TaskStatusID"]
        TaskTypeID <- map["TaskTypeID"]
        TaskPriorityID <- map["TaskPriorityID"]
        RelatedToName <- map["RelatedToName"]
        WhatName <- map["WhatName"]
        AssignedToName <- map["AssignedToName"]
    }
}

class GetTaskTypeData: Mappable {
    
    var TaskTypeID:String?
    var TaskType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskTypeID <- map["TaskTypeID"]
        TaskType <- map["TaskType"]
    }
}

class GetTaskStatusData: Mappable {
    
    var TaskStatusID:String?
    var TaskStatus:String?
    var IsDefault:String?
    var IsClosed:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskStatusID <- map["TaskStatusID"]
        TaskStatus <- map["TaskStatus"]
        IsDefault <- map["IsDefault"]
        IsClosed <- map["IsClosed"]
    }
}

class GetTaskPriorityData: Mappable {
    
    var TaskPriorityID:String?
    var Priority:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskPriorityID <- map["TaskPriorityID"]
        Priority <- map["Priority"]
    }
}

class GetRelatedObjListData: Mappable {
    
    var Name:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        Name <- map["Name"]
    }
}
class GetRelatedToListData: Mappable {
    
    var ID:String?
    var Name:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        ID <- map["ID"]
        Name <- map["Name"]
    }
}
class TaskGetViewsData: Mappable {
    
    var TaskViewID:String?
    var TaskViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        TaskViewID <- map["TaskViewID"]
        TaskViewName <- map["TaskViewName"]
    }
}

class GetViewsFileData: Mappable {
    
    var FileViewID:String?
    var FileViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FileViewID <- map["FileViewID"]
        FileViewName <- map["FileViewName"]
    }
}

class RecentFilesData: Mappable {
    
    var FileID:String?
    var Subject:String?
    var FileName:String?
    var RelatedTo:String?
    var Description:String?
    var ContentType:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        FileID <- map["FileID"]
        Subject <- map["Subject"]
        FileName <- map["FileName"]
        RelatedTo <- map["RelatedTo"]
        Description <- map["Description"]
        ContentType <- map["ContentType"]
    }
}

class FileDetailsData: Mappable {
    
    var FileID:String?
    var ParentID:String?
    var RelatedTo:String?
    var What:String?
    var AssignedTo:String?
    var ContentType:String?
    var Description:String?
    var FileName:String?
    var FileSize:String?
    var Subject:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    var AssignedToName:String?
    var RelatedToName:String?
    var WhatName:String?
    var FileURL:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        FileID <- map["FileID"]
        ParentID <- map["ParentID"]
        RelatedTo <- map["RelatedTo"]
        What <- map["What"]
        AssignedTo <- map["AssignedTo"]
        ContentType <- map["ContentType"]
        Description <- map["Description"]
        FileName <- map["FileName"]
        FileSize <- map["FileSize"]
        Subject <- map["Subject"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        AssignedToName <- map["AssignedToName"]
        RelatedToName <- map["RelatedToName"]
        WhatName <- map["WhatName"]
        FileURL <- map["FileURL"]
    }
    
}
class LocationDetailsData: Mappable {
    
    var LocationID:String?
    var LocationNo:String?
    var Name:String?
    var Account:String?
    var PrimaryContact:String?
    var Owner:String?
    var PreferredTechnician:String?
    var ParentLocation:String?
    var Address:String?
    var City:String?
    var State:String?
    var Country:String?
    var PostalCode:String?
    var Latitude:String?
    var Longitude:String?
    var Notes:String?
    var LocationType:String?
    var AccessNotes:String?
    var IsActive:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    var AccountName:String?
    var PrimaryContactName:String?
    var LocationTypeName:String?
    var PreferredTechnicianName:String?
    var ParentLocationName:String?
    var OwnerName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        LocationID <- map["LocationID"]
        LocationNo <- map["LocationNo"]
        Name <- map["Name"]
        Account <- map["Account"]
        PrimaryContact <- map["PrimaryContact"]
        Owner <- map["Owner"]
        PreferredTechnician <- map["PreferredTechnician"]
        ParentLocation <- map["ParentLocation"]
        Address <- map["Address"]
        City <- map["City"]
        State <- map["State"]
        Country <- map["Country"]
        PostalCode <- map["PostalCode"]
        Latitude <- map["Latitude"]
        Longitude <- map["Longitude"]
        Notes <- map["Notes"]
        LocationType <- map["LocationType"]
        AccessNotes <- map["AccessNotes"]
        IsActive <- map["IsActive"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        AccountName <- map["AccountName"]
        PrimaryContactName <- map["PrimaryContactName"]
        LocationTypeName <- map["LocationTypeName"]
        PreferredTechnicianName <- map["PreferredTechnicianName"]
        ParentLocationName <- map["ParentLocationName"]
        OwnerName <- map["OwnerName"]
    }
    
}

class RecentLocationsData: Mappable {
    
    var LocationID:String?
    var LC:String?
    var Name:String?
    var AccountName:String?
    var LocationType:String?
    var OwnerName:String?
    var IsActive:String?
    var PreferredTechnicianName:String?
    var City:String?
    var State:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        LocationID <- map["LocationID"]
        LC <- map["LC#"]
        Name <- map["Name"]
        AccountName <- map["AccountName"]
        LocationType <- map["LocationType"]
        OwnerName <- map["OwnerName"]
        IsActive <- map["IsActive"]
        PreferredTechnicianName <- map["PreferredTechnicianName"]
        City <- map["City"]
        State <- map["State"]
    }
    
}

class GetLocationTypesData: Mappable {
    
    var LocationTypeID:String?
    var LocationType:String?
    var IsDeleted:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        LocationTypeID <- map["LocationTypeID"]
        LocationType <- map["LocationType"]
        IsDeleted <- map["IsDeleted"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        
    }
    
}
class GetParentLocationsData: Mappable {
    
    var LocationID:String?
    var Name:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        LocationID <- map["LocationID"]
        Name <- map["Name"]
        
    }
    
}

class GetViewsLocationsData: Mappable {
    
    var LocationViewID:String?
    var LocationViewName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        LocationViewID <- map["LocationViewID"]
        LocationViewName <- map["LocationViewName"]
        
    }
    
}

class GetEmailTemplatesData: Mappable {
    
    var EmailTemplateID:String?
    var Title:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        EmailTemplateID <- map["EmailTemplateID"]
        Title <- map["Title"]
        
    }
    
}
class GetEventPrioritiesData: Mappable {
    
    var EventPriorityID:String?
    var Priority:String?
    var IsDefault:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        EventPriorityID <- map["EventPriorityID"]
        Priority <- map["Priority"]
        IsDefault <- map["IsDefault"]
    }
    
}

class GetEventTypesData: Mappable {
    
    var EventTypeID:String?
    var EventTypeName:String?
    var IsDefault:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        EventTypeID <- map["EventTypeID"]
        EventTypeName <- map["EventTypeName"]
        IsDefault <- map["IsDefault"]
    }
    
}

class GetScheduleData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var Address:String?
    var Time:String?
    var RelatedTo:String?
    var RelatedObjID:String?
    var RelatedObjNo:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        Address <- map["Address"]
        Time <- map["Time"]
        RelatedTo <- map["RelatedTo"]
        RelatedObjID <- map["RelatedObjID"]
        RelatedObjNo <- map["RelatedObjNo"]
        
    }
    
}

class GetTasksData: Mappable {
    
    var TaskID:String?
    var Subject:String?
    var Date:String?
    var RelatedTo:String?
    var RelatedObjID:String?
    var RelatedObjNo:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        TaskID <- map["TaskID"]
        Subject <- map["Subject"]
        Date <- map["Date"]
        RelatedTo <- map["RelatedTo"]
        RelatedObjID <- map["RelatedObjID"]
        RelatedObjNo <- map["RelatedObjNo"]
        
    }
    
}

class GetRecentsData: Mappable {
    
    var ID:String?
    var Object:String?
    var Name:String?
    var Date:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        ID <- map["ID"]
        Object <- map["Object"]
        Name <- map["Name"]
        Date <- map["Date"]
    }
}

class GetCustomViewDetailsDict: Mappable {
    
    var ViewID:String?
    var ViewName:String?
    var RestrictVisibility:String?
    var DisplayedColumns:[DisplayedColumnsData]?
    var Filters:[FiltersData]?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        ViewID <- map["ViewID"]
        ViewName <- map["ViewName"]
        RestrictVisibility <- map["RestrictVisibility"]
        DisplayedColumns <- map["DisplayedColumns"]
        Filters <- map["Filters"]
        
    }
    
}


class DisplayedColumnsData: Mappable {
    
    var FieldName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        FieldName <- map["FieldName"]
        
    }
    
}
class FiltersData: Mappable {
    
    var WorkOrderFilterID:String?
    var FilterField:String?
    var FilterCondition:String?
    var FilterValue:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        WorkOrderFilterID <- map["WorkOrderFilterID"]
        FilterField <- map["FilterField"]
        FilterCondition <- map["FilterCondition"]
        FilterValue <- map["FilterValue"]
        
    }
    
}

class EventDetailsData: Mappable {
    
    var EventID:String?
    var Subject:String?
    var EventType:String?
    var EventSubType:String?
    var AssignedTo:String?
    var Who:String?
    var RelatedTo:String?
    var What:String?
    var Email:String?
    var PhoneNo:String?
    var EventStartDate:String?
    var EventEndDate:String?
    var EventStartTime:String?
    var EventEndTime:String?
    var Description:String?
    var IsAllDayEvent:String?
    var IsReminderSet:String?
    var EventStatus:String?
    var EventPriority:String?
    var IsRecurrence:String?
    var RecurrenceID:String?
    var IsDeleted:String?
    var CreatedBy:String?
    var LastModifiedBy:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var ContactName:String?
    var AssignedToName:String?
    var EventTypeName:String?
    var EventStatusID:String?
    var EventTypeID:String?
    var EventPriorityID:String?
    var RelatedToName:String?
    var WhatName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        EventID <- map["EventID"]
        Subject <- map["Subject"]
        EventType <- map["EventType"]
        EventSubType <- map["EventSubType"]
        AssignedTo <- map["AssignedTo"]
        Who <- map["Who"]
        RelatedTo <- map["RelatedTo"]
        What <- map["What"]
        Email <- map["Email"]
        PhoneNo <- map["PhoneNo"]
        EventStartDate <- map["EventStartDate"]
        EventEndDate <- map["EventEndDate"]
        EventStartTime <- map["EventStartTime"]
        EventEndTime <- map["EventEndTime"]
        Description <- map["Description"]
        IsAllDayEvent <- map["IsAllDayEvent"]
        IsReminderSet <- map["IsReminderSet"]
        EventStatus <- map["EventStatus"]
        EventPriority <- map["EventPriority"]
        IsRecurrence <- map["IsRecurrence"]
        RecurrenceID <- map["RecurrenceID"]
        IsDeleted <- map["IsDeleted"]
        CreatedBy <- map["CreatedBy"]
        LastModifiedBy <- map["LastModifiedBy"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        ContactName <- map["ContactName"]
        AssignedToName <- map["AssignedToName"]
        EventTypeName <- map["EventTypeName"]
        EventStatusID <- map["EventStatusID"]
        EventTypeID <- map["EventTypeID"]
        EventPriorityID <- map["EventPriorityID"]
        RelatedToName <- map["RelatedToName"]
        WhatName <- map["WhatName"]
    }
    
}

class NoteDetailsData: Mappable {
    
    var NoteID:String?
    var RelatedTo:String?
    var What:String?
    var Owner:String?
    var Body:String?
    var Subject:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedByName:String?
    var LastModifiedByName:String?
    var RelatedObjNo:String?
    var Title:String?
    var OwnerName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        NoteID <- map["NoteID"]
        RelatedTo <- map["RelatedTo"]
        What <- map["What"]
        Owner <- map["Owner"]
        Body <- map["Body"]
        Subject <- map["Subject"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedByName <- map["CreatedByName"]
        LastModifiedByName <- map["LastModifiedByName"]
        RelatedObjNo <- map["RelatedObjNo"]
        Title <- map["Title"]
        OwnerName <- map["OwnerName"]
    }
    
}

class UserDetailsData: Mappable {
    
    var UserID:String?
    var EmployeeNo:String?
    var Title:String?
    var FirstName:String?
    var LastName:String?
    var Email:String?
    var Profile:String?
    var Manager:String?
    var ReceiveAdminEmails:String?
    var PhoneNo:String?
    var MobileNo:String?
    var Department:String?
    var Division:String?
    var SenderEmail:String?
    var SenderName:String?
    var EmailSignature:String?
    var StartDate:String?
    var EndDate:String?
    var EndDay:String?
    var TimeZone:String?
    var Address:String?
    var City:String?
    var State:String?
    var LastModifiedBy:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var Country:String?
    var PostalCode:String?
    var DefaultGrpNotificationFreq:String?
    var StartOfDay:String?
    var EndOfDay:String?
    var IsActive:String?
    var CreatedBy:String?
    var FullName:String?
    var ProfileName:String?
    var DepartmentName:String?
    var DivisionName:String?
    var CompanyName:String?
    var ManagerName:String?
    var TimeZoneName:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        UserID <- map["UserID"]
        EmployeeNo <- map["EmployeeNo"]
        Title <- map["Title"]
        FirstName <- map["FirstName"]
        LastName <- map["LastName"]
        Email <- map["Email"]
        Profile <- map["Profile"]
        Manager <- map["Manager"]
        ReceiveAdminEmails <- map["ReceiveAdminEmails"]
        PhoneNo <- map["PhoneNo"]
        MobileNo <- map["MobileNo"]
        Department <- map["Department"]
        Division <- map["Division"]
        SenderEmail <- map["SenderEmail"]
        SenderName <- map["SenderName"]
        EmailSignature <- map["EmailSignature"]
        StartDate <- map["StartDate"]
        EndDate <- map["EndDate"]
        EndDay <- map["EndDay"]
        TimeZone <- map["TimeZone"]
        Address <- map["Address"]
        City <- map["City"]
        State <- map["State"]
        LastModifiedBy <- map["LastModifiedBy"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        Country <- map["Country"]
        PostalCode <- map["PostalCode"]
        DefaultGrpNotificationFreq <- map["DefaultGrpNotificationFreq"]
        StartOfDay <- map["StartOfDay"]
        EndOfDay <- map["EndOfDay"]
        IsActive <- map["IsActive"]
        CreatedBy <- map["CreatedBy"]
        FullName <- map["FullName"]
        ProfileName <- map["ProfileName"]
        DepartmentName <- map["DepartmentName"]
        DivisionName <- map["DivisionName"]
        CompanyName <- map["CompanyName"]
        ManagerName <- map["ManagerName"]
        TimeZoneName <- map["TimeZoneName"]
    }
    
}

class ProductDetailsData: Mappable {
    
    var ProductName:String?
    var Description:String?
    var DatePurchased:String?
    var QuantityUnitOfMeasure:String?
    var ProductCost:String?
    var ListPrice:String?
    var CreatedDate:String?
    var LastModifiedDate:String?
    var CreatedByName:String?
    var LastModifiedByName:String?
    var DefaultQuantity:String?
    var IsListPriceEditable:String?
    var IsQuantityEditable:String?
    var Taxable:String?
    var AccountName:String?
    var ProductFamilyName:String?
    
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        ProductName <- map["ProductName"]
        Description <- map["Description"]
        DatePurchased <- map["DatePurchased"]
        QuantityUnitOfMeasure <- map["QuantityUnitOfMeasure"]
        ProductCost <- map["ProductCost"]
        ListPrice <- map["ListPrice"]
        CreatedDate <- map["CreatedDate"]
        LastModifiedDate <- map["LastModifiedDate"]
        CreatedByName <- map["CreatedByName"]
        LastModifiedByName <- map["LastModifiedByName"]
        DefaultQuantity <- map["DefaultQuantity"]
        IsListPriceEditable <- map["IsListPriceEditable"]
        IsQuantityEditable <- map["IsQuantityEditable"]
        Taxable <- map["Taxable"]
        AccountName <- map["AccountName"]
        ProductFamilyName <- map["ProductFamilyName"]
    }
}

class GetGenDocTemplatesData: Mappable {
    
    var GenDocTemplateID:String?
    var TemplateName:String?
    var RelatedTo:String?
    var TemplateFile:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        GenDocTemplateID <- map["GenDocTemplateID"]
        TemplateName <- map["TemplateName"]
        RelatedTo <- map["RelatedTo"]
        TemplateFile <- map["TemplateFile"]
    }
}

class GenerateDocumentData: Mappable {
    
    var GenerateDocumentURL:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map)
    {
        GenerateDocumentURL <- map["GenerateDocumentURL"]
    }
}

class GetCustomFieldsData: Mappable {
    
    var CustomFieldID:String?
    var FieldName:String?
    var FieldLabel:String?
    var FieldType:String?
    var OptionValues:String?
    var OptionLabels:String?
    var FieldValue:String?
    var IsRequired:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        CustomFieldID <- map["CustomFieldID"]
        FieldName <- map["FieldName"]
        FieldLabel <- map["FieldLabel"]
        FieldType <- map["FieldType"]
        OptionValues <- map["OptionValues"]
        OptionLabels <- map["OptionLabels"]
        FieldValue <- map["FieldValue"]
        IsRequired <- map["IsRequired"]
    }
    
}
