
//
//  SortTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 13/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSort:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
