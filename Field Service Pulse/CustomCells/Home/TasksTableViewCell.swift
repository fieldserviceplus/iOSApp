//
//  TasksTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 06/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TasksTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblObject: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
