//
//  RecentTasksTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 17/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentTasksTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblTaskType: UILabel!
    @IBOutlet weak var lblTaskStatus: UILabel!
    @IBOutlet weak var lblTaskPriority: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
