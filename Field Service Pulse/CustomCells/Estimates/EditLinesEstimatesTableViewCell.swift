//
//  EditLinesEstimatesTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 10/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EditLinesEstimatesTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var txtDiscount: UITextField!
    @IBOutlet weak var lblNetTotal: UILabel!
    @IBOutlet weak var txtListPrice: UITextField!
    @IBOutlet weak var lblUnitPrice: UILabel!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var btnTaxable: UIButton!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnSelectCheckbox: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
