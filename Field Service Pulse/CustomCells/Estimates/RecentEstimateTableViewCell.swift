//
//  RecentEstimateTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentEstimateTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var lblEstimateName: UILabel!
    @IBOutlet weak var lblEstimateNo: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
