//
//  EstimateRelatedSubTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 04/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EstimateRelatedSubTableViewCell: UITableViewCell {

    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblThird: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
