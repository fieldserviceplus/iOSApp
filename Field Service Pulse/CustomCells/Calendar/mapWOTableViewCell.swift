//
//  mapWOTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 28/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class mapWOTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgShape: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
