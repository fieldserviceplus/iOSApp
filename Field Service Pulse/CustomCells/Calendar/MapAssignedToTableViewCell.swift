//
//  MapAssignedToTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MapAssignedToTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
