//
//  EventsTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 23/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {

    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
