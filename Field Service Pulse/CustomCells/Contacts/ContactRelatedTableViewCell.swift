//
//  ContactRelatedTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ContactRelatedTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var subTable: UITableView!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
