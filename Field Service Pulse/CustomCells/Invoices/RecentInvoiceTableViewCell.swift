//
//  RecentInvoiceTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 23/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentInvoiceTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var lblInvoiceNo: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
