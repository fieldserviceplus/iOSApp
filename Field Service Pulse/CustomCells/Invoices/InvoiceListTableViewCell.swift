//
//  InvoiceListTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class InvoiceListTableViewCell: UITableViewCell {

    //MARK: IButlet
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var stackview: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
