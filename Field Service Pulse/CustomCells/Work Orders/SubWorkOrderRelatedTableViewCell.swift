//
//  SubWorkOrderRelatedTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 14/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SubWorkOrderRelatedTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
