//
//  EditChemicalTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EditChemicalTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    
    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var txtChemical:UITextField!
    @IBOutlet weak var txtTested1:UITextField!
    @IBOutlet weak var txtTested2:UITextField!
    @IBOutlet weak var txtApplication1:UITextField!
    @IBOutlet weak var txtApplication2:UITextField!
    @IBOutlet weak var txtviewAppArea:UITextView!
    @IBOutlet weak var txtviewAppNotes:UITextView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var imgArrow: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
