//
//  SearchChemicalTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 23/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SearchChemicalTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    
    @IBOutlet weak var lblChemicalName: UILabel!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var btnEditChemical: UIButton!
    @IBOutlet weak var imgArrow: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
