//
//  RecentWorkOrderTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 09/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentWorkOrderTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
    @IBOutlet weak var lblWO_No: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblAccountType: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
