//
//  SubWorkOrdersTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 09/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SubWorkOrdersTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
   
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var viewBack: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
