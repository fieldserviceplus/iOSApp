//
//  AccountViewAllTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 11/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AccountViewAllTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblThird: UILabel!
    @IBOutlet weak var view_back:UIView!
    @IBOutlet weak var lblFourth: UILabel!
    @IBOutlet weak var lblFifth: UILabel!
    @IBOutlet weak var lblSixth: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnDetail: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
