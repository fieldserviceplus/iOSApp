//
//  LeftMenuTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 01/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
