//
//  RecentFilesTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentFilesTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblContentType: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
