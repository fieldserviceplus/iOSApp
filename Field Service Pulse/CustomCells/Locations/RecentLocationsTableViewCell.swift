//
//  RecentLocationsTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 21/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentLocationsTableViewCell: UITableViewCell {

    //MARK: IBOutlets
    @IBOutlet weak var lblLocNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocType: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblPrefTech: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
