//
//  LocationDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 21/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LocationDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    
    //MARK: IBOutlet
    
    @IBOutlet weak var lblLocNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblPrimaryContact: UILabel!
    @IBOutlet weak var lblParentLoc: UILabel!
    @IBOutlet weak var lblAccessNote: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblLocType: UILabel!
    @IBOutlet weak var lblPrefTech: UILabel!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblLastDate: UILabel!
    @IBOutlet weak var lblLastBy: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        webserviceCall()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "LocationID":User.instance.locationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.locationDetails(urlString: API.locationDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:LocationDetails) in
            
            if response.Result == "True"
            {
                self.lblName.text = response.data?.Name
                self.lblLocNo.text = response.data?.LocationNo
                self.lblAccount.text = response.data?.AccountName
                self.lblPrimaryContact.text  = response.data?.PrimaryContactName
                self.lblParentLoc.text = response.data?.ParentLocationName
                self.lblAccessNote.text = response.data?.AccessNotes
                self.lblNotes.text = response.data?.Notes
                self.lblAddress.text = (response.data?.Address) ?? ""
                self.lblCity.text = (response.data?.City)! + ", " + (response.data?.State)! + ", " + (response.data?.PostalCode)!
                self.lblCountry.text = (response.data?.Country)!
                self.lblAssignedTo.text = response.data?.OwnerName
                self.lblLocType.text = response.data?.LocationTypeName
                self.lblPrefTech.text = response.data?.PreferredTechnicianName
                
                self.lblCreatedBy.text = response.data?.CreatedBy
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblLastBy.text = response.data?.LastModifiedBy
                self.lblLastDate.text = response.data?.LastModifiedDate
                
                if response.data?.IsActive == "1" {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                } else {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
                }
            } else {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? LocationDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? LocationDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = true
                    parent.actionSheetView.isHidden = true
                    parent.btnClose.isHidden = true// Here you hide it when animation done
                }
            })
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? LocationDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? LocationDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = false // Here you hide it when animation done
                }
            })
        }
        else
        {
            // didn't move
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
