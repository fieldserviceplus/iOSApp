//
//  CreateLocationViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 21/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateLocationViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchDelegate {
    
    //MARK: Variables
    var latAddress:String?
    var longAddress:String?
    var textfieldTag = 0
    var isActive:String?
    
    var accountID:String?
    var contactID:String?
    var assignedToID:String?
    var contactsArr:[ContactsData] = []
    var locationTypesArr:[GetLocationTypesData] = []
    var assignedToArr:[UsersData] = []
    var parentLocationsArr:[GetParentLocationsData] = []
    var pickerView = UIPickerView()
    
    var locationTypeID:String?
    var prefTechID:String?
    var parentLocationID:String?
    var flagResponse = "0"
    var response:LocationDetails?
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtName: AkiraTextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPrimaryContact: UITextField!
    @IBOutlet weak var txtParentLoc: UITextField!
    @IBOutlet weak var txtAccessNotes: AkiraTextField!
    @IBOutlet weak var txtNotes: AkiraTextField!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtLocType: UITextField!
    @IBOutlet weak var txtPrefTech: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: AkiraTextField!
    @IBOutlet weak var txtState: AkiraTextField!
    @IBOutlet weak var txtCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnIsActive: UIButton!
    
    //UILabel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForContacts()
        webserviceCallForGetLocationTypes()
        webserviceCallForGetAllUsers()
        webserviceCallForGetParentLocations()
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtAccountTapped))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        txtAccount.isUserInteractionEnabled = true
        txtAccount.addGestureRecognizer(tap)
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        pickerView.dataSource = self
        txtLocType.inputView = pickerView
        txtPrimaryContact.inputView = pickerView
        txtAssignedTo.inputView = pickerView
        txtParentLoc.inputView = pickerView
        txtAccount.inputView = pickerView
        txtPrefTech.inputView = pickerView
        btnAddress.giveBorderToButton()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtPrimaryContact.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtLocType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtParentLoc.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAssignedTo.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtPrefTech.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtAddress.layer.borderWidth = 1.5
        self.txtAddress.layer.borderColor = UIColor.darkGray.cgColor
        if flagResponse == "1" {
            responseLocationDetails()
            flagResponse = "0"
        }
    }
    
    @objc func txtAccountTapped()
    {
        
        let searchVC = UIStoryboard(name: "WorkOrder", bundle: nil).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = "Account"
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        txtAccount.text = searchVC.accountName
        accountID = searchVC.accountID
    }
    
    func webserviceCallForGetLocationTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getLocationTypes(urlString: API.getLocationTypesURL, parameters: parameters , headers: headers, vc: self) { (response:GetLocationTypes) in
            
            if response.Result == "True"
            {
                
                self.locationTypesArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetParentLocations()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getParentLocations(urlString: API.getParentLocationsURL, parameters: parameters , headers: headers, vc: self) { (response:GetParentLocations) in
            
            if response.Result == "True"
            {
                self.parentLocationsArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForContacts()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contacts(urlString: API.allContactsURL, parameters: parameters , headers: headers, vc: self) { (response:ContactsResponse) in
            
            if response.Result == "True"
            {
                
                self.contactsArr = response.ContactsData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.assignedToArr = response.UsersData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    func webserviceCall()
    {
        if accountID == nil || locationTypeID == nil ||  assignedToID == nil || txtName.text == "" ||  txtAddress.text == "" || txtCity.text == "" || txtState.text == "" || txtCountry.text == "" || txtPostalCode.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["Account"] = accountID
        parameters["Owner"] = assignedToID ?? ""
        parameters["LocationType"] = locationTypeID ?? ""
        parameters["ParentLocation"] = parentLocationID ?? ""
        parameters["PreferredTechnician"] = prefTechID ?? ""
        parameters["PrimaryContact"] = contactID ?? ""
        parameters["Address"] = txtAddress.text ?? ""
        parameters["City"] = txtCity.text ?? ""
        parameters["PostalCode"] = txtPostalCode.text ?? ""
        parameters["State"] = txtState.text ?? ""
        parameters["Country"] = txtCountry.text ?? ""
        parameters["Latitude"] = latAddress ?? ""
        parameters["Longitude"] = longAddress ?? ""
        parameters["Name"] = txtName.text ?? ""
        parameters["Notes"] = txtNotes.text ?? ""
        parameters["AccessNotes"] = txtAccessNotes.text ?? ""
        
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createLocation(urlString: API.createLocationURL, parameters: parameters , headers: headers, vc: self) { (response:CreateLocation) in
            
            if response.Result == "True"
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationDetailsViewController") as! LocationDetailsViewController
                User.instance.locationID = String(response.LocationID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func responseLocationDetails()
    {
        self.assignedToID = response?.data?.Owner
        self.txtAssignedTo.text = response?.data?.OwnerName
        self.txtAccount.text = response?.data?.AccountName
        self.accountID = response?.data?.Account
        self.txtLocType.text = response?.data?.LocationTypeName
        self.locationTypeID = response?.data?.LocationType
        self.txtParentLoc.text = response?.data?.ParentLocationName
        self.parentLocationID = response?.data?.ParentLocation
        self.prefTechID = response?.data?.PreferredTechnician
        self.txtPrefTech.text = response?.data?.PreferredTechnicianName
        self.txtPrefTech.text = response?.data?.PrimaryContactName
        self.contactID = response?.data?.PrimaryContact
        self.txtAddress.text = response?.data?.Address
        self.txtCity.text = response?.data?.City
        self.txtPostalCode.text = response?.data?.PostalCode
        self.txtState.text = response?.data?.State
        self.txtCountry.text = response?.data?.Country
        self.latAddress = response?.data?.Latitude
        self.longAddress = response?.data?.Longitude
        self.txtName.text = response?.data?.Name
        self.txtAccessNotes.text = response?.data?.AccessNotes
        self.txtNotes.text = response?.data?.Notes
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 3
        {
            return contactsArr.count + 1
        }
        else if textfieldTag == 4
        {
            return parentLocationsArr.count + 1
        }
        else if textfieldTag == 7
        {
            return assignedToArr.count + 1
        }
        else if textfieldTag == 8
        {
            return locationTypesArr.count + 1
        }
        else if textfieldTag == 9
        {
            return assignedToArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 3
        {
            return row == 0 ? "--None--":contactsArr[row-1].FullName
        }
        else if textfieldTag == 4
        {
            return row == 0 ? "--None--":parentLocationsArr[row-1].Name
        }
            
        else if textfieldTag == 7
        {
            return row == 0 ? "--None--":assignedToArr[row-1].FullName
        }
        else if textfieldTag == 8
        {
            return row == 0 ? "--None--":locationTypesArr[row-1].LocationType
        }
        else if textfieldTag == 9
        {
            return row == 0 ? "--None--":assignedToArr[row-1].FullName
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 3
        {
            if row == 0
            {
                txtPrimaryContact.text = ""
            }
            else{
                txtPrimaryContact.text = contactsArr[row-1].FullName
                contactID = contactsArr[row-1].ContactID
            }
            
        }
        if textfieldTag == 4
        {
            if row == 0
            {
                txtParentLoc.text = ""
            }
            else{
                txtParentLoc.text = parentLocationsArr[row-1].Name
                parentLocationID = parentLocationsArr[row-1].LocationID
            }
            
        }
            
        else if textfieldTag == 7
        {
            if row == 0
            {
                txtAssignedTo.text = ""
            }
            else{
                txtAssignedTo.text = assignedToArr[row-1].FullName
                assignedToID = assignedToArr[row-1].UserID
            }
            
        }
        else if textfieldTag == 8
        {
            if row == 0
            {
                txtLocType.text = ""
            }
            else{
                txtLocType.text = locationTypesArr[row-1].LocationType
                locationTypeID = locationTypesArr[row-1].LocationTypeID
            }
            
        }
        else if textfieldTag == 9
        {
            if row == 0
            {
                txtPrefTech.text = ""
            }
            else{
                txtPrefTech.text = assignedToArr[row-1].FullName
                prefTechID = assignedToArr[row-1].UserID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtAccount || textField == txtPrimaryContact || textField == txtParentLoc || textField == txtAssignedTo || textField == txtLocType || textField == txtPrefTech
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtPrimaryContact.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtParentLoc.becomeFirstResponder()
            
        }
        else if textField.tag == 4
        {
            txtAccessNotes.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtNotes.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtLocType.becomeFirstResponder()
        }
            
        else if textField.tag == 8
        {
            txtPrefTech.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 9
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self as GMSAutocompleteViewControllerDelegate
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
    }
    
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        
        webserviceCall()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddress(_ sender: Any) {
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    @IBAction func btnIsActive(_ sender: Any) {
        
        if (btnIsActive.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isActive = "0"
        }
        else{
            btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            isActive = "1"
        }
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        if let SelectLocationVC = sender.source as? SelectLocationViewController {
            
            txtAddress.text = SelectLocationVC.address
            txtCity.text = SelectLocationVC.city
            txtState.text = SelectLocationVC.state
            txtCountry.text = SelectLocationVC.country
            txtPostalCode.text = SelectLocationVC.postalCode
            self.latAddress = SelectLocationVC.latitude
            self.longAddress = SelectLocationVC.longitude
        }
        
    }
}
extension CreateLocationViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            latAddress = String(place.coordinate.latitude)
            longAddress = String(place.coordinate.longitude)
            if component.type == "sublocality_level_1" {
                print(component.name)
                txtAddress.text = "\(place.name) \(area ?? "") \(component.name)"
            }
            if component.type == "postal_code"
            {
                print("Code: \(component.name)")
                txtPostalCode.text = component.name
            }
            if component.type == "administrative_area_level_2" {
                print("City: \(component.name)")
                txtCity.text = component.name
            }
            if component.type == "administrative_area_level_1"
            {
                print("State: \(component.name)")
                txtState.text = component.name
                
            }
            if component.type == "country"
            {
                print("Country: \(component.name)")
                txtCountry.text = component.name
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
