//
//  LocationDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 21/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces
import Fusuma

class LocationDetailsViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchDelegate {
    
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var latAddress:String?
    var longAddress:String?
    var textfieldTag = 0
    
    var headerTitle = ""
    var status = ""
    var isActive:String?
    
    var accountID:String?
    var contactID:String?
    var assignedToID:String?
    var contactsArr:[ContactsData] = []
    var locationTypesArr:[GetLocationTypesData] = []
    var assignedToArr:[UsersData] = []
    var parentLocationsArr:[GetParentLocationsData] = []
    var pickerView = UIPickerView()
    
    var locationTypeID:String?
    var prefTechID:String?
    var parentLocationID:String?
    var menuView:ActionSheetLocation?
    var response_locationDetails:LocationDetails?
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtName: AkiraTextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPrimaryContact: UITextField!
    @IBOutlet weak var txtParentLoc: UITextField!
    @IBOutlet weak var txtAccessNotes: AkiraTextField!
    @IBOutlet weak var txtNotes: AkiraTextField!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtLocType: UITextField!
    @IBOutlet weak var txtPrefTech: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: AkiraTextField!
    @IBOutlet weak var txtState: AkiraTextField!
    @IBOutlet weak var txtCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnIsActive: UIButton!
    
    //UILabel
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    
    @IBOutlet weak var topConstraintActionView: NSLayoutConstraint!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var view_back: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
        webserviceCallForLocationDetails()
        webserviceCallForContacts()
        webserviceCallForGetLocationTypes()
        webserviceCallForGetAllUsers()
        webserviceCallForGetParentLocations()
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtAccountTapped))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        txtAccount.isUserInteractionEnabled = true
        txtAccount.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.lblHeaderTitle.text = headerTitle
        
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        
        if let actionsheetView = Bundle.main.loadNibNamed("ActionSheetLocation", owner: self, options: nil)?.first as? ActionSheetLocation
        {
            menuView = actionsheetView
            actionsheetView.btnClose.addTarget(self, action: #selector(btnClose1), for: .touchUpInside)
            actionsheetView.btnCall.addTarget(self, action: #selector(btnCall1), for: .touchUpInside)
            actionsheetView.btnText.addTarget(self, action: #selector(btnMessage1), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnNewWO.addTarget(self, action: #selector(btnNewWO), for: .touchUpInside)
            actionsheetView.btnNewEstimate.addTarget(self, action: #selector(btnNewEstimate), for: .touchUpInside)
            actionsheetView.btnNewContact.addTarget(self, action: #selector(btnNewContact), for: .touchUpInside)
            actionsheetView.btnNewInvoice.addTarget(self, action: #selector(btnNewInvoice), for: .touchUpInside)
            actionsheetView.btnNewFile.addTarget(self, action: #selector(btnNewFile), for: .touchUpInside)
            actionsheetView.btnNewTask.addTarget(self, action: #selector(btnNewTask), for: .touchUpInside)
            actionsheetView.btnNewAccount.addTarget(self, action: #selector(btnNewAccount), for: .touchUpInside)
            actionsheetView.btnCopyLocation.addTarget(self, action: #selector(btnCopyLocation), for: .touchUpInside)
            actionsheetView.btnNewEvent.addTarget(self, action: #selector(btnNewEvent), for: .touchUpInside)
            actionsheetView.btnDeleteLocation.addTarget(self, action: #selector(btnDeleteLocation), for: .touchUpInside)
            
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetView.bounds
            self.actionSheetView.addSubview(actionsheetView)
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        txtLocType.inputView = pickerView
        txtPrimaryContact.inputView = pickerView
        txtAssignedTo.inputView = pickerView
        txtParentLoc.inputView = pickerView
        txtAccount.inputView = pickerView
        txtPrefTech.inputView = pickerView
        btnAddress.giveBorderToButton()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtPrimaryContact.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtLocType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtParentLoc.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAssignedTo.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtPrefTech.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtAddress.layer.borderWidth = 1.5
        self.txtAddress.layer.borderColor = UIColor.darkGray.cgColor
    }
    func loadNIB()
    {
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse
        {
            customView.tag = 101
            customView.btnAttachFile.addTarget(self, action: #selector(btnAttachFile), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(btnGallery), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    @objc func btnCopyLocation()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateLocationViewController") as! CreateLocationViewController
        vc.flagResponse = "1"
        vc.response = response_locationDetails
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func btnAttachFile()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: false) {
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = true
            }
        }
    }
    @objc func btnGallery()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        //imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        //fusuma.hasVideo = true //To allow for video capturing with .library and .camera available by default
        fusuma.cropHeightRatio = 0.6 // Height-to-width ratio. The default value is 1, which means a squared-size photo.
        fusuma.allowMultipleSelection = true // You can select multiple photos from the camera roll. The default value is false.
        self.present(fusuma, animated: true, completion: nil)
    }
    
    
    @objc func btnClose1()
    {
        self.view_back.isHidden = true
        self.menuView?.isHidden = true
        self.viewBottomContainer.isHidden = false
    }
    @objc func btnEmail()
    {
        let emailVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailViewController") as! EmailViewController
        emailVC.strRelatedTo = "Contact"
        emailVC.strWhat = User.instance.contactID
        self.navigationController?.pushViewController(emailVC, animated: true)
    }
    @objc func btnNewEvent()
    {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "Location"
        createEventVC.objectID = User.instance.locationID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    @objc func btnNewAccount()
    {
        let vc = UIStoryboard(name: "WorkOrder", bundle: nil).instantiateViewController(withIdentifier: "CreateAccountViewController") as? CreateAccountViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewWO()
    {
        let vc = UIStoryboard(name: "WorkOrder", bundle: nil).instantiateViewController(withIdentifier: "CreateWOViewController") as? CreateWOViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewEstimate()
    {
        let vc = UIStoryboard(name: "Estimate", bundle: nil).instantiateViewController(withIdentifier: "CreateEstimateViewController") as? CreateEstimateViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewContact()
    {
        let vc = UIStoryboard(name: "Contact", bundle: nil).instantiateViewController(withIdentifier: "CreateContactViewController") as? CreateContactViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewInvoice()
    {
        let vc = UIStoryboard(name: "Invoice", bundle: nil).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as? CreateInvoiceViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewFile()
    {
        loadNIB()
        
    }
    @objc func btnNewTask()
    {
        let vc = UIStoryboard(name: "Task", bundle: nil).instantiateViewController(withIdentifier: "CreateTaskViewController") as? CreateTaskViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionSheetView?.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    @objc func btnCall1()
    {
//        if let url = URL(string: "tel://\(String(describing: self.txtPhone.text ?? ""))"), UIApplication.shared.canOpenURL(url) {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
    }
    @objc func btnMessage1()
    {
//        if (MFMessageComposeViewController.canSendText()) {
//            let controller = MFMessageComposeViewController()
//            controller.body = ""
//            controller.recipients = [self.txtPhone.text ?? ""]
//            controller.messageComposeDelegate = self
//            self.present(controller, animated: true, completion: nil)
//        }
    }
    @objc func btnDeleteLocation()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Location",
                          "What":User.instance.locationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    
    @objc func txtAccountTapped()
    {
        let searchVC = UIStoryboard(name: "WorkOrder", bundle: nil).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = "Account"
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        txtAccount.text = searchVC.accountName
        accountID = searchVC.accountID
    }
    
    func webserviceCallForGetLocationTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getLocationTypes(urlString: API.getLocationTypesURL, parameters: parameters , headers: headers, vc: self) { (response:GetLocationTypes) in
            
            if response.Result == "True"
            {
                
                self.locationTypesArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetParentLocations()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getParentLocations(urlString: API.getParentLocationsURL, parameters: parameters , headers: headers, vc: self) { (response:GetParentLocations) in
            
            if response.Result == "True"
            {
                self.parentLocationsArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForContacts()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contacts(urlString: API.allContactsURL, parameters: parameters , headers: headers, vc: self) { (response:ContactsResponse) in
            
            if response.Result == "True"
            {
                
                self.contactsArr = response.ContactsData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.assignedToArr = response.UsersData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForLocationDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "LocationID":User.instance.locationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.locationDetails(urlString: API.locationDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:LocationDetails) in
            
            if response.Result == "True"
            {
                self.response_locationDetails = response
                self.txtName.text = response.data?.Name
                self.txtAccount.text = response.data?.AccountName
                self.txtPrimaryContact.text  = response.data?.PrimaryContactName
                self.txtParentLoc.text = response.data?.ParentLocationName
                self.txtAccessNotes.text = response.data?.AccessNotes
                self.txtNotes.text = response.data?.Notes
                self.txtAddress.text = response.data?.Address
                self.txtCity.text = response.data?.City
                self.txtPostalCode.text =  response.data?.PostalCode
                self.txtState.text = response.data?.State
                self.txtCountry.text = (response.data?.Country) ?? ""
                self.latAddress = (response.data?.Latitude ?? "")
                self.longAddress = (response.data?.Longitude ?? "")
                self.txtAssignedTo.text = response.data?.OwnerName
                self.txtLocType.text = response.data?.LocationTypeName
                self.txtPrefTech.text = response.data?.PreferredTechnicianName
                self.lblAccount.text = "Account: " + (response.data?.AccountName ?? "")!
                self.lblAssignedTo.text = "Assigned To: " + (response.data?.OwnerName ?? "")!
                self.lblHeaderTitle.text = response.data?.LocationTypeName
                self.accountID = response.data?.Account
                self.contactID = response.data?.PrimaryContact
                self.parentLocationID = response.data?.ParentLocation
                self.locationTypeID = response.data?.LocationType
                self.assignedToID = response.data?.Owner
                self.prefTechID = response.data?.PreferredTechnician
                
                self.txtCreatedBy.text = response.data?.CreatedBy
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtLastModifiedBy.text = response.data?.LastModifiedBy
                self.txtLastModifiedDate.text = response.data?.LastModifiedDate
                
                if response.data?.IsActive == "1" {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                } else {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
                }
            } else {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForEditLocation()
    {
        if accountID == nil || locationTypeID == nil ||  assignedToID == nil || txtName.text == "" ||  txtAddress.text == "" || txtCity.text == "" || txtState.text == "" || txtCountry.text == "" || txtPostalCode.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["LocationID"] = User.instance.locationID
        parameters["Account"] = accountID
        parameters["Owner"] = assignedToID ?? ""
        parameters["LocationType"] = locationTypeID ?? ""
        parameters["ParentLocation"] = parentLocationID ?? ""
        parameters["PreferredTechnician"] = prefTechID ?? ""
        parameters["PrimaryContact"] = contactID ?? ""
        parameters["Address"] = txtAddress.text ?? ""
        parameters["City"] = txtCity.text ?? ""
        parameters["PostalCode"] = txtPostalCode.text ?? ""
        parameters["State"] = txtState.text ?? ""
        parameters["Country"] = txtCountry.text ?? ""
        parameters["Latitude"] = latAddress ?? ""
        parameters["Longitude"] = longAddress ?? ""
        parameters["Name"] = txtName.text ?? ""
        parameters["Notes"] = txtNotes.text ?? ""
        parameters["AccessNotes"] = txtAccessNotes.text ?? ""
        
        print(parameters)
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.editLocation(urlString: API.editLocationURL, parameters: parameters , headers: headers, vc: self) { (response:EditLocation) in
            
            if response.Result == "True"
            {
                self.webserviceCallForLocationDetails()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[0] as! LocationDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            view_back.isHidden = true
            actionSheetView.isHidden = true
            btnCancel.isHidden = true
            //view_backNewFile.isHidden = true
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 3
        {
            return contactsArr.count + 1
        }
        else if textfieldTag == 4
        {
            return parentLocationsArr.count + 1
        }
            
        else if textfieldTag == 7
        {
            return assignedToArr.count + 1
        }
        else if textfieldTag == 8
        {
            return locationTypesArr.count + 1
        }
        else if textfieldTag == 9
        {
            return assignedToArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 3
        {
            return row == 0 ? "--None--":contactsArr[row-1].FullName
        }
        else if textfieldTag == 4
        {
            return row == 0 ? "--None--":parentLocationsArr[row-1].Name
        }
            
        else if textfieldTag == 7
        {
            return row == 0 ? "--None--":assignedToArr[row-1].FullName
        }
        else if textfieldTag == 8
        {
            return row == 0 ? "--None--":locationTypesArr[row-1].LocationType
        }
        else if textfieldTag == 9
        {
            return row == 0 ? "--None--":assignedToArr[row-1].FullName
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 3
        {
            if row == 0
            {
                txtPrimaryContact.text = ""
            }
            else{
                txtPrimaryContact.text = contactsArr[row-1].FullName
                contactID = contactsArr[row-1].ContactID
            }
            
        }
        if textfieldTag == 4
        {
            if row == 0
            {
                txtParentLoc.text = ""
            }
            else{
                txtParentLoc.text = parentLocationsArr[row-1].Name
                parentLocationID = parentLocationsArr[row-1].LocationID
            }
            
        }
            
        else if textfieldTag == 7
        {
            if row == 0
            {
                txtAssignedTo.text = ""
            }
            else{
                txtAssignedTo.text = assignedToArr[row-1].FullName
                assignedToID = assignedToArr[row-1].UserID
            }
            
        }
        else if textfieldTag == 8
        {
            if row == 0
            {
                txtLocType.text = ""
            }
            else{
                txtLocType.text = locationTypesArr[row-1].LocationType
                locationTypeID = locationTypesArr[row-1].LocationTypeID
            }
            
        }
        else if textfieldTag == 9
        {
            if row == 0
            {
                txtPrefTech.text = ""
            }
            else{
                txtPrefTech.text = assignedToArr[row-1].FullName
                prefTechID = assignedToArr[row-1].UserID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtAccount || textField == txtPrimaryContact || textField == txtParentLoc || textField == txtAssignedTo || textField == txtLocType || textField == txtPrefTech
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtPrimaryContact.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtParentLoc.becomeFirstResponder()
            
        }
        else if textField.tag == 4
        {
            txtAccessNotes.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtNotes.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtLocType.becomeFirstResponder()
        }
            
        else if textField.tag == 8
        {
            txtPrefTech.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 9
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self as GMSAutocompleteViewControllerDelegate
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
    }
    
    
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        webserviceCallForEditLocation()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
        
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        btnCancel.isHidden = false
        btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
    }
    
   
    
    @IBAction func btnAddress(_ sender: Any) {
        
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func btnIsActive(_ sender: Any) {
        
        if (btnIsActive.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isActive = "0"
        }
        else{
            btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            isActive = "1"
        }
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        
        if let SelectLocationVC = sender.source as? SelectLocationViewController {
            
            txtAddress.text = SelectLocationVC.address
            txtCity.text = SelectLocationVC.city
            txtState.text = SelectLocationVC.state
            txtCountry.text = SelectLocationVC.country
            txtPostalCode.text = SelectLocationVC.postalCode
            self.latAddress = SelectLocationVC.latitude
            self.longAddress = SelectLocationVC.longitude
        }
        
        
    }
    
    
    @IBAction func btnCall(_ sender: Any) {
        
        if let url = URL(string: "tel://\(String(describing: lblAssignedTo.text))"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func btnMessage(_ sender: Any) {
    }
    @IBAction func btnCalender(_ sender: Any) {
    }
    
    @IBAction func btnMore(_ sender: Any) {
        
        //self.btnCancel.isHidden = false
        self.actionSheetView.isHidden = false
        self.viewBottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
}

extension LocationDetailsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                area = component.name
            }
            latAddress = String(place.coordinate.latitude)
            longAddress = String(place.coordinate.longitude)
            if component.type == "sublocality_level_1" {
                print(component.name)
                txtAddress.text = "\(place.name) \(area ?? "") \(component.name)"
            }
            if component.type == "postal_code"
            {
                print("Code: \(component.name)")
                txtPostalCode.text = component.name
            }
            if component.type == "administrative_area_level_2" {
                print("City: \(component.name)")
                txtCity.text = component.name
            }
            if component.type == "administrative_area_level_1"
            {
                print("State: \(component.name)")
                txtState.text = component.name
            }
            if component.type == "country"
            {
                print("Country: \(component.name)")
                txtCountry.text = component.name
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension LocationDetailsViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtLocType.text ?? "",
                          "AssignedTo":self.assignedToID!,
                          "RelatedTo":"Location",
                          "What":User.instance.locationID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.uploadDocs(urlString: API.createNewFileAllURL, fileName:"FileName", URLs: urls as [NSURL], parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
}

extension LocationDetailsViewController:FusumaDelegate {
    
    // Return the image which is selected from camera roll or is taken via the camera.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(image: UIImage, source: FusumaMode) {
        
        print("Called just after FusumaViewController is dismissed.")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        print("Called just after a video has been selected.")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // Return selected images when you allow to select multiple photos.
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtLocType.text ?? "",
                          "AssignedTo":self.assignedToID!,
                          "RelatedTo":"Location",
                          "What":User.instance.locationID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createNewFileAll(urlString: API.createNewFileAllURL, pickedImages: images, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // Return an image and the detailed information.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
    }
}
