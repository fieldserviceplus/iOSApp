//
//  ActionSheetLocation.swift
//  Field Service Pulse
//
//  Created by Apple on 04/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ActionSheetLocation: UIView {
    
    @IBOutlet weak var btnCall:UIButton!
    @IBOutlet weak var btnText:UIButton!
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnNewAccount:UIButton!
    @IBOutlet weak var btnNewWO:UIButton!
    @IBOutlet weak var btnNewEvent:UIButton!
    @IBOutlet weak var btnNewEstimate:UIButton!
    @IBOutlet weak var btnNewContact:UIButton!
    @IBOutlet weak var btnNewInvoice:UIButton!
    @IBOutlet weak var btnNewFile:UIButton!
    @IBOutlet weak var btnNewTask:UIButton!
    @IBOutlet weak var btnCopyLocation:UIButton!
    @IBOutlet weak var btnDeleteLocation:UIButton!
    @IBOutlet weak var btnNewProduct: UIButton!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var back_view: UIView!
    
    @IBOutlet weak var scrollview:UIScrollView!
    
    override func awakeFromNib() {
        
        self.btnCall.leftImage(image: #imageLiteral(resourceName: "phone-1"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnText.leftImage(image: #imageLiteral(resourceName: "message"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEdit.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewAccount.leftImage(image: #imageLiteral(resourceName: "account"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewWO.leftImage(image: #imageLiteral(resourceName: "orders"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewEstimate.leftImage(image: #imageLiteral(resourceName: "estimate"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewEvent.leftImage(image: #imageLiteral(resourceName: "calendar_black"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewInvoice.leftImage(image: #imageLiteral(resourceName: "invoice"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewContact.leftImage(image: #imageLiteral(resourceName: "contacts"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewFile.leftImage(image: #imageLiteral(resourceName: "file"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewTask.leftImage(image: #imageLiteral(resourceName: "task"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnCopyLocation.leftImage(image: #imageLiteral(resourceName: "copy"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnDeleteLocation.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewProduct.leftImage(image: #imageLiteral(resourceName: "product"), renderMode: UIImage.RenderingMode.alwaysOriginal)
    }
}
