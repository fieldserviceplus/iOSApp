//
//  RecentLocationListViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 24/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentLocationListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, sendingLocationDataDelegate {
    
    //MARK: Variables
    private let CELL_LIST_LOCATIONS = "cell_LocationList"
    
    private let SEGUE_CREATE_LOCATION = "segueNewLocation"
    private let SEGUE_LOCATION_DETAILS = "segueDetails"
    
    var getLocationViewsArr:[GetViewsLocationsData] = []
    var listArr:[[String:String]] = []
    var arrTag:Int?
    var LocationViewID = ""
    var LocationViewName = ""
    var count:Int?
    var sortingFlag = 0
    var flag = 0
    var city:String?
    var state:String?
    var addressLabel:PaddingLabel?
    var lastContentOffset: CGFloat = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var table_locationList: UITableView!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSorting: UIButton!
    @IBOutlet weak var viewBottomContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForLocationListView()
        webserviceCallForGetViews()
        lblDropdown.text = "  " + LocationViewName + "  "
        if LocationViewID == "MyLocations" || LocationViewID == "AllLocations" || LocationViewID == "LocationsCreatedThisWeek" 
        {
            btnFilter.isEnabled = true
        }
        else
        {
            btnFilter.isEnabled = false
        }
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        table_locationList.tableFooterView = UIView()
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
        
    }
    func sendData(array: [[String : String]]) {
        
        listArr = array
        self.table_locationList.reloadData()
    }
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getViewsLocations(urlString: API.getViewsLocationsURL, parameters: parameters, headers: headers, vc: self) { (response:GetViewsLocations) in
            
            if response.Result == "True"
            {
                
                self.getLocationViewsArr = response.data!
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForLocationListView()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "LocationViewID":LocationViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.viewLocationList(urlString: API.viewLocationListURL, parameters: parameters, headers: headers, vc: self) { (response:ViewLocationList) in
            
            if response.Result == "True"
            {
                self.listArr = response.data as! [[String : String]]
                
                for i in 0..<self.listArr.count
                {
                    for (key, value) in self.listArr[i] {
                        
                        if key == "LocationNo"
                        {
                            self.flag = 1
                        }
                    }
                }
                if self.flag == 0
                {
                    self.btnSorting.isEnabled = false
                }
                else
                {
                    self.btnSorting.isEnabled = true
                }
                
                self.table_locationList.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_LOCATION_DETAILS
        {
            let detailVC = segue.destination as! LocationDetailsViewController
            
            let location = sender as? [String:String]
            User.instance.locationID = location?["LocationID"] ?? ""
            detailVC.headerTitle = location?["LocationNo"] ?? ""
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == table_locationList
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
        
    }
    
    //MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table_locationList
        {
            return self.listArr.count
        }
        return getLocationViewsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table_locationList
        {
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LIST_LOCATIONS) as! RecentLocationListTableViewCell
            
            
            for subview in cell.stackview.subviews {
                subview.removeFromSuperview()
            }
            
            cell.view_back.dropShadow()
            
            if LocationViewName == "My Locations" || LocationViewName == "All Locations" || LocationViewName == "Locations Created This Week"
            {
                let myArr = ["LocationNo","Name","LocationType","AccountName","PrimaryContactName","PreferredTechnicianName","City","State"]
                
                if listArr.count > 0
                {
                    
                    let dict = listArr[indexPath.row]
                    for j in 0..<myArr.count
                    {
                        
                        if myArr[j] == "City"
                        {
                            city = dict[myArr[j]]
                        }
                        else if myArr[j] == "State"
                        {
                            state = dict[myArr[j]]
                            
                            let label = PaddingLabel()
                            label.textAlignment = .left
                            
                            let address = (city ?? "")  + "," + (state ?? "")
                            label.text = " Address" + ": " + address
                            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                                count = count! + 20
                                label.font = label.font.withSize(15)
                            } else { //IPAD
                                count = count! + 25
                                label.font = label.font.withSize(20)
                            }
                            
                            label.textColor = UIColor.darkGray
                            cell.stackview.addArrangedSubview(label)
                        }
                        else
                        {
                            let label = PaddingLabel()
                            label.textAlignment = .left
                            
                            
                            label.text = myArr[j].camelCaseToWords()  + ": " + (dict[myArr[j]] ?? "")
                            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                                count = count! + 20
                                label.font = label.font.withSize(15)
                            } else { //IPAD
                                count = count! + 25
                                label.font = label.font.withSize(20)
                            }
                            
                            label.textColor = UIColor.darkGray
                            cell.stackview.addArrangedSubview(label)
                        }
                        
                    }
                    
                    
                }
            }
            else
            {
                for (key, value) in listArr[indexPath.row] {
                    // here use key and value
                    
                    print(key)
                    print(value)
                    
                    
                    if key == "LocationNo" || key == "Name" || key == "Account" || key == "PrimaryContact" || key == "Owner" || key == "PreferredTechnician" || key == "ParentLocation" || key == "Notes" || key == "LocationType" || key == "AccessNotes"
                    {
                        
                        let label = PaddingLabel()
                        label.textAlignment = .left
                        
                        
                        label.text = key.camelCaseToWords()  + ": " + value
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            label.font = label.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            label.font = label.font.withSize(20)
                        }
                        
                        label.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(label)
                    }
                    
                    if key == "City"
                    {
                        if addressLabel?.text != nil
                        {
                            addressLabel?.text = ""
                            addressLabel?.text = " Address: " + value + ", " + (state ?? "")
                        }
                        else
                        {
                            city = value
                        }
                    }
                    
                    if key == "State"
                    {
                        state = value
                        addressLabel = PaddingLabel()
                        addressLabel?.textAlignment = .left
                        addressLabel?.text = " Address: " + (city ?? "") + ", " + value
                        
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            addressLabel?.font = addressLabel?.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            addressLabel?.font = addressLabel?.font.withSize(20)
                        }
                        addressLabel?.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(addressLabel!)
                        
                    }
                }
            }
            
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            
            cell1.textLabel?.text = getLocationViewsArr[indexPath.row].LocationViewName
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_locationList
        {
            self.performSegue(withIdentifier: SEGUE_LOCATION_DETAILS, sender: listArr[indexPath.row])
        }
        else
        {
            LocationViewID = (getLocationViewsArr[indexPath.row].LocationViewID)!
            LocationViewName = (getLocationViewsArr[indexPath.row].LocationViewName)!
            flag = 0
            webserviceCallForLocationListView()
            viewDropdown.isHidden = true
            lblDropdown.text = "  " + LocationViewName + "  "
            
            if LocationViewID == "MyLocations" || LocationViewID == "AllLocations" || LocationViewID == "LocationsCreatedThisWeek"
            {
                btnFilter.isEnabled = true
            }
            else
            {
                btnFilter.isEnabled = false
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == table_locationList
        {
            
            return CGFloat(count! + 10)
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 40
            } else { //IPAD
                return 50
            }
        }
    }
    //MARK: IBAction
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNewLocation(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_CREATE_LOCATION, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func btnSorting(_ sender: Any) {
        
        if sortingFlag == 0
        {
            sortingFlag = 1
            listArr = listArr.sorted{($0["LocationNo"])! > ($1["LocationNo"])!}
        }
        else
        {
            sortingFlag = 0
            listArr = listArr.sorted{($1["LocationNo"])! > ($0["LocationNo"])!}
        }
        
        table_locationList.reloadData()
    }
    @IBAction func btnFilter(_ sender: Any) {
        
        let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationFilterViewController") as! LocationFilterViewController
        filterVC.delegate = self
        filterVC.LocationViewID = LocationViewID
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
