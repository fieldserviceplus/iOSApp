//
//  RecentLocationsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 21/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentLocationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    
    private let SEGUE_DETAILS = "segueDetails"
    private let SEGUE_NEW_LOCATION = "segueNewLocation"
    private let SEGUE_LOCATION_LIST = "segueLocationList"
    
    private let CELL_RECENT_LOCATION = "cell_recentLocation"
    
    var getViewsArr:[GetViewsLocationsData] = []
    var recentLocationsArr:[RecentLocationsData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown:UIButton!
    
    @IBOutlet weak var table_getViews: UITableView!
    @IBOutlet weak var table_recentLocation:UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForRecentLocation()
        webserviceCallForGetViews()
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        table_recentLocation.tableFooterView = UIView()
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            table_recentLocation.rowHeight = 226
        } else { //IPAD
            table_recentLocation.rowHeight = 273
        }
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    func  webserviceCallForRecentLocation()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentLocations(urlString: API.recentLocationsURL, parameters: parameters, headers: headers, vc: self) { (response:RecentLocations) in
            
            if response.Result == "True"
            {
                self.recentLocationsArr = response.data!
                self.table_recentLocation.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getViewsLocations(urlString: API.getViewsLocationsURL, parameters: parameters, headers: headers, vc: self) { (response:GetViewsLocations) in
            
            if response.Result == "True"
            {
                self.getViewsArr = response.data!
                self.table_getViews.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_LOCATION_LIST
        {
            let listVC = segue.destination as! RecentLocationListViewController

            let location = sender as? GetViewsLocationsData
            listVC.LocationViewID = location?.LocationViewID ?? ""
            listVC.LocationViewName = location?.LocationViewName ?? ""
        }
        else if segue.identifier == SEGUE_DETAILS
        {
            let detailVC = segue.destination as! LocationDetailsViewController
            
            let location = sender as? RecentLocationsData
            User.instance.locationID = location?.LocationID ?? ""
            detailVC.headerTitle = location?.Name ?? ""
        }
        
    }
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == table_recentLocation
        {
            return recentLocationsArr.count
        }
        return getViewsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == table_recentLocation
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_RECENT_LOCATION) as! RecentLocationsTableViewCell
            
            cell.lblLocNo.text = recentLocationsArr[indexPath.row].LC
            cell.lblAccountName.text = recentLocationsArr[indexPath.row].AccountName
            cell.lblName.text = recentLocationsArr[indexPath.row].Name
            cell.lblCity.text = recentLocationsArr[indexPath.row].City
            cell.lblState.text = recentLocationsArr[indexPath.row].State
            cell.lblLocType.text = recentLocationsArr[indexPath.row].LocationType
            cell.lblPrefTech.text = recentLocationsArr[indexPath.row].PreferredTechnicianName
            cell.lblOwnerName.text = recentLocationsArr[indexPath.row].OwnerName
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getViewsArr[indexPath.row].LocationViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_getViews {
            self.performSegue(withIdentifier: SEGUE_LOCATION_LIST, sender: getViewsArr[indexPath.row])
            
        }
        else if tableView == table_recentLocation
        {
            self.performSegue(withIdentifier: SEGUE_DETAILS, sender: recentLocationsArr[indexPath.row])
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func btnNewLocation(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_NEW_LOCATION, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
