//
//  LocationFilterViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 24/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
protocol sendingLocationDataDelegate {
    
    func sendData(array:[[String:String]])
}

class LocationFilterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: Variables
    
    var senderTag = 0
    var name:String?
    var isCancel = false
    var dropdown2 = ["Equals","Contains","StartsWith","DoesNotContain","NotEqualTo","LessThan","GreaterThan","LessOREqualTo","GreaterOREqualTo"]
    var locationFieldsArr:[String] = []
    var locationConditionsArr:[String] = []
    var picker = UIPickerView()
    var pickerInputField: UITextField!
    var locationValuesArr:[String] = []
    var cell:FilterTableViewCell?
    var cell1:FilterTableViewCell?
    var LocationViewID:String?
    var estimateFieldsArr:[GetEstimateViewFieldsData] = []
    var locationFilterArr:[[String:String]] = []
    var SortByValue:String?
    var indexOfWOField:Int?
    var indexOfDropdown2:Int?
    var delegate:sendingLocationDataDelegate?
    var textFieldIndexPath:IndexPath?
    var count = 0
    
    //MARK: IBOutlet
    
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var btnAscending: UIButton!
    @IBOutlet weak var btnDescending: UIButton!
    @IBOutlet weak var btnClearAll: UIButton!
    @IBOutlet weak var btnAddFilter: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            table.rowHeight = 110
        } else { //IPAD
            table.rowHeight = 171
        }
        webserviceCallForGetLocationViewFields()
        pickerKeyboard()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnSave.isEnabled = false
    }
    
    //MARK: Function
    
    func setupUI()
    {
        table.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "id")
        viewBackground.giveBorderToView()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnClearAll.giveCornerRadius()
        btnAddFilter.giveCornerRadius()
        view_back.dropShadow()
        btnAscending.contentHorizontalAlignment = .left
        btnDescending.contentHorizontalAlignment = .left
    }
    
    func  webserviceCallForGetLocationViewFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEstimateViewFields(urlString: API.getLocationViewFieldsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:GetEstimateViewFields) in
            
            if response.Result == "True"{
                self.estimateFieldsArr = response.data!
                self.table.reloadData()
            }else{
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForLocationFilter()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "LocationViewID":LocationViewID ?? "",
                          "SortByField":"LocationNo",
                          "SortByValue":SortByValue ?? "",
                          "FilterFields":locationFieldsArr,
                          "FilterConditions":locationConditionsArr,
                          "FilterValues":locationValuesArr] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.locationFilter(urlString: API.locationFilterURL, parameters: parameters, headers: headers, vc: self) { (response:LocationFilter) in
            
            if response.Result == "True"
            {
                self.locationFilterArr = response.data!
                self.delegate?.sendData(array: self.locationFilterArr)
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func pickerKeyboard()
    {
    
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        
        pickerInputField = {
            let field = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            self.view.addSubview(field)
            field.delegate = self
            return field
        }()
        pickerInputField.inputView = picker
    }
    
    @objc func btnDropdown1(sender:UIButton)
    {
        senderTag = 1
        picker.reloadAllComponents()
        
        pickerInputField.becomeFirstResponder()
        //Get Button cell position.
        let ButtonPosition = (sender as AnyObject).convert(CGPoint.zero, to: table)
        let indexPath = table.indexPathForRow(at: ButtonPosition)
        indexOfWOField = indexPath?.row
        if indexPath != nil {
            
            cell = table.cellForRow(at: indexPath!) as? FilterTableViewCell
            print("Cell indexPath: \(String(describing: indexPath?.row))")
        }
    }
    
    @objc func btnDropdown2(sender:UIButton)
    {
        senderTag = 2
        picker.reloadAllComponents()
        if pickerInputField.isFirstResponder {
            pickerInputField.resignFirstResponder()
        }
        
        pickerInputField.inputView = picker
        pickerInputField.becomeFirstResponder()
        //Get Button cell position.
        let ButtonPosition = (sender as AnyObject).convert(CGPoint.zero, to: table)
        let indexPath = table.indexPathForRow(at: ButtonPosition)
        indexOfDropdown2 = indexPath?.row
        if indexPath != nil {
            
            cell1 = table.cellForRow(at: indexPath!) as? FilterTableViewCell
            
            print("Cell indexPath: \(String(describing: indexPath?.row))")
        }
    }
    
    @objc func btnCancel(sender:UIButton)
    {
        isCancel = true
        if textFieldIndexPath != nil{
            let cell3 = table.cellForRow(at: textFieldIndexPath!) as! FilterTableViewCell
            locationValuesArr[cell3.txtTitle.tag] = cell3.txtTitle.text ?? ""
        }
        locationFieldsArr.remove(at: sender.tag)
        locationConditionsArr.remove(at: sender.tag)
        locationValuesArr.remove(at: sender.tag)
        count -= 1
        self.viewWillLayoutSubviews()
        self.table.reloadData()
        textFieldIndexPath = nil
    }
    
    //MARK: IBAction
    
    @IBAction func btnAddFilter(_ sender: Any) {
        
        isCancel = false
        for i in 0..<locationFieldsArr.count
        {
            if locationFieldsArr[i] == "--None--" {
                Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                return
            }
        }
        for i in 0..<locationConditionsArr.count
        {
            if locationConditionsArr[i] == "--None--" {
                Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                return
            }
        }
        
        count += 1
        btnSave.isEnabled = true
        locationFieldsArr.append("--None--")
        locationConditionsArr.append("--None--")
        locationValuesArr.append("")
        print(locationFieldsArr)
        print(locationConditionsArr)
        print(locationValuesArr)
        table.reloadData()
    }
    
    
    @IBAction func btnSave(_ sender: Any) {
        
        if count == 0{
            return
        }
        
        if count == 1{
            let firstIndex = IndexPath(row:0,section:0)
            let firstCell = table.cellForRow(at: firstIndex) as! FilterTableViewCell
            if firstCell.lblDropdown1.text == "--None--" || firstCell.lblDropdown2.text == "--None--"{
                Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                return
            }
        }
        
        let index = IndexPath(row:count-1,section:0)
        let cell = table.cellForRow(at: index) as! FilterTableViewCell
        
        if cell.lblDropdown1.text == "--None--" || cell.lblDropdown2.text == "--None--"{
            locationValuesArr.remove(at: count-1)
            locationFieldsArr.remove(at: count-1)
            locationConditionsArr.remove(at: count-1)
        }
        
        //locationValuesArr[cell.txtTitle.tag] = cell.txtTitle.text ?? ""
        if textFieldIndexPath != nil{
            let cell3 = table.cellForRow(at: textFieldIndexPath!) as! FilterTableViewCell
            locationValuesArr[cell3.txtTitle.tag] = cell3.txtTitle.text ?? ""
        }
        
        webserviceCallForLocationFilter()
        print(locationValuesArr)
        print(locationFieldsArr)
        print(locationConditionsArr)
    }
    
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnClearAll(_ sender: Any) {
        
        count = 0
        self.tableHeightConstraint?.constant = 10
        locationFieldsArr.removeAll()
        locationValuesArr.removeAll()
        locationConditionsArr.removeAll()
        table.reloadData()
    }
    
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "id") as! FilterTableViewCell
        
        cell.txtTitle.delegate = self
        cell.txtTitle.tag = indexPath.row
        cell.btnCancel.tag = indexPath.row
        cell.txtTitle.text = locationValuesArr[indexPath.row]
        cell.lblDropdown1.text = locationFieldsArr[indexPath.row]
        cell.lblDropdown2.text = locationConditionsArr[indexPath.row]
        
        cell.btnDropdown1.addTarget(self, action: #selector(btnDropdown1(sender:)), for: .touchUpInside)
        cell.btnDropdown2.addTarget(self, action: #selector(btnDropdown2(sender:)), for: .touchUpInside)
        cell.btnCancel.addTarget(self, action: #selector(btnCancel(sender:)), for: .touchUpInside)
        cell.lblDropdown1.giveBorderToLabel()
        cell.lblDropdown2.giveBorderToLabel()
        cell.btnDropdown1.giveBorderToButton()
        cell.btnDropdown2.giveBorderToButton()
        return cell
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text != ""{
            if isCancel{
                isCancel = false
            }else{
                if textField.tag < locationValuesArr.count{
                    locationValuesArr[textField.tag] = textField.text!
                }
            }
        }
        print(locationValuesArr)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let pointInTable = textField.convert(textField.bounds.origin, to: self.table)
        textFieldIndexPath = self.table.indexPathForRow(at: pointInTable)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        self.viewWillLayoutSubviews()
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        if count > 0{
            if self.table.contentSize.height <= self.view.frame.height*(0.50){
                self.tableHeightConstraint?.constant = self.table.contentSize.height
            }
            else{
                self.tableHeightConstraint?.constant = self.view.frame.height*(0.50)
            }
        }
        else{
            self.tableHeightConstraint?.constant = 10
        }
        
        //self.tableHeightConstraint?.constant = self.table.contentSize.height
    }
    
    //MARK: UIPickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if senderTag == 1{
            return estimateFieldsArr.count + 1
        }
        return dropdown2.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if senderTag == 1{
            return row == 0 ? "--None--":estimateFieldsArr[row-1].FieldName
        }
        return row == 0 ? "--None--":dropdown2[row-1]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if senderTag == 1{
            if row == 0{
                cell?.lblDropdown1.text = ""
            }else{
                name = estimateFieldsArr[row-1].FieldName
                cell?.lblDropdown1.text = name
                locationFieldsArr.remove(at: indexOfWOField!)
                locationFieldsArr.insert(cell?.lblDropdown1.text ?? "", at: indexOfWOField!)
            }
        }
        else{
            if row == 0{
                cell1?.lblDropdown2.text = ""
            }else{
                name = dropdown2[row-1]
                cell1?.lblDropdown2.text = name
                locationConditionsArr.remove(at: indexOfDropdown2!)
                locationConditionsArr.insert(cell1?.lblDropdown2.text ?? "", at: indexOfDropdown2!)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

