//
//  RecentTaskListViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 17/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentTaskListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, sendingTaskDataDelegate {
    
    //MARK: Variables
    private let CELL_LIST_TASKS = "cell_TaskList"
    
    private let SEGUE_CREATE_TASK = "segueNewTask"
    private let SEGUE_TASK_DETAILS = "segueDetails"
    
    var getTaskViewsArr:[TaskGetViewsData] = []
    var listArr:[[String:String]] = []
    var arrTag:Int?
    var TaskViewID = ""
    var TaskViewName = ""
    var count:Int?
    var sortingFlag = 0
    var flag = 0
    var lastContentOffset: CGFloat = 0
    var flagForDelegate = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var table_taskList: UITableView!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSorting: UIButton!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var btnMoreMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customViewWillAppear()
        
    }
    func customViewWillAppear() {
        if flagForDelegate == 0 {
            webserviceCallForTaskListView()
            webserviceCallForGetViews()
        } else {
            flagForDelegate = 0
        }
        
        lblDropdown.text = "  " + TaskViewName + "  "
        if TaskViewID == "MyOpenTasksToday" || TaskViewID == "MyOpenTasksThisWeek" || TaskViewID == "AllMyOpenTasks" || TaskViewID == "MyCompletedTasksThisWeek"
        {
            btnMoreMenu.isEnabled = false
            btnSorting.isEnabled = false
        }
        else
        {
            btnMoreMenu.isEnabled = true
            btnSorting.isEnabled = true
        }
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        table_taskList.tableFooterView = UIView()
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    
    func sendData(array: [[String : String]], flag: Int) {
        
        listArr = array
        flagForDelegate = flag
        self.table_taskList.reloadData()
    }
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.taskGetViews(urlString: API.taskGetViewsURL, parameters: parameters, headers: headers, vc: self) { (response:TaskGetViews
            ) in
            
            if response.Result == "True"
            {
                self.getTaskViewsArr = response.data!
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForTaskListView()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "TaskViewID":TaskViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.viewTaskList(urlString: API.viewTaskListURL, parameters: parameters, headers: headers, vc: self) { (response:ViewTaskList) in
            
            if response.Result == "True"
            {
                self.listArr = response.data as! [[String : String]]
                
                self.table_taskList.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    func  webserviceCallForDeleteCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"Task",
                          "ViewID":TaskViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForCopyCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"Task",
                          "ViewID":TaskViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.copyCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_TASK_DETAILS
        {
            let detailVC = segue.destination as! TaskDetailsViewController
            
            let task = sender as? [String:String]
            User.instance.taskID = task?["TaskID"] ?? ""
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == table_taskList
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
        
    }
    @objc func btnCreateView() {
        
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "Task"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    @objc func btnRenameView() {
        
        let renameViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RenameView_ViewController") as!  RenameView_ViewController
        renameViewVC.object = "Task"
        renameViewVC.ViewID = TaskViewID
        self.navigationController?.pushViewController(renameViewVC, animated: true)
    }
    @objc func btnEditSharing() {
        
        let editSharingVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditSharingViewController") as!  EditSharingViewController
        editSharingVC.object = "Task"
        editSharingVC.ViewID = TaskViewID
        self.navigationController?.pushViewController(editSharingVC, animated: true)
    }
    @objc func btnCopy() {
        
        webserviceCallForCopyCustomView()
    }
    @objc func btnDeleteRow() {
        
        webserviceCallForDeleteCustomView()
    }
    @objc func btnEditColumns() {
        
        let editColumnsVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DisplayedColumnsViewController") as!  DisplayedColumnsViewController
        editColumnsVC.flagSpecifyAction = "Edit"
        editColumnsVC.object = "Task"
        editColumnsVC.ViewID = TaskViewID
        self.navigationController?.pushViewController(editColumnsVC, animated: true)
    }
    @objc func btnEditFilter() {
        
        let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
        
        editFilterVC.object = "Task"
        editFilterVC.ViewID = TaskViewID
        self.navigationController?.pushViewController(editFilterVC, animated: true)
    }
    //MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table_taskList
        {
            return self.listArr.count
        }
        return getTaskViewsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table_taskList
        {
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LIST_TASKS) as! RecentTaskListTableViewCell
            
            
            for subview in cell.stackview.subviews {
                subview.removeFromSuperview()
            }
            
            cell.view_back.dropShadow()
            
            if TaskViewName == "My Open Tasks Today" || TaskViewName == "My Open Tasks This Week" || TaskViewName == "All My Open Tasks" || TaskViewName == "My Completed Tasks This Week"
            {
                let myArr = ["AssignedTo","Subject","ContactName","RelatedTo","DueDate","TaskStatus","TaskPriority","TaskType","CreatedDate","CreatedBy"]
                
                if listArr.count > 0
                {
                    
                    let dict = listArr[indexPath.row]
                    for j in 0..<myArr.count
                    {
                        let label = PaddingLabel()
                        label.textAlignment = .left
                        
                        label.text = myArr[j].camelCaseToWords() + ": " + (dict[myArr[j]] ?? "")
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            label.font = label.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            label.font = label.font.withSize(20)
                        }
                            
                        label.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(label)
                        
                    }
                }
            }
            else
            {
                for (key, value) in listArr[indexPath.row] {
                    // here use key and value
                    
                    print(key)
                    print(value)
                    
                    
                    if key == "Subject" || key == "RelatedTo" || key == "TaskType" || key == "DueDate" || key == "AssignedTo" || key == "ContactName"
                    {
                        
                        let label = PaddingLabel()
                        label.textAlignment = .left
                        
                        
                        label.text = key.camelCaseToWords()  + ": " + value
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            label.font = label.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            label.font = label.font.withSize(20)
                        }
                        
                        label.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(label)
                        
                    }
                }
            }
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            
            cell1.textLabel?.text = getTaskViewsArr[indexPath.row].TaskViewName
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_taskList
        {
            self.performSegue(withIdentifier: SEGUE_TASK_DETAILS, sender: listArr[indexPath.row])
        }
        else
        {
            TaskViewID = (getTaskViewsArr[indexPath.row].TaskViewID)!
            TaskViewName = (getTaskViewsArr[indexPath.row].TaskViewName)!
            flag = 0
            webserviceCallForTaskListView()
            viewDropdown.isHidden = true
            lblDropdown.text = "  " + TaskViewName + "  "
            
            if TaskViewID == "MyOpenTasksToday" || TaskViewID == "MyOpenTasksThisWeek" || TaskViewID == "AllMyOpenTasks" || TaskViewID == "MyCompletedTasksThisWeek"
            {
                btnMoreMenu.isEnabled = false
                btnSorting.isEnabled = false
            }
            else
            {
                btnMoreMenu.isEnabled = true
                btnSorting.isEnabled = true
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == table_taskList
        {
            
            return CGFloat(count! + 10)
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 40
            } else { //IPAD
                return 50
            }
        }
    }
    //MARK: IBAction
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNewTask(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_CREATE_TASK, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func btnSorting(_ sender: Any) {
        
        let sortVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SortViewController") as! SortViewController
        sortVC.viewID = TaskViewID
        sortVC.object = "Task"
        self.navigationController?.pushViewController(sortVC, animated: true)
    }
    @IBAction func btnFilter(_ sender: Any) {
       
        if TaskViewID != "MyOpenTasksToday" && TaskViewID != "MyOpenTasksThisWeek" && TaskViewID != "AllMyOpenTasks" && TaskViewID != "MyCompletedTasksThisWeek" {
            
            let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
            
            editFilterVC.object = "Task"
            editFilterVC.ViewID = TaskViewID
            self.navigationController?.pushViewController(editFilterVC, animated: true)
        }
        else{
            let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskFilterViewController") as! TaskFilterViewController
            filterVC.delegate = self
            filterVC.TaskViewID = TaskViewID
            self.navigationController?.pushViewController(filterVC, animated: true)
        }
        
    }
    @IBAction func btnOpenMenu(_ sender: Any) {
        
        if TaskViewID != "MyOpenTasksToday" && TaskViewID != "MyOpenTasksThisWeek" && TaskViewID != "AllMyOpenTasks" && TaskViewID != "MyCompletedTasksThisWeek" {
            if let customView = Bundle.main.loadNibNamed("CustomViewMenu", owner: self, options: nil)?.first as? CustomViewMenu {
                
                customView.tag = 101
                customView.btnCreateView.addTarget(self, action: #selector(btnCreateView), for: .touchUpInside)
                customView.btnRename.addTarget(self, action: #selector(btnRenameView), for: .touchUpInside)
                customView.btnEditSharing.addTarget(self, action: #selector(btnEditSharing), for: .touchUpInside)
                customView.btnCopy.addTarget(self, action: #selector(btnCopy), for: .touchUpInside)
                customView.btnDeleteRow.addTarget(self, action: #selector(btnDeleteRow), for: .touchUpInside)
                customView.btnEditColumns.addTarget(self, action: #selector(btnEditColumns), for: .touchUpInside)
                customView.btnEditFilter.addTarget(self, action: #selector(btnEditFilter), for: .touchUpInside)
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            }
        }
        
    }
    
    @IBAction func btnCreateNewView(_ sender: Any) {
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "Task"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.viewWithTag(101)?.removeFromSuperview()
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
