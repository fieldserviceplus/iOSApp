//
//  TaskDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 19/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MessageUI

class TaskDetailsViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIPickerViewDelegate,UIPickerViewDataSource, MFMessageComposeViewControllerDelegate, searchRelatedToDelegate, searchDelegate{
    
    
    //MARK: Variables
    private let SEGUE_RECURRING = "segue_recurring"
    private let SEGUE_LAYOUT = "segueDetailsLayout"
    
    var lastContentOffset: CGFloat = 0
    var textfieldTag = 0
    
    var relatedObj:String?
    var taskTypeID:String?
    var taskStatusID:String?
    var taskPriorityID:String?
    var contactID:String?
    var assignedToID:String?
    var relatedToID:String?
    var relatedObjListArr:[GetRelatedObjListData] = []
    var taskTypeArr:[GetTaskTypeData] = []
    var taskStatusArr:[GetTaskStatusData] = []
    var taskPriorityArr:[GetTaskPriorityData] = []
    var pickerView = UIPickerView()
    var menuView:ActionSheetTask?
    var responseTask_Details:TaskDetails?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtActivityDate: AkiraTextField!
    @IBOutlet weak var txtWho: UITextField!
    @IBOutlet weak var txtRelatedTo: UITextField!
    @IBOutlet weak var txtWhat: UITextField!
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPriority: UITextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    //UILabel
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var btnClose: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForCustomFields()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        
        webserviceCallForTaskDetails()
        webserviceCallForGetRelatedObjList()
        webserviceCallForGetTaskType()
        webserviceCallForGetTaskStatus()
        webserviceCallForGetTaskPriority()
        
        if let actionsheetView = Bundle.main.loadNibNamed("ActionSheetTask", owner: self, options: nil)?.first as? ActionSheetTask
        {
            menuView = actionsheetView

            actionsheetView.btnClose.addTarget(self, action: #selector(btnCancelMenu), for: .touchUpInside)
            actionsheetView.btnCall.addTarget(self, action: #selector(btnCall1), for: .touchUpInside)
            actionsheetView.btnText.addTarget(self, action: #selector(btnMessage1), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnCloneTask.addTarget(self, action: #selector(btnCloneTask), for: .touchUpInside)
            actionsheetView.btnDeleteTask.addTarget(self, action: #selector(btnDeleteTask), for: .touchUpInside)
            actionsheetView.btnEmail.addTarget(self, action: #selector(btnEmail), for: .touchUpInside)
            actionsheetView.btnMarkComplete.addTarget(self, action: #selector(btnMarkComplete), for: .touchUpInside)
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetView.bounds
            self.actionSheetView.addSubview(actionsheetView)
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        txtType.inputView = pickerView
        txtPriority.inputView = pickerView
        txtStatus.inputView = pickerView
        txtWhat.inputView = pickerView
        txtRelatedTo.inputView = pickerView
        txtviewDescription.giveBorder()
        
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtWho.setRightImage(name: "search_small", placeholder: "--None--")
        txtType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        txtPriority.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtWhat.setRightImage(name: "search_small", placeholder: "--None--")
        txtRelatedTo.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtWhatTapped))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        txtWhat.isUserInteractionEnabled = true
        txtWhat.addGestureRecognizer(tap)
        
    }
    func sendData(relatedToId: String, relatedToName: String) {
        
        relatedToID = relatedToId
        txtWhat.text = relatedToName
        
    }
    func sendData(searchVC: SearchAccountViewController) {
        
        if textfieldTag == 3 {
            txtWho.text = searchVC.contactName
            contactID = searchVC.contactID
            
        } else if textfieldTag == 8 {
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        }
    }
    @objc func txtWhatTapped()
    {
        if txtRelatedTo.text == "--None--" || txtRelatedTo.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.enterRelatedTo, vc: self)
            return
        }
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchRelatedToViewController") as! SearchRelatedToViewController
        searchVC.delegate = self
        searchVC.relatedTo = relatedObj
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @objc func btnMarkComplete()
    {
        self.actionSheetView.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
        webserviceCallForMarkCompleted()
    }
    @objc func btnEmail()
    {
        let emailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmailViewController") as! EmailViewController
        emailVC.strRelatedTo = "Task"
        emailVC.strWhat = User.instance.taskID
        self.navigationController?.pushViewController(emailVC, animated: true)
    }
    @objc func btnCloneTask()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
        vc.flagResponse = "1"
        vc.response = responseTask_Details
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @objc func btnDeleteTask()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Task",
                          "What":User.instance.taskID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    @objc func btnCancelMenu()
    {
        self.actionSheetView.isHidden = true
        self.btnCancel.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
        //self.view_backNewFile.isHidden = true
    }
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionSheetView.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
        
    }
    @objc func btnCall1()
    {
        if let number = self.txtPhone.text {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    @objc func btnMessage1()
    {
        if let number = self.txtPhone.text {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    func webserviceCallForMarkCompleted()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "TaskID":User.instance.taskID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.markATaskURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.webserviceCallForTaskDetails()
                let vc  = self.children[0] as! TaskDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetRelatedObjList()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getRelatedObjList(urlString: API.getRelatedObjListURL, parameters: parameters , headers: headers, vc: self) { (response:GetRelatedObjList) in
            
            if response.Result == "True"
            {
                
                self.relatedObjListArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    func webserviceCallForGetTaskType()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTaskType(urlString: API.getTaskTypeURL, parameters: parameters , headers: headers, vc: self) { (response:GetTaskType) in
            
            if response.Result == "True"
            {
                
                self.taskTypeArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetTaskStatus()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTaskStatus(urlString: API.getTaskStatusURL, parameters: parameters , headers: headers, vc: self) { (response:GetTaskStatus) in
            
            if response.Result == "True"
            {
                
                self.taskStatusArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetTaskPriority()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTaskPriority(urlString: API.getTaskPriorityURL, parameters: parameters , headers: headers, vc: self) { (response:GetTaskPriority) in
            
            if response.Result == "True"
            {
                
                self.taskPriorityArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForTaskDetails()
    {
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "TaskID":User.instance.taskID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.taskDetails(urlString: API.taskDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:TaskDetails) in
            
            if response.Result == "True"
            {
                self.responseTask_Details = response 
                self.txtSubject.text = response.data?.Subject
                self.txtActivityDate.text = response.data?.DueDate
                self.txtWho.text  = response.data?.ContactName
                self.txtRelatedTo.text = response.data?.RelatedToName
                self.txtWhat.text = response.data?.WhatName
                self.txtEmail.text = response.data?.Email
                self.txtPhone.text = response.data?.Phone
                self.txtviewDescription.text = response.data?.Description
                self.txtAssignedTo.text =  response.data?.AssignedToName
                self.txtType.text = response.data?.TaskType
                self.txtStatus.text = (response.data?.TaskStatus) ?? ""
                self.txtPriority.text = (response.data?.TaskPriority ?? "")
                self.txtLastModifiedBy.text = (response.data?.LastModifiedBy ?? "")
                self.txtLastModifiedDate.text = (response.data?.LastModifiedDate ?? "")
                self.txtCreatedBy.text = (response.data?.CreatedBy ?? "")
                self.txtCreatedDate.text = (response.data?.CreatedDate ?? "")
                
                self.contactID = (response.data?.Who ?? "")
                self.taskTypeID = (response.data?.TaskTypeID ?? "")
                self.taskStatusID = (response.data?.TaskStatusID ?? "")
                self.taskPriorityID = (response.data?.TaskPriorityID ?? "")
                self.assignedToID = (response.data?.AssignedTo ?? "")
                self.relatedToID = (response.data?.What ?? "")
                self.relatedObj = (response.data?.RelatedToName ?? "")
                
                self.lblStatus.text = "Status: " + ((response.data?.TaskStatus) ?? "")
                self.lblAssignedTo.text = "Assigned To: " + ((response.data?.AssignedToName) ?? "")
                self.lblHeaderTitle.text = response.data?.Subject
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForEditTask()
    {
        if contactID == nil || relatedToID == nil || assignedToID == nil || relatedObj == nil || taskTypeID == nil || taskStatusID == nil || taskPriorityID == nil || txtSubject.text == "" || txtActivityDate.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
       
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Who"] = contactID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["TaskStatus"] = taskStatusID ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        parameters["DueDate"] = txtActivityDate.text ?? ""
        parameters["Email"] = txtEmail.text ?? ""
        parameters["Phone"] = txtPhone.text ?? ""
        parameters["TaskType"] = taskTypeID ?? ""
        parameters["TaskPriority"] = taskPriorityID ?? ""
        parameters["TaskID"] = User.instance.taskID
        
        print(parameters)
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editTaskURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.webserviceCallForTaskDetails()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[0] as! TaskDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.taskID,
                          "Object":"Task",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                var arrayFieldValue:[String]?
                
                var viewFromConstrain:Any = self.txtPriority
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtPriority)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPriority, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPriority, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            //
                            customView.txtLabelField.text = response.data?[i].FieldValue
                            
                            //
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtLabelField.text ?? "")
                            
                            //
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            
                            if response.data?[i].IsRequired == "1" {
                                
                                if response.data?[i].FieldValue == "" {
                                    customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                    customView.txtviewLabelField.textColor = UIColor.lightGray
                                } else {
                                    customView.txtviewLabelField.text = response.data?[i].FieldValue
                                    
                                }
                                
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtviewLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtviewLabelField.text ?? "")
                            
                            //
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            //Edit
                            
                            if response.data?[i].FieldValue == "" {
                                self.checkboxValueArr.append("")
                            } else{
                                self.checkboxValueArr.append("\((response.data?[i].FieldValue ?? "")),")
                            }
                            //
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                if ((arrayFieldValue?.contains((arrayCheckboxValues[j])))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                
                //Edit
                if value == "," {
                    checkboxValueArr.insert("\(sender.accessibilityLabel!),", at: index)
                } else {
                    checkboxValueArr.insert(value + "\(sender.accessibilityLabel!),", at: index)
                }
                //
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: "\(sender.accessibilityLabel!),", with: "")
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
            }
            else
            {
                // didn't move
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            view_back.isHidden = true
            actionSheetView.isHidden = true
            btnCancel.isHidden = true
            //view_backNewFile.isHidden = true
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 4
        {
            return relatedObjListArr.count + 1
        }
        else if textfieldTag == 9
        {
            return taskTypeArr.count + 1
        }
        else if textfieldTag == 10
        {
            return taskStatusArr.count + 1
        }
        else if textfieldTag == 11
        {
            return taskPriorityArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 4
        {
            return row == 0 ? "--None--":relatedObjListArr[row-1].Name
        }
        else if textfieldTag == 9
        {
            return row == 0 ? "--None--":taskTypeArr[row-1].TaskType
        }
        else if textfieldTag == 10
        {
            return row == 0 ? "--None--":taskStatusArr[row-1].TaskStatus
        }
        else if textfieldTag == 11
        {
            return row == 0 ? "--None--":taskPriorityArr[row-1].Priority
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 4
        {
            if row == 0
            {
                txtRelatedTo.text = ""
            }
            else{
                txtRelatedTo.text = relatedObjListArr[row-1].Name
            }
            txtWhat.text = ""
        }
        else if textfieldTag == 9
        {
            if row == 0
            {
                txtType.text = ""
            }
            else{
                txtType.text = taskTypeArr[row-1].TaskType
                taskTypeID = taskTypeArr[row-1].TaskTypeID
            }
            
        }
        else if textfieldTag == 10
        {
            if row == 0
            {
                txtStatus.text = ""
            }
            else{
                txtStatus.text = taskStatusArr[row-1].TaskStatus
                taskStatusID = taskStatusArr[row-1].TaskStatusID
            }
            
        }
        else if textfieldTag == 11
        {
            if row == 0
            {
                txtPriority.text = ""
            }
            else{
                txtPriority.text = taskPriorityArr[row-1].Priority
                taskPriorityID = taskPriorityArr[row-1].TaskPriorityID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        //
        if textField == txtWho || textField == txtRelatedTo || textField == txtWhat || textField == txtAssignedTo || textField == txtType || textField == txtStatus || textField == txtPriority
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.tag == 1
        {
            txtActivityDate.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtWho.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtRelatedTo.becomeFirstResponder()
        }
        else if textField.tag == 4
        {
            txtWhat.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 8
        {
            txtType.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtStatus.becomeFirstResponder()
        }else if textField.tag == 10
        {
            txtPriority.becomeFirstResponder()
        }
        else if textField.tag == 11
        {
            txtPriority.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 9
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 11
        {
            textfieldTag = textField.tag
        }
        
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    //MARK: UITextview Delegate Methods
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description*"
        {
            textView.text = ""
        }
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description*"
        }
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtviewDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        if customFieldKeyArr.count != 0 {
            
            if lastTextfieldTapped != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                    customFieldValueArr.append(lastTextfieldTapped.text ?? "")
                }
            }
            
        }
        
        
        //CustomTextView
        if customFieldKeyArr.count != 0 {
            
            if lastTappedTextView != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                    customFieldValueArr.append(lastTappedTextView.text ?? "")
                }
            }
            
        }
        
        checkboxEditedValueArr.removeAll()
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropLast()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCallForEditTask()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
        
        txtviewDescription.isUserInteractionEnabled = true
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        btnCancel.isHidden = false
        btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        txtStatus.layer.borderColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    
    @IBAction func txtActivityDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        txtActivityDate.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func btnCall(_ sender: Any) {
        
        if let number = self.txtPhone.text {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    
    @IBAction func btnMessage(_ sender: Any) {
        if let number = self.txtPhone.text {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnMarkAComplete(_ sender: Any) {
        webserviceCallForMarkCompleted()
    }
    
    
    @IBAction func btnMore(_ sender: Any) {
        
        //self.btnCancel.isHidden = false
        self.actionSheetView.isHidden = false
        self.viewBottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
}

