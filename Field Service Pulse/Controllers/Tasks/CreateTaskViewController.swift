//
//  CreateTaskViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 19/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CreateTaskViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchRelatedToDelegate, searchDelegate {
    
    
    //MARK: Variables
    
    private let SEGUE_RECURRENCE = "segueRecurrence"
    var textfieldTag = 0
    
    var relatedObj:String?
    var taskTypeID:String?
    var taskStatusID:String?
    var taskPriorityID:String?
    var contactID:String?
    var assignedToID:String?
    var relatedToID:String?
    var relatedObjListArr:[GetRelatedObjListData] = []
    var taskTypeArr:[GetTaskTypeData] = []
    var taskStatusArr:[GetTaskStatusData] = []
    var taskPriorityArr:[GetTaskPriorityData] = []
    var pickerView = UIPickerView()
    
    var Ends:String?
    var EndsAfterOccurrences:String?
    var EndsOnDate:String?
    var EndTime:String?
    var IntervalEvery:String?
    var RepeatEvery:String?
    var RepeatOn:String?
    var StartTime:String?
    var StartOn:String?
    
     var recurring:String = "0"
    var flagResponse = "0"
    var response:TaskDetails?
    var accountName:String?
    var workOrderSubject:String?
    var estimateName:String?
    var contactName:String?
    var invoiceNo:String?
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtActivityDate: AkiraTextField!
    @IBOutlet weak var txtWho: UITextField!
    @IBOutlet weak var txtRelatedTo: UITextField!
    @IBOutlet weak var txtWhat: UITextField!
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPriority: UITextField!
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCheckbox: UIButton!
    
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetRelatedObjList()
        webserviceCallForGetTaskType()
        webserviceCallForGetTaskStatus()
        webserviceCallForGetTaskPriority()
        webserviceCallForCustomFields()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        pickerView.dataSource = self
        txtType.inputView = pickerView
        txtPriority.inputView = pickerView
        txtStatus.inputView = pickerView
        txtRelatedTo.inputView = pickerView
        txtviewDescription.giveBorder()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtWho.setRightImage(name: "search_small", placeholder: "--None--")
        txtType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        txtPriority.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtWhat.setRightImage(name: "search_small", placeholder: "--None--")
        txtRelatedTo.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        if flagResponse == "1" {
            responseTaskDetails()
            flagResponse = "0"
        }
        if accountName != nil {
            self.txtRelatedTo.text = "Account"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = accountName
        }
        if workOrderSubject != nil {
            self.txtRelatedTo.text = "WorkOrder"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = workOrderSubject
        }
        if estimateName != nil {
            self.txtRelatedTo.text = "Estimate"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = estimateName
        }
        if contactName != nil {
            self.txtRelatedTo.text = "Contact"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = contactName
        }
        if invoiceNo != nil {
            self.txtRelatedTo.text = "Invoice"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = invoiceNo
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtWhatTapped))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        txtWhat.addGestureRecognizer(tap)
        
    }
    
    func sendData(relatedToId: String, relatedToName: String) {
      
        relatedToID = relatedToId
        txtWhat.text = relatedToName
        
    }
    
    func sendData(searchVC: SearchAccountViewController) {
        
        if textfieldTag == 3 {
            txtWho.text = searchVC.contactName
            contactID = searchVC.contactID
            
        } else if textfieldTag == 8 {
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        }
    }
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @objc func txtWhatTapped()
    {
        if txtRelatedTo.text == "--None--" || txtRelatedTo.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.enterRelatedTo, vc: self)
            return
        }
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchRelatedToViewController") as! SearchRelatedToViewController
        searchVC.delegate = self
        searchVC.relatedTo = relatedObj
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    
    func webserviceCallForGetRelatedObjList()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getRelatedObjList(urlString: API.getRelatedObjListURL, parameters: parameters , headers: headers, vc: self) { (response:GetRelatedObjList) in
            
            if response.Result == "True"
            {
                
                self.relatedObjListArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetTaskType()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTaskType(urlString: API.getTaskTypeURL, parameters: parameters , headers: headers, vc: self) { (response:GetTaskType) in
            
            if response.Result == "True"
            {
                
                self.taskTypeArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetTaskStatus()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTaskStatus(urlString: API.getTaskStatusURL, parameters: parameters , headers: headers, vc: self) { (response:GetTaskStatus) in
            
            if response.Result == "True"
            {
                
                self.taskStatusArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetTaskPriority()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTaskPriority(urlString: API.getTaskPriorityURL, parameters: parameters , headers: headers, vc: self) { (response:GetTaskPriority) in
            
            if response.Result == "True"
            {
                
                self.taskPriorityArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCall()
    {
        if contactID == nil || relatedToID == nil || assignedToID == nil || relatedObj == nil || taskTypeID == nil || taskStatusID == nil || taskPriorityID == nil || txtSubject.text == "" || txtActivityDate.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["TaskType"] = taskTypeID ?? ""
        parameters["TaskPriority"] = taskPriorityID ?? ""
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Who"] = contactID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["TaskStatus"] = taskStatusID ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        parameters["DueDate"] = txtActivityDate.text ?? ""
        parameters["Email"] = txtEmail.text ?? ""
        parameters["Phone"] = txtPhone.text ?? ""
        parameters["IsRecurring"] = recurring
        parameters["StartOn"] = StartOn ?? ""
        parameters["RepeatEvery"] = RepeatEvery ?? ""
        parameters["IntervalEvery"] = IntervalEvery ?? ""
        parameters["RepeatOn"] = RepeatOn ?? ""
        parameters["Ends"] = Ends ?? ""
        parameters["EndsOnDate"] = EndsOnDate ?? ""
        parameters["EndsAfterOccurrences"] = EndsAfterOccurrences ?? ""
        parameters["StartTime"] = StartTime ?? ""
        parameters["EndTime"] = EndTime ?? ""
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createTask(urlString: API.createTaskURL, parameters: parameters , headers: headers, vc: self) { (response:CreateTask) in
            
            if response.Result == "True"
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                User.instance.taskID = String(response.TaskID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func responseTaskDetails()
    {
        self.txtSubject.text = response?.data?.Subject
                self.txtActivityDate.text = response?.data?.DueDate
                self.txtWho.text  = response?.data?.ContactName
        self.txtRelatedTo.text = response?.data?.RelatedToName
        self.txtWhat.text = response?.data?.WhatName
        self.txtEmail.text = response?.data?.Email
        self.txtPhone.text = response?.data?.Phone
        self.txtviewDescription.text = response?.data?.Description
        self.txtAssignedTo.text =  response?.data?.AssignedToName
        self.txtType.text = response?.data?.TaskType
        self.txtStatus.text = (response?.data?.TaskStatus) ?? ""
        self.txtPriority.text = (response?.data?.TaskPriority ?? "")
        
        self.contactID = (response?.data?.Who ?? "")
        self.taskTypeID = (response?.data?.TaskTypeID ?? "")
        self.taskStatusID = (response?.data?.TaskStatusID ?? "")
        self.taskPriorityID = (response?.data?.TaskPriorityID ?? "")
        self.assignedToID = (response?.data?.AssignedTo ?? "")
        self.relatedToID = (response?.data?.What ?? "")
        self.relatedObj = (response?.data?.RelatedToName ?? "")
        self.recurring = (response?.data?.IsRecurrence ?? "")
    }
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        if let RecurrenceDetailsVC = sender.source as? RecurrenceSettingsViewController {
            
            Ends = RecurrenceDetailsVC.ends ?? ""
            IntervalEvery = RecurrenceDetailsVC.txtInterval.text ?? ""
            RepeatEvery = RecurrenceDetailsVC.txtRepeats.text ?? ""
            EndsOnDate = RecurrenceDetailsVC.txtOn.text ?? ""
            EndsAfterOccurrences = RecurrenceDetailsVC.txtAfter.text ?? ""
            StartTime = RecurrenceDetailsVC.txtStartTime.text ?? ""
            EndTime = RecurrenceDetailsVC.txtEndTime.text ?? ""
            RepeatOn = RecurrenceDetailsVC.weekArr.joined(separator: ",")
            StartOn = RecurrenceDetailsVC.txtStartsOn.text ?? ""
        }
    }
    
    
    @IBAction func txtDueDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        txtActivityDate.text = dateFormatter.string(from: sender.date)
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "Object":"Task",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                
                var viewFromConstrain:Any = self.btnCheckbox
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.btnCheckbox)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPriority, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPriority, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                customView.txtviewLabelField.textColor = UIColor.lightGray
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            self.checkboxValueArr.append("")
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.btnSave, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(value + "," + sender.accessibilityLabel!, at: index)
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: ",\(sender.accessibilityLabel!)", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxKey:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 4
        {
            return relatedObjListArr.count + 1
        }
        else if textfieldTag == 9
        {
            return taskTypeArr.count + 1
        }
        else if textfieldTag == 10
        {
            return taskStatusArr.count + 1
        }
        else if textfieldTag == 11
        {
            return taskPriorityArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 4
        {
            return row == 0 ? "--None--":relatedObjListArr[row-1].Name
        }
        else if textfieldTag == 9
        {
            return row == 0 ? "--None--":taskTypeArr[row-1].TaskType
        }
        else if textfieldTag == 10
        {
            return row == 0 ? "--None--":taskStatusArr[row-1].TaskStatus
        }
        else if textfieldTag == 11
        {
            return row == 0 ? "--None--":taskPriorityArr[row-1].Priority
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if textfieldTag == 4
        {
            if row == 0
            {
                txtRelatedTo.text = ""
            }
            else{
                txtRelatedTo.text = relatedObjListArr[row-1].Name
                relatedObj = txtRelatedTo.text
            }
            txtWhat.text = ""
        }
        else if textfieldTag == 9
        {
            if row == 0
            {
                txtType.text = ""
            }
            else{
                txtType.text = taskTypeArr[row-1].TaskType
                taskTypeID = taskTypeArr[row-1].TaskTypeID
            }
            
        }
        else if textfieldTag == 10
        {
            if row == 0
            {
                txtStatus.text = ""
            }
            else{
                txtStatus.text = taskStatusArr[row-1].TaskStatus
                taskStatusID = taskStatusArr[row-1].TaskStatusID
            }
            
        }
        else if textfieldTag == 11
        {
            if row == 0
            {
                txtPriority.text = ""
            }
            else{
                txtPriority.text = taskPriorityArr[row-1].Priority
                taskPriorityID = taskPriorityArr[row-1].TaskPriorityID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        
        //
        if textField == txtWho || textField == txtRelatedTo || textField == txtWhat || textField == txtAssignedTo || textField == txtType || textField == txtStatus || textField == txtPriority
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtActivityDate.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtWho.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtRelatedTo.becomeFirstResponder()
        }
        else if textField.tag == 4
        {
            txtWhat.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 8
        {
            txtType.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtStatus.becomeFirstResponder()
        }else if textField.tag == 10
        {
            txtPriority.becomeFirstResponder()
        }
        else if textField.tag == 11
        {
            txtPriority.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 9
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 11
        {
            textfieldTag = textField.tag
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    var lastTextfieldTapped:UITextField!
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    //MARK: UITextview Delegate Methods
    var lastTappedTextView:UITextView!
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description*"
        {
            textView.text = ""
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description*"
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtviewDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    //MARK: IBActions
    
    @IBAction func btnCheckbox(_ sender: Any) {
        
        if (btnCheckbox.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnCheckbox.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            recurring = "0"
        }
        else{
            btnCheckbox.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            self.performSegue(withIdentifier: SEGUE_RECURRENCE, sender: self)
            recurring = "1"
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        
        if lastTextfieldTapped != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                customFieldValueArr.append(lastTextfieldTapped.text ?? "")
            }
        }
        
        
        
        //CustomTextView
        
        if lastTappedTextView != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                customFieldValueArr.append(lastTappedTextView.text ?? "")
            }
        }
        
        
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropFirst()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCall()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
