//
//  TaskDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 25/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TaskDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var contactID:String?
    var assignedToID:String?
    var relatedToID:String?
    var RelatedToName:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblWho: UILabel!
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var lblWhat: UILabel!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnRecurring: UIButton!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblLastDate: UILabel!
    @IBOutlet weak var lblLastBy: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewLast: UIView!
    @IBOutlet weak var lblSystemInfo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        webserviceCall()
        webserviceCallForCustomFields()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "TaskID":User.instance.taskID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.taskDetails(urlString: API.taskDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:TaskDetails) in
            
            if response.Result == "True"
            {
                self.lblSubject.text = response.data?.Subject
                self.lblDate.text = response.data?.DueDate
                self.lblWho.text  = response.data?.ContactName
                self.lblRelatedTo.text = (response.data?.RelatedTo ?? "")
                self.lblWhat.text = response.data?.WhatName
                self.lblEmail.text = response.data?.Email
                self.lblPhone.text = response.data?.Phone
                self.txtviewDescription.text = response.data?.Description
                self.lblAssignedTo.text =  response.data?.AssignedToName
                self.lblType.text = response.data?.TaskType
                self.lblStatus.text = (response.data?.TaskStatus) ?? ""
                self.lblPriority.text = (response.data?.TaskPriority ?? "")
                self.lblLastBy.text = (response.data?.LastModifiedBy ?? "")
                self.lblLastDate.text = (response.data?.LastModifiedDate ?? "")
                self.lblCreatedBy.text = (response.data?.CreatedBy ?? "")
                self.lblCreatedDate.text = (response.data?.CreatedDate ?? "")
                
                if response.data?.IsRecurrence == "0"
                {
                    self.btnRecurring.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
                    
                }
                else{
                    self.btnRecurring.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                
                self.contactID = response.data?.Who
                self.assignedToID = response.data?.AssignedTo
                self.relatedToID = response.data?.What
                self.RelatedToName = response.data?.RelatedToName
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
        
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.taskID,
                          "Object":"Task",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayOptionValues:[String]?
                var arrayFieldValue:[String]?
                
                var viewFromConstrain = self.viewLast
                for i in 0..<(response.data?.count)!  {
                    print(response.data?[i].FieldType)
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 5
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 85
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 70
                        case "LongText":
                            constantHeight = 110
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.viewLast)
                    }
                    
                    let myView = UIView()
                    
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    print(constant)
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewLast, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewLast, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        if let customView = Bundle.main.loadNibNamed("DefaultSizeView", owner: self, options: nil)?.first as? DefaultSizeView {
                            
                            customView.lblFieldLabel.text = response.data?[i].FieldLabel
                            customView.lblFieldValue.text = response.data?[i].FieldValue
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextSizeView", owner: self, options: nil)?.first as? LongTextSizeView {
                            
                            customView.txtview.text = response.data?[i].FieldValue
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            for i in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                
                                if ((arrayFieldValue?.contains((arrayOptionValues?[i])!))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![i]
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? TaskDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? TaskDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = true
                    parent.actionSheetView.isHidden = true
                    parent.btnClose.isHidden = true// Here you hide it when animation done
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? TaskDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? TaskDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = false // Here you hide it when animation done
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if self.contactID != nil && self.contactID != "" {
                let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                User.instance.contactID = contactID!
                User.instance.contactName = self.lblWho.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        } else if sender.tag == 2 {
            if self.relatedToID != nil && self.relatedToID != "" {
                
                if RelatedToName == "Account" {
                    
                    let acVC = UIStoryboard(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController")  as! AccountDetailsViewController
                    User.instance.accountID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Contact" {
                    
                    let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                    User.instance.contactID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "WorkOrder" {
                    
                    let acVC = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController")  as! WorkOrdersDetailsViewController
                    User.instance.workorderID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Estimate" {
                    
                    let acVC = UIStoryboard(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController")  as! EstimateDetailsViewController
                    User.instance.estimateID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Invoice" {
                    
                    let acVC = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController")  as! InvoiceDetailsViewController
                    User.instance.invoiceID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                
            }
            
        } else if sender.tag == 3 {
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
