//
//  SearchRelatedToViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 03/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol searchRelatedToDelegate {
    
    func sendData(relatedToId:String, relatedToName:String)
}
class SearchRelatedToViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: Variables
    var relatedToListArr:[GetRelatedToListData] = []
    var filteredArray:[GetRelatedToListData] = []
    var isSeaching = false
    var relatedToId = ""
    var relatedToName = ""
    var delegate:searchRelatedToDelegate!
    var relatedTo:String?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webserviceCallForGetRelatedToList()
        self.table.tableFooterView = UIView()
    }
    
    //MARK: Function
    
    func webserviceCallForGetRelatedToList()
    {
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":relatedTo ?? ""] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getRelatedToList(urlString: API.getRelatedToListURL, parameters: parameters , headers: headers, vc: self) { (response:GetRelatedToList) in
            
            if response.Result == "True"
            {
                self.relatedToListArr = response.data!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSeaching
        {
            return filteredArray.count
        }
        return relatedToListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "id")
        if isSeaching
        {
            cell.textLabel?.text = filteredArray[indexPath.row].Name
        }
        else
        {
            cell.textLabel?.text = relatedToListArr[indexPath.row].Name
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSeaching
        {
            relatedToId = filteredArray[indexPath.row].ID!
            relatedToName = filteredArray[indexPath.row].Name!
            
        }
        else
        {
            relatedToId = relatedToListArr[indexPath.row].ID!
            relatedToName = relatedToListArr[indexPath.row].Name!
        }
        
        self.navigationController?.popViewController(animated: true)
        delegate.sendData(relatedToId: relatedToId, relatedToName: relatedToName)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredArray = relatedToListArr.filter({ (text) -> Bool in
            let tmp: NSString = text.Name! as NSString
            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(filteredArray.count == 0){
            isSeaching = false;
        } else {
            isSeaching = true;
        }
        self.table.reloadData()
    }
    
    
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
