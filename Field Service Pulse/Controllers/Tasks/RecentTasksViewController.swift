//
//  RecentTasksViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 17/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentTasksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    
    private let SEGUE_DETAILS = "segueDetails"
    private let SEGUE_NEW_TASK = "segueNewTask"
    private let SEGUE_TASK_LIST = "segueTaskList"
    
    private let CELL_RECENT_TASK = "cell_recentTask"
    
    var getViewsArr:[TaskGetViewsData] = []
    var recentTasksArr:[RecentTasksData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown:UIButton!
    
    @IBOutlet weak var table_getViews: UITableView!
    @IBOutlet weak var table_recentTask:UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForRecentTasks()
        webserviceCallForGetViews()
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        table_recentTask.tableFooterView = UIView()
        
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    func  webserviceCallForRecentTasks()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentTasks(urlString: API.recentTasksURL, parameters: parameters, headers: headers, vc: self) { (response:RecentTasks) in
            
            if response.Result == "True"
            {
                self.recentTasksArr = response.data!
                self.table_recentTask.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.taskGetViews(urlString: API.taskGetViewsURL, parameters: parameters, headers: headers, vc: self) { (response:TaskGetViews
            ) in
            
            if response.Result == "True"
            {
                
                self.getViewsArr = response.data!
                self.table_getViews.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_TASK_LIST
        {
            let listVC = segue.destination as! RecentTaskListViewController
            
            let task = sender as? TaskGetViewsData
            listVC.TaskViewID = task?.TaskViewID ?? ""
            listVC.TaskViewName = task?.TaskViewName ?? ""
            
        }
        else if segue.identifier == SEGUE_DETAILS
        {
            let detailVC = segue.destination as! TaskDetailsViewController
            
            let task = sender as? RecentTasksData
            User.instance.taskID = task?.TaskID ?? ""
        }
        
    }
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == table_recentTask
        {
            return recentTasksArr.count
        }
        return getViewsArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == table_recentTask
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_RECENT_TASK) as! RecentTasksTableViewCell
            
            cell.lblContactName.text = recentTasksArr[indexPath.row].ContactName
            cell.lblRelatedTo.text = recentTasksArr[indexPath.row].RelatedTo
            cell.lblSubject.text = recentTasksArr[indexPath.row].Subject
            cell.lblTaskType.text = recentTasksArr[indexPath.row].TaskType
            cell.lblTaskStatus.text = recentTasksArr[indexPath.row].TaskStatus
            cell.lblTaskPriority.text = recentTasksArr[indexPath.row].TaskPriority
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getViewsArr[indexPath.row].TaskViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_getViews
        {
            self.performSegue(withIdentifier: SEGUE_TASK_LIST, sender: getViewsArr[indexPath.row])
            
        }
        else if tableView == table_recentTask
        {
            self.performSegue(withIdentifier: SEGUE_DETAILS, sender: recentTasksArr[indexPath.row])
        }
        
    }
    
    
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func btnNewTask(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_NEW_TASK, sender: self)
    }
    
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
