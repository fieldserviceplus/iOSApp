//
//  ActionSheetTask.swift
//  Field Service Pulse
//
//  Created by Apple on 12/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ActionSheetTask: UIView {

    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnText: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnMarkComplete: UIButton!
    @IBOutlet weak var btnCloneTask: UIButton!
    @IBOutlet weak var btnDeleteTask: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var heightBackView: NSLayoutConstraint!
    @IBOutlet weak var btnEmail: UIButton!
    
    
    
    override func awakeFromNib() {
        self.btnCall.leftImage(image: #imageLiteral(resourceName: "phone-1"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnText.leftImage(image: #imageLiteral(resourceName: "message"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEdit.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnMarkComplete.leftImage(image: #imageLiteral(resourceName: "check-box_black"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnCloneTask.leftImage(image: #imageLiteral(resourceName: "copy"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnDeleteTask.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEmail.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: UIImage.RenderingMode.alwaysOriginal)
    }
    
}
