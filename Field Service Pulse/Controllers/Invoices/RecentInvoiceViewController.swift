//
//  RecentInvoiceViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 23/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentInvoiceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    
    private let SEGUE_DETAILS = "segueDetails"
    private let SEGUE_NEW_INVOICE = "segueNewInvoice"
    private let SEGUE_INVOICE_LIST = "segueInvoiceList"
    
    private let CELL_RECENT_INVOICE = "cell_recentInvoice"
    
    var getViewsArr:[InvoiceGetViewsData] = []
    var recentInvoicesArr:[RecentInvoicesData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown:UIButton!
    
    @IBOutlet weak var table_getViews: UITableView!
    @IBOutlet weak var table_recentInvoices:UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForRecentInvoices()
        webserviceCallForGetViews()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        table_recentInvoices.tableFooterView = UIView()
        
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    
    func  webserviceCallForRecentInvoices()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentInvoices(urlString: API.recentInvoicesURL, parameters: parameters, headers: headers, vc: self) { (response:RecentInvoices) in
            
            if response.Result == "True"
            {
                self.recentInvoicesArr = response.data!
                self.table_recentInvoices.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoiceViews(urlString: API.getViewsInvoiceURL, parameters: parameters, headers: headers, vc: self) { (response:InvoiceGetViews
            ) in
            
            if response.Result == "True"
            {
                
                self.getViewsArr = response.data!
                self.table_getViews.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_INVOICE_LIST
        {
            let listVC = segue.destination as! InvoiceListViewController
            
            let invoice = sender as? InvoiceGetViewsData
            listVC.InvoiceViewID = invoice?.InvoiceViewID ?? ""
            listVC.InvoiceViewName = invoice?.InvoiceViewName ?? ""
            
        }
        else if segue.identifier == SEGUE_DETAILS
        {
            let detailVC = segue.destination as! InvoiceDetailsViewController
            
            let invoice = sender as? RecentInvoicesData
            User.instance.invoiceID = invoice?.InvoiceID ?? ""
            User.instance.invoiceNo = invoice?.IN ?? ""
        }
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == table_recentInvoices
        {
            return recentInvoicesArr.count
        }
        return getViewsArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == table_recentInvoices
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_RECENT_INVOICE) as! RecentInvoiceTableViewCell
            
            cell.lblInvoiceNo.text = recentInvoicesArr[indexPath.row].IN
            cell.lblStatus.text = recentInvoicesArr[indexPath.row].InvoiceStatus
            cell.lblAssignedTo.text = recentInvoicesArr[indexPath.row].AssignedToName
            cell.lblAccountName.text = recentInvoicesArr[indexPath.row].AccountName
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getViewsArr[indexPath.row].InvoiceViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_getViews
        {
            self.performSegue(withIdentifier: SEGUE_INVOICE_LIST, sender: getViewsArr[indexPath.row])
            
        }
        else if tableView == table_recentInvoices
        {
            self.performSegue(withIdentifier: SEGUE_DETAILS, sender: recentInvoicesArr[indexPath.row])
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func btnNewEstimate(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_NEW_INVOICE, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
