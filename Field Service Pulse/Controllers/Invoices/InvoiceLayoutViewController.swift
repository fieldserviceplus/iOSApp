//
//  InvoiceLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 25/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class InvoiceLayoutViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var accountID:String?
    var assignedToID:String?
    var woID:String?
    var contactID:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblWorkOwner: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var lblInvoiceDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblPaymentTerms: UILabel!
    @IBOutlet weak var lblAddInfo: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblLineCountItems: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblLastModifiedDate: UILabel!
    @IBOutlet weak var lblLastModifiedBy: UILabel!
    @IBOutlet weak var imgviewSignature: UIImageView!
    @IBOutlet weak var lblInvoiceNo: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    @IBOutlet weak var viewLast: UIView!
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        webserviceCall()
        webserviceCallForCustomFields()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "InvoiceID":User.instance.invoiceID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.invoiceDetails(urlString: API.invoiceDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:InvoiceDetails) in
            
            if response.Result == "True"
            {
                self.lblInvoiceNo.text = response.data?.InvoiceNo
                self.lblStatus.text  = response.data?.InvoiceStatus
                self.lblAssignedTo.text = response.data?.AssignedToName
                self.lblAccount.text = response.data?.AccountName
                User.instance.accountNameForProductUI = response.data?.AccountName ?? ""
                self.lblWorkOwner.text = response.data?.WorkOrderSubject
                self.lblContact.text = response.data?.ContactName
                self.txtviewDescription.text = response.data?.Description
                self.lblAddress.text = (response.data?.Address) ?? ""
                self.lblCity.text = (response.data?.City)! + ", " + (response.data?.State)! + ", " + (response.data?.PostalCode)!
                self.lblCountry.text = (response.data?.Country)!
                self.lblPaymentTerms.text = response.data?.PaymentTerms
                self.lblDueDate.text = response.data?.DueDate
                self.lblAddInfo.text = response.data?.AdditionalInformation
                if response.data?.InvoiceDate?.range(of: "00/00/0000") != nil{
                    self.lblInvoiceDate.text = ""
                }else{
                    self.lblInvoiceDate.text = response.data?.InvoiceDate
                }
                self.lblTotalPrice.text = (response.data?.TotalPrice)?.convertToCurrencyFormat() ?? "$0.00"
                self.lblTax.text = (response.data?.Tax)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblSubTotal.text = (response.data?.SubTotal)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblLineCountItems.text = response.data?.LineItemCount
                self.lblGrandTotal.text = (response.data?.GrandTotal)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblDiscount.text = response.data?.Discount

                
                self.lblLastModifiedDate.text = response.data?.LastModifiedDate
                self.lblLastModifiedBy.text = response.data?.LastModifiedBy
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblCreatedBy.text = response.data?.CreatedBy
                
                self.accountID = response.data?.Account
                self.assignedToID = response.data?.AssignedTo
                self.woID = response.data?.WorkOrder
                self.contactID = response.data?.Contact
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.invoiceID,
                          "Object":"Invoice",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayOptionValues:[String]?
                var arrayFieldValue:[String]?
                
                var viewFromConstrain = self.viewLast
                for i in 0..<(response.data?.count)!  {
                    print(response.data?[i].FieldType)
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 5
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 85
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 70
                        case "LongText":
                            constantHeight = 110
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.viewLast)
                    }
                    
                    let myView = UIView()
                    
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    print(constant)
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewLast, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewLast, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        if let customView = Bundle.main.loadNibNamed("DefaultSizeView", owner: self, options: nil)?.first as? DefaultSizeView {
                            
                            customView.lblFieldLabel.text = response.data?[i].FieldLabel
                            customView.lblFieldValue.text = response.data?[i].FieldValue
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextSizeView", owner: self, options: nil)?.first as? LongTextSizeView {
                            
                            customView.txtview.text = response.data?[i].FieldValue
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            for i in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                
                                if ((arrayFieldValue?.contains((arrayOptionValues?[i])!))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![i]
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? InvoiceDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? InvoiceDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = true
                    parent.actionSheetView.isHidden = true
                    parent.btnClose.isHidden = true// Here you hide it when animation done
                }
            })
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? InvoiceDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? InvoiceDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = false // Here you hide it when animation done
                }
            })
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
            
        } else if sender.tag == 2 {
            
            if self.accountID != nil && self.accountID != "" {
                let acVC = UIStoryboard(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController")  as! AccountDetailsViewController
                User.instance.accountID = accountID!
                User.instance.accountName = self.lblAccount.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        } else if sender.tag == 3 {
            if self.woID != nil && self.woID != "" {
                let acVC = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController")  as! WorkOrdersDetailsViewController
                User.instance.workorderID = woID!
                User.instance.workorderSubject = self.lblWorkOwner.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        } else if sender.tag == 4 {
            if self.contactID != nil && self.contactID != "" {
                let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                User.instance.contactID = contactID!
                User.instance.contactName = self.lblContact.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
