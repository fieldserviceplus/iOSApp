//
//  InvoiceListViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 24/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class InvoiceListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, sendingInvoiceDataDelegate {
    
    //MARK: Variables
    private let CELL_LIST_INVOICES = "cell_InvoiceList"
    
    private let SEGUE_CREATE_INVOICE = "segueNewInvoice"
    private let SEGUE_INVOICE_DETAILS = "segueDetails"
    
    var getInvoiceViewsArr:[InvoiceGetViewsData]? = []
    var listArr:[[String:String]] = []
    var arrTag:Int?
    
    var InvoiceViewID = ""
    var InvoiceViewName = ""
    var city:String?
    var state:String?
    var count:Int?
    var sortingFlag = 0
    var flag = 0
    var lastContentOffset: CGFloat = 0
    var addressLabel:PaddingLabel?
    var flagForDelegate = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var table_InvoiceList: UITableView!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSorting: UIButton!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var btnMoreMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        customViewWillAppear()
    }
     func customViewWillAppear() {
        if flagForDelegate == 0 {
            webserviceCallForInvoiceListView()
            webserviceCallForGetViews()
        } else {
            flagForDelegate = 0
        }
        
        lblDropdown.text = "  " + InvoiceViewName + "  "
        if InvoiceViewID == "MyOpenInvoices" || InvoiceViewID == "AllOpenInvoices" || InvoiceViewID == "AllOpenInvoicesPastDue" || InvoiceViewID == "InvoicesCreatedThisWeek"
        {
            btnMoreMenu.isEnabled = false
            btnSorting.isEnabled = false
        }
        else
        {
            btnMoreMenu.isEnabled = true
            btnSorting.isEnabled = true
        }
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        table_InvoiceList.tableFooterView = UIView()
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    
    func sendData(array: [[String : String]], flag: Int) {
        
        listArr = array
        flagForDelegate = flag
        self.table_InvoiceList.reloadData()
    }
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoiceViews(urlString: API.getViewsInvoiceURL, parameters: parameters, headers: headers, vc: self) { (response:InvoiceGetViews
            ) in
            
            if response.Result == "True"
            {
                
                self.getInvoiceViewsArr = response.data!
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForInvoiceListView()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "InvoiceViewID":InvoiceViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.viewInvoiceList(urlString: API.viewInvoiceListURL, parameters: parameters, headers: headers, vc: self) { (response:ViewInvoiceList) in
            
            if response.Result == "True"
            {
                self.listArr = response.data as! [[String : String]]
            
                self.table_InvoiceList.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForDeleteCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"Invoice",
                          "ViewID":InvoiceViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForCopyCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"Invoice",
                          "ViewID":InvoiceViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.copyCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_INVOICE_DETAILS
        {
            let detailVC = segue.destination as! InvoiceDetailsViewController
            
            let invoice = sender as? [String:String]
            User.instance.invoiceID = invoice?["InvoiceID"] ?? ""
            User.instance.invoiceNo = invoice?["InvoiceNo"] ?? ""
        }
        
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == table_InvoiceList
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
        
    }
    
    
    @objc func btnCreateView() {
        
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "Invoice"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    @objc func btnRenameView() {
        
        let renameViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RenameView_ViewController") as!  RenameView_ViewController
        renameViewVC.object = "Invoice"
        renameViewVC.ViewID = InvoiceViewID
        self.navigationController?.pushViewController(renameViewVC, animated: true)
    }
    @objc func btnEditSharing() {
        
        let editSharingVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditSharingViewController") as!  EditSharingViewController
        editSharingVC.object = "Invoice"
        editSharingVC.ViewID = InvoiceViewID
        self.navigationController?.pushViewController(editSharingVC, animated: true)
    }
    @objc func btnCopy() {
        
        webserviceCallForCopyCustomView()
    }
    @objc func btnDeleteRow() {
        
        webserviceCallForDeleteCustomView()
    }
    @objc func btnEditColumns() {
        
        let editColumnsVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DisplayedColumnsViewController") as!  DisplayedColumnsViewController
        editColumnsVC.flagSpecifyAction = "Edit"
        editColumnsVC.object = "Invoice"
        editColumnsVC.ViewID = InvoiceViewID
        self.navigationController?.pushViewController(editColumnsVC, animated: true)
    }
    @objc func btnEditFilter() {
        
        let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
        
        editFilterVC.object = "Invoice"
        editFilterVC.ViewID = InvoiceViewID
        self.navigationController?.pushViewController(editFilterVC, animated: true)
    }
    //MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table_InvoiceList
        {
            return self.listArr.count
        }
        return getInvoiceViewsArr!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table_InvoiceList
        {
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LIST_INVOICES) as! InvoiceListTableViewCell
            
            for subview in cell.stackview.subviews {
                subview.removeFromSuperview()
            }
            
            cell.view_back.dropShadow()
            
            if InvoiceViewName == "My Open Invoices" || InvoiceViewName == "All Open Invoices" || InvoiceViewName == "All Open Invoices Past Due" || InvoiceViewName == "Invoices Created This Week"
            {
                let myArr = ["AssignedTo","InvoiceNo","AccountName","ContactName","InvoiceStatus","City","State"]
                
                if listArr.count > 0
                {
                    
                    let dict = listArr[indexPath.row]
                    for j in 0..<myArr.count
                    {
                        
                        if myArr[j] == "City"
                        {
                            city = dict[myArr[j]]
                        }
                        else if myArr[j] == "State"
                        {
                            state = dict[myArr[j]]
                            
                            let label = PaddingLabel()
                            label.textAlignment = .left
                            
                            let address = (city ?? "")  + "," + (state ?? "")
                            label.text = " Address" + ": " + address
                            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                                count = count! + 20
                                label.font = label.font.withSize(15)
                            } else { //IPAD
                                count = count! + 25
                                label.font = label.font.withSize(20)
                            }
                            
                            label.textColor = UIColor.darkGray
                            cell.stackview.addArrangedSubview(label)
                        }
                        else
                        {
                            let label = PaddingLabel()
                            label.textAlignment = .left
                            
                            
                            label.text = myArr[j].camelCaseToWords()  + ": " + (dict[myArr[j]] ?? "")
                            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                                count = count! + 20
                                label.font = label.font.withSize(15)
                            } else { //IPAD
                                count = count! + 25
                                label.font = label.font.withSize(20)
                            }
                            
                            label.textColor = UIColor.darkGray
                            cell.stackview.addArrangedSubview(label)
                        }
                        
                    }
                    
                    
                }
            }
            else
            {
                for (key, value) in listArr[indexPath.row] {
                    // here use key and value
                    
                    print(key)
                    print(value)
                    
                    
                    if key == "InvoiceNo" || key == "WorkOrder" || key == "Account" || key == "Contact" || key == "AssignedTo" || key == "InvoiceStatus" || key == "InvoiceDate" || key == "DueDate" || key == "PaymentTerms" || key == "AdditionalInformation"
                    {
                        
                        let label = PaddingLabel()
                        label.textAlignment = .left
                        
                        
                        label.text = key.camelCaseToWords()  + ": " + value
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            label.font = label.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            label.font = label.font.withSize(20)
                        }
                        
                        label.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(label)
                        
                    }
                    
                    if key == "City"
                    {
                        if addressLabel?.text != nil
                        {
                            addressLabel?.text = ""
                            addressLabel?.text = " Address: " + value + ", " + (state ?? "")
                        }
                        else
                        {
                            city = value
                        }
                        
                        
                    }
                    
                    if key == "State"
                    {
                        state = value
                        addressLabel = PaddingLabel()
                        addressLabel?.textAlignment = .left
                        addressLabel?.text = " Address: " + (city ?? "") + ", " + value
                        
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            addressLabel?.font = addressLabel?.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            addressLabel?.font = addressLabel?.font.withSize(20)
                        }
                        addressLabel?.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(addressLabel!)
                        
                    }
                    
                }
            }
            
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            
            cell1.textLabel?.text = getInvoiceViewsArr?[indexPath.row].InvoiceViewName
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_InvoiceList
        {
            self.performSegue(withIdentifier: SEGUE_INVOICE_DETAILS, sender: listArr[indexPath.row])
        }
        else
        {
            InvoiceViewID = (getInvoiceViewsArr?[indexPath.row].InvoiceViewID)!
            InvoiceViewName = (getInvoiceViewsArr?[indexPath.row].InvoiceViewName)!
            flag = 0
            webserviceCallForInvoiceListView()
            viewDropdown.isHidden = true
            lblDropdown.text = "  " + InvoiceViewName + "  "
            
            if InvoiceViewID == "MyOpenInvoices" || InvoiceViewID == "AllOpenInvoices" || InvoiceViewID == "AllOpenInvoicesPastDue" || InvoiceViewID == "InvoicesCreatedThisWeek"
            {
                btnMoreMenu.isEnabled = false
                btnSorting.isEnabled = false
            }
            else
            {
                btnMoreMenu.isEnabled = true
                btnSorting.isEnabled = true
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == table_InvoiceList
        {
            
            return CGFloat(count! + 10)
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 40
            } else { //IPAD
                return 50
            }
        }
    }
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNewInvoice(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_CREATE_INVOICE, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func btnSorting(_ sender: Any) {
        
        let sortVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SortViewController") as! SortViewController
        sortVC.viewID = InvoiceViewID
        sortVC.object = "Invoice"
        self.navigationController?.pushViewController(sortVC, animated: true)
    }
    @IBAction func btnFilter(_ sender: Any) {
        
        if InvoiceViewID != "MyOpenInvoices" && InvoiceViewID != "AllOpenInvoices" && InvoiceViewID != "AllOpenInvoicesPastDue" && InvoiceViewID != "InvoicesCreatedThisWeek" {
            let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
            
            editFilterVC.object = "Invoice"
            editFilterVC.ViewID = InvoiceViewID
            self.navigationController?.pushViewController(editFilterVC, animated: true)
        }
        else {
            let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceFilterViewController") as! InvoiceFilterViewController
            filterVC.delegate = self
            filterVC.InvoiceViewID = InvoiceViewID
            self.navigationController?.pushViewController(filterVC, animated: true)
        }
        
    }
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        
        if InvoiceViewID != "MyOpenInvoices" && InvoiceViewID != "AllOpenInvoices" && InvoiceViewID != "AllOpenInvoicesPastDue" && InvoiceViewID != "InvoicesCreatedThisWeek" {
            if let customView = Bundle.main.loadNibNamed("CustomViewMenu", owner: self, options: nil)?.first as? CustomViewMenu {
                
                customView.tag = 101
                customView.btnCreateView.addTarget(self, action: #selector(btnCreateView), for: .touchUpInside)
                customView.btnRename.addTarget(self, action: #selector(btnRenameView), for: .touchUpInside)
                customView.btnEditSharing.addTarget(self, action: #selector(btnEditSharing), for: .touchUpInside)
                customView.btnCopy.addTarget(self, action: #selector(btnCopy), for: .touchUpInside)
                customView.btnDeleteRow.addTarget(self, action: #selector(btnDeleteRow), for: .touchUpInside)
                customView.btnEditColumns.addTarget(self, action: #selector(btnEditColumns), for: .touchUpInside)
                customView.btnEditFilter.addTarget(self, action: #selector(btnEditFilter), for: .touchUpInside)
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            }
        }
        
    }
    
    @IBAction func btnCreateNewView(_ sender: Any) {
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "Invoice"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.viewWithTag(101)?.removeFromSuperview()
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
