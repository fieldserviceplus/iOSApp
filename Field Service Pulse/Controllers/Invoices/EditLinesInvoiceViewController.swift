//
//  EditLinesInvoiceViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EditLinesInvoiceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    //MARK: Variables
    private let CELL_ID = "cell_editLines"
    var productArr:[String] = []
    var quantityArr:[String] = []
    var listPriceArr:[String] = []
    var taxableArr:[String] = []
    var taxArr:[String] = []
    var listPriceEditableArr:[String] = []
    var quantityEditableArr:[String] = []
    var productIDArr:[String] = []
    
    var discountArr:[String] = []
    var unitPriceArr:[String] = []
    var subTotalArr:[String] = []
    var netTotalArr:[String] = []
    
    var checkboxArr:[String] = []
    
    var editedCell:EditLinesInvoiceTableViewCell!
    var index:Int!
    var listPrice:Double?
    var discount:Double?
    var unitPrice:Double?
    var subTotal:Double?
    var netTotal:Double?
    var quantity:Double?
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var grandTotal = 0.0
    
    var finalProductArr:[String] = []
    var finalListPriceArr:[String] = []
    var finalQuantityArr:[String] = []
    var finalDiscountArr:[String] = []
    var finalUnitPriceArr:[String] = []
    var finalTaxableArr:[String] = []
    var finalSubTotalArr:[String] = []
    var finalNetTotalArr:[String] = []
    var finalProductIDArr:[String] = []
    
    var bunchArr :[[String]] = []
    
    //MARK: IBOutlet
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnQuickSave: UIButton!
    @IBOutlet weak var btnAddProducts: UIButton!
    @IBOutlet weak var btnDeleteLines: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("listPriceArr:\(listPriceArr)")
        print("discountArr:\(discountArr)")
        print("quantityArr:\(quantityArr)")
        print("taxableArr:\(taxableArr)")
        print("taxArr:\(taxArr)")
        print("listPriceEditableArr:\(listPriceEditableArr)")
        print("quantityEditableArr:\(quantityEditableArr)")
        print("productIDArr:\(productIDArr)")
        print("productArr:\(productArr)")
        
        for _ in 0..<listPriceArr.count {
            checkboxArr.append("0")
        }
        setupUI()
        
    }
    
    
    //MARK: Function
    func setupUI()
    {
        if listPriceArr.count == 0 {
            
            btnSave.isEnabled = false
            btnQuickSave.isEnabled = false
            btnDeleteLines.isEnabled = false
        } else {
            btnSave.isEnabled = true
            btnQuickSave.isEnabled = true
            btnDeleteLines.isEnabled = true
        }
        btnSave.contentHorizontalAlignment = .left
        btnQuickSave.contentHorizontalAlignment = .left
        btnAddProducts.contentHorizontalAlignment = .left
        btnDeleteLines.contentHorizontalAlignment = .left
        btnCancel.contentHorizontalAlignment = .left
        btnDropdown.giveBorderToButton()
        lblDropdown.giveBorderToLabel()
        view_back.dropShadow()
        
        if User.instance.flagAddInvoiceProducts == "1"
        {
            if User.instance.flagSkipInvoiceProduct == "1"
            {
                discountArr = User.instance.discountArr5
                User.instance.flagSkipInvoiceProduct = "0"
            }
            else
            {
                if discountArr.count == 0
                {
                    for i in 0..<productArr.count
                    {
                        discountArr.insert("0.0", at: i)
                    }
                }
            }
            
            
            
            for i in 0..<productArr.count
            {
                //discountArr.insert("0.0", at: i)
                
                listPrice = Double(listPriceArr[i] ?? "0.0")!
                discount = Double(discountArr[i] ?? "0.0")!
                
                if String(discount!) != "0.0" && String(discount!) != ""
                {
                    unitPrice = listPrice! - (listPrice!*discount!)/100
                }
                else{
                    unitPrice = listPrice
                }
                
                
                quantity = Double(quantityArr[i] ?? "1")
                
                subTotal = unitPrice!*quantity!
                
                if taxableArr[i] == "1"
                {
                    netTotal = (subTotal! + Double(taxArr[i] ?? "0")!)
                    
                }
                else{
                    netTotal = subTotal!
                }
                
                unitPriceArr.insert(String(unitPrice!), at: i)
                subTotalArr.insert(String(subTotal!), at: i)
                netTotalArr.insert(String(netTotal!), at: i)
                
                self.table.reloadData()
                
            }
        }
        else if User.instance.flagAddInvoiceProducts == "0"
        {
            var indexArr:[Int] = []
            
            
            for i in 0..<User.instance.productIDArr4.count
            {
                
                if productIDArr.contains(User.instance.productIDArr4[i]) {
                    print("yes")
                }
                else
                {
                    indexArr.append(i)
                    
                }
            }
            
            User.instance.productIDArr4.remove(at: indexArr)
            User.instance.listPriceArr4.remove(at: indexArr)
            User.instance.quantityArr4.remove(at: indexArr)
            User.instance.taxableArr4.remove(at: indexArr)
            User.instance.taxArr4.remove(at: indexArr)
            User.instance.listPriceEditableArr4.remove(at: indexArr)
            User.instance.quantityEditableArr4.remove(at: indexArr)
            User.instance.productArr4.remove(at: indexArr)
            User.instance.discountArr4.remove(at: indexArr)
            
            
            indexArr.removeAll()
            
            
            print(User.instance.productIDArr4)
            print(User.instance.listPriceArr4)
            print(User.instance.quantityArr4)
            print(User.instance.taxableArr4)
            print(User.instance.taxArr4)
            print(User.instance.listPriceEditableArr4)
            print(User.instance.quantityEditableArr4)
            print(User.instance.productArr4)
            print(User.instance.discountArr4)
            
            
            for i in 0..<User.instance.productIDArr4.count
            {
                let index = productIDArr.index(of: User.instance.productIDArr4[i])
                productIDArr.remove(at: index!)
                listPriceArr.remove(at: index!)
                quantityArr.remove(at: index!)
                taxableArr.remove(at: index!)
                taxArr.remove(at: index!)
                listPriceEditableArr.remove(at: index!)
                quantityEditableArr.remove(at: index!)
                productArr.remove(at: index!)
                
            }
            
            for i in 0..<productArr.count
            {
                discountArr.insert("0.0", at: i)
            }
            
            listPriceArr  = User.instance.listPriceArr4 + listPriceArr
            
            quantityArr = User.instance.quantityArr4 + quantityArr
            taxableArr = User.instance.taxableArr4 + taxableArr
            taxArr = User.instance.taxArr4 + taxArr
            listPriceEditableArr = User.instance.listPriceEditableArr4 + listPriceEditableArr
            quantityEditableArr = User.instance.quantityEditableArr4 + quantityEditableArr
            productIDArr = User.instance.productIDArr4 + productIDArr
            productArr = User.instance.productArr4 + productArr
            
            
            discountArr =  User.instance.discountArr4 + discountArr
            
            
            for i in 0..<productArr.count
            {
                //discountArr.insert("0.0", at: i)
                
                listPrice = Double(listPriceArr[i] ?? "0.0")!
                discount = Double(discountArr[i] ?? "0.0")!
                
                if String(discount!) != "0.0" && String(discount!) != ""
                {
                    unitPrice = listPrice! - (listPrice!*discount!)/100
                }
                else{
                    unitPrice = listPrice
                }
                
                
                quantity = Double(quantityArr[i] ?? "1")
                
                subTotal = unitPrice!*quantity!
                
                if taxableArr[i] == "1"
                {
                    netTotal = (subTotal! + Double(taxArr[i] ?? "0")!)
                    
                }
                else{
                    netTotal = subTotal!
                }
                
                unitPriceArr.insert(String(unitPrice!), at: i)
                subTotalArr.insert(String(subTotal!), at: i)
                netTotalArr.insert(String(netTotal!), at: i)
                
                self.table.reloadData()
                
            }
        }
        
    }
    
    @objc func btnSelectProductCheckbox(sender:UIButton)
    {
        editedCell = sender.superview?.superview as! EditLinesInvoiceTableViewCell
        let indexPath = table.indexPath(for: editedCell)
        
        if (editedCell.btnSelectCheckbox.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            editedCell.btnSelectCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            
            let indexOfA = finalProductArr.index(of:productArr[(indexPath?.row)!])
            finalProductArr.remove(at: indexOfA!)
            finalListPriceArr.remove(at: indexOfA!)
            finalQuantityArr.remove(at: indexOfA!)
            finalDiscountArr.remove(at: indexOfA!)
            finalUnitPriceArr.remove(at: indexOfA!)
            finalTaxableArr.remove(at: indexOfA!)
            finalSubTotalArr.remove(at: indexOfA!)
            finalNetTotalArr.remove(at: indexOfA!)
            finalProductIDArr.remove(at: indexOfA!)
        }
        else{
            editedCell.btnSelectCheckbox.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            
            finalProductArr.append(productArr[(indexPath?.row)!])
            finalListPriceArr.append(listPriceArr[(indexPath?.row)!])
            finalQuantityArr.append(quantityArr[(indexPath?.row)!])
            finalDiscountArr.append(discountArr[(indexPath?.row)!])
            finalUnitPriceArr.append(unitPriceArr[(indexPath?.row)!])
            finalTaxableArr.append(taxableArr[(indexPath?.row)!])
            finalSubTotalArr.append(subTotalArr[(indexPath?.row)!])
            finalNetTotalArr.append(netTotalArr[(indexPath?.row)!])
            finalProductIDArr.append(productIDArr[(indexPath?.row)!])
        }
        
    }
    
    @objc func btnCheckbox(sender:UIButton)
    {
        editedCell = sender.superview?.superview as! EditLinesInvoiceTableViewCell
        let indexPath = table.indexPath(for: editedCell)
        index = (indexPath?.row)!
        
        if (editedCell.btnTaxable.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            
            editedCell.btnTaxable.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            
            subTotal = Double((editedCell.lblSubTotal.text ?? "0.0").convertFromCurrencyFormat())
            netTotal = Double((editedCell.lblNetTotal.text ?? "0.0").convertFromCurrencyFormat())
            
            netTotal = (netTotal! - Double(taxArr[index] ?? "0.0")!)
            
            netTotalArr.remove(at: index)
            netTotalArr.insert(String(netTotal!), at: index)
            
            taxableArr.remove(at: index)
            taxableArr.insert("0", at: index)
            
        }
        else{
            editedCell.btnTaxable.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            subTotal = Double((editedCell.lblSubTotal.text ?? "0.0").convertFromCurrencyFormat())
            
            netTotal = (subTotal! + Double(taxArr[index] ?? "0.0")!)
            
            netTotalArr.remove(at: index)
            netTotalArr.insert(String(netTotal!), at: index)
            
            taxableArr.remove(at: index)
            taxableArr.insert("1", at: index)
        }
        grandTotal = 0.0
        self.table.reloadData()
    }
    
    func webserviceCallForSaveInvoiceLineItems()
    {
        //        if finalProductIDArr.count == 0
        //        {
        //            Helper.instance.showAlertNotification(message: Message.selectProduct, vc: self)
        //            return
        //        }
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "InvoiceID": User.instance.invoiceID,
                          "Product": productIDArr,
                          "ListPrice": listPriceArr,
                          "Discount": discountArr,
                          "UnitPrice": unitPriceArr,
                          "Quantity": quantityArr,
                          "SubTotal": subTotalArr,
                          "Taxable": taxableArr,
                          "TotalPrice": netTotalArr] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.saveInvoiceLineItemsURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return productArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! EditLinesInvoiceTableViewCell
        cell.btnSelectCheckbox.addTarget(self, action: #selector(btnSelectProductCheckbox(sender:)), for: .touchUpInside)
        cell.btnTaxable.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
        cell.lblProductName?.text = productArr[indexPath.row]
        cell.txtListPrice.text = listPriceArr[indexPath.row].convertToCurrencyFormat()
        cell.txtDiscount.text = discountArr[indexPath.row]
        cell.lblUnitPrice.text = unitPriceArr[indexPath.row].convertToCurrencyFormat()
        cell.txtQuantity.text = quantityArr[indexPath.row]
        cell.lblSubTotal.text = subTotalArr[indexPath.row].convertToCurrencyFormat()
        cell.lblNetTotal.text = netTotalArr[indexPath.row].convertToCurrencyFormat()
        if taxableArr[indexPath.row] == "1"
        {
            cell.btnTaxable.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
        }
        else{
            cell.btnTaxable.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
        
        
        if listPriceEditableArr[indexPath.row] == "1"
        {
            cell.txtListPrice.isEnabled = true
            cell.txtListPrice.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else{
            cell.txtListPrice.isEnabled = false
            cell.txtListPrice.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        if quantityEditableArr[indexPath.row] == "1"
        {
            cell.txtQuantity.isEnabled = true
            cell.txtQuantity.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else{
            cell.txtQuantity.isEnabled = false
            cell.txtQuantity.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        if checkboxArr[indexPath.row] == "0" {
            cell.btnSelectCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
        if(selectedArray.contains(indexPath))
        {
            // use selected image
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        }
        else
        {
            // use normal image
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
            //cell.btnAdd.isHidden = true
        }
        cell.viewBackground?.dropShadow()
        grandTotal = grandTotal + Double(netTotalArr[indexPath.row])!
        lblGrandTotal.text = String(grandTotal).convertToCurrencyFormat()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        grandTotal = 0.0
        let cell = tableView.cellForRow(at: indexPath) as! EditLinesInvoiceTableViewCell
        cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(!selectedArray.contains(indexPath))
        {
            selectedArray.removeAll()
            selectedArray.append(indexPath)
        }
        else
        {
            selectedArray = selectedArray.filter{$0 != indexPath}
            // remove from array here if required
        }
        
        if indexPath.row == selectedIndex{
            selectedIndex = -1
            
            
        }else{
            selectedIndex = indexPath.row
            
        }
        self.table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == selectedIndex
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 210
            } else { //IPAD
                return 250
            }
            
        }else{
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 82
            } else { //IPAD
                return 100
            }
        }
    }
    
    //MARK: UITexifield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        editedCell = textField.superview?.superview as! EditLinesInvoiceTableViewCell
        let indexPath = table.indexPath(for: editedCell)
        index = (indexPath?.row)!
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        listPrice = Double((editedCell.txtListPrice.text ?? "0.0").convertFromCurrencyFormat())
        discount = Double(editedCell.txtDiscount.text ?? "0.0")
        
        if discount == nil
        {
            discount = 0.0
        }
        
        if String(discount!) != "0.0" && String(discount!) != ""
        {
            unitPrice = listPrice! - (listPrice!*discount!)/100
        }
        else{
            unitPrice = listPrice
        }
        
        quantity = Double(editedCell.txtQuantity.text ?? "1.0")
        
        subTotal = unitPrice!*(quantity ?? 1.0)!
        
        if (editedCell.btnTaxable.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            netTotal = (subTotal! + Double(taxArr[index] ?? "0")!)
            
        }
        else{
            netTotal = subTotal!
        }
        
        listPriceArr.remove(at: index)
        discountArr.remove(at: index)
        unitPriceArr.remove(at: index)
        quantityArr.remove(at: index)
        subTotalArr.remove(at: index)
        netTotalArr.remove(at: index)
        
        listPriceArr.insert(String(listPrice!), at: index)
        discountArr.insert(String(discount!), at: index)
        unitPriceArr.insert(String(unitPrice!), at: index)
        quantityArr.insert(String(quantity ?? 1.0), at: index)
        subTotalArr.insert(String(subTotal!), at: index)
        netTotalArr.insert(String(netTotal!), at: index)
        
        self.table.reloadData()
    }
    
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if view_back.isHidden == true{
            view_back.isHidden = false
        }
        else{
            view_back.isHidden = true
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        self.view_back.isHidden = true
        print("listPriceArr:\(listPriceArr)")
        print("discountArr:\(discountArr)")
        print("quantityArr:\(quantityArr)")
        print("taxableArr:\(taxableArr)")
        print("taxArr:\(taxArr)")
        print("listPriceEditableArr:\(listPriceEditableArr)")
        print("quantityEditableArr:\(quantityEditableArr)")
        print("productIDArr:\(productIDArr)")
        print("productArr:\(productArr)")
        webserviceCallForSaveInvoiceLineItems()
        
        self.performSegue(withIdentifier: "segueInvoiceDetails", sender: self)
//        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
//        detailVC.flag = 1
//        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @IBAction func btnQuickSave(_ sender: Any) {
        self.view_back.isHidden = true
        webserviceCallForSaveInvoiceLineItems()
    }
    
    @IBAction func btnAddProducts(_ sender: Any) {
        
        print(listPriceArr)
        print(discountArr)
        print(quantityArr)
        print(taxableArr)
        print(taxArr)
        print(listPriceEditableArr)
        print(quantityEditableArr)
        print(productIDArr)
        print(productArr)
        
        User.instance.productIDArr4 = productIDArr
        User.instance.discountArr4 = discountArr
        User.instance.quantityArr4 = quantityArr
        User.instance.taxableArr4 = taxableArr
        User.instance.taxArr4 = taxArr
        User.instance.listPriceEditableArr4 = listPriceEditableArr
        User.instance.quantityEditableArr4 = quantityEditableArr
        User.instance.productArr4 = productArr
        User.instance.listPriceArr4 = listPriceArr
        
        User.instance.flagAddInvoiceProducts = "0"
        
        self.view_back.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDeleteLines(_ sender: Any) {
        
        self.view_back.isHidden = true
        for i in 0..<finalProductArr.count
        {
            let indexOfA = productArr.index(of:finalProductArr[i])
            productArr.remove(at: indexOfA!)
            listPriceArr.remove(at: indexOfA!)
            quantityArr.remove(at: indexOfA!)
            discountArr.remove(at: indexOfA!)
            unitPriceArr.remove(at: indexOfA!)
            taxableArr.remove(at: indexOfA!)
            subTotalArr.remove(at: indexOfA!)
            netTotalArr.remove(at: indexOfA!)
            productIDArr.remove(at: indexOfA!)
            listPriceEditableArr.remove(at: indexOfA!)
            quantityEditableArr.remove(at: indexOfA!)
            taxArr.remove(at: indexOfA!)
            
            checkboxArr.remove(at: indexOfA!)
        }
        print(productArr)
        print(listPriceArr)
        print(taxableArr)
        grandTotal = 0.0
        self.table.reloadData()
        
        finalProductArr.removeAll()
        if productArr.count == 0 {
            performSegue(withIdentifier: "segueInvoiceDetails", sender: self)
        }
        
        webserviceCallForSaveInvoiceLineItems()
        
        bunchArr.removeAll()
        bunchArr.append(productArr)
        bunchArr.append(quantityArr)
        bunchArr.append(listPriceArr)
        bunchArr.append(taxableArr)
        bunchArr.append(taxArr)
        bunchArr.append(quantityEditableArr)
        bunchArr.append(listPriceEditableArr)
        bunchArr.append(productIDArr)
        
        NotificationCenter.default.post(name: NSNotification.Name("sendDataEditLinesInvoice"), object: bunchArr, userInfo: nil)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.view_back.isHidden = true
        let detailVC1 = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
        detailVC1.flag = 1
        self.navigationController?.pushViewController(detailVC1, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
