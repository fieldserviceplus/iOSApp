//
//  InvoiceDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 24/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces
import Fusuma
import MessageUI

class InvoiceDetailsViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchDelegate, MFMessageComposeViewControllerDelegate {
    
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var latAddress:String?
    var longAddress:String?
    var textfieldTag = 0
    
    var accountID:String?
    var invoiceStatusID:String?
    var InvoicePaymentTermID:String?
    var contactID:String?
    var assignedToID:String?
    var WOID:String?
    var invoiceStatusArr:[GetInvoiceStatusData] = []
    var paymentTermsArr:[GetInvoicePaymentTermsData] = []
    var pickerView = UIPickerView()
    var menuView:ActionSheetInvoiceView?
    var flag = 0
    var responseInvoice_Details:InvoiceDetails?
    var strPhoneNo:String?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtInvoiceStatus: UITextField!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtWO: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtInvoiceDate: AkiraTextField!
    @IBOutlet weak var txtPaymentTerms: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtAddInfo: AkiraTextField!
    @IBOutlet weak var txtDueDate: AkiraTextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: AkiraTextField!
    @IBOutlet weak var txtState: AkiraTextField!
    @IBOutlet weak var txtCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    @IBOutlet weak var txtSubTotal: AkiraTextField!
    @IBOutlet weak var txtDiscount: AkiraTextField!
    @IBOutlet weak var txtTax: AkiraTextField!
    @IBOutlet weak var txtTotalPrice: AkiraTextField!
    @IBOutlet weak var txtGrandTotal: AkiraTextField!
    @IBOutlet weak var txtLineCountItems: AkiraTextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    @IBOutlet weak var txtInvoiceNo: AkiraTextField!
    
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnRelated: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    //UILabel
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var heightConstraintActionSheet: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
        webserviceCallForGetInvoiceStatus()
        webserviceCallForInvoiceDetails()
        webserviceCallForGetPaymentTerms()
        webserviceCallForCustomFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if flag == 1
        {
            let vc = self.children[0] as! InvoiceRelatedViewController
            vc.viewWillAppear(true)
            
            flag = 0
        }
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        
        if let actionsheetView = Bundle.main.loadNibNamed("ActionSheetInvoice", owner: self, options: nil)?.first as? ActionSheetInvoiceView
        {
            menuView = actionsheetView
            actionsheetView.btnSignature.addTarget(self, action: #selector(btnSignature), for: .touchUpInside)
            actionsheetView.btnClose.addTarget(self, action: #selector(btnCancelMenu), for: .touchUpInside)
            actionsheetView.btnNewLines.addTarget(self, action: #selector(btnNewLineItems), for: .touchUpInside)
            actionsheetView.btnEditLines.addTarget(self, action: #selector(btnEditLineItems), for: .touchUpInside)
            actionsheetView.btnNewFile.addTarget(self, action: #selector(btnNewFile), for: .touchUpInside)
            actionsheetView.btnNewTask.addTarget(self, action: #selector(btnNewTask), for: .touchUpInside)
            actionsheetView.btnEmail.addTarget(self, action: #selector(btnEmail1), for: .touchUpInside)
            actionsheetView.btnCloneInvoice.addTarget(self, action: #selector(btnCloneInvoice), for: .touchUpInside)
            actionsheetView.btnNewEvents.addTarget(self, action: #selector(btnNewEvent), for: .touchUpInside)
            actionsheetView.btnDeleteInvoice.addTarget(self, action: #selector(btnDeleteInvoice), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnCall.addTarget(self, action: #selector(btnCall1), for: .touchUpInside)
            actionsheetView.btnText.addTarget(self, action: #selector(btnText1), for: .touchUpInside)
            actionsheetView.btnNewNote.addTarget(self, action: #selector(btnNewNote), for: .touchUpInside)
            actionsheetView.btnGenerateDoc.addTarget(self, action: #selector(btnGenerateDoc), for: .touchUpInside)
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetView.bounds
            self.actionSheetView.addSubview(actionsheetView)
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        txtInvoiceStatus.inputView = pickerView
        txtContact.inputView = pickerView
        txtWO.inputView = pickerView
        txtAssignedTo.inputView = pickerView
        txtPaymentTerms.inputView = pickerView
        txtDescription.giveBorder()
        btnAddress.giveBorderToButton()
        btnDetails.giveCornerRadius()
        btnRelated.giveCornerRadius()
        btnRelated.giveBorderToButton()
        btnDetails.giveBorderToButton()
        
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtContact.setRightImage(name: "search_small", placeholder: "--None--")
        txtWO.setRightImage(name: "search_small", placeholder: "--None--")
        txtInvoiceStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        txtPaymentTerms.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtAddress.layer.borderWidth = 1.5
        self.txtAddress.layer.borderColor = UIColor.darkGray.cgColor
        heightConstraintActionSheet.constant = User.instance.screenHeight * (0.50)
    }
    func loadNIB()
    {
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse
        {
            customView.tag = 101
            customView.btnAttachFile.addTarget(self, action: #selector(btnAttachFile), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(btnGallery), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    
    @objc func btnCall1()
    {
        if let number = strPhoneNo {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    @objc func btnText1()
    {
        if let number = strPhoneNo {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()] as? [String]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
        
    }
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionSheetView.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
        
    }
    @objc func btnCloneInvoice()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateInvoiceViewController") as! CreateInvoiceViewController
        vc.flagResponse = "1"
        vc.response = responseInvoice_Details
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func btnGenerateDoc()
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenerateDocumentViewController") as? GenerateDocumentViewController
        vc?.object = "Invoice"
        vc?.objectID = User.instance.invoiceID
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @objc func btnDeleteInvoice()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Invoice",
                          "What":User.instance.invoiceID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    @objc func btnAttachFile()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: false) {
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = true
            }
        }
    }
    @objc func btnGallery()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        //imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        //fusuma.hasVideo = true //To allow for video capturing with .library and .camera available by default
        fusuma.cropHeightRatio = 0.6 // Height-to-width ratio. The default value is 1, which means a squared-size photo.
        fusuma.allowMultipleSelection = true // You can select multiple photos from the camera roll. The default value is false.
        self.present(fusuma, animated: true, completion: nil)
    }
    @objc func btnNewEvent()
    {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "Invoice"
        createEventVC.objectID = User.instance.invoiceID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    @objc func btnNewNote()
    {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateNoteViewController") as! CreateNoteViewController
        vc.relatedTo = "Invoice"
        vc.objectID = User.instance.invoiceID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func btnNewFile()
    {
        loadNIB()
        
    }
    @objc func btnEmail1()
    {
        let emailVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailViewController") as! EmailViewController
        emailVC.strRelatedTo = "Invoice"
        emailVC.strWhat = User.instance.invoiceID
        self.navigationController?.pushViewController(emailVC, animated: true)
    }
    @objc func btnNewTask()
    {
        let vc = UIStoryboard(name: "Task", bundle: nil).instantiateViewController(withIdentifier: "CreateTaskViewController") as? CreateTaskViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnSignature()
    {
        actionSheetView.isHidden = true
        btnClose.isHidden = true
        viewBottomContainer.isHidden = false
        view_back.isHidden = true
        //view_backNewFile.isHidden = true
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as? SignatureViewController
        vc?.flag = "1"
        self.present(vc!, animated: true, completion: nil)
    }
    
    @objc func btnNewLineItems()
    {
        actionSheetView.isHidden = true
        btnClose.isHidden = true
        viewBottomContainer.isHidden = false
        view_back.isHidden = true
        //view_backNewFile.isHidden = true
        
        let vc = UIStoryboard.init(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchInvoiceViewController") as? SearchInvoiceViewController
        vc?.selectedProducts = User.instance.productNameArr5
        vc?.selectedQuantity = User.instance.quantityArr5
        vc?.selectedListPriceArr = User.instance.listPriceArr5
        vc?.selectedTaxableArr = User.instance.taxableArr5
        vc?.selectedTaxArr = User.instance.taxArr5
        vc?.selectedQuantityEditableArr = User.instance.quantityEditableArr5
        vc?.selectedListPriceEditableArr = User.instance.listPriceEditableArr5
        vc?.selectedProductIDArr = User.instance.productIDArr5
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnEditLineItems()
    {
        actionSheetView.isHidden = true
        btnClose.isHidden = true
        viewBottomContainer.isHidden = false
        view_back.isHidden = true
        //view_backNewFile.isHidden = true
        
        
        let vc = UIStoryboard.init(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchInvoiceViewController") as? SearchInvoiceViewController
        
        vc?.selectedProducts = User.instance.productNameArr5
        vc?.selectedQuantity = User.instance.quantityArr5
        vc?.selectedListPriceArr = User.instance.listPriceArr5
        vc?.selectedTaxableArr = User.instance.taxableArr5
        vc?.selectedTaxArr = User.instance.taxArr5
        vc?.selectedQuantityEditableArr = User.instance.quantityEditableArr5
        vc?.selectedListPriceEditableArr = User.instance.listPriceEditableArr5
        vc?.selectedProductIDArr = User.instance.productIDArr5
        User.instance.flagSkipInvoiceProduct = "1"
        
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @objc func btnCancelMenu()
    {
        self.actionSheetView.isHidden = true
        self.btnCancel.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
        //self.view_backNewFile.isHidden = true
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        if textfieldTag == 2 {
            
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        } else if textfieldTag == 3 {
            
            txtAccount.text = searchVC.accountName
            accountID = searchVC.accountID
            webserviceCallForAccountDetails()
            
        } else if textfieldTag == 4 {
            txtWO.text = searchVC.parentWO_Name
            WOID = searchVC.parentWO_ID
            
        } else if textfieldTag == 5 {
            txtContact.text = searchVC.contactName
            contactID = searchVC.contactID
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtAddress.text = response.data?.BillingAddress
                self.txtCity.text = response.data?.BillingCity
                self.txtState.text = response.data?.BillingState
                self.txtCountry.text = response.data?.BillingCountry
                self.txtPostalCode.text = response.data?.BillingPostalCode
                self.latAddress = response.data?.BillingLatitude
                self.longAddress = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetInvoiceStatus()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoiceStatus(urlString: API.getInvoiceStatusURL, parameters: parameters , headers: headers, vc: self) { (response:GetInvoiceStatus) in
            
            if response.Result == "True"
            {
                
                self.invoiceStatusArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    
    func webserviceCallForGetPaymentTerms()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoicePaymentTerms(urlString: API.getInvoicePaymentTermsURL, parameters: parameters , headers: headers, vc: self) { (response:GetInvoicePaymentTerms) in
            
            if response.Result == "True"
            {
                
                self.paymentTermsArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForInvoiceDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "InvoiceID":User.instance.invoiceID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.invoiceDetails(urlString: API.invoiceDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:InvoiceDetails) in
            
            if response.Result == "True"
            {
                self.responseInvoice_Details = response
                self.txtInvoiceNo.text = response.data?.InvoiceNo
                self.txtAccount.text = response.data?.AccountName
                self.txtInvoiceStatus.text  = response.data?.InvoiceStatus
                self.txtAssignedTo.text = response.data?.AssignedToName
                self.txtWO.text = response.data?.WorkOrderSubject
                self.txtContact.text = response.data?.ContactName
                self.txtAddress.text = response.data?.Address
                self.txtCity.text = response.data?.City
                self.txtPostalCode.text =  response.data?.PostalCode
                self.txtState.text = response.data?.State
                self.txtCountry.text = (response.data?.Country) ?? ""
                self.latAddress = (response.data?.Latitude ?? "")
                self.longAddress = (response.data?.Longitude ?? "")
                self.txtDescription.text = (response.data?.Description ?? "")
                self.txtInvoiceDate.text = response.data?.InvoiceDate
                self.txtDueDate.text = response.data?.DueDate
                self.txtPaymentTerms.text = response.data?.PaymentTerms
                self.lblStatus.text = "Status: " + (response.data?.InvoiceStatus ?? "")!
                self.lblAssignedTo.text = "Assigned To: " + (response.data?.AssignedToName ?? "")!
                self.lblHeaderTitle.text = response.data?.InvoiceNo
                User.instance.invoiceNo = (response.data?.InvoiceNo)!
                self.accountID = response.data?.Account
                self.invoiceStatusID = response.data?.InvoiceStatusID
                self.contactID = response.data?.Contact
                self.WOID = response.data?.WorkOrder
                self.assignedToID = response.data?.AssignedTo
                self.InvoicePaymentTermID = response.data?.InvoicePaymentTermID
                self.txtTotalPrice.text = response.data?.TotalPrice
                self.txtTax.text = response.data?.Tax
                self.txtSubTotal.text = response.data?.SubTotal
                self.txtLineCountItems.text = response.data?.LineItemCount
                self.txtGrandTotal.text = response.data?.GrandTotal
                self.txtDiscount.text = response.data?.Discount
                self.txtCreatedBy.text = response.data?.CreatedBy
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtLastModifiedBy.text = response.data?.LastModifiedBy
                self.txtLastModifiedDate.text = response.data?.LastModifiedDate
                self.txtAddInfo.text = response.data?.AdditionalInformation
                self.strPhoneNo = response.data?.PhoneNo
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForEditInvoice()
    {
        if accountID == nil || invoiceStatusID == nil || contactID == nil || assignedToID == nil || WOID == nil || txtDescription.text == "Description*" || txtInvoiceDate.text == "" || InvoicePaymentTermID == nil || txtDueDate.text == "" || txtCity.text == "" || txtState.text == "" || txtCountry.text == "" || txtPostalCode.text == "" || txtAddress.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }

        self.showHUD()


        var parameters:[String:String] = [:]
        
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////

        parameters["InvoiceID"] = User.instance.invoiceID
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["Account"] = accountID
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["WorkOrder"] = WOID ?? ""
        parameters["Contact"] = contactID ?? ""
        parameters["Address"] = txtAddress.text ?? ""
        parameters["City"] = txtCity.text ?? ""
        parameters["PostalCode"] = txtPostalCode.text ?? ""
        parameters["State"] = txtState.text ?? ""
        parameters["Country"] = txtCountry.text ?? ""
        parameters["Latitude"] = latAddress ?? ""
        parameters["Longitude"] = longAddress ?? ""
        parameters["Description"] = txtDescription.text ?? ""
        parameters["InvoiceStatus"] = invoiceStatusID ?? ""
        parameters["InvoiceDate"] = txtInvoiceDate.text ?? ""
        parameters["DueDate"] = txtDueDate.text ?? ""
        parameters["PaymentTerms"] = InvoicePaymentTermID
        parameters["AdditionalInformation"] = txtAddInfo.text ?? ""

        print(parameters)

        let headers = ["key":User.instance.key,
                       "token":User.instance.token]

        NetworkManager.sharedInstance.webserviceCall(urlString: API.editInvoiceURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in

            if response.Result == "True"
            {

                self.webserviceCallForInvoiceDetails()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true

                let vc  = self.children[1] as! InvoiceLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.invoiceID,
                          "Object":"Invoice",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                var arrayFieldValue:[String]?
                
                var viewFromConstrain:Any = self.txtLineCountItems
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtLineCountItems)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtLineCountItems, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtLineCountItems, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            //
                            customView.txtLabelField.text = response.data?[i].FieldValue
                            
                            //
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtLabelField.text ?? "")
                            
                            //
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            
                            if response.data?[i].IsRequired == "1" {
                                
                                if response.data?[i].FieldValue == "" {
                                    customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                    customView.txtviewLabelField.textColor = UIColor.lightGray
                                } else {
                                    customView.txtviewLabelField.text = response.data?[i].FieldValue
                                    
                                }
                                
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtviewLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtviewLabelField.text ?? "")
                            
                            //
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            //Edit
                            if response.data?[i].FieldValue == "" {
                                self.checkboxValueArr.append("")
                            } else{
                                self.checkboxValueArr.append("\((response.data?[i].FieldValue ?? "")),")
                            }
                            //
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                if ((arrayFieldValue?.contains((arrayCheckboxValues[j])))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                
                //Edit
                if value == "," {
                    checkboxValueArr.insert("\(sender.accessibilityLabel!),", at: index)
                } else {
                    checkboxValueArr.insert(value + "\(sender.accessibilityLabel!),", at: index)
                }
                //
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: "\(sender.accessibilityLabel!),", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            view_back.isHidden = true
            actionSheetView.isHidden = true
            btnCancel.isHidden = true
            //view_backNewFile.isHidden = true
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 1
        {
            return invoiceStatusArr.count + 1
        }
        else if textfieldTag == 7
        {
            return paymentTermsArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 1
        {
            return row == 0 ? "--None--":invoiceStatusArr[row-1].InvoiceStatus
        }
        else if textfieldTag == 7
        {
            return row == 0 ? "--None--":paymentTermsArr[row-1].PaymentTerms
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 1
        {
            if row == 0
            {
                txtInvoiceStatus.text = ""
            }
            else{
                txtInvoiceStatus.text = invoiceStatusArr[row-1].InvoiceStatus
                invoiceStatusID = invoiceStatusArr[row-1].InvoiceStatusID
            }
            
        }
        else if textfieldTag == 7
        {
            if row == 0
            {
                txtPaymentTerms.text = ""
            }
            else{
                txtPaymentTerms.text = paymentTermsArr[row-1].PaymentTerms
                InvoicePaymentTermID = paymentTermsArr[row-1].InvoicePaymentTermID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        //
        
        if textField == txtAssignedTo || textField == txtAccount || textField == txtWO || textField == txtContact || textField == txtInvoiceStatus || textField == txtPaymentTerms
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtWO.becomeFirstResponder()
            
        }
        else if textField.tag == 4
        {
            txtContact.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtInvoiceDate.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPaymentTerms.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtDueDate.becomeFirstResponder()
        }
            
        else if textField.tag == 15
        {
            txtAddInfo.becomeFirstResponder()
        }
        
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Account")
        }
        if textField.tag == 4
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "WorkOrder")
        }
        if textField.tag == 5
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self as GMSAutocompleteViewControllerDelegate
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    
    //MARK: UITextview Delegate Methods
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description*"
        {
            textView.text = ""
        }
        
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description*"
        }
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        if customFieldKeyArr.count != 0 {
            
            if lastTextfieldTapped != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                    customFieldValueArr.append(lastTextfieldTapped.text ?? "")
                }
            }
            
        }
        
        
        //CustomTextView
        if customFieldKeyArr.count != 0 {
            
            if lastTappedTextView != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                    customFieldValueArr.append(lastTappedTextView.text ?? "")
                }
            }
            
        }
        
        checkboxEditedValueArr.removeAll()
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropLast()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        
        webserviceCallForEditInvoice()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
        
        txtDescription.isUserInteractionEnabled = true

        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        btnCancel.isHidden = false
        btnSave.isHidden = false
        txtInvoiceStatus.layer.borderColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnAddress(_ sender: Any) {
        
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func txtDueDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtInvoiceDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
   
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        if textfieldTag == 6
        {
            txtInvoiceDate.text = dateFormatter.string(from: sender.date)
        }
        else if textfieldTag == 8
        {
            txtDueDate.text = dateFormatter.string(from: sender.date)
        }
        
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        
        if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
            txtAddress.text = SelectLocationVC.address
            txtCity.text = SelectLocationVC.city
            txtState.text = SelectLocationVC.state
            txtCountry.text = SelectLocationVC.country
            txtPostalCode.text = SelectLocationVC.postalCode
            self.latAddress = SelectLocationVC.latitude
            self.longAddress = SelectLocationVC.longitude
        }
        
        if let _ = sender.source as? EditLinesInvoiceViewController
        {
            self.flag = 1
        }
        
    }
    
    @IBAction func btnDetails(_ sender: Any) {
        containerviewLayout.isHidden = false
        self.containerview.isHidden = true
        self.scrollview.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func btnRelated(_ sender: Any) {
        
        containerviewLayout.isHidden = true
        self.containerview.isHidden = false
        self.scrollview.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnCall(_ sender: Any) {
        
        if let number = strPhoneNo {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    
    @IBAction func btnMessage(_ sender: Any) {
        
        if let number = strPhoneNo {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()] as? [String]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnEmail(_ sender: Any) {
        let emailVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailViewController") as! EmailViewController
        emailVC.strRelatedTo = "Invoice"
        emailVC.strWhat = User.instance.invoiceID
        self.navigationController?.pushViewController(emailVC, animated: true)
    }
    
    @IBAction func btnMore(_ sender: Any) {
        
        //self.btnCancel.isHidden = false
        self.actionSheetView.isHidden = false
        self.viewBottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
}
extension InvoiceDetailsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                area = component.name
            }
            latAddress = String(place.coordinate.latitude)
            longAddress = String(place.coordinate.longitude)
            if component.type == "sublocality_level_1" {
                    print(component.name)
                txtAddress.text = "\(place.name) \(area ?? "") \(component.name)"
            }
            if component.type == "postal_code"
            {
                print("Code: \(component.name)")
                txtPostalCode.text = component.name
            }
            if component.type == "administrative_area_level_2" {
                print("City: \(component.name)")
                txtCity.text = component.name
            }
            if component.type == "administrative_area_level_1"
            {
                print("State: \(component.name)")
                txtState.text = component.name
            }
            if component.type == "country"
            {
                print("Country: \(component.name)")
                txtCountry.text = component.name
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension InvoiceDetailsViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtInvoiceNo.text ?? "",
                          "AssignedTo":self.assignedToID!,
                          "RelatedTo":"Invoice",
                          "What":User.instance.invoiceID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.uploadDocs(urlString: API.createNewFileAllURL, fileName: "FileName", URLs: urls as [NSURL], parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
}
extension InvoiceDetailsViewController:FusumaDelegate {
    
    // Return the image which is selected from camera roll or is taken via the camera.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(image: UIImage, source: FusumaMode) {
        
        print("Called just after FusumaViewController is dismissed.")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        print("Called just after a video has been selected.")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // Return selected images when you allow to select multiple photos.
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtInvoiceNo.text ?? "",
                          "AssignedTo":self.assignedToID!,
                          "RelatedTo":"Invoice",
                          "What":User.instance.invoiceID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createNewFileAll(urlString: API.createNewFileAllURL, pickedImages: images, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // Return an image and the detailed information.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
    }
}
