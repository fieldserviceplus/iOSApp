//
//  ActionSheetInvoiceView.swift
//  Field Service Pulse
//
//  Created by Apple on 25/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ActionSheetInvoiceView: UIView {

    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnText: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSignature: UIButton!
    @IBOutlet weak var btnEditLines: UIButton!
    
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnNewLines: UIButton!
    @IBOutlet weak var btnNewEvents: UIButton!
    @IBOutlet weak var btnNewFile: UIButton!
    @IBOutlet weak var btnNewTask: UIButton!
    @IBOutlet weak var btnCloneInvoice: UIButton!
    @IBOutlet weak var btnDeleteInvoice: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var heightConstraintBtnEditLineItems: NSLayoutConstraint!
    @IBOutlet weak var heightBackView: NSLayoutConstraint!
    @IBOutlet weak var btnNewNote: UIButton!
    @IBOutlet weak var btnGenerateDoc: UIButton!
    @IBOutlet weak var heightConstraintBtnNewLineItems: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        self.btnCall.leftImage(image: #imageLiteral(resourceName: "phone-1"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnText.leftImage(image: #imageLiteral(resourceName: "message"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEdit.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEmail.leftImage(image: #imageLiteral(resourceName: "email"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnSignature.leftImage(image: #imageLiteral(resourceName: "signature"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEditLines.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        
        self.btnNewLines.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        
        self.btnNewEvents.leftImage(image: #imageLiteral(resourceName: "calendar_black"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewFile.leftImage(image: #imageLiteral(resourceName: "file"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewTask.leftImage(image: #imageLiteral(resourceName: "task"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnCloneInvoice.leftImage(image: #imageLiteral(resourceName: "copy"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnDeleteInvoice.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnGenerateDoc.leftImage(image: #imageLiteral(resourceName: "product"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewNote.leftImage(image: #imageLiteral(resourceName: "signature"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        
    }

}
