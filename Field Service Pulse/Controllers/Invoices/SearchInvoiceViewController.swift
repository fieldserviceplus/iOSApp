//
//  SearchInvoiceViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SearchInvoiceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK: Variables
    
    private let CELL_ID = "cell_addInvoice"
    
    
    var filteredProductArr:[GetInvoicesData] = []
    
    var pickerView = UIPickerView()
    var isSeaching = false
    var accountID = ""
    var accountName = ""
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var isSelect = false
    var productArr:[GetInvoicesData] = []
    var customViewCopy:productFilterWOView?
    var productFamilyArr:[GetProductFamilyData] = []
    var productFamilyId:String?
    
    var selectedProducts:[String] = []
    var selectedQuantity:[String] = []
    var selectedListPriceArr:[String] = []
    var selectedTaxableArr:[String] = []
    var selectedTaxArr:[String] = []
    var selectedListPriceEditableArr:[String] = []
    var selectedQuantityEditableArr:[String] = []
    var selectedProductIDArr:[String] = []
    //var Add_SelectedProducts:[String] = []
    //var Add_SelectedQuantity:[String] = []
    //var Add_SelectedListPriceArr:[String] = []
    //var Add_SelectedTaxableArr:[String] = []
    //var Add_SelectedTaxArr:[String] = []
    //var Add_SelectedListPriceEditableArr:[String] = []
    //var Add_SelectedQuantityEditableArr:[String] = []
    //var Add_SelectedProductIDArr:[String] = []
    
    var productIDArr:[String] = []
    var productNameArr:[String] = []
    
    //MARK: IBOutlet
    
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var btnSelectAdd: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblDropdown: PaddingLabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //webserviceCallForAccountTypes()
        User.instance.flagAddInvoiceProducts = "1"
        
        User.instance.productIDArr4.removeAll()
        User.instance.discountArr4.removeAll()
        User.instance.quantityArr4.removeAll()
        User.instance.taxableArr4.removeAll()
        User.instance.taxArr4.removeAll()
        User.instance.listPriceEditableArr4.removeAll()
        User.instance.quantityEditableArr4.removeAll()
        User.instance.productArr4.removeAll()
        User.instance.listPriceArr4.removeAll()
        setupUI()
        customViewCopy?.txtProductCode.text = ""
        webserviceCallForGetProducts()
        webserviceCallForGetProductFamily()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getDataFromEditLines(noti:)), name: NSNotification.Name("sendDataEditLinesInvoice"), object: nil)
    }
    
    @objc func getDataFromEditLines(noti:Notification) {
        
        let bunchArr = noti.object as! [[String]]
        
        selectedProducts = bunchArr[0]
        selectedQuantity = bunchArr[1]
        selectedListPriceArr = bunchArr[2]
        selectedTaxableArr = bunchArr[3]
        selectedTaxArr = bunchArr[4]
        selectedQuantityEditableArr = bunchArr[5]
        selectedListPriceEditableArr = bunchArr[6]
        selectedProductIDArr = bunchArr[7]
    }
    
    func setupUI()
    {
        self.table.tableFooterView = UIView()
        self.table.rowHeight = 70
        view_back.dropShadow()
        btnSelect.contentHorizontalAlignment = .left
        btnSelectAdd.contentHorizontalAlignment = .left
        btnCancel.contentHorizontalAlignment = .left
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        pickerView.delegate = self
    }
    
    //MARK: Function
    
    func webserviceCallForGetProducts()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ProductFamily":productFamilyId ?? "",
                          "ProductCode":customViewCopy?.txtProductCode.text ?? "",
                          "InvoiceID":User.instance.invoiceID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoiceProducts(urlString: API.invoiceGetProductsURL, parameters: parameters , headers: headers, vc: self) { (response:GetInvoices) in
            
            if response.Result == "True"
            {
                
                self.productArr = response.data!
                self.productIDArr.removeAll()
                self.productNameArr.removeAll()
                for i in 0..<self.productArr.count
                {
                    self.productIDArr.append(self.productArr[i].ProductID!)
                    self.productNameArr.append(self.productArr[i].ProductName!)
                }
                self.table.reloadData()
                if User.instance.flagSkipInvoiceProduct == "1"
                {
                    self.btnSelect.sendActions(for: .touchUpInside)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForGetProductFamily()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getProductFamily(urlString: API.getProductFamilyURL, parameters: parameters , headers: headers, vc: self) { (response:GetProductFamily) in
            
            if response.Result == "True"
            {
                
                self.productFamilyArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    @objc func btnCheckbox(sender:UIButton)
    {
        if isSeaching
        {
            
            if (sender.currentImage?.isEqual(UIImage(named: "uncheck-box")))!
            {
                sender.setImage(UIImage(named: "check-box"), for: .normal)
                let _ = filteredProductArr[sender.tag].ProductName
                selectedProducts.append(filteredProductArr[sender.tag].ProductName!)
                selectedQuantity.append(filteredProductArr[sender.tag].DefaultQuantity!)
                selectedListPriceArr.append(filteredProductArr[sender.tag].ListPrice!)
                
                selectedTaxableArr.append(filteredProductArr[sender.tag].Taxable!)
                selectedTaxArr.append(filteredProductArr[sender.tag].Tax!)
                selectedQuantityEditableArr.append(filteredProductArr[sender.tag].IsQuantityEditable!)
                selectedListPriceEditableArr.append(filteredProductArr[sender.tag].IsListPriceEditable!)
            }
            else
            {
                sender.setImage(UIImage(named: "uncheck-box"), for: .normal)
                
                let index = selectedProducts.index(of: filteredProductArr[sender.tag].ProductName!)
                
                selectedProducts.remove(at: index!)
                selectedQuantity.remove(at: index!)
                selectedListPriceArr.remove(at: index!)
                
                selectedTaxableArr.remove(at: index!)
                selectedTaxArr.remove(at: index!)
                selectedQuantityEditableArr.remove(at: index!)
                selectedListPriceEditableArr.remove(at: index!)
                
            }
        }
        else
        {
            
            if (sender.currentImage?.isEqual(UIImage(named: "uncheck-box")))!
            {
                sender.setImage(UIImage(named: "check-box"), for: .normal)
                let _ = productArr[sender.tag].ProductName
                selectedProducts.append(productArr[sender.tag].ProductName!)
                selectedQuantity.append(productArr[sender.tag].DefaultQuantity!)
                selectedListPriceArr.append(productArr[sender.tag].ListPrice!)
                
                selectedTaxableArr.append(productArr[sender.tag].Taxable!)
                selectedTaxArr.append(productArr[sender.tag].Tax!)
                selectedQuantityEditableArr.append(productArr[sender.tag].IsQuantityEditable!)
                selectedListPriceEditableArr.append(productArr[sender.tag].IsListPriceEditable!)
                print(selectedProducts)
                print(selectedQuantity)
            }
            else
            {
                sender.setImage(UIImage(named: "uncheck-box"), for: .normal)
                
                let index = selectedProducts.index(of: productArr[sender.tag].ProductName!)
                
                selectedProducts.remove(at: index!)
                selectedQuantity.remove(at: index!)
                selectedListPriceArr.remove(at: index!)
                
                selectedTaxableArr.remove(at: index!)
                selectedTaxArr.remove(at: index!)
                selectedQuantityEditableArr.remove(at: index!)
                selectedListPriceEditableArr.remove(at: index!)
            }
        }
    }
    
    @objc func btnApply()
    {
        webserviceCallForGetProducts()
        self.view.viewWithTag(1001)?.removeFromSuperview()
        
    }
    
    @objc func btnClear()
    {
        productFamilyId = ""
        customViewCopy?.txtProductFamily.text = ""
        customViewCopy?.txtProductCode.text = ""
        webserviceCallForGetProducts()
        
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        return productFamilyArr.count + 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return row == 0 ? "--None--":productFamilyArr[row-1].ProductFamily
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row == 0
        {
            customViewCopy?.txtProductFamily.text = ""
        }
        else{
            customViewCopy?.txtProductFamily.text = productFamilyArr[row-1].ProductFamily
            productFamilyId = productFamilyArr[row-1].ProductFamilyID
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: UITextfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            
        }
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSeaching
        {
            return filteredProductArr.count
        }
        return productArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! SearchInvoiceTableViewCell
        cell.btnCheckbox.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
        cell.textview.giveBorder()
        cell.btnCheckbox.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
        
        if isSeaching
        {
            cell.lblProductName?.text = filteredProductArr[indexPath.row].ProductName
            cell.lblLastPrice?.text = filteredProductArr[indexPath.row].ListPrice
            cell.textview?.text = filteredProductArr[indexPath.row].Description
            cell.btnCheckbox.tag = indexPath.row
            
            
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                //cell.btnAdd.isHidden = true
            }
        }
        else
        {
            
            cell.lblProductName?.text = productArr[indexPath.row].ProductName
            cell.lblLastPrice?.text = (productArr[indexPath.row].ListPrice ?? "").convertToCurrencyFormat()
            cell.textview?.text = productArr[indexPath.row].Description
            cell.btnCheckbox.tag = indexPath.row
            
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                //cell.btnAdd.isHidden = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSeaching
        {
            //accountID = filteredProductArr[indexPath.row].AccountID!
            accountName = filteredProductArr[indexPath.row].ProductName!
            
        }
        else
        {
            //accountID = accountTypeArr[indexPath.row].AccountID!
            let dict = productArr[indexPath.row]
            accountName = productArr[indexPath.row].ProductName!
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! SearchInvoiceTableViewCell
        cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(!selectedArray.contains(indexPath))
        {
            selectedArray.removeAll()
            selectedArray.append(indexPath)
        }
        else
        {
            selectedArray = selectedArray.filter{$0 != indexPath}
            // remove from array here if required
        }
        
        if indexPath.row == selectedIndex{
            selectedIndex = -1
            
            
        }else{
            selectedIndex = indexPath.row
            
        }
        
        self.table.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == selectedIndex
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 160
            } else { //IPAD
                return 223
            }
            
            
        }else{
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 70
            } else { //IPAD
                return 90
            }
            
        }
        
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        filteredProductArr = productArr.filter({ (text) -> Bool in
            let tmp: NSString = text.ProductName as! NSString
            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        if(filteredProductArr.count == 0){
            isSeaching = false;
        } else {
            isSeaching = true;
        }
        self.table.reloadData()
    }
    
    
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelect(_ sender: Any) {
        
        table.reloadData()
        print(selectedProducts)
        print(selectedProductIDArr)
        selectedProductIDArr.removeAll()
        for i in 0..<selectedProducts.count
        {
            let index = productNameArr.index(of: selectedProducts[i])
            selectedProductIDArr.append(productIDArr[index!])
        }
        //Push to edit line items
        let editLineVC = self.storyboard?.instantiateViewController(withIdentifier: "EditLinesInvoiceViewController") as! EditLinesInvoiceViewController
        editLineVC.productArr = selectedProducts
        editLineVC.quantityArr = selectedQuantity
        editLineVC.listPriceArr = selectedListPriceArr
        
        editLineVC.taxableArr = selectedTaxableArr
        editLineVC.taxArr = selectedTaxArr
        editLineVC.quantityEditableArr = selectedQuantityEditableArr
        editLineVC.listPriceEditableArr = selectedListPriceEditableArr
        editLineVC.productIDArr = selectedProductIDArr
        self.navigationController?.pushViewController(editLineVC, animated: true)
        //}
        view_back.isHidden = true
        
    }
    @IBAction func btnSelectAdd(_ sender: Any) {
        
        table.reloadData()
        view_back.isHidden = true
        isSelect = true
       
    }
    @IBAction func btnCancel(_ sender: Any) {
        view_back.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDropdown(_ sender: Any) {
        
        if view_back.isHidden
        {
            view_back.isHidden = false
        }
        else{
            view_back.isHidden = true
        }
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        
        if let customView = Bundle.main.loadNibNamed("productFilterView", owner: self, options: nil)?.first as? productFilterWOView
        {
            customViewCopy = customView
            
            customView.tag = 1001
            customView.btnApply.addTarget(self, action: #selector(btnApply), for: .touchUpInside)
            customView.btnClear.addTarget(self, action: #selector(btnClear), for: .touchUpInside)
            
            
            customView.txtProductFamily.inputView = pickerView
            customView.txtProductFamily.delegate = self
            customView.txtProductFamily.setRightImage(name: "down-arrow_black", placeholder: "--None--")
            
            customView.frame = CGRect(x: 0, y: 120, width: User.instance.screenWidth, height: User.instance.screenHeight-120)
            self.view.addSubview(customView)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
