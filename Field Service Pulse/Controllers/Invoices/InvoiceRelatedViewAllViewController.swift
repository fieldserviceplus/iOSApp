//
//  InvoiceRelatedViewAllViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 01/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class InvoiceRelatedViewAllViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    
    private let CELL_VIEWALL = "cellViewAll"
    var tag:Int?
    var titleLabel:String?
    var headerTitle:String?
    var lineItemsArr:[InvoiceRelatedLineItemData] = []
    var eventArr:[InvoiceRelatedEventData] = []
    var taskArr:[InvoiceRelatedTaskData] = []
    var fileArr:[InvoiceRelatedFileData] = []
    var noteArr:[InvoiceRelatedNoteData] = []
    
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var tableViewAll: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
    }
    func setupUI()
    {
        tableViewAll.tableFooterView = UIView()
        lblTitle.text = titleLabel
        lblHeaderTitle.text = headerTitle
    }
    @objc func btnDetails(sender:UIButton) {
        
        if tag == 0
        {
            let productVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
            productVC.navigationController?.isNavigationBarHidden = true
            productVC.productID = lineItemsArr[sender.tag].Product!
            productVC.prouctType = "Product"
            productVC.what = User.instance.invoiceID
            productVC.relatedTo = "Invoice"
            self.navigationController?.pushViewController(productVC, animated: true)
        }
        else if tag == 1
        {
            let fileVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            fileVC.navigationController?.isNavigationBarHidden = true
            User.instance.eventID = eventArr[sender.tag].EventID!
            self.navigationController?.pushViewController(fileVC, animated: true)
        }
        else if tag == 2
        {
            let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
            taskVC.navigationController?.isNavigationBarHidden = true
            User.instance.taskID = taskArr[sender.tag].TaskID!
            self.navigationController?.pushViewController(taskVC, animated: true)
        }
        else if tag == 3
        {
            let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
            fileVC.navigationController?.isNavigationBarHidden = true
            User.instance.fileID = fileArr[sender.tag].FileID!
            self.navigationController?.pushViewController(fileVC, animated: true)
        }
        else if tag == 4
        {
            let noteVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "NoteDetailsViewController") as! NoteDetailsViewController
            noteVC.navigationController?.isNavigationBarHidden = true
            User.instance.noteID = noteArr[sender.tag].NoteID!
            noteVC.relatedTo = "Invoice"
            noteVC.objectID = User.instance.invoiceID
            self.navigationController?.pushViewController(noteVC, animated: true)
        }
    }
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tag == 0
        {
            return lineItemsArr.count
        }
        else if tag == 1
        {
            return eventArr.count
        }
        else if tag == 2
        {
            return taskArr.count
        }
        else if tag == 3
        {
            return fileArr.count
        }
        else if tag == 4
        {
            return noteArr.count
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_VIEWALL) as! InvoiceRelatedViewAllTableViewCell
        cell.btnDetail.addTarget(self, action: #selector(btnDetails(sender:)), for: .touchUpInside)
        cell.btnDetail.tag = indexPath.row
        cell.view_back.dropShadow()
        if tag == 0
        {
            cell.lblFirst?.text = "Product Name: " + (lineItemsArr[indexPath.row].ProductName ?? "")
            cell.lblSecond?.text = "List Price: " +  (lineItemsArr[indexPath.row].ListPrice ?? "").convertToCurrencyFormat()
            cell.lblThird?.text = "Discount(%): " + (lineItemsArr[indexPath.row].Discount ?? "")
            cell.lblFourth?.text = "Unit Price: " + (lineItemsArr[indexPath.row].UnitPrice ?? "").convertToCurrencyFormat()
            cell.lblFifth?.text = "Quantity: " + (lineItemsArr[indexPath.row].Quantity ?? "")
            cell.lblSixth?.text = "Total Price: " + (lineItemsArr[indexPath.row].TotalPrice ?? "").convertToCurrencyFormat()
        }
        else if tag == 1
        {
            cell.lblFirst?.text = "Event Type Name: " + (eventArr[indexPath.row].EventTypeName ?? "")
            cell.lblSecond?.text = "Event Status: " +  (eventArr[indexPath.row].EventStatus ?? "")
            cell.lblThird?.text = "Subject: " + (eventArr[indexPath.row].Subject ?? "")
            cell.lblFourth?.text = "Event Start Date: " + (eventArr[indexPath.row].EventStartDate ?? "")
            cell.lblFifth?.text = "Event End Date: " +  (eventArr[indexPath.row].EventEndDate ?? "")
            cell.lblSixth?.text = "Event Start Time: " + (eventArr[indexPath.row].EventStartTime ?? "")
        }
        else if tag == 2
        {
            cell.lblFirst?.text = "TaskType: " + (taskArr[indexPath.row].TaskType ?? "")
            cell.lblSecond?.text = "Task Status: " + (taskArr[indexPath.row].TaskStatus ?? "")
            cell.lblThird?.text = "Priority: " + (taskArr[indexPath.row].Priority ?? "")
            cell.lblFourth?.text = "Call Disposition: " + (taskArr[indexPath.row].CallDisposition ?? "")
            cell.lblFifth?.text = ""
            cell.lblSixth?.text = ""
        }
        else if tag == 3
        {
            cell.lblFirst?.text = "File Name: " + (fileArr[indexPath.row].FileName ?? "")
            cell.lblSecond?.text = "Content Type: " +  (fileArr[indexPath.row].ContentType ?? "")
            cell.lblThird?.text = "Subject: " + (fileArr[indexPath.row].Subject ?? "")
            cell.lblFourth?.text = ""
            cell.lblFifth?.text = ""
            cell.lblSixth?.text = ""
        }
            
        else if tag == 4
        {
            cell.lblFirst?.text = "Owner Name: " + (noteArr[indexPath.row].OwnerName ?? "")
            cell.lblSecond?.text = "Subject: " +  (noteArr[indexPath.row].Subject ?? "")
            cell.lblThird?.text = "Created Date: " + (noteArr[indexPath.row].CreatedDate ?? "")
            cell.lblFourth?.text = ""
            cell.lblFifth?.text = ""
            cell.lblSixth?.text = ""
        }
        if(selectedArray.contains(indexPath))
        {
            // use selected image
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        }
        else
        {
            // use normal image
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewAll
        {
            let cell = tableView.cellForRow(at: indexPath) as! InvoiceRelatedViewAllTableViewCell
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath))
            {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }
            else
            {
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            
            if indexPath.row == selectedIndex{
                selectedIndex = -1
                
            }else{
                selectedIndex = indexPath.row
                
            }
            tableView.reloadData()
        }
        else
        {
            //self.performSegue(withIdentifier: SEGUE_AC_DETAILS, sender: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableViewAll
        {
            if indexPath.row == selectedIndex
            {
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    return 193
                } else { //IPAD
                    return 227
                }
                
            }else{
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    return 98
                } else { //IPAD
                    return 115
                }
            }
        }
        return 0
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}

