//
//  CreateInvoiceViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 24/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateInvoiceViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchDelegate {
    
    //MARK: Variables
    var latAddress:String?
    var longAddress:String?
    var textfieldTag = 0
    
    var accountID:String?
    var invoiceStatusID:String?
    var InvoicePaymentTermID:String?
    var contactID:String?
    var assignedToID:String?
    var WOID:String?
    var invoiceStatusArr:[GetInvoiceStatusData] = []
    var paymentTermsArr:[GetInvoicePaymentTermsData] = []
    
    var pickerView = UIPickerView()
    var flagResponse = "0"
    var response:InvoiceDetails?
    
    var accountName:String?
    var workOrderSubject:String?
    var contactName:String?
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtInvoiceStatus: UITextField!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtWO: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtInvoiceDate: AkiraTextField!
    @IBOutlet weak var txtPaymentTerms: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtAddInfo: AkiraTextField!
    @IBOutlet weak var txtDueDate: AkiraTextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: AkiraTextField!
    @IBOutlet weak var txtState: AkiraTextField!
    @IBOutlet weak var txtCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddress: UIButton!
    
    @IBOutlet weak var viewBackground: UIView!
    //UILabel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetInvoiceStatus()
        webserviceCallForGetPaymentTerms()
        webserviceCallForCustomFields()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        pickerView.dataSource = self
        txtInvoiceStatus.inputView = pickerView
        txtPaymentTerms.inputView = pickerView
        txtDescription.giveBorder()
        btnAddress.giveBorderToButton()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtContact.setRightImage(name: "search_small", placeholder: "--None--")
        txtWO.setRightImage(name: "search_small", placeholder: "--None--")
        txtInvoiceStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        txtPaymentTerms.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtAddress.layer.borderWidth = 1.5
        self.txtAddress.layer.borderColor = UIColor.darkGray.cgColor
        
        if flagResponse == "1" {
            responseInvoiceDetails()
            flagResponse = "0"
        }
        if accountName != nil {
            self.txtAccount.text = accountName
        }
        if workOrderSubject != nil {
            self.txtWO.text = workOrderSubject
        }
        if contactName != nil {
            self.txtContact.text = contactName
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        if textfieldTag == 2 {
            
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        } else if textfieldTag == 3 {
            
            txtAccount.text = searchVC.accountName
            accountID = searchVC.accountID
            webserviceCallForAccountDetails()
            
        } else if textfieldTag == 4 {
            txtWO.text = searchVC.parentWO_Name
            WOID = searchVC.parentWO_ID
            
        } else if textfieldTag == 5 {
            txtContact.text = searchVC.contactName
            contactID = searchVC.contactID
        }
        
    }
    
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtAddress.text = response.data?.BillingAddress
                self.txtCity.text = response.data?.BillingCity
                self.txtState.text = response.data?.BillingState
                self.txtCountry.text = response.data?.BillingCountry
                self.txtPostalCode.text = response.data?.BillingPostalCode
                self.latAddress = response.data?.BillingLatitude
                self.longAddress = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetInvoiceStatus()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoiceStatus(urlString: API.getInvoiceStatusURL, parameters: parameters , headers: headers, vc: self) { (response:GetInvoiceStatus) in
            
            if response.Result == "True"
            {
                self.invoiceStatusArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    
    func webserviceCallForGetPaymentTerms()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getInvoicePaymentTerms(urlString: API.getInvoicePaymentTermsURL, parameters: parameters , headers: headers, vc: self) { (response:GetInvoicePaymentTerms) in
            
            if response.Result == "True"
            {
                
                self.paymentTermsArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCall()
    {
        if accountID == nil || invoiceStatusID == nil || assignedToID == nil || WOID == nil || txtDescription.text == "Description*" || txtInvoiceDate.text == "" || InvoicePaymentTermID == nil || txtDueDate.text == "" || txtCity.text == "" || txtState.text == "" || txtCountry.text == "" || txtPostalCode.text == "" || txtAddress.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["Account"] = accountID
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["WorkOrder"] = WOID ?? ""
        parameters["Contact"] = contactID ?? ""
        parameters["Address"] = txtAddress.text ?? ""
        parameters["City"] = txtCity.text ?? ""
        parameters["PostalCode"] = txtPostalCode.text ?? ""
        parameters["State"] = txtState.text ?? ""
        parameters["Country"] = txtCountry.text ?? ""
        parameters["Latitude"] = latAddress ?? ""
        parameters["Longitude"] = longAddress ?? ""
        parameters["Description"] = txtDescription.text ?? ""
        parameters["InvoiceStatus"] = invoiceStatusID ?? ""
        parameters["InvoiceDate"] = txtInvoiceDate.text ?? ""
        parameters["DueDate"] = txtDueDate.text ?? ""
        parameters["PaymentTerms"] = InvoicePaymentTermID
        parameters["AdditionalInformation"] = txtAddInfo.text ?? ""
        
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createInvoice(urlString: API.createInvoiceURL, parameters: parameters , headers: headers, vc: self) { (response:CreateInvoice) in
            
            if response.Result == "True"
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
                User.instance.invoiceID = String(response.InvoiceID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func responseInvoiceDetails()
    {
        self.txtAccount.text = response?.data?.AccountName
        self.txtInvoiceStatus.text  = response?.data?.InvoiceStatus
        self.txtAssignedTo.text = response?.data?.AssignedToName
        self.txtWO.text = response?.data?.WorkOrderSubject
        self.txtContact.text = response?.data?.ContactName
        self.txtAddress.text = response?.data?.Address
        self.txtCity.text = response?.data?.City
        self.txtPostalCode.text =  response?.data?.PostalCode
        self.txtState.text = response?.data?.State
        self.txtCountry.text = (response?.data?.Country) ?? ""
        self.latAddress = (response?.data?.Latitude ?? "")
        self.longAddress = (response?.data?.Longitude ?? "")
        self.txtDescription.text = (response?.data?.Description ?? "")
        self.txtInvoiceDate.text = response?.data?.InvoiceDate
        self.txtDueDate.text = response?.data?.DueDate
        self.txtPaymentTerms.text = response?.data?.PaymentTerms
        self.accountID = response?.data?.Account
        self.invoiceStatusID = response?.data?.InvoiceStatusID
        self.contactID = response?.data?.Contact
        self.WOID = response?.data?.WorkOrder
        self.assignedToID = response?.data?.AssignedTo
        self.InvoicePaymentTermID = response?.data?.InvoicePaymentTermID
        self.txtAddInfo.text = response?.data?.AdditionalInformation
            
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "Object":"Invoice",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                
                var viewFromConstrain:Any = self.txtPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                customView.txtviewLabelField.textColor = UIColor.lightGray
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            self.checkboxValueArr.append("")
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.btnSave, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(value + "," + sender.accessibilityLabel!, at: index)
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: ",\(sender.accessibilityLabel!)", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxKey:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    @IBAction func txtDueDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtInvoiceDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        if textfieldTag == 6
        {
           txtInvoiceDate.text = dateFormatter.string(from: sender.date)
        }
        else if textfieldTag == 8
        {
            txtDueDate.text = dateFormatter.string(from: sender.date)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 1
        {
            return invoiceStatusArr.count + 1
        }
        else if textfieldTag == 7
        {
            return paymentTermsArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 1
        {
            return row == 0 ? "--None--":invoiceStatusArr[row-1].InvoiceStatus
        }
        else if textfieldTag == 7
        {
            return row == 0 ? "--None--":paymentTermsArr[row-1].PaymentTerms
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 1
        {
            if row == 0
            {
                txtInvoiceStatus.text = ""
            }
            else{
                txtInvoiceStatus.text = invoiceStatusArr[row-1].InvoiceStatus
                invoiceStatusID = invoiceStatusArr[row-1].InvoiceStatusID
            }
            
        }
        else if textfieldTag == 7
        {
            if row == 0
            {
                txtPaymentTerms.text = ""
            }
            else{
                txtPaymentTerms.text = paymentTermsArr[row-1].PaymentTerms
                InvoicePaymentTermID = paymentTermsArr[row-1].InvoicePaymentTermID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        
        //
        if textField == txtAssignedTo || textField == txtAccount || textField == txtWO || textField == txtContact || textField == txtInvoiceStatus || textField == txtPaymentTerms 
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtWO.becomeFirstResponder()
            
        }
        else if textField.tag == 4
        {
            txtContact.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtInvoiceDate.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPaymentTerms.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtDueDate.becomeFirstResponder()
        }
            
        else if textField.tag == 15
        {
            txtAddInfo.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Account")
        }
        if textField.tag == 4
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "WorkOrder")
        }
        if textField.tag == 5
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self as GMSAutocompleteViewControllerDelegate
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    //MARK: UITextview Delegate Methods
    var lastTappedTextView:UITextView!
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description*"
        {
            textView.text = ""
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description*"
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        
        if lastTextfieldTapped != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                customFieldValueArr.append(lastTextfieldTapped.text ?? "")
            }
        }
        
        
        
        //CustomTextView
        
        if lastTappedTextView != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                customFieldValueArr.append(lastTappedTextView.text ?? "")
            }
        }
        
        
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropFirst()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCall()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddress(_ sender: Any) {
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "createEstimate"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
            txtAddress.text = SelectLocationVC.address
            txtCity.text = SelectLocationVC.city
            txtState.text = SelectLocationVC.state
            txtCountry.text = SelectLocationVC.country
            txtPostalCode.text = SelectLocationVC.postalCode
            self.latAddress = SelectLocationVC.latitude
            self.longAddress = SelectLocationVC.longitude
            }
    }
}
extension CreateInvoiceViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            latAddress = String(place.coordinate.latitude)
            longAddress = String(place.coordinate.longitude)
            if component.type == "sublocality_level_1" {
                print(component.name)
                txtAddress.text = "\(place.name) \(area ?? "") \(component.name)"
            }
            if component.type == "postal_code"
            {
                print("Code: \(component.name)")
                txtPostalCode.text = component.name
            }
            if component.type == "administrative_area_level_2" {
                print("City: \(component.name)")
                txtCity.text = component.name
            }
            if component.type == "administrative_area_level_1"
            {
                print("State: \(component.name)")
                txtState.text = component.name
                    
            }
            if component.type == "country"
            {
                print("Country: \(component.name)")
                txtCountry.text = component.name
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
