//
//  SortViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 13/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class SortViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    private let CELL_SORT = "cell_sort"
    
    var viewID:String!
    var object:String!
    var fieldsArr:[String] = []
    var strField:String!
    var strSortBy:String!
    var btnPrevious:UIButton?
    var sortingIndexArr:[String] = []
    var count:Int!
    
    //MARK: IBOutlet
    
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tableSort: UITableView!
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetCustomViewDetails()
    }
    
    //MARK: Function
    
    func setupUI() {
        tableSort.tableFooterView = UIView()
        viewBackground.giveBorderToView()
        btnApply.giveCornerRadius()
        btnCancel.giveCornerRadius()
    }
    
    @objc func btnSort(sender:UIButton) {
        
        if btnPrevious != nil && btnPrevious != sender {

            btnPrevious?.setTitle("Ascending", for: .normal)
            btnPrevious?.setImage(#imageLiteral(resourceName: "up-arrow-1"), for: .normal)
        }
        if sender.currentTitle == "Ascending" {

            sender.setTitle("Descending", for: .normal)
            sender.setImage(#imageLiteral(resourceName: "down-arrow-1"), for: .normal)
        } else {
            sender.setTitle("Ascending", for: .normal)
            sender.setImage(#imageLiteral(resourceName: "up-arrow-1"), for: .normal)
        }
        
        strField = fieldsArr[sender.tag]
        strSortBy = sender.currentTitle
        
        btnPrevious = sender
    }
    
    func  webserviceCallForGetCustomViewDetails()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object!,
                          "ViewID":viewID!]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.getCustomViewDetails(urlString: API.getCustomViewDetailsURL, parameters: parameters as [String : Any], headers: headers, vc: self) { (response:GetCustomViewDetails) in
            
            if response.Result == "True"
            {
                
                let displayedColumnsArr = response.data?.DisplayedColumns as! [DisplayedColumnsData]
                
                self.count = displayedColumnsArr.count
                for i in 0..<displayedColumnsArr.count {
                    
                    self.fieldsArr.append(displayedColumnsArr[i].FieldName!)
                }
                self.tableSort.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }

    func  webserviceCallForSortCustomView()
    {
        if strField == nil {
            return
        }
        self.showHUD()
        
        var parameters:[String:String] = [:]
        if strSortBy == "Ascending" {
            parameters["SortByValue"] = "asc"
        } else {
            parameters["SortByValue"] = "desc"
        }
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["Object"] = object!
        parameters["ViewID"] = viewID!
        parameters["SortByField"] = strField
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.sortCustomViewURL, parameters: parameters as [String : Any], headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.navigationController?.popViewController(animated: true)
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_SORT) as! SortTableViewCell
        cell.lblTitle.text = fieldsArr[indexPath.row]
        cell.btnSort.tag = indexPath.row
        cell.btnSort.giveBorderToButton()
        cell.btnSort.addTarget(self, action: #selector(btnSort(sender:)), for: .touchUpInside)
        
        if sortingIndexArr.count > 0 {
            if sortingIndexArr[indexPath.row] == "1" {
                
                cell.btnSort.isHidden = false
            } else {
                
                cell.btnSort.isHidden = true
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        sortingIndexArr.removeAll()
        for _ in 0..<count
        {
            sortingIndexArr.append("0")
        }
        sortingIndexArr.remove(at: indexPath.row)
        sortingIndexArr.insert("1", at: indexPath.row)
        
        strField = fieldsArr[indexPath.row]
        strSortBy = "Ascending"
        if btnPrevious != nil  {
            
            btnPrevious?.setTitle("Ascending", for: .normal)
            btnPrevious?.setImage(#imageLiteral(resourceName: "up-arrow-1"), for: .normal)
        }
        tableSort.reloadData()
    }
    
    //MARK: IBAction
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApply(_ sender: Any) {
        
        webserviceCallForSortCustomView()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
