//
//  NoteDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 18/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class NoteDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    
    var lastContentOffset: CGFloat = 0
    var assignedToID:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var lblModDate: UILabel!
    @IBOutlet weak var lblModBy: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCall()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "NoteID":User.instance.noteID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.noteDetails(urlString: API.noteDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:NoteDetails) in
            
            
            if response.Result == "True"
            {
                self.lblSubject.text = response.data?.Subject
                self.lblOwner.text = response.data?.OwnerName
                self.txtview.text = response.data?.Body
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblCreatedBy.text = response.data?.CreatedByName
                self.lblModDate.text = response.data?.LastModifiedDate
                self.lblModBy.text = response.data?.LastModifiedByName
                
                self.assignedToID = response.data?.Owner
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? NoteDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? NoteDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = true
                    // parent.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                if let parent = self.parent as? NoteDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
                
            }, completion: { _ in
                if let parent = self.parent as? NoteDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = false
                    //parent.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
        
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        if sender.tag == 1 {
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
        }
    }
    
    
}
