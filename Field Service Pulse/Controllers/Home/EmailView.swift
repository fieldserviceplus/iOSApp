//
//  EmailView.swift
//  Field Service Pulse
//
//  Created by Apple on 27/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EmailView: UIView {

    //MARK: Variables
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtUrl: UITextField!
    @IBOutlet weak var btnDone: UIButton!
    
    
    //MARK: IBOutlet
    
    override func awakeFromNib() {
        
        
    }
    
    //MARK: IBAction
    
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    

}
