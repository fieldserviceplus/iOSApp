//
//  UserDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 22/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class UserDetailsLayoutViewController: UIViewController {

    //MARK: Variables
    
    var viewUserID:String?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblDivision: UILabel!
    @IBOutlet weak var lblManager: UILabel!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var lblCellNo: UILabel!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartOfDay: UILabel!
    @IBOutlet weak var lblEndOfDay: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var lblEmailName: UILabel!
    @IBOutlet weak var lblEmailSignature: UILabel!
    @IBOutlet weak var lblTimeZone: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblLastModifiedDate: UILabel!
    @IBOutlet weak var lblLastModifiedBy: UILabel!
    @IBOutlet weak var lblNotiFreq: UILabel!
    @IBOutlet weak var btnReceiveEmail: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func viewDidAppear(_ animated: Bool) {
        webserviceCall()
    }
    func webserviceCall()
    {
        self.showHUD()
        
        if let vc = self.parent as? UserDetailsViewController {
            
            viewUserID = vc.viewUserID
        }
        let parameters = ["UserID":User.instance.UserID,
                          "ViewUserID":viewUserID ?? "",
                          "OrganizationID":User.instance.OrganizationID]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.userDetails(urlString: API.userDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:UserDetails) in
            
            if response.Result == "True"
            {
                self.lblName.text = response.data?.FullName
                self.lblEmail.text  = response.data?.Email
                self.lblCompany.text = response.data?.CompanyName
                self.lblDepartment.text = response.data?.DepartmentName
                self.lblDivision.text = response.data?.DivisionName
                self.lblManager.text = response.data?.ManagerName
                self.lblPhoneNo.text = response.data?.PhoneNo
                self.lblAddress.text = (response.data?.Address) ?? ""
                self.lblStreet.text = (response.data?.City)! + ", " + (response.data?.State)! + ", " + (response.data?.PostalCode)!
                self.lblCountry.text = (response.data?.Country)!
                self.lblCellNo.text = response.data?.MobileNo
                self.lblProfile.text = response.data?.ProfileName
                if response.data?.EndDate?.range(of: "00/00/0000") != nil{
                    self.lblEndDate.text = ""
                }else{
                    self.lblEndDate.text = response.data?.EndDate
                }
                if response.data?.StartDate?.range(of: "00/00/0000") != nil{
                    self.lblStartDate.text = ""
                }else{
                    self.lblStartDate.text = response.data?.StartDate
                }
                self.lblTimeZone.text = response.data?.TimeZoneName
                self.lblStartOfDay.text = response.data?.StartOfDay
                self.lblEndOfDay.text = response.data?.EndOfDay
                self.lblEmailAddress.text = response.data?.SenderEmail
                self.lblEmailName.text = response.data?.SenderName
                self.lblEmailSignature.text = response.data?.EmailSignature
                self.lblNotiFreq.text = response.data?.DefaultGrpNotificationFreq
                self.lblLastModifiedDate.text = response.data?.LastModifiedDate
                self.lblLastModifiedBy.text = response.data?.LastModifiedBy
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblCreatedBy.text = response.data?.CreatedBy
                
                if response.data?.IsActive == "1" {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                if response.data?.ReceiveAdminEmails == "1" {
                    self.btnReceiveEmail.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                
                if let vc = self.parent as? UserDetailsViewController {
                    
                    vc.lblTitle.text = "Title: " + (response.data?.Title ?? "")
                    vc.lblPhone.text = "Phone: " + (response.data?.PhoneNo ?? "")
                }
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    

}
