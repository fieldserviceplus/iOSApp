//
//  NoteActionSheet.swift
//  Field Service Pulse
//
//  Created by Apple on 22/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class NoteActionSheet: UIView {

    //MARK: IBOutlet
    
    
    @IBOutlet weak var btnEditNote: UIButton!
    @IBOutlet weak var btnDeleteNote: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    override func awakeFromNib() {
        
        btnEditNote.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: .alwaysOriginal)
        btnDeleteNote.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: .alwaysOriginal)
        viewBackground.giveBorderToView()
    }
    
    //MARK: IBAction
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }

}
