//
//  UserDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 22/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {

    var viewUserID:String?
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
