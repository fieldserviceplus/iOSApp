//
//  ProductDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 23/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController {

    
    var productID:String?
    var prouctType:String?
    var relatedTo:String?
    var what:String?
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblFamilyName: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblHeader.text = prouctType! + " Details"
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
