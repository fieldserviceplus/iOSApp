//
//  EventDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 17/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class EventDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {

    //MARK: Variables
    
    var lastContentOffset: CGFloat = 0
    var contactID:String?
    var assignedToID:String?
    var relatedToID:String?
    var RelatedToName:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEventType: UILabel!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var lblEventPriority: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblModBy: UILabel!
    @IBOutlet weak var lblModDate: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var btnAllDay: UIButton!
    @IBOutlet weak var lblRelatedTo: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCall()
    }

    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "EventID":User.instance.eventID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.eventDetails(urlString: API.eventDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:EventDetails) in
            
            
            if response.Result == "True"
            {
                self.lblSubject.text = response.data?.Subject
                self.lblAssignedTo.text = response.data?.AssignedToName
                self.lblStartDate.text = response.data?.EventStartDate
                self.lblEndDate.text = response.data?.EventEndDate
                self.txtviewDescription.text = response.data?.Description
                self.lblEventType.text = response.data?.EventTypeName
                self.lblEventPriority.text = response.data?.EventPriority
                self.lblContact.text = response.data?.ContactName
                self.lblEmail.text = response.data?.Email
                self.lblPhone.text = (response.data?.PhoneNo)
                self.lblRelatedTo.text = (response.data?.RelatedTo ?? "") + "-" + (response.data?.Subject ?? "")
                
                
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblCreatedBy.text = response.data?.CreatedBy
                self.lblModDate.text = response.data?.LastModifiedDate
                self.lblModBy.text = response.data?.LastModifiedBy
                
                
                
                if response.data?.IsAllDayEvent == "1"
                {
                    self.btnAllDay.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                
                self.contactID = response.data?.Who
                self.assignedToID = response.data?.AssignedTo
                self.relatedToID = response.data?.What
                self.RelatedToName = response.data?.RelatedToName
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? EventDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? EventDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = true
                   // parent.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                if let parent = self.parent as? EventDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
                
            }, completion: { _ in
                if let parent = self.parent as? EventDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = false
                    //parent.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
        
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
            
        }  else if sender.tag == 2 {
            if self.contactID != nil && self.contactID != "" {
                let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                User.instance.contactID = contactID!
                User.instance.contactName = self.lblContact.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        } else if sender.tag == 3 {
            
            if self.relatedToID != nil && self.relatedToID != "" {
                
                if RelatedToName == "Account" {
                    
                    let acVC = UIStoryboard(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController")  as! AccountDetailsViewController
                    User.instance.accountID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Contact" {
                    
                    let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                    User.instance.contactID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "WorkOrder" {
                    
                    let acVC = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController")  as! WorkOrdersDetailsViewController
                    User.instance.workorderID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Estimate" {
                    
                    let acVC = UIStoryboard(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController")  as! EstimateDetailsViewController
                    User.instance.estimateID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Invoice" {
                    
                    let acVC = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController")  as! InvoiceDetailsViewController
                    User.instance.invoiceID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                
            }
        }
        
    }

}
