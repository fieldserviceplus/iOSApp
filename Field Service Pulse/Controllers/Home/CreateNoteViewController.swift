//
//  CreateNoteViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 18/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CreateNoteViewController: UIViewController, UITextViewDelegate {
    
    //MARK: Variables
    
    var objectID:String?
    var relatedTo:String?
    
    //MARK: IBOutlets
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtviewBody: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
       
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        
        self.txtviewBody.layer.borderWidth = 1.5
        self.txtviewBody.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtSubject.text ?? "",
                          "Body":txtviewBody.text ?? "",
                          "RelatedTo":relatedTo!,
                          "What":objectID!] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        NetworkManager.sharedInstance.webserviceCall(urlString: API.createNoteURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotificationWithBack(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    //MARK: Textview Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Body"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Body"
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    //MARK: IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        webserviceCall()
    }
    
    
}




