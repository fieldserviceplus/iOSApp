//
//  GenerateDocumentViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 30/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import WebKit

class GenerateDocumentViewController: UIViewController, UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate {

    
    //MARK: Variables
    var pickerView = UIPickerView()
    var object:String?
    var getGenDocTemplatesArr:[GetGenDocTemplatesData] = []
    var textfieldTag = 0
    var opFormatArr:[String] = ["Word","PDF"]
    var templateID:String?
    var SaveToObject:String?
    var objectID:String?
    var fileURL:String?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var txtOpFormat: UITextField!
    @IBOutlet weak var txtTemplate: UITextField!
    @IBOutlet weak var btnSaveWO: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblSaveTo: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetGenDocTemplates()
        
    }
    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        pickerView.dataSource = self
        txtTemplate.inputView = pickerView
        txtOpFormat.inputView = pickerView
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtTemplate.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtOpFormat.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        
        
        if object == "WorkOrder" {
            lblSaveTo.text = "Save To Work Order"
        } else {
            lblSaveTo.text = "Save To \(object ?? "")"
        }
    }

    func webserviceCallForGetGenDocTemplates()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "RelatedTo":object,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getGenDocTemplates(urlString: API.getGenDocTemplatesURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:GetGenDocTemplates) in
            
            if response.Result == "True"
            {
                self.getGenDocTemplatesArr = response.data!
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCall()
    {
        if txtOpFormat.text == "" || txtTemplate.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = object
        parameters["ObjectID"] = objectID ?? ""
        parameters["GenDocTemplateID"] = templateID ?? ""
        parameters["SaveToObject"] = SaveToObject ?? "1"
        parameters["OutputFormat"] = txtOpFormat.text ?? ""
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.generateDocument(urlString: API.generateDocumentURL, parameters: parameters , headers: headers, vc: self) { (response:GenerateDocument) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                
                self.fileURL = response.data?.GenerateDocumentURL
                self.btnShare.isHidden = false
                let webview = WKWebView(frame: CGRect(x: 0, y: 44 + UIApplication.shared.statusBarFrame.height, width: self.view.frame.width, height: self.view.frame.height-(44 + UIApplication.shared.statusBarFrame.height)))
                webview.load(URLRequest.init(url: URL(string: (response.data?.GenerateDocumentURL)!)!))
                self.view.addSubview(webview)
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 1
        {
            return getGenDocTemplatesArr.count + 1
        }
        else if textfieldTag == 2
        {
            return opFormatArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 1
        {
            return row == 0 ? "--None--":getGenDocTemplatesArr[row-1].TemplateName
        }
        else if textfieldTag == 2
        {
            return row == 0 ? "--None--":opFormatArr[row-1]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 1
        {
            if row == 0
            {
                txtTemplate.text = ""
            }
            else{
                txtTemplate.text = getGenDocTemplatesArr[row-1].TemplateName
                templateID = getGenDocTemplatesArr[row-1].GenDocTemplateID
            }
            
        }
        else if textfieldTag == 2
        {
            if row == 0
            {
                txtOpFormat.text = ""
            }
            else{
                txtOpFormat.text = opFormatArr[row-1]
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        
        pickerView.reloadAllComponents()
    }
    //MARK: IBAction
    
    @IBAction func btnSave(_ sender: Any) {
        webserviceCall()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveWO(_ sender: Any) {
        
        if (btnSaveWO.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            btnSaveWO.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            SaveToObject = "0"
        } else {
            btnSaveWO.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            SaveToObject = "1"
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShare(_ sender: Any) {
        
        if fileURL != nil {
            
            let activityItem : NSURL = NSURL(string: fileURL!)!
            
            
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [activityItem], applicationActivities: nil)
            
            // This lines is for the popover you need to show in iPad
            activityViewController.popoverPresentationController?.sourceView = (sender ) as? UIView
            
            // This line remove the arrow of the popover to show in iPad
            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
            
            // Anything you want to exclude
            activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.postToWeibo,
                UIActivity.ActivityType.print,
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.addToReadingList,
                UIActivity.ActivityType.postToFlickr,
                UIActivity.ActivityType.postToVimeo,
                UIActivity.ActivityType.postToTencentWeibo
            ]
            
            self.present(activityViewController, animated: true, completion: nil)
            
        }
        
    }
    
    
}
