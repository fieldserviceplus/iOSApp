//
//  NoteDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 18/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class NoteDetailsViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate {
    
    //MARK Variables
    
    var lastContentOffset: CGFloat = 0
    
    var assignedToID:String?
    var textfieldTag = 0
    var menuView:NoteActionSheet?
    var noteID:String?
    var objectID:String?
    var relatedTo:String?
    
    //MARK: IBOutlet
    
    
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtModDate: AkiraTextField!
    @IBOutlet weak var txtModBy: AkiraTextField!
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        webserviceCall()
    }
    
    
    //MARK: Functions
    
    func setupUI()
    {
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtview.giveBorder()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "NoteID":User.instance.noteID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.noteDetails(urlString: API.noteDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:NoteDetails) in
            
            if response.Result == "True"
            {
                self.txtSubject.text = response.data?.Subject
                self.txtview.text = response.data?.Body
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtCreatedBy.text = response.data?.CreatedByName
                self.txtModDate.text = response.data?.LastModifiedDate
                self.txtModBy.text = response.data?.LastModifiedByName
                
                self.lblRelatedTo.text = "Related To: " + (self.relatedTo ?? "")
                self.lblSubject.text = "Subject: " + (self.txtSubject.text ?? "")
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForEditNote() {
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":relatedTo!,
                          "NoteID":User.instance.noteID,
                          "Subject":txtSubject.text ?? "",
                          "Body":txtview.text ?? "",
                          "What":objectID!] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editNoteURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.webserviceCall()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[0] as! NoteDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnDeleteNote()
    {
        self.view.viewWithTag(1001)?.removeFromSuperview()
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Note",
                          "What":User.instance.noteID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    @objc func btnEditNote()
    {
        self.view.viewWithTag(1001)?.removeFromSuperview()
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
    }
    
    
    
    //MARK: Textview Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Body"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Body"
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: UIScrollViewDelegate
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnMore(_ sender: Any) {
        
        //self.viewBottomContainer.isHidden = true
        
        if let actionsheetView = Bundle.main.loadNibNamed("NoteActionSheet", owner: self, options: nil)?.first as? NoteActionSheet
        {
            menuView = actionsheetView
            actionsheetView.tag = 1001
            actionsheetView.btnDeleteNote.addTarget(self, action: #selector(btnDeleteNote), for: .touchUpInside)
            actionsheetView.btnEditNote.addTarget(self, action: #selector(btnEditNote), for: .touchUpInside)
            actionsheetView.viewBackground.giveBorderToView()
            actionsheetView.frame = self.view.bounds
            self.view.addSubview(actionsheetView)
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        webserviceCallForEditNote()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
    }
    
}
