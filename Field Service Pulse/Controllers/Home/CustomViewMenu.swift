//
//  CustomViewMenu.swift
//  Field Service Pulse
//
//  Created by Apple on 28/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CustomViewMenu: UIView {

    //MARK: IBOutlet
    
    @IBOutlet weak var btnCreateView: UIButton!
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnRename: UIButton!
    @IBOutlet weak var btnEditSharing: UIButton!
    @IBOutlet weak var btnEditFilter: UIButton!
    @IBOutlet weak var btnEditColumns: UIButton!
    @IBOutlet weak var btnDeleteRow: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    override func awakeFromNib() {
        
        btnCreateView.leftImage(image: #imageLiteral(resourceName: "file"), renderMode: .alwaysOriginal)
        btnCopy.leftImage(image: #imageLiteral(resourceName: "copy"), renderMode: .alwaysOriginal)
        btnRename.leftImage(image: #imageLiteral(resourceName: "invoice"), renderMode: .alwaysOriginal)
        btnEditSharing.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: .alwaysOriginal)
        btnEditFilter.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: .alwaysOriginal)
        btnEditColumns.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: .alwaysOriginal)
        btnDeleteRow.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: .alwaysOriginal)
        viewBackground.giveBorderToView()
    }
    
    //MARK: IBAction
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
