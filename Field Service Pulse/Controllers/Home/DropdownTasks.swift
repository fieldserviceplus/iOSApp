//
//  DropdownTasks.swift
//  Field Service Pulse
//
//  Created by Apple on 25/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DropdownTasks: UIView {

    //MARK: IBOutlet
    @IBOutlet weak var btnToday: UIButton!
    @IBOutlet weak var btnTodayOverdue: UIButton!
    @IBOutlet weak var btnOverdue: UIButton!
    
    override func awakeFromNib() {
        
        btnToday.contentHorizontalAlignment = .left
        btnOverdue.contentHorizontalAlignment = .left
        btnTodayOverdue.contentHorizontalAlignment = .left
    }

}
