//
//  EditSharingViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 29/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EditSharingViewController: UIViewController {

    //MARK: Variables
    var RestrictVisibility:String?
    var object:String?
    var ViewID:String?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnVisibleToMe: UIButton!
    @IBOutlet weak var btnVisibleToAll: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webserviceCallForGetCustomViewDetails()
        setupUI()
    }
    
    func setupUI() {
        btnCancel.giveCornerRadius()
        btnSave.giveCornerRadius()
    }
    
    //MARK: API Call
    
    func  webserviceCallForEditSharingCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object!,
                          "ViewID":ViewID!,
                          "RestrictVisibility":RestrictVisibility] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editSharingCustomViewURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotificationWithBack(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetCustomViewDetails()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object,
                          "ViewID":ViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.getCustomViewDetails(urlString: API.getCustomViewDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:GetCustomViewDetails) in
            
            if response.Result == "True"
            {
                self.RestrictVisibility = response.data?.RestrictVisibility
                
                if self.RestrictVisibility == "VisibleOnlyToMe" {
                    self.btnVisibleToMe.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
                    self.btnVisibleToAll.setImage(#imageLiteral(resourceName: "blankRadio"), for: .normal)
                } else {
                    self.btnVisibleToAll.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
                    self.btnVisibleToMe.setImage(#imageLiteral(resourceName: "blankRadio"), for: .normal)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }

    //MARK: IBAction
    
    @IBAction func btnVisible(_ sender: UIButton) {
        
        if (sender as AnyObject).tag == 1 {
            btnVisibleToAll.setImage(#imageLiteral(resourceName: "blankRadio"), for: .normal)
            sender.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            RestrictVisibility = "VisibleOnlyToMe"
        } else {
            btnVisibleToMe.setImage(#imageLiteral(resourceName: "blankRadio"), for: .normal)
            sender.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            RestrictVisibility = "VisibleToEveryone"
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        webserviceCallForEditSharingCustomView()
    }
}
