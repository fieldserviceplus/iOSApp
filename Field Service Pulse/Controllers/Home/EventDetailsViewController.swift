//
//  EventDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 17/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, searchDelegate {

    //MARK Variables
    
    var lastContentOffset: CGFloat = 0
    
    var assignedToID:String?
    var contactID:String?
    var typeID:String?
    var priorityID:String?
    
    var pickerView = UIPickerView()
    
    var eventTypeArr:[GetEventTypesData] = []
    var eventPrioritiesArr:[GetEventPrioritiesData] = []
    var contactsArr:[ContactsData] = []
    
    var textfieldTag = 0
    
    var isAllDay:String?
    var response:accountDetailsResponse?
    var relatedTo:String?
    var objectID:String?
    var menuView:NoteActionSheet?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtStartDate: AkiraTextField!
    @IBOutlet weak var txtEndDate: AkiraTextField!
    @IBOutlet weak var btnAllDay: UIButton!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtEventType: UITextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtEventPriority: UITextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtModDate: AkiraTextField!
    @IBOutlet weak var txtModBy: AkiraTextField!
    @IBOutlet weak var txtStartTime: AkiraTextField!
    @IBOutlet weak var txtEndTime: AkiraTextField!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        pickerView.delegate = self
        txtEventType.inputView = pickerView
        txtEventPriority.inputView = pickerView
        setupUI()
        webserviceCall()
        webserviceCallForGetEventPriorities()
        webserviceCallForGetEventTypes()
    }
    

    //MARK: Functions
    
    func setupUI()
    {
        
        txtviewDescription.giveBorder()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        
        self.txtEventPriority.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtContact.setRightImage(name: "search_small", placeholder: "--None--")
        
        self.txtEventType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC: SearchAccountViewController) {
        
        if textfieldTag == 2 {
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        } else if textfieldTag == 6 {
            txtContact.text = searchVC.contactName
            contactID = searchVC.contactID
        }
    }

    func webserviceCallForGetEventPriorities()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventPriorities(urlString: API.getEventPrioritiesURL, parameters: parameters , headers: headers, vc: self) { (response:GetEventPriorities) in
            
            if response.Result == "True"
            {
                self.eventPrioritiesArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetEventTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTypes(urlString: API.getEventTypesURL, parameters: parameters , headers: headers, vc: self) { (response:GetEventTypes) in
            
            if response.Result == "True"
            {
                self.eventTypeArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "EventID":User.instance.eventID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.eventDetails(urlString: API.eventDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:EventDetails) in
            
            
            if response.Result == "True"
            {
                self.relatedTo = response.data?.RelatedToName
                self.objectID = response.data?.What
                self.txtSubject.text = response.data?.Subject
                self.txtAssignedTo.text = response.data?.AssignedToName
                self.txtviewDescription.text = response.data?.Description
                self.txtEventType.text = response.data?.EventTypeName
                self.txtEventPriority.text = response.data?.EventPriority
                self.txtContact.text = response.data?.ContactName
                
                self.assignedToID = response.data?.AssignedTo
                self.typeID = response.data?.EventTypeID
                self.contactID = response.data?.Who
                self.priorityID = response.data?.EventPriorityID
                
                self.isAllDay = response.data?.IsAllDayEvent
                
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtCreatedBy.text = response.data?.CreatedBy
                self.txtModDate.text = response.data?.LastModifiedDate
                self.txtModBy.text = response.data?.LastModifiedBy
                
                self.lblAssignedTo.text = "Assigned To: " + (self.txtAssignedTo.text ?? "")
                self.lblPriority.text = "Priority: " + (self.txtEventPriority.text ?? "")
                
                self.txtStartDate.text = Helper.instance.convertDateFormat(date: (response.data?.EventStartDate)!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "MM/dd/YYYY")
                self.txtEndDate.text = Helper.instance.convertDateFormat(date: (response.data?.EventEndDate)!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "MM/dd/YYYY")
                self.txtStartTime.text = Helper.instance.convertDateFormat(date: (response.data?.EventStartDate)!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "hh:mm a")
                self.txtEndTime.text = Helper.instance.convertDateFormat(date: (response.data?.EventEndDate)!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "hh:mm a")
                if self.isAllDay == "1"
                {
                    self.btnAllDay.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForEditEvent() {
        
        if assignedToID == nil || priorityID == nil || txtSubject.text == "" || txtStartDate.text == "" || txtStartTime.text == "" || txtEndDate.text == "" || txtEndTime.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        self.showHUD()
        var parameters:[String:Any] = [:]
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = relatedTo!
        parameters["EventID"] = User.instance.eventID
        parameters["Subject"] = txtSubject.text ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["What"] = objectID!
        parameters["AssignedTo"] = assignedToID
        parameters["Who"] = contactID ?? ""
        parameters["IsAllDayEvent"] = isAllDay
        parameters["EventPriority"] = priorityID
        parameters["EventType"] = typeID
        parameters["EventStartDate"] = (txtStartDate.text! + " " + txtStartTime.text!)
        parameters["EventEndDate"] = (txtEndDate.text! + " " + txtEndTime.text!)
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editEventURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.webserviceCall()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[0] as! EventDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    @objc func btnDeleteEvent()
    {
        self.view.viewWithTag(1001)?.removeFromSuperview()
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Event",
                          "What":User.instance.eventID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    @objc func btnEditEvent()
    {
        self.view.viewWithTag(1001)?.removeFromSuperview()
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
    }
    @objc func btnCancelMenu()
    {
        self.btnCancel.isHidden = true
        self.viewBottomContainer.isHidden = false
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 5
        {
            return eventTypeArr.count + 1
        }
        else if textfieldTag == 9
        {
            return eventPrioritiesArr.count + 1
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 5
        {
            return row == 0 ? "--None--" : eventTypeArr[row-1].EventTypeName
        }
        else if textfieldTag == 9
        {
            return row == 0 ? "--None--" : eventPrioritiesArr[row-1].Priority
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 5
        {
            if row == 0
            {
                txtEventType.text = ""
            }
            else
            {
                txtEventType.text = eventTypeArr[row-1].EventTypeName
                typeID = eventTypeArr[row-1].EventTypeID
            }
        }
        else if textfieldTag == 9
        {
            if row == 0
            {
                txtEventPriority.text = ""
            }
            else
            {
                txtEventPriority.text = eventPrioritiesArr[row-1].Priority
                priorityID = eventPrioritiesArr[row-1].EventPriorityID
            }
        }
        
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtAssignedTo || textField == txtEventType || textField == txtEventPriority || textField == txtContact
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAssignedTo.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtStartDate.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtEndDate.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtEventType.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtContact.becomeFirstResponder()
        }
        else if textField.tag == 8
        {
            txtEventPriority.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtEventPriority.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 9
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        pickerView.reloadAllComponents()
    }
    
    //MARK: Textview Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description"
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: UIScrollViewDelegate
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
    }
    
    //MARK: IBAction
    
    @IBAction func txtStartDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.tag = 1
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndDate), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtEndDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 2
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndDate), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtStartTime(_ sender: Any) {
        let datePicker = UIDatePicker()
        datePicker.tag = 1
        datePicker.datePickerMode = .time
        txtStartTime.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndTime), for: .valueChanged)
    }
    
    @IBAction func txtEndTime(_ sender: Any) {
        let datePicker = UIDatePicker()
        datePicker.tag = 2
        datePicker.datePickerMode = .time
        txtEndTime.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndTime), for: .valueChanged)
    }
    
    
    
    
    
    
    @objc func datePickerValueChangedForStartEndDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        if sender.tag == 1 {
            txtStartDate.text = dateFormatter.string(from: sender.date)
        } else {
            txtEndDate.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func datePickerValueChangedForStartEndTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        if sender.tag == 1 {
            txtStartTime.text = dateFormatter.string(from: sender.date)
        } else {
            txtEndTime.text = dateFormatter.string(from: sender.date)
        }
    }
    @IBAction func btnMore(_ sender: Any) {
        
        if let actionsheetView = Bundle.main.loadNibNamed("NoteActionSheet", owner: self, options: nil)?.first as? NoteActionSheet
        {
            menuView = actionsheetView
            actionsheetView.tag = 1001
            actionsheetView.btnDeleteNote.setTitle("Delete Event", for: .normal)
            actionsheetView.btnEditNote.setTitle("Edit Event", for: .normal)
            actionsheetView.btnDeleteNote.addTarget(self, action: #selector(btnDeleteEvent), for: .touchUpInside)
            actionsheetView.btnEditNote.addTarget(self, action: #selector(btnEditEvent), for: .touchUpInside)
            actionsheetView.viewBackground.giveBorderToView()
            actionsheetView.frame = self.view.bounds
            self.view.addSubview(actionsheetView)
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        webserviceCallForEditEvent()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAllDayEvent(_ sender: Any) {
        
        if (btnAllDay.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnAllDay.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isAllDay = "0"
        }
        else{
            btnAllDay.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            isAllDay = "1"
        }
    }
    
    @IBAction func btnEditEventTapped(_ sender: Any) {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
    }
    
}
