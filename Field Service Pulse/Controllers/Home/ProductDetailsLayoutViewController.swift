//
//  ProductDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 23/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ProductDetailsLayoutViewController: UIViewController {

    //MARK: Variable
    
    var productID:String?
    var relatedTo:String?
    var what:String?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblFamilyName: UILabel!
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var lblDatePurchased: UILabel!
    @IBOutlet weak var btnListPrice: UIButton!
    @IBOutlet weak var btnQuantity: UIButton!
    @IBOutlet weak var btnTax: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblProductCost: UILabel!
    @IBOutlet weak var lblListPrice: UILabel!
    @IBOutlet weak var lblDefaultQuantity: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblModDate: UILabel!
    @IBOutlet weak var lblModBy: UILabel!
    @IBOutlet weak var lblSectionTitle: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        webserviceCall()
    }
    func webserviceCall()
    {
        self.showHUD()
        
        if let vc = self.parent as? ProductDetailsViewController {
            
            productID = vc.productID
            what = vc.what
            relatedTo = vc.relatedTo
            self.lblAccountName.text = User.instance.accountNameForProductUI
            lblSectionTitle.text = vc.prouctType! + " Information"
        }
        let parameters = ["UserID":User.instance.UserID,
                          "ProductID":productID ?? "",
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":relatedTo,
                          "What":what]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.productDetails(urlString: API.productDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:ProductDetails) in
            
            if response.Result == "True"
            {
                self.lblProductName.text = response.data?.ProductName
                //self.lblAccountName.text  = response.data?.AccountName
                self.lblFamilyName.text = response.data?.ProductFamilyName
                self.txtview.text = response.data?.Description
                self.lblDatePurchased.text = response.data?.DatePurchased
                self.lblListPrice.text = (response.data?.ListPrice)?.convertToCurrencyFormat()
                self.lblQuantity.text = response.data?.QuantityUnitOfMeasure
                self.lblProductCost.text = (response.data?.ProductCost)?.convertToCurrencyFormat()
                self.lblDefaultQuantity.text = response.data?.DefaultQuantity
                self.lblModDate.text = response.data?.LastModifiedDate
                self.lblModBy.text = response.data?.LastModifiedByName
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblCreatedBy.text = response.data?.CreatedByName
                
                if response.data?.IsQuantityEditable == "1" {
                    self.btnQuantity.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                if response.data?.IsListPriceEditable == "1" {
                    self.btnListPrice.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                if response.data?.Taxable == "1" {
                    self.btnTax.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                if let vc = self.parent as? ProductDetailsViewController {
                    
                    vc.lblAccountName.text = "Account: " + User.instance.accountNameForProductUI
                    vc.lblFamilyName.text = "Family: " + (response.data?.ProductFamilyName ?? "")
                }
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }

}
