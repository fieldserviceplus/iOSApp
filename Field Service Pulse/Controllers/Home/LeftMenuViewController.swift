//
//  LeftMenuViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 01/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    private let CELL_ID = "cell_menu"
    
    private let SEGUE_LOGIN = "segueLogin"
    
    //let menuTitleArr = ["Home","Accounts","Contacts","Calendar","Work Orders","Estimates","Invoices","Files","Tasks","Locations"]
    
    let menuTitleArr = ["Home","Accounts","Contacts","Calendar","Work Orders","Estimates","Invoices","Files","Tasks"]
    
    //TODO
    //let menuTitleArr2 = ["Settings","Help","Logout"]
    let menuTitleArr2 = ["Logout"]
    
    //let menuIconsArr = ["home","account","contacts","calendar","orders","estimate","invoice","file","task","location"]
    
    let menuIconsArr = ["home","account","contacts","calendar","orders","estimate","invoice","file","task"]
    //TODO
    //let menuIconsArr2 = ["settings","help","logout"]
    let menuIconsArr2 = ["logout"]
    //MARK: IBOutlets
    
    @IBOutlet weak var tableLeftMenu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        self.tableLeftMenu.tableFooterView = UIView()
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            tableLeftMenu.rowHeight = User.instance.screenHeight * 0.060
        } else { //IPAD
            tableLeftMenu.rowHeight = User.instance.screenHeight * 0.060
        }
    }
    
    //MARK: Tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return menuTitleArr.count
        }
        else
        {
            return menuTitleArr2.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! LeftMenuTableViewCell
        if indexPath.section == 0
        {
            
            cell.lblTitle.text = menuTitleArr[indexPath.row] as? String
            cell.imgview.image = UIImage.init(named: menuIconsArr[indexPath.row])
        }
        else
        {
            cell.lblTitle.text = menuTitleArr2[indexPath.row] as? String
            cell.imgview.image = UIImage.init(named: menuIconsArr2[indexPath.row])
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 1
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecenetAccountsViewController") as! RecenetAccountsViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 2
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentContactsViewController") as! RecentContactsViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 3
            {
                let calendarViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
                let calendarNavController = UINavigationController(rootViewController: calendarViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = calendarNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 4
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentWorkOrderViewController") as! RecentWorkOrderViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
                
            else if indexPath.row == 5
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentEstimateViewController") as! RecentEstimateViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 6
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentInvoiceViewController") as! RecentInvoiceViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 7
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentFilesViewController") as! RecentFilesViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            else if indexPath.row == 8
            {
                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentTasksViewController") as! RecentTasksViewController
                let centerNavController = UINavigationController(rootViewController: centerViewController)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = centerNavController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
//            else if indexPath.row == 9
//            {
//                let centerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecentLocationsViewController") as! RecentLocationsViewController
//                let centerNavController = UINavigationController(rootViewController: centerViewController)
//                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.centerContainer!.centerViewController = centerNavController
//                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
//            }
        }
        else
        {
            //TODO
            //if indexPath.row == 2
            if indexPath.row == 0
            {
                Helper.instance.saveToUserDefaults(key: "userID", value: "")
                Helper.instance.saveToUserDefaults(key: "token", value: "")
                Helper.instance.saveToUserDefaults(key: "OrganizationID", value: "")
                self.performSegue(withIdentifier: SEGUE_LOGIN, sender: self)
                
                let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDel.centerContainer?.closeDrawer(animated: false, completion: nil)
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        if (section == 1) {
            headerView.backgroundColor = UIColor.lightGray
        }
        return headerView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
