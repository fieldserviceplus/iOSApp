//
//  Browse.swift
//  Field Service Pulse
//
//  Created by Apple on 23/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class Browse: UIView {

    //MARK: IBOutlet
    @IBOutlet weak var btnAttachFile: UIButton!
    @IBOutlet weak var btnTakePhotos: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewBackgrond: UIView!
    
    
    override func awakeFromNib() {
        
        viewBackgrond.giveBorderToView()
        btnAttachFile.leftImage(image: #imageLiteral(resourceName: "attachment"), renderMode: .alwaysOriginal)
        btnGallery.leftImage(image: #imageLiteral(resourceName: "gallery"), renderMode: .alwaysOriginal)
        btnTakePhotos.leftImage(image: #imageLiteral(resourceName: "camera"), renderMode: .alwaysOriginal)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    

}
