//
//  EmailViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 26/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import RichEditorView
import Alamofire
import SVProgressHUD

class EmailViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate {

    //MARK: Variables
    
    var textviewText:String?
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    var customViewCopy:EmailView?
    var customViewInsertLinkViewCopy:InsertLinkView?
    var cico:URL?
    var pickerView = UIPickerView()
    var usersDataArr:[UsersData]?
    var emailTemplatesArr:[GetEmailTemplatesData]?
    var textfieldTag:Int?
    var tempID:String?
    var strRelatedTo:String?
    var strWhat:String?
    var URLs: [URL]?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var editorView: RichEditorView!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtCC: UITextField!
    @IBOutlet weak var txtBCC: UITextField!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var heightConstraintCC: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintBCC: NSLayoutConstraint!
    @IBOutlet weak var btnCC: UIButton!
    @IBOutlet weak var btnBCC: UIButton!
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var txtFromEmail: UITextField!
    @IBOutlet weak var txtTemplate: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    //MARK: Function
    
    func setupUI()
    {
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        editorView.placeholder = "Type Here..."
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        var options = toolbar.options
        options.append(item)
        toolbar.options = options
        
        heightConstraintCC.constant = 0
        heightConstraintBCC.constant = 0
        btnBCC.setTitle("", for: .normal)
        editorView.giveBorderToView()
        self.hideKeyboardWhenTappedAround()
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFromEmail.inputView = pickerView
        txtFromEmail.setRightImage(name: "down-arrow_black", placeholder: "--From--")
        txtTemplate.inputView = pickerView
        txtTemplate.setRightImage(name: "down-arrow_black", placeholder: "--Choose Template--")
        btnSend.giveCornerRadius()
        btnCancel.giveCornerRadius()
        webserviceCallForGetAllUsers()
        webserviceCallForGetEmailTemplates()
    }
    
    
    func API_sendEmail() {
        
        if txtFrom.text == "" || txtFromEmail.text == "" || txtTo.text == "" || txtSubject.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        self.showHUD()
        
        var parameters:[String:String] = [:]
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = strRelatedTo
        parameters["What"] = strWhat ?? ""
        parameters["FromName"] = txtFrom.text ?? ""
        parameters["EmailSubject"] = txtSubject.text ?? ""
        parameters["FromEmailID"] = txtFromEmail.text ?? ""
        parameters["ToEmailID"] = txtTo.text ?? ""
        
        parameters["EmailBody"] = textviewText ?? ""
        parameters["EmailTemplate"] = tempID ?? ""
        parameters["CCEmailID"] = txtCC.text ?? ""
        parameters["BCCEmailID"] = txtBCC.text ?? ""
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        if (URLs?.count ?? 0) > 0 {
            
            NetworkManager.sharedInstance.uploadDocs(urlString: API.sendEmailURL, fileName: "EmailAttachment", URLs: URLs! as [NSURL], parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
                
                if response.Result == "True"
                {
                    Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                }
                else
                {
                    Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                }
            }
        } else {
            
            NetworkManager.sharedInstance.webserviceCall(urlString: API.sendEmailURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
                
                if response.Result == "True"
                {
                    
                    Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                }
                else
                {
                    Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                }
            }
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.usersDataArr = response.UsersData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetEmailTemplates()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEmailTemplates(urlString: API.getEmailTemplatesURL, parameters: parameters , headers: headers, vc: self) { (response:GetEmailTemplates) in
            
            if response.Result == "True"
            {
                
                self.emailTemplatesArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 2
        {
            return usersDataArr!.count + 1
        }
        else if textfieldTag == 6
        {
            return emailTemplatesArr!.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 2
        {
            return row == 0 ? "--None--":usersDataArr![row-1].Email
        }
        else if textfieldTag == 6
        {
            return row == 0 ? "--None--":emailTemplatesArr![row-1].Title
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 2
        {
            if row == 0
            {
                txtFromEmail.text = ""
            }
            else{
                txtFromEmail.text = usersDataArr![row-1].Email
            }
        }
        else if textfieldTag == 6
        {
            if row == 0
            {
                txtTemplate.text = ""
                tempID = ""
            }
            else{
                txtTemplate.text = emailTemplatesArr![row-1].Title
                tempID = emailTemplatesArr![row-1].EmailTemplateID
            }
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtFromEmail.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtTo.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtCC.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtBCC.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtTemplate.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtSubject.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtSubject.resignFirstResponder()
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        
        pickerView.reloadAllComponents()
    }
    
    //MARK: IBAction
    
    @IBAction func btnAttchFile(_ sender: Any) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: false) {
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = true
            }
        }
    }
    
    @IBAction func btnSend(_ sender: Any) {
        API_sendEmail()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCC(_ sender: Any) {
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            heightConstraintCC.constant = 40
        } else { //IPAD
            heightConstraintCC.constant = 60
        }
        
        txtCC.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        txtCC.layer.borderWidth = 1.0
        btnBCC.setTitle("BCC", for: .normal)
        btnCC.isEnabled = false
    }
    
    @IBAction func btnBCC(_ sender: Any) {
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            heightConstraintBCC.constant = 40
        } else { //IPAD
            heightConstraintBCC.constant = 60
        }
        txtBCC.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        txtBCC.layer.borderWidth = 1.0
        btnBCC.isEnabled = false
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension EmailViewController: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
            textviewText = ""
        } else {
            textviewText = content
            print(textviewText)
        }
    }
}

extension EmailViewController: RichEditorToolbarDelegate {
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        
        self.view.endEditing(true)
        if let customView = Bundle.main.loadNibNamed("EmailView", owner: self, options: nil)?.first as? EmailView {
            
           customView.tag = 101
           customViewCopy = customView
            customView.btnDone.addTarget(self, action: #selector(btnDoneInsertImage), for: .touchUpInside)
            customView.frame = self.view.frame
            self.view.addSubview(customView)
        }
    }
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        
        self.view.endEditing(true)
        if let customView = Bundle.main.loadNibNamed("InsertLinkView", owner: self, options: nil)?.first as? InsertLinkView {
            
            customView.tag = 102
            customViewInsertLinkViewCopy = customView
            customView.btnDone.addTarget(self, action: #selector(btnDoneInsertLink), for: .touchUpInside)
            customView.frame = self.view.frame
            self.view.addSubview(customView)
        }
    }
    
    @objc func btnDoneInsertImage()
    {
        if !((customViewCopy?.txtUrl.text)?.isValidURL)! {
            
            Helper.instance.showAlertNotification(message: Message.enterValidUrl, vc: self)
            return
        }
        self.view.viewWithTag(101)?.removeFromSuperview()
        //toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
        toolbar.editor?.insertImage((customViewCopy?.txtUrl.text ?? "")!, alt: (customViewCopy?.txtTitle.text)!)
    }
    
    @objc func btnDoneInsertLink()
    {
//        if !((customViewInsertLinkViewCopy?.txtUrl.text)?.isValidURL)! {
//
//            Helper.instance.showAlertNotification(message: Message.enterValidUrl, vc: self)
//            return
//        }
        
        if toolbar.editor?.hasRangeSelection == true {
            toolbar.editor?.insertLink((customViewInsertLinkViewCopy?.txtUrl.text ?? "")!, title: "")
        }
        self.view.viewWithTag(102)?.removeFromSuperview()
    }
}

extension EmailViewController: UIDocumentPickerDelegate{
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        URLs = urls
    }
}

