//
//  SearchAccountViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 31/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol searchDelegate {
    
    func sendData(searchVC:SearchAccountViewController)
}
class SearchAccountViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: Variables
    var accountTypeArr:[GetAccountTypesData] = []
    var filteredArray:[GetAccountTypesData] = []
    var isSeaching = false
    var accountID = ""
    var accountName = ""
    var delegate:searchDelegate!
    
    var contactsArr:[ContactsData] = []
    var contactsFilteredArr:[ContactsData] = []
    var contactID = ""
    var contactName = ""
    var objectFlag = ""
    var allUsersArr:[UsersData] = []
    var filteredAllUsersArr:[UsersData] = []
    var fullName = ""
    var userID = ""
    var relatedContactsArr:[AccountRelatedContactsData] = []
    var relatedContactFilteredArray:[AccountRelatedContactsData] = []
    var relatedContactID = ""
    var relatedContactName = ""
    var allOrdersArr:[AllOrdersData]? = []
    var filteredAllOrdersArr:[AllOrdersData]? = []
    var parentWO_Name = ""
    var parentWO_ID = ""
    
    
    //MARK: IBOutlet
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if objectFlag == "Contact" {
            webserviceCallForContacts()
            lblHeaderTitle.text = "Search Contact"
            searchBar.placeholder = "Search Contact"
        } else if objectFlag == "Account" {
            webserviceCallForAccountTypes()
            lblHeaderTitle.text = "Search Account"
            searchBar.placeholder = "Search Account"
        }  else if objectFlag == "Owner" {
            webserviceCallForGetAllUsers()
            lblHeaderTitle.text = "Search User"
            searchBar.placeholder = "Search User"
        } else if objectFlag == "WorkOrder" {
            webserviceCallForAllOrders()
            lblHeaderTitle.text = "Search Work Order"
            searchBar.placeholder = "Search Work Order"
        }
        else if objectFlag == "RelatedContact" {
            webserviceCallForAccountRelatedContacts()
            lblHeaderTitle.text = "Search Contact"
            searchBar.placeholder = "Search Contact"
        }
        self.table.tableFooterView = UIView()
    }
    
    //MARK: Function
    
    func webserviceCallForAccountTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAccountTypes(urlString: API.getAccountTypesURL, parameters: parameters , headers: headers, vc: self) { (response:GetAccountTypesResponse) in
            
            if response.Result == "True"
            {
                
                self.accountTypeArr = response.GetAccountTypesData!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForContacts()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contacts(urlString: API.allContactsURL, parameters: parameters , headers: headers, vc: self) { (response:ContactsResponse) in
            
            if response.Result == "True"
            {
                
                self.contactsArr = response.ContactsData!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.allUsersArr = response.UsersData!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForAccountRelatedContacts()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":User.instance.accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedContacts(urlString: API.accountRelatedContactURL, parameters: parameters , headers: headers, vc: self) { (response:AccountRelatedContactsResponse) in
            
            if response.Result == "True"
            {
                
                self.relatedContactsArr = response.data!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForAllOrders()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.allOrders(urlString: API.allOrdersURL, parameters: parameters, headers: headers, vc: self) { (response:AllOrdersResponse) in
            
            if response.Result == "True"
            {
                
                self.allOrdersArr = response.data
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if objectFlag == "Contact"
        {
            if isSeaching
            {
                return contactsFilteredArr.count
            }
            return contactsArr.count
        }
        else if objectFlag == "Account"{
            if isSeaching
            {
                return filteredArray.count
            }
            return accountTypeArr.count
        }
        else if objectFlag == "Owner"{
            if isSeaching
            {
                return filteredAllUsersArr.count
            }
            return allUsersArr.count
        }
        else if objectFlag == "WorkOrder"{
            if isSeaching
            {
                return filteredAllOrdersArr!.count
            }
            return allOrdersArr!.count
        }
        else if objectFlag == "RelatedContact"{
            if isSeaching
            {
                return relatedContactFilteredArray.count
            }
            return relatedContactsArr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "id")
        
        if objectFlag == "Contact" {
            
            if isSeaching
            {
                cell.textLabel?.text = contactsFilteredArr[indexPath.row].FullName
            }
            else
            {
                cell.textLabel?.text = contactsArr[indexPath.row].FullName
            }
        } else if objectFlag == "Account" {
            if isSeaching
            {
                cell.textLabel?.text = filteredArray[indexPath.row].AccountName
            }
            else
            {
                cell.textLabel?.text = accountTypeArr[indexPath.row].AccountName
            }
        }
        else if objectFlag == "Owner" {
            if isSeaching
            {
                cell.textLabel?.text = filteredAllUsersArr[indexPath.row].FullName
            }
            else
            {
                cell.textLabel?.text = allUsersArr[indexPath.row].FullName
            }
        }
        else if objectFlag == "WorkOrder" {
            if isSeaching
            {
                cell.textLabel?.text = filteredAllOrdersArr?[indexPath.row].Subject
            }
            else
            {
                cell.textLabel?.text = (allOrdersArr?[indexPath.row].WorkOrderNo ?? "") + " - " + (allOrdersArr?[indexPath.row].Subject ?? "")
            }
        }
        else if objectFlag == "RelatedContact" {
            if isSeaching
            {
                cell.textLabel?.text = relatedContactFilteredArray[indexPath.row].FullName
            }
            else
            {
                cell.textLabel?.text = relatedContactsArr[indexPath.row].FullName
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if objectFlag == "Contact" {
            if isSeaching
            {
                contactID = contactsFilteredArr[indexPath.row].ContactID!
                contactName = contactsFilteredArr[indexPath.row].FullName!
            }
            else
            {
                contactID = contactsArr[indexPath.row].ContactID!
                contactName = contactsArr[indexPath.row].FullName!
            }
        } else if objectFlag == "Account" {
            if isSeaching
            {
                accountID = filteredArray[indexPath.row].AccountID!
                accountName = filteredArray[indexPath.row].AccountName!
            }
            else
            {
                accountID = accountTypeArr[indexPath.row].AccountID!
                accountName = accountTypeArr[indexPath.row].AccountName!
            }
        } else if objectFlag == "Owner" {
            if isSeaching
            {
                userID = filteredAllUsersArr[indexPath.row].UserID!
                fullName = filteredAllUsersArr[indexPath.row].FullName!
            }
            else
            {
                userID = allUsersArr[indexPath.row].UserID!
                fullName = allUsersArr[indexPath.row].FullName!
            }
        } else if objectFlag == "WorkOrder" {
            if isSeaching
            {
                parentWO_ID = (filteredAllOrdersArr?[indexPath.row].WorkOrderID)!
                parentWO_Name = (filteredAllOrdersArr?[indexPath.row].Subject)!
            }
            else
            {
                parentWO_ID = allOrdersArr![indexPath.row].WorkOrderID!
                parentWO_Name = allOrdersArr![indexPath.row].Subject!
            }
        } else if objectFlag == "RelatedContact" {
            if isSeaching
            {
                relatedContactID = relatedContactFilteredArray[indexPath.row].ContactID!
                relatedContactName = relatedContactFilteredArray[indexPath.row].FullName!
            }
            else
            {
                relatedContactID = relatedContactsArr[indexPath.row].ContactID!
                relatedContactName = relatedContactsArr[indexPath.row].FullName!
            }
        }
        
        
        self.navigationController?.popViewController(animated: true)
        delegate.sendData(searchVC: self)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if objectFlag == "Contact" {
            
            contactsFilteredArr = contactsArr.filter({ (text) -> Bool in
                let tmp: NSString = text.FullName! as NSString
                let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(contactsFilteredArr.count == 0){
                isSeaching = false;
            } else {
                isSeaching = true;
            }
        } else if objectFlag == "Account" {
            
            filteredArray = accountTypeArr.filter({ (text) -> Bool in
                let tmp: NSString = text.AccountName! as NSString
                let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(filteredArray.count == 0){
                isSeaching = false;
            } else {
                isSeaching = true;
            }
        }
        else if objectFlag == "Owner" {
            
            filteredAllUsersArr = allUsersArr.filter({ (text) -> Bool in
                let tmp: NSString = text.FullName! as NSString
                let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(filteredAllUsersArr.count == 0){
                isSeaching = false;
            } else {
                isSeaching = true;
            }
        }
        else if objectFlag == "WorkOrder" {
            
            filteredAllOrdersArr = allOrdersArr?.filter({ (text) -> Bool in
                let tmp: NSString = text.Subject! as NSString
                let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(filteredAllOrdersArr?.count == 0){
                isSeaching = false;
            } else {
                isSeaching = true;
            }
        }
        else if objectFlag == "RelatedContact" {
            
            relatedContactFilteredArray = relatedContactsArr.filter({ (text) -> Bool in
                let tmp: NSString = text.FullName! as NSString
                let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(relatedContactFilteredArray.count == 0){
                isSeaching = false;
            } else {
                isSeaching = true;
            }
        }
        self.table.reloadData()
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
