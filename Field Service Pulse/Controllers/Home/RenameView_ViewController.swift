//
//  RenameView_ViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 29/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RenameView_ViewController: UIViewController {

    //MARK: Variables
    
    var object:String?
    var ViewID:String?
    
    //MARK: IBOutlet
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtViewName: AkiraTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webserviceCallForGetCustomViewDetails()
        setupUI()
    }
    
    func setupUI() {
        
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
    }
    
    //MARK: API Call
    
    func  webserviceCallForRenameCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object!,
                          "ViewID":ViewID!,
                          "ViewName":self.txtViewName.text ?? ""] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.renameCustomViewURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotificationWithBack(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetCustomViewDetails()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object,
                          "ViewID":ViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.getCustomViewDetails(urlString: API.getCustomViewDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:GetCustomViewDetails) in
            
            if response.Result == "True"
            {
                self.txtViewName.text = response.data?.ViewName
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSave(_ sender: Any) {
        webserviceCallForRenameCustomView()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
