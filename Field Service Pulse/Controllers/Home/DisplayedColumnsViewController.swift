//
//  DisplayedColumnsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 29/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DisplayedColumnsViewController: UIViewController, UITextFieldDelegate {

    //MARK: Variable
    
    private let CELL_DISPLAYED_COLUMNS = "cellDisplayedColumns"
    
    var picker = UIPickerView()
    var pickerInputField: UITextField!
    var objectFieldsArr:[AccountFieldsData] = []
    var selectedFieldsArr:[String] = []
    var strFieldName = ""
    var object:String?
    var ViewID = ""
    
    var flagSpecifyAction:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddField: UIButton!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var tableDisplayedColumns: UITableView!
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            tableDisplayedColumns.rowHeight = 60
        } else { //IPAD
            tableDisplayedColumns.rowHeight = 80
        }
        webserviceCallForGetFields()
        if flagSpecifyAction == "Edit" {
            webserviceCallForGetCustomViewDetails()
        }
        pickerKeyboard()
        setupUI()
    }
    
    //MARK: Function
    
    func setupUI()
    {
        tableDisplayedColumns.tableFooterView = UIView()
        picker.dataSource = self
        picker.delegate = self
        viewBackground.giveBorderToView()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnAddField.giveCornerRadius()
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        //self.tableDisplayedColumns.isEditing = true
        if object == "Account" {
            lblDropdown.text = " Account Fields List"
        }
        else if object == "WorkOrder" {
            lblDropdown.text = " WorkOrder Fields List"
        }
        else if object == "Task" {
            lblDropdown.text = " Task Fields List"
        }
        else if object == "File" {
            lblDropdown.text = " File Fields List"
        }
        else if object == "Estimate" {
            lblDropdown.text = " Estimate Fields List"
        }
        else if object == "Contact" {
           lblDropdown.text = " Contact Fields List"
        }
        else if object == "Invoice" {
            lblDropdown.text = " Invoice Fields List"
        }
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognized(gestureRecognizer:)))
        self.tableDisplayedColumns.addGestureRecognizer(longpress)
    }

    func pickerKeyboard()
    {
        pickerInputField = {
            let field = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            self.view.addSubview(field)
            field.delegate = self
            return field
        }()
        pickerInputField.inputView = picker
    }
    
    @objc func btnCancel(sender:UIButton) {
        
        selectedFieldsArr.remove(at: sender.tag)
        tableDisplayedColumns.reloadData()
    }
    
    //Drag & Drop Table Rows
    @objc func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        
        let longpress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longpress.state
        let locationInView = longpress.location(in: self.tableDisplayedColumns)
        var indexPath = self.tableDisplayedColumns.indexPathForRow(at: locationInView)
        
        switch state {
        case .began:
            if indexPath != nil {
                Path.initialIndexPath = indexPath
                let cell = self.tableDisplayedColumns.cellForRow(at: indexPath!) as! DisplayedColumnsTableViewCell
                My.cellSnapShot = snapshopOfCell(inputView: cell)
                var center = cell.center
                My.cellSnapShot?.center = center
                My.cellSnapShot?.alpha = 0.0
                self.tableDisplayedColumns.addSubview(My.cellSnapShot!)
                
                UIView.animate(withDuration: 0.25, animations: {
                    center.y = locationInView.y
                    My.cellSnapShot?.center = center
                    My.cellSnapShot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    My.cellSnapShot?.alpha = 0.98
                    cell.alpha = 0.0
                }, completion: { (finished) -> Void in
                    if finished {
                        cell.isHidden = true
                    }
                })
            }
            
        case .changed:
            var center = My.cellSnapShot?.center
            center?.y = locationInView.y
            My.cellSnapShot?.center = center!
            if ((indexPath != nil) && (indexPath != Path.initialIndexPath)) {
                
                self.selectedFieldsArr.swapAt((indexPath?.row)!, (Path.initialIndexPath?.row)!)
                //swap(&self.wayPoints[(indexPath?.row)!], &self.wayPoints[(Path.initialIndexPath?.row)!])
                self.tableDisplayedColumns.moveRow(at: Path.initialIndexPath!, to: indexPath!)
                Path.initialIndexPath = indexPath
            }
            
        default:
            let cell = self.tableDisplayedColumns.cellForRow(at: Path.initialIndexPath!) as! DisplayedColumnsTableViewCell
            cell.isHidden = false
            cell.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                My.cellSnapShot?.center = cell.center
                My.cellSnapShot?.transform = .identity
                My.cellSnapShot?.alpha = 0.0
                cell.alpha = 1.0
            }, completion: { (finished) -> Void in
                if finished {
                    Path.initialIndexPath = nil
                    My.cellSnapShot?.removeFromSuperview()
                    My.cellSnapShot = nil
                }
            })
        }
        
        print(selectedFieldsArr)
    }
    
    func snapshopOfCell(inputView: UIView) -> UIView {
        
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    struct My {
        static var cellSnapShot: UIView? = nil
    }
    
    struct Path {
        static var initialIndexPath: IndexPath? = nil
    }
    //MARK: API Call
    func  webserviceCallForGetFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        var url:String = ""
        if object == "Account" {
            url = API.accountFieldsURL
        }
        else if object == "WorkOrder" {
            url = API.getWOViewFieldsURL
        }
        else if object == "Task" {
            url = API.getTaskViewFieldsURL
        }
        else if object == "File" {
            url = API.getFileViewFieldsURL
        }
        else if object == "Estimate" {
            url = API.getEstimateViewFieldsURL
        }
        else if object == "Contact" {
            url = API.getContactViewFieldsURL
        }
        else if object == "Invoice" {
            url = API.getInvoiceViewFieldsURL
        }
        
        NetworkManager.sharedInstance.accountFields(urlString: url, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:AccountFields) in
            
            if response.Result == "True"
            {
                self.objectFieldsArr = response.data!
                self.tableDisplayedColumns.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForEditDisplayedColumns()
    {
        if selectedFieldsArr.count == 0 {
            return
        }
        
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object!,
                          "ViewID":ViewID,
                          "SpecifyFieldsDisplay":selectedFieldsArr] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editDisplayedColumnsURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotificationWithBack(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetCustomViewDetails()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":object,
                          "ViewID":ViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        
        NetworkManager.sharedInstance.getCustomViewDetails(urlString: API.getCustomViewDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:GetCustomViewDetails) in
            
            if response.Result == "True"
            {
                
                let displayedColumnsArr = response.data?.DisplayedColumns as! [DisplayedColumnsData]
                
                for i in 0..<displayedColumnsArr.count {
                    
                    self.selectedFieldsArr.append(displayedColumnsArr[i].FieldName!)
                    self.tableDisplayedColumns.reloadData()
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        if flagSpecifyAction == "Edit" {
            
            webserviceCallForEditDisplayedColumns()
            
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "passSelectedFields"), object: selectedFieldsArr)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnAddField(_ sender: Any) {
        if strFieldName != "" && strFieldName != "--None--" {
            selectedFieldsArr.append(strFieldName)
        }
        tableDisplayedColumns.reloadData()
    }
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        pickerInputField.becomeFirstResponder()
    }
    
}

extension DisplayedColumnsViewController:UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return selectedFieldsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_DISPLAYED_COLUMNS) as! DisplayedColumnsTableViewCell
        
        cell.lblTitle.text = selectedFieldsArr[indexPath.row]
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action: #selector(btnCancel(sender:)), for: .touchUpInside)
        return cell
    }
    
//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//
//        return .none
//    }
//
//    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
//
//        return false
//    }
//
//    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//
//        return true
//    }
//    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//
//        let movedObject = selectedFieldsArr[sourceIndexPath.row]
//        selectedFieldsArr.remove(at: sourceIndexPath.row)
//        selectedFieldsArr.insert(movedObject, at: destinationIndexPath.row)
//        print(selectedFieldsArr)
//    }
}

extension DisplayedColumnsViewController:UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
         return objectFieldsArr.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if row == 0 {
            return "--None--"
        } else {
            
            return objectFieldsArr[row-1].FieldName
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row != 0 {
        strFieldName = objectFieldsArr[row-1].FieldName!
        print(strFieldName)
        }
        
    }
}
