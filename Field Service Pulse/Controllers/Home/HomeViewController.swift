//
//  HomeViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 01/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    private let SEGUE_LOGIN_SIGNUP = "segueLoginSignUp"
    private let CELL_SCHEDULE = "cell_schedule"
    private let CELL_TASKS = "cell_tasks"
    private let CELL_RECENT_ITEMS = "cell_recentItems"
    
    var scheduleArr:[GetScheduleData] = []
    var taskArr:[GetTasksData] = []
    var recentArr:[GetRecentsData] = []
    
    var viewTasksDropdown:UIView?
    var strFilter:String = "Today"
    
    //MARK: IBOutlets
    
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var viewFirst: UIView!
    @IBOutlet weak var viewSecond: UIView!
    @IBOutlet weak var viewThird: UIView!
    
    @IBOutlet weak var tableSchedule: UITableView!
    
    @IBOutlet weak var tableTasks: UITableView!
    @IBOutlet weak var tableRecentItems: UITableView!
    @IBOutlet weak var btnTodatSchedule: UIButton!
    @IBOutlet weak var btnTodayTasks: UIButton!
    
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnWorkOrder: UIButton!
    @IBOutlet weak var btnEstimate: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var lblCreateNew: UILabel!
    @IBOutlet weak var btnFile: UIButton!
    @IBOutlet weak var lblNoRecordsRecents: UILabel!
    @IBOutlet weak var lblNoRecordsSchedule: UILabel!
    @IBOutlet weak var lblNoRecordsTasks: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForGetSchedule()
        webserviceCallForGetTask()
        webserviceCallForGetRecent()
    }

    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        viewDropdown.dropShadow()
        viewFirst.giveBorderToView()
        viewSecond.giveBorderToView()
        viewThird.giveBorderToView()
        btnTodayTasks.giveBorderToButton()
        btnTodatSchedule.giveBorderToButton()
        
        btnAccount.leftImage(image: #imageLiteral(resourceName: "account"), renderMode: .alwaysOriginal)
        btnContact.leftImage(image: #imageLiteral(resourceName: "contacts"), renderMode: .alwaysOriginal)
        btnWorkOrder.leftImage(image: #imageLiteral(resourceName: "orders"), renderMode: .alwaysOriginal)
        btnEstimate.leftImage(image: #imageLiteral(resourceName: "estimate"), renderMode: .alwaysOriginal)
        btnInvoice.leftImage(image: #imageLiteral(resourceName: "invoice"), renderMode: .alwaysOriginal)
        btnTask.leftImage(image: #imageLiteral(resourceName: "task"), renderMode: .alwaysOriginal)
        btnFile.leftImage(image: #imageLiteral(resourceName: "file"), renderMode: .alwaysOriginal)
        
    }
    
    func  webserviceCallForGetSchedule()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getSchedules(urlString: API.getScheduleURL, parameters: parameters, headers: headers, vc: self) { (response:GetSchedule) in
            
            if response.Result == "True"
            {
                self.scheduleArr = response.data!
                
                if self.scheduleArr.count == 0 {
                    self.lblNoRecordsSchedule.isHidden = false
                    self.tableSchedule.isHidden = true
                } else {
                    self.lblNoRecordsSchedule.isHidden = true
                    self.tableSchedule.isHidden = false
                    self.tableSchedule.reloadData()
                }
            }
            else
            {
                if response.ResponseMsg == "You are not authorized to access associated web-services; it seems there is mismatch in key-token pair." {
                    
                    Helper.instance.saveToUserDefaults(key: "userID", value: "")
                    Helper.instance.saveToUserDefaults(key: "token", value: "")
                    Helper.instance.saveToUserDefaults(key: "OrganizationID", value: "")
                    self.performSegue(withIdentifier: self.SEGUE_LOGIN_SIGNUP, sender: self)
                    return
                }
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetTask()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Filter":strFilter]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getTasks(urlString: API.getTasksURL, parameters: parameters, headers: headers, vc: self) { (response:GetTasks) in
            
            if response.Result == "True"
            {
                self.taskArr = response.data!
                
                if self.taskArr.count == 0 {
                    self.lblNoRecordsTasks.isHidden = false
                    self.tableTasks.isHidden = true
                    
                } else {
                    self.lblNoRecordsTasks.isHidden = true
                    self.tableTasks.isHidden = false
                    self.tableTasks.reloadData()
                }
            }
            else
            {
                if response.ResponseMsg == "You are not authorized to access associated web-services; it seems there is mismatch in key-token pair." {
                    
                    return
                }
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetRecent()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getRecents(urlString: API.getRecentsURL, parameters: parameters, headers: headers, vc: self) { (response:GetRecents) in
            
            if response.Result == "True"
            {
                self.recentArr = response.data!
                if self.recentArr.count == 0 {
                    
                    self.lblNoRecordsRecents.isHidden = false
                    self.tableRecentItems.isHidden = true
                    
                } else {
                    self.lblNoRecordsRecents.isHidden = true
                    self.tableRecentItems.isHidden = false
                    self.tableRecentItems.reloadData()
                }
            }
            else
            {
                if response.ResponseMsg == "You are not authorized to access associated web-services; it seems there is mismatch in key-token pair." {
                    
                    return
                }
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view != viewTasksDropdown {
            
            self.viewTasksDropdown?.removeFromSuperview()
            viewTasksDropdown = nil
        }
    }
    
    @objc func btnTasksDropdown(sender:UIButton) {
        
        if sender.tag == 1 {
            btnTodayTasks.setTitle("Today", for: .normal)
        } else if sender.tag == 2 {
            btnTodayTasks.setTitle("Today + Overdue", for: .normal)
        } else if sender.tag == 3 {
            btnTodayTasks.setTitle("Overdue", for: .normal)
        }
        strFilter = sender.accessibilityLabel!
        
        viewTasksDropdown?.removeFromSuperview()
        viewTasksDropdown = nil
        webserviceCallForGetTask()
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableSchedule {
            return scheduleArr.count
        }
        else if tableView == tableTasks {
            return taskArr.count
        }
        else if tableView == tableRecentItems {
            return recentArr.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableSchedule
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: CELL_SCHEDULE) as! ScheduleTableViewCell
            cell1.view_back.dropShadow()
            cell1.lblObject.text = (scheduleArr[indexPath.row].RelatedTo ?? "") + " " + (scheduleArr[indexPath.row].RelatedObjNo ?? "")
            cell1.lblTime.text = scheduleArr[indexPath.row].Time
            cell1.lblAddress.text = scheduleArr[indexPath.row].Address
            cell1.lblSubject.text = scheduleArr[indexPath.row].Subject
            
            return cell1
        }
        else if tableView == tableTasks
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: CELL_TASKS) as! TasksTableViewCell
            cell2.view_back.dropShadow()
            cell2.lblObject.text = (taskArr[indexPath.row].RelatedTo ?? "") + " " + (taskArr[indexPath.row].RelatedObjNo ?? "")
            cell2.lblSubject.text = taskArr[indexPath.row].Subject
            cell2.lblDate.text = taskArr[indexPath.row].Date
            
            return cell2
        }
        else if tableView == tableRecentItems
        {
            let cell3 = tableView.dequeueReusableCell(withIdentifier: CELL_RECENT_ITEMS) as! RecentItemsTableViewCell
            cell3.view_back.dropShadow()
            cell3.lblName.text = recentArr[indexPath.row].Name ?? ""
            return cell3
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableSchedule
        {
            let fileVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            fileVC.navigationController?.isNavigationBarHidden = true
            User.instance.eventID = scheduleArr[indexPath.row].EventID!
            self.navigationController?.pushViewController(fileVC, animated: true)
        }
        else if tableView == tableTasks
        {
            let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
            taskVC.navigationController?.isNavigationBarHidden = true
            User.instance.taskID = taskArr[indexPath.row].TaskID!
            self.navigationController?.pushViewController(taskVC, animated: true)
        }
        else if tableView == tableRecentItems
        {
            if recentArr[indexPath.row].Object == "Account" {
                
                let acVC = UIStoryboard(name: "Account", bundle:  Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController") as! AccountDetailsViewController
                acVC.navigationController?.isNavigationBarHidden = true
                User.instance.accountID = recentArr[indexPath.row].ID!
                User.instance.accountName = recentArr[indexPath.row].Name!
                self.navigationController?.pushViewController(acVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "WorkOrder" {
                
                let woVC = UIStoryboard(name: "WorkOrder", bundle:  Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
                woVC.navigationController?.isNavigationBarHidden = true
                User.instance.workorderID = recentArr[indexPath.row].ID!
                User.instance.workorderSubject = recentArr[indexPath.row].Name!
                self.navigationController?.pushViewController(woVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "Contact" {
                
                let contactVC = UIStoryboard(name: "Contact", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
                contactVC.navigationController?.isNavigationBarHidden = true
                User.instance.contactID = recentArr[indexPath.row].ID!
                User.instance.contactName = recentArr[indexPath.row].Name!
                self.navigationController?.pushViewController(contactVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "Estimate" {
                
                let estimateVC = UIStoryboard(name: "Estimate", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController") as! EstimateDetailsViewController
                estimateVC.navigationController?.isNavigationBarHidden = true
                User.instance.estimateID = recentArr[indexPath.row].ID!
                User.instance.estimateName = recentArr[indexPath.row].Name!
                self.navigationController?.pushViewController(estimateVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "Invoice" {
                
                let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
                invoiceVC.navigationController?.isNavigationBarHidden = true
                User.instance.invoiceID = recentArr[indexPath.row].ID!
                User.instance.invoiceNo = recentArr[indexPath.row].Name!
                self.navigationController?.pushViewController(invoiceVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "Task" {
                
                let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                taskVC.navigationController?.isNavigationBarHidden = true
                User.instance.taskID = recentArr[indexPath.row].ID!
                self.navigationController?.pushViewController(taskVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "File" {
                
                let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.fileID = recentArr[indexPath.row].ID!
                self.navigationController?.pushViewController(fileVC, animated: true)
                
            } else if recentArr[indexPath.row].Object == "Event" {
                
                let fileVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.eventID = recentArr[indexPath.row].ID!
                self.navigationController?.pushViewController(fileVC, animated: true)
                
            }
            
           
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableSchedule
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 62
            } else { //IPAD
                return 85
            }
        }
        else if tableView == tableTasks
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 38
            } else { //IPAD
                return 55
            }
           
        }
        else if tableView == tableRecentItems
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 30
            } else { //IPAD
                return 60
            }
        }
        return 0
    }
    
    //MARK: IBActions
    
    @IBAction func btnOpenLeftMenu(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if self.viewDropdown.isHidden == true
        {
            UIView.transition(with: viewDropdown, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.viewDropdown.isHidden = false
            })
        }
        else
        {
            UIView.transition(with: viewDropdown, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.viewDropdown.isHidden = true
            })
        }
    }
    
    @IBAction func btnAccount(_ sender: Any) {
        
        let acVC = UIStoryboard(name: "Account", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        acVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(acVC, animated: true)
    }
    
    @IBAction func btnContact(_ sender: Any) {
        let contactVC = UIStoryboard(name: "Contact", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateContactViewController") as! CreateContactViewController
        contactVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    @IBAction func btnWO(_ sender: Any) {
        let woVC = UIStoryboard(name: "WorkOrder", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateWOViewController") as! CreateWOViewController
        woVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(woVC, animated: true)
    }
    
    @IBAction func btnEstimate(_ sender: Any) {
        let estimateVC = UIStoryboard(name: "Estimate", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateEstimateViewController") as! CreateEstimateViewController
        estimateVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(estimateVC, animated: true)
    }
    
    @IBAction func btnInvoice(_ sender: Any) {
        let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as! CreateInvoiceViewController
        invoiceVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(invoiceVC, animated: true)
    }
    
    @IBAction func btnTask(_ sender: Any) {
        let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
        taskVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(taskVC, animated: true)
    }
    
    @IBAction func btnFile(_ sender: Any) {
        let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "CreateFileViewController") as! CreateFileViewController
        fileVC.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(fileVC, animated: true)
    }
    
    @IBAction func btnTasksDropdown(_ sender: Any) {
        
        if viewTasksDropdown == nil {
            viewTasksDropdown = UIView()
            viewTasksDropdown?.dropShadow()
            self.view.addSubview(viewTasksDropdown!)
            
            viewTasksDropdown?.translatesAutoresizingMaskIntoConstraints = false
            
            let topConstraint = NSLayoutConstraint(item: viewTasksDropdown!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: btnTodayTasks, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 10)
            let heightConstraint = NSLayoutConstraint(item: viewTasksDropdown!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 120)
            let widthConstraint = NSLayoutConstraint(item: viewTasksDropdown!, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 150)
            let trailingConstraint = NSLayoutConstraint(item: viewTasksDropdown!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: btnTodayTasks, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0.0)
            view.addConstraints([topConstraint,heightConstraint,widthConstraint,trailingConstraint])
            
            if let customView = Bundle.main.loadNibNamed("DropdownTasks", owner: self, options: nil)?.first as? DropdownTasks {
                
                customView.btnToday.addTarget(self, action: #selector(btnTasksDropdown(sender:)), for: .touchUpInside)
                customView.btnTodayOverdue.addTarget(self, action: #selector(btnTasksDropdown(sender:)), for: .touchUpInside)
                customView.btnOverdue.addTarget(self, action: #selector(btnTasksDropdown(sender:)), for: .touchUpInside)
                customView.frame = (viewTasksDropdown?.frame)!
                viewTasksDropdown?.addSubview(customView)
            }
        } else {
            
            viewTasksDropdown?.removeFromSuperview()
            viewTasksDropdown = nil
        }
    }
    
    @IBAction func btnTodaySchedule(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Calendar", bundle: Bundle.main).instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
