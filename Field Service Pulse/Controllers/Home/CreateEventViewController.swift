//
//  CreateEventViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 03/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CreateEventViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, searchDelegate {
    
    //MARK: Variables
    
    private let SEGUE_RECURRING = "segueRecurrence"
    var assignedToID:String?
    var contactID:String?
    var typeID:String?
    var priorityID:String?
    
    var pickerView = UIPickerView()
    
    var eventTypeArr:[GetEventTypesData] = []
    var eventPrioritiesArr:[GetEventPrioritiesData] = []
    var contactsArr:[ContactsData] = []
    
    var textfieldTag = 0
    
    var isRecurring:String?
    var isAllDay:String?
    var response:accountDetailsResponse?
    var relatedTo:String?
    var objectID:String?
    
    var Ends =  ""
    var IntervalEvery = ""
    var RepeatEvery = ""
    var EndsOnDate = ""
    var EndsAfterOccurrences = ""
    var StartTime = ""
    var EndTime = ""
    var RepeatOn = ""
    var StartOn = ""
    
    //MARK: IBOutlets
    
    //UITextfield
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtPriority: UITextField!
    @IBOutlet weak var txtStartDate: AkiraTextField!
    @IBOutlet weak var txtEndDate: AkiraTextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtStartTime: AkiraTextField!
    @IBOutlet weak var txtEndTime: AkiraTextField!
    
    
    //UIButton
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAllDay: UIButton!
    @IBOutlet weak var btnIsRecurring: UIButton!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        webserviceCallForGetEventPriorities()
        webserviceCallForGetEventTypes()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        
        txtType.inputView = pickerView
        txtPriority.inputView = pickerView
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
       
        self.txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtPriority.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtName.setRightImage(name: "search_small", placeholder: "--None--")
        
        self.txtviewDescription.layer.borderWidth = 1.5
        self.txtviewDescription.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC: SearchAccountViewController) {
        
        if textfieldTag == 1 {
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        } else if textfieldTag == 7 {
            txtName.text = searchVC.contactName
            contactID = searchVC.contactID
        }
    }
    
    func webserviceCall()
    {
        if assignedToID == nil || typeID == nil || priorityID == nil || relatedTo == nil || objectID == nil || txtSubject.text! == "" || txtStartDate.text! == "" || txtEndDate.text! == "" || txtStartTime.text! == "" || txtEndTime.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["AssignedTo"] = assignedToID
        parameters["Subject"] = txtSubject.text!
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["RelatedTo"] =  relatedTo
        parameters["What"] = objectID
        parameters["Who"] = contactID ?? ""
        parameters["EventPriority"] = priorityID
        parameters["EventType"] = typeID
        parameters["EventStartDate"] = txtStartDate.text! + " " + txtStartTime.text!
        parameters["EventEndDate"] = txtEndDate.text! + " " + txtEndTime.text!
        parameters["IsRecurring"] = isRecurring ?? "0"
        parameters["IsAllDayEvent"] = isAllDay ?? "0"
        
        parameters["StartOn"] = StartOn
        parameters["RepeatEvery"] = RepeatEvery
        parameters["IntervalEvery"] = IntervalEvery
        parameters["RepeatOn"] = RepeatOn
        parameters["Ends"] = Ends
        parameters["EndsOnDate"] = EndsOnDate
        parameters["EndsAfterOccurrences"] = EndsAfterOccurrences
        parameters["StartTime"] = StartTime
        parameters["EndTime"] = EndTime
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createEvent(urlString: API.createEventURL, parameters: parameters , headers: headers, vc: self) { (response:CreateEvent) in
            
            if response.Result == "True"
            {
                self.navigationController?.popViewController(animated: true)
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetEventPriorities()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventPriorities(urlString: API.getEventPrioritiesURL, parameters: parameters , headers: headers, vc: self) { (response:GetEventPriorities) in
            
            if response.Result == "True"
            {
                self.eventPrioritiesArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetEventTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTypes(urlString: API.getEventTypesURL, parameters: parameters , headers: headers, vc: self) { (response:GetEventTypes) in
            
            if response.Result == "True"
            {
                self.eventTypeArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        if let RecurrenceDetailsVC = sender.source as? RecurrenceSettingsViewController {
            
            Ends = RecurrenceDetailsVC.ends ?? ""
            IntervalEvery = RecurrenceDetailsVC.txtInterval.text ?? ""
            RepeatEvery = RecurrenceDetailsVC.txtRepeats.text ?? ""
            EndsOnDate = RecurrenceDetailsVC.txtOn.text ?? ""
            EndsAfterOccurrences = RecurrenceDetailsVC.txtAfter.text ?? ""
            StartTime = RecurrenceDetailsVC.txtStartTime.text ?? ""
            EndTime = RecurrenceDetailsVC.txtEndTime.text ?? ""
            RepeatOn = RecurrenceDetailsVC.weekArr.joined(separator: ",")
            StartOn = RecurrenceDetailsVC.txtStartsOn.text ?? ""
        }
    }
    
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 3
        {
            return eventTypeArr.count + 1
        }
        else if textfieldTag == 4
        {
            return eventPrioritiesArr.count + 1
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 3
        {
            return row == 0 ? "--None--" : eventTypeArr[row-1].EventTypeName
        }
        else if textfieldTag == 4
        {
            return row == 0 ? "--None--" : eventPrioritiesArr[row-1].Priority
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 3
        {
            if row == 0
            {
                txtType.text = ""
            }
            else
            {
                txtType.text = eventTypeArr[row-1].EventTypeName
                typeID = eventTypeArr[row-1].EventTypeID
            }
        }
        else if textfieldTag == 4
        {
            if row == 0
            {
                txtPriority.text = ""
            }
            else
            {
                txtPriority.text = eventPrioritiesArr[row-1].Priority
                priorityID = eventPrioritiesArr[row-1].EventPriorityID
            }
        }
        
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtAssignedTo || textField == txtType || textField == txtPriority || textField == txtStartDate || textField == txtEndDate || textField == txtName || textField == txtStartTime  || textField == txtEndTime
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtSubject.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtType.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtPriority.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtStartDate.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtEndDate.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtName.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtName.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 3
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        pickerView.reloadAllComponents()
    }
    
    //MARK: Textview Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description"
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    //MARK: IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        webserviceCall()
    }
    
    @IBAction func btnIsRecurring(_ sender: Any) {
        
        if (btnIsRecurring.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnIsRecurring.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isRecurring = "0"
        }
        else{
            btnIsRecurring.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            self.performSegue(withIdentifier: SEGUE_RECURRING, sender: self)
            isRecurring = "1"
        }
    }
    
    @IBAction func btnAllDay(_ sender: Any) {
        if (btnAllDay.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnAllDay.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isAllDay = "0"
        }
        else{
            btnAllDay.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            isAllDay = "1"
        }
    }
    
    @IBAction func txtStartDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.tag = 1
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndDate), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtEndDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 2
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndDate), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtStartTime(_ sender: Any) {
        let datePicker = UIDatePicker()
        datePicker.tag = 1
        datePicker.datePickerMode = .time
        txtStartTime.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndTime), for: .valueChanged)
    }
    
    @IBAction func txtEndTime(_ sender: Any) {
        let datePicker = UIDatePicker()
        datePicker.tag = 2
        datePicker.datePickerMode = .time
        txtEndTime.inputView = datePicker
        datePicker.addTarget(self, action: #selector(self.datePickerValueChangedForStartEndTime), for: .valueChanged)
    }
    
    @objc func datePickerValueChangedForStartEndDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        if sender.tag == 1 {
            txtStartDate.text = dateFormatter.string(from: sender.date)
        } else {
            txtEndDate.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func datePickerValueChangedForStartEndTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        if sender.tag == 1 {
            txtStartTime.text = dateFormatter.string(from: sender.date)
        } else {
            txtEndTime.text = dateFormatter.string(from: sender.date)
        }
    }
}




