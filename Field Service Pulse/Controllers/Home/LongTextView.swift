//
//  LongTextView.swift
//  Field Service Pulse
//
//  Created by Apple on 15/04/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class LongTextView: UIView {

    @IBOutlet weak var txtviewLabelField: UITextView!

    override func awakeFromNib() {
        txtviewLabelField.giveBorder()
    }
}
