//
//  AccountRelatedViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 09/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AccountRelatedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    //MARK: VAriables
    
    private let CELL_ACCOUNT_RELATED = "cell_AccountRelated"
    private let CELL_SUB_ACCOUNT_RELATED = "cell_subAccountRelated"
    
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var accountID:String?
    
    var titleArr:NSMutableArray = []
    
    var contactArr:[ContactData] = []
    var workOrdersArr:[WorkOrderData] = []
    var tasksArr:[AccountRelatedTasksData] = []
    var estimateArr:[AccountRelatedEstimateData] = []
    var eventArr:[AccountRelatedEventData] = []
    var invoiceArr:[AccountRelatedInvoiceData] = []
    var fileArr:[AccountRelatedFileData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var tableRelated: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        titleArr.removeAllObjects()
        if (self.parent as? AccountDetailsViewController) != nil {

            self.accountID = User.instance.accountID
            print(self.accountID ?? "")
            webseriviceCall()
            webseriviceCallForContacts()
            webseriviceCallForWorkOrders()
            webseriviceCallForTasks()
            webseriviceCallForFile()
            webseriviceCallForEstimate()
            webseriviceCallForInvoice()
            webseriviceCallForEvent()
        }

    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        tableRelated.rowHeight = 45
        tableRelated.tableFooterView = UIView()

    }
    
    func webseriviceCall()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedList(urlString: API.accountRelatedListURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:accountRelatedListResponse?) in
            
            if response?.Result == "True"
            {
                
                self.titleArr.add(response?.data?.AccountContact?.title ?? "")
                self.titleArr.add(response?.data?.AccountWorkOrder?.title ?? "")
                self.titleArr.add(response?.data?.AccountEstimate?.title ?? "")
                self.titleArr.add(response?.data?.AccountInvoice?.title ?? "")
                self.titleArr.add(response?.data?.AccountFile?.title ?? "")
                self.titleArr.add(response?.data?.AccountEvent?.title ?? "")
                self.titleArr.add(response?.data?.AccountTask?.title ?? "")
                self.tableRelated.reloadData()
                print(self.titleArr)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForContacts()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contacts(urlString: API.contactsURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactResponse?) in
            
            if response?.Result == "True"
            {
                
                self.contactArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    
    func webseriviceCallForWorkOrders()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.workOrders(urlString: API.workOrdersURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:WorkOrderResponse?) in
            
            if response?.Result == "True"
            {
                self.workOrdersArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForTasks()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedTasks(urlString: API.accountRelatedTaskURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:AccountRelatedTasks?) in
            
            if response?.Result == "True"
            {
                self.tasksArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForEstimate()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedEstimate(urlString: API.accountRelatedEstimateURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:AccountRelatedEstimate?) in
            
            if response?.Result == "True"
            {
                self.estimateArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForInvoice()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedInvoice(urlString: API.accountRelatedInvoiceURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:AccountRelatedInvoice?) in
            
            if response?.Result == "True"
            {
                self.invoiceArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForEvent()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedEvent(urlString: API.accountRelatedEventURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:AccountRelatedEvent?) in
            
            if response?.Result == "True"
            {
                self.eventArr = (response?.data)!
                self.tableRelated.reloadData()
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForFile()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedFile(urlString: API.accountRelatedFileURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:AccountRelatedFile?) in
            
            if response?.Result == "True"
            {
                self.fileArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    
    @objc func btnViewAll(sender:UIButton)
    {
        let viewAllVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewAllViewController") as! AccountViewAllViewController
        
        if let parent = self.parent as? AccountDetailsViewController {
            
            viewAllVC.headerTitle = parent.lblHeaderTitle.text
            
        }
        
        viewAllVC.titleLabel = titleArr[(sender as AnyObject).tag] as? String
        viewAllVC.tag = (sender as AnyObject).tag
        if (sender as AnyObject).tag == 0
        {
            viewAllVC.contactArr =  contactArr
        }
        else if (sender as AnyObject).tag == 1
        {
            viewAllVC.workOrdersArr = workOrdersArr
        }
        else if (sender as AnyObject).tag == 2
        {
            viewAllVC.estimateArr = estimateArr
        }
        else if (sender as AnyObject).tag == 3
        {
            viewAllVC.invoiceArr = invoiceArr
        }
        else if (sender as AnyObject).tag == 4
        {
            viewAllVC.fileArr = fileArr
        }
        else if (sender as AnyObject).tag == 5
        {
            viewAllVC.eventArr = eventArr
        }
        else if (sender as AnyObject).tag == 6
        {
            viewAllVC.tasksArr = tasksArr
        }
        
        self.navigationController?.pushViewController(viewAllVC, animated: true)
    }
    
    @objc func btnAdd(sender:UIButton)
    {
        
        if sender.tag == 0 {
            
            let vc = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateContactViewController") as! CreateContactViewController
            vc.accountID = User.instance.accountID
            vc.accountName = User.instance.accountName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 1 {
            
            let vc = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateWOViewController") as! CreateWOViewController
            vc.acTypeID = User.instance.accountID
            vc.accountName = User.instance.accountName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 2 {
            
            let vc = UIStoryboard(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEstimateViewController") as! CreateEstimateViewController
            vc.accountID = User.instance.accountID
            vc.accountName = User.instance.accountName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 3 {
            
            let vc = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as! CreateInvoiceViewController
            vc.accountID = User.instance.accountID
            vc.accountName = User.instance.accountName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 4 {
            
            let vc = UIStoryboard(name: "File", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateFileViewController") as! CreateFileViewController
            
            vc.relatedToID = User.instance.accountID
            vc.accountName = User.instance.accountName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 5 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
            vc.relatedTo = "Account"
            vc.objectID = User.instance.accountID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 6 {
            
            let vc = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
            vc.relatedToID = User.instance.accountID
            vc.accountName = User.instance.accountName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = tableRelated.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? AccountDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? AccountDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = true
                    
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                if let parent = self.parent as? AccountDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
                
            }, completion: { _ in
                if let parent = self.parent as? AccountDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = false
                    
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
        
    }
    
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableRelated
        {
            return titleArr.count
        }
        else
        {
            if selectedIndex == 0
            {
                return contactArr.count
            }
            else if selectedIndex == 1
            {
                return workOrdersArr.count
            }
            else if selectedIndex == 2
            {
                return estimateArr.count
            }
            else if selectedIndex == 3
            {
                return invoiceArr.count
            }
            else if selectedIndex == 4
            {
                return fileArr.count
            }
            else if selectedIndex == 5
            {
                return eventArr.count
            }
            else if selectedIndex == 6
            {
                return tasksArr.count
            }
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableRelated
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ACCOUNT_RELATED) as! RelatedTableViewCell
            cell.lblTitle.text = titleArr[indexPath.row] as? String
            cell.btnViewAll.tag = indexPath.row
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(sender:)), for: .touchUpInside)
            cell.btnAdd.giveBorderToButton()
            cell.btnAdd.tag = indexPath.row
            cell.btnAdd.addTarget(self, action: #selector(btnAdd(sender:)), for: .touchUpInside)
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell.subTable.rowHeight = 70
            } else { //IPAD
                cell.subTable.rowHeight = 100
            }
            
            cell.subTable.tableFooterView = UIView()
            cell.subTable.reloadData()
            
            
            if (self.titleArr[indexPath.row] as! String).range(of: "(0)") != nil
            {
                cell.btnViewAll.isHidden = true
                cell.subTable.isHidden = true
                
            } else {
                cell.btnViewAll.isHidden = false
                cell.subTable.isHidden = false
            }
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgview.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgview.image = #imageLiteral(resourceName: "arrow")
                cell.btnAdd.isHidden = true
            }
            return cell
        }
        else
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: CELL_SUB_ACCOUNT_RELATED) as! SubRelatedTableViewCell
            if selectedIndex == 0
            {
                
                cell2.lblNumber?.text = "Name: " + contactArr[indexPath.row].FirstName! + "" + contactArr[indexPath.row].LastName!
                cell2.lblStatus?.text = "Title: " + contactArr[indexPath.row].Title!
                cell2.lblCategory?.text = "Phone: " + contactArr[indexPath.row].PhoneNo!
                
            }
            else if selectedIndex == 1
            {
                cell2.lblNumber?.text = "Work Order No: " + workOrdersArr[indexPath.row].WorkOrderNo!
                cell2.lblStatus?.text = "Status: " + workOrdersArr[indexPath.row].Status!
                cell2.lblCategory?.text = "Start Date: " + workOrdersArr[indexPath.row].StartDate!
                
            }
            else if selectedIndex == 2
            {
                cell2.lblNumber?.text = "Estimate No: " + estimateArr[indexPath.row].EstimateNo!
                cell2.lblStatus?.text = "Status: " + estimateArr[indexPath.row].Status!
                cell2.lblCategory?.text = "Estimate Name: " + estimateArr[indexPath.row].EstimateName!
                
            }
            else if selectedIndex == 3
            {
                cell2.lblNumber?.text = "Invoice Number: " + invoiceArr[indexPath.row].InvoiceNumber!
                cell2.lblStatus?.text = "Invoice Status: " + invoiceArr[indexPath.row].InvoiceStatus!
                cell2.lblCategory?.text = "DueDate: " + invoiceArr[indexPath.row].DueDate!
                
            }
            else if selectedIndex == 4
            {
                cell2.lblNumber?.text = "FileName: " + fileArr[indexPath.row].FileName!
                cell2.lblStatus?.text = "Content Type: " + fileArr[indexPath.row].ContentType!
                cell2.lblCategory?.text = "Created Date: " + fileArr[indexPath.row].CreatedDate!
                
            }
            else if selectedIndex == 5
            {
                cell2.lblNumber?.text = "Assigned To: " + eventArr[indexPath.row].AssignedTo!
                cell2.lblStatus?.text = "Name: " + eventArr[indexPath.row].Name!
                cell2.lblCategory?.text = "Event Start Date: " + eventArr[indexPath.row].EventStartDate!
                
            }
            else if selectedIndex == 6
            {
                cell2.lblNumber?.text = "Assigned To: " + tasksArr[indexPath.row].AssignedTo!
                cell2.lblStatus?.text = "Task Status: " + tasksArr[indexPath.row].TaskStatus!
                cell2.lblCategory?.text = "Priority: " + tasksArr[indexPath.row].Priority!
                
            }
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableRelated
        {
            let cell = tableView.cellForRow(at: indexPath) as! RelatedTableViewCell
            cell.imgview.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath))
            {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }
            else
            {
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            if indexPath.row == selectedIndex{
                selectedIndex = -1
                
            }else{
                selectedIndex = indexPath.row
                
            }
            
            if (self.titleArr[indexPath.row] as? String == "Contacts (0)") || (self.titleArr[indexPath.row] as? String == "Work Orders (0)") || (self.titleArr[indexPath.row] as? String == "Estimates (0)") || (self.titleArr[indexPath.row] as? String == "Invoices (0)") || (self.titleArr[indexPath.row] as? String == "Tasks (0)") || (self.titleArr[indexPath.row] as? String == "Events (0)") || (self.titleArr[indexPath.row] as? String == "Files (0)")
            {
                
                let newText = "Add " + ((self.titleArr[indexPath.row]) as! String).replacingOccurrences(of: " (0)", with: "", options: NSString.CompareOptions.literal, range:nil)
                cell.btnAdd.setTitle(newText, for: .normal)
                
                cell.btnAdd.isHidden = false
                
            }
            else
            {
                cell.btnAdd.isHidden = true
            }
            
            tableView.reloadData()
        }
        else
        {
            if selectedIndex == 0
            {
                let contactVC = UIStoryboard(name: "Contact", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
                contactVC.navigationController?.isNavigationBarHidden = true
                User.instance.contactID = contactArr[indexPath.row].ContactID!
                User.instance.contactName = contactArr[indexPath.row].FullName ?? ""
                self.navigationController?.pushViewController(contactVC, animated: true)
            }
            else if selectedIndex == 1
            {
                let woVC = UIStoryboard(name: "WorkOrder", bundle:  Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
                woVC.navigationController?.isNavigationBarHidden = true
                User.instance.workorderID = workOrdersArr[indexPath.row].WorkOrderID!
                User.instance.workorderSubject = workOrdersArr[indexPath.row].Subject!
                self.navigationController?.pushViewController(woVC, animated: true)
            }
            else if selectedIndex == 2
            {
                let estimateVC = UIStoryboard(name: "Estimate", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController") as! EstimateDetailsViewController
                estimateVC.navigationController?.isNavigationBarHidden = true
                User.instance.estimateID = estimateArr[indexPath.row].EstimateID!
                User.instance.estimateName = estimateArr[indexPath.row].EstimateName!
                self.navigationController?.pushViewController(estimateVC, animated: true)
            }
            else if selectedIndex == 3
            {
                let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
                invoiceVC.navigationController?.isNavigationBarHidden = true
                User.instance.invoiceID = invoiceArr[indexPath.row].InvoiceID!
                User.instance.invoiceNo = invoiceArr[indexPath.row].InvoiceNumber!
                self.navigationController?.pushViewController(invoiceVC, animated: true)
            }
            else if selectedIndex == 4
            {
                let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.fileID = fileArr[indexPath.row].FileID!
                self.navigationController?.pushViewController(fileVC, animated: true)
            }
            else if selectedIndex == 5
            {
                let fileVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.eventID = eventArr[indexPath.row].EventID!
                self.navigationController?.pushViewController(fileVC, animated: true)
            }
            else if selectedIndex == 6
            {
                let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                taskVC.navigationController?.isNavigationBarHidden = true
                User.instance.taskID = tasksArr[indexPath.row].TaskID!
                self.navigationController?.pushViewController(taskVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableRelated
        {
            if indexPath.row == selectedIndex
            {
                return 286
            }else{
                return 45
            }
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 70
            } else { //IPAD
                return 100
            }
        }
    }
}
