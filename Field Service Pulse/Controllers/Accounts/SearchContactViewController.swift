//
//  SearchContactViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 22/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol searchContactDelegate {
    
    func sendData(searchVC:SearchContactViewController)
}

class SearchContactViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: Variables
    var contactsArr:[AccountRelatedContactsData] = []
    var filteredArray:[AccountRelatedContactsData] = []
    var isSeaching = false
    var contactID = ""
    var contactName = ""
    var delegate:searchContactDelegate!
    
    //MARK: IBOutlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webserviceCallForAccountRelatedContacts()
        self.table.tableFooterView = UIView()
    }
    
    //MARK: Function
    
    func webserviceCallForAccountRelatedContacts()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":User.instance.accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountRelatedContacts(urlString: API.accountRelatedContactURL, parameters: parameters , headers: headers, vc: self) { (response:AccountRelatedContactsResponse) in
            
            if response.Result == "True"
            {
                
                self.contactsArr = response.data!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSeaching
        {
            return filteredArray.count
        }
        return contactsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "id")
        if isSeaching
        {
            cell.textLabel?.text = filteredArray[indexPath.row].FullName
        }
        else
        {
            cell.textLabel?.text = contactsArr[indexPath.row].FullName
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSeaching
        {
            contactID = filteredArray[indexPath.row].ContactID!
            contactName = filteredArray[indexPath.row].FullName!
            
        }
        else
        {
            contactID = contactsArr[indexPath.row].ContactID!
            contactName = contactsArr[indexPath.row].FullName!
        }
        
        self.navigationController?.popViewController(animated: true)
        delegate.sendData(searchVC: self)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredArray = contactsArr.filter({ (text) -> Bool in
            let tmp: NSString = text.FullName! as NSString
            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(filteredArray.count == 0){
            isSeaching = false;
        } else {
            isSeaching = true;
        }
        self.table.reloadData()
    }
    
    
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
