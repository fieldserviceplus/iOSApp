//
//  AccountViewAllViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 11/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AccountViewAllViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    private let CELL_VIEWALL = "cellViewAll"
    var tag:Int?
    var titleLabel:String?
    var headerTitle:String?
    var contactArr:[ContactData] = []
    var workOrdersArr:[WorkOrderData] = []
    var tasksArr:[AccountRelatedTasksData] = []
    var estimateArr:[AccountRelatedEstimateData] = []
    var eventArr:[AccountRelatedEventData] = []
    var invoiceArr:[AccountRelatedInvoiceData] = []
    var fileArr:[AccountRelatedFileData] = []
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    //MARK: IBOutlet
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableViewAll: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    //MARK: Function
    
    func setupUI()
    { 
        tableViewAll.tableFooterView = UIView()
        lblTitle.text = titleLabel
        lblHeaderTitle.text = headerTitle
    }
    
    @objc func btnDetails(sender:UIButton) {
        
        if tag == 0
        {
            let contactVC = UIStoryboard(name: "Contact", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
            contactVC.navigationController?.isNavigationBarHidden = true
            User.instance.contactID = contactArr[sender.tag].ContactID!
            User.instance.contactName = contactArr[sender.tag].FullName ?? ""
            self.navigationController?.pushViewController(contactVC, animated: true)
            
        }
        else if tag == 1
        {
            let woVC = UIStoryboard(name: "WorkOrder", bundle:  Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
            woVC.navigationController?.isNavigationBarHidden = true
            User.instance.workorderID = workOrdersArr[sender.tag].WorkOrderID!
            User.instance.workorderSubject = workOrdersArr[sender.tag].Subject!
            self.navigationController?.pushViewController(woVC, animated: true)
        }
        else if tag == 2
        {
            let estimateVC = UIStoryboard(name: "Estimate", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController") as! EstimateDetailsViewController
            estimateVC.navigationController?.isNavigationBarHidden = true
            User.instance.estimateID = estimateArr[sender.tag].EstimateID!
            User.instance.estimateName = estimateArr[sender.tag].EstimateName!
            self.navigationController?.pushViewController(estimateVC, animated: true)
        }
        else if tag == 3
        {
            let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
            invoiceVC.navigationController?.isNavigationBarHidden = true
            User.instance.invoiceID = invoiceArr[sender.tag].InvoiceID!
            User.instance.invoiceNo = invoiceArr[sender.tag].InvoiceNumber!
            self.navigationController?.pushViewController(invoiceVC, animated: true)
        }
        else if tag == 4
        {
            let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
            fileVC.navigationController?.isNavigationBarHidden = true
            User.instance.fileID = fileArr[sender.tag].FileID!
            self.navigationController?.pushViewController(fileVC, animated: true)
        }
        else if tag == 5
        {
            let fileVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            fileVC.navigationController?.isNavigationBarHidden = true
            User.instance.eventID = eventArr[sender.tag].EventID!
            self.navigationController?.pushViewController(fileVC, animated: true)
        }
        else if tag == 6
        {
            let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
            taskVC.navigationController?.isNavigationBarHidden = true
            User.instance.taskID = tasksArr[sender.tag].TaskID!
            self.navigationController?.pushViewController(taskVC, animated: true)
        }
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tag == 0
        {
            return contactArr.count
        }
        else if tag == 1
        {
            return workOrdersArr.count
        }
        else if tag == 2
        {
            return estimateArr.count
        }
        else if tag == 3
        {
            return invoiceArr.count
        }
        else if tag == 4
        {
            return fileArr.count
        }
        else if tag == 5
        {
            return eventArr.count
        }
        else if tag == 6
        {
            return tasksArr.count
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_VIEWALL) as! AccountViewAllTableViewCell
        cell.btnDetail.addTarget(self, action: #selector(btnDetails(sender:)), for: .touchUpInside)
        cell.btnDetail.tag = indexPath.row
        cell.view_back.dropShadow()
        if tag == 0
        {
            cell.lblFirst.text = "Name: " + contactArr[indexPath.row].FirstName! + "" + contactArr[indexPath.row].LastName!
            cell.lblSecond?.text = "Title: " + contactArr[indexPath.row].Title!
            cell.lblThird?.text = "Phone: " + contactArr[indexPath.row].PhoneNo!
            cell.lblFourth?.text = "Email: " + contactArr[indexPath.row].Email!
            cell.lblFifth?.text = ""
            cell.lblSixth?.text = ""
        }
        else if tag == 1
        {
            cell.lblFirst?.text = "Work Order No: " + workOrdersArr[indexPath.row].WorkOrderNo!
            cell.lblSecond?.text = "Status: " + workOrdersArr[indexPath.row].Status!
            cell.lblThird?.text = "Start Date: " + workOrdersArr[indexPath.row].StartDate!
            cell.lblFourth?.text = "Priority: " + workOrdersArr[indexPath.row].Priority!
            cell.lblFifth?.text = "Subject: " + workOrdersArr[indexPath.row].Subject!
            cell.lblSixth?.text = "Category Name: " + workOrdersArr[indexPath.row].CategoryName!
        }
        else if tag == 2
        {
            cell.lblFirst?.text = "Estimate No: " + estimateArr[indexPath.row].EstimateNo!
            cell.lblSecond?.text = "Status: " + estimateArr[indexPath.row].Status!
            cell.lblThird?.text = "Estimate Name: " + estimateArr[indexPath.row].EstimateName!
            cell.lblFourth?.text = "Created Date: " + estimateArr[indexPath.row].CreatedDate!
            cell.lblFifth?.text = "Expiration Date: " + estimateArr[indexPath.row].ExpirationDate!
            cell.lblSixth?.text = "Grand Total: " + estimateArr[indexPath.row].GrandTotal!
        }
        else if tag == 3
        {
            cell.lblFirst?.text = "Invoice Number: " + invoiceArr[indexPath.row].InvoiceNumber!
            cell.lblSecond?.text = "Invoice Status: " + invoiceArr[indexPath.row].InvoiceStatus!
            cell.lblThird?.text = "Due Date: " + invoiceArr[indexPath.row].DueDate!
            cell.lblFourth?.text = "Subject: " + invoiceArr[indexPath.row].Subject!
            cell.lblFifth?.text = "Sub Total: " + invoiceArr[indexPath.row].SubTotal!
            cell.lblSixth?.text = "Total Price: " + invoiceArr[indexPath.row].TotalPrice!
        }
        else if tag == 4
        {
            cell.lblFirst?.text = "FileName: " + fileArr[indexPath.row].FileName!
            cell.lblSecond?.text = "Content Type: " + fileArr[indexPath.row].ContentType!
            cell.lblThird?.text = "Created Date: " + fileArr[indexPath.row].CreatedDate!
            cell.lblFourth?.text = "Created By: " + fileArr[indexPath.row].CreatedBy!
            cell.lblFifth?.text = "Subject: " + fileArr[indexPath.row].Subject!
            cell.lblSixth?.text = ""
        }
        else if tag == 5
        {
            cell.lblFirst?.text = "Assigned To: " + eventArr[indexPath.row].AssignedTo!
            cell.lblSecond?.text = "Name: " + eventArr[indexPath.row].Name!
            cell.lblThird?.text = "Event Start Date: " + eventArr[indexPath.row].EventStartDate!
            cell.lblFourth?.text = "Created By: " + eventArr[indexPath.row].CreatedBy!
            cell.lblFifth?.text = "Created Date: " + eventArr[indexPath.row].CreatedDate!
            cell.lblSixth?.text = "Subject: " + eventArr[indexPath.row].Subject!
        }
        else if tag == 6
        {
            cell.lblFirst?.text = "Assigned To: " + tasksArr[indexPath.row].AssignedTo!
            cell.lblSecond?.text = "Task Status: " + tasksArr[indexPath.row].TaskStatus!
            cell.lblThird?.text = "Priority: " + tasksArr[indexPath.row].Priority!
            cell.lblFourth?.text = "Name: " + tasksArr[indexPath.row].Name!
            cell.lblFifth?.text = "Task Type: " + tasksArr[indexPath.row].TaskType!
            cell.lblSixth?.text = "Subject: " + tasksArr[indexPath.row].Subject!
        }
        
        if(selectedArray.contains(indexPath))
        {
            // use selected image
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        }
        else
        {
            // use normal image
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewAll
        {
            let cell = tableView.cellForRow(at: indexPath) as! AccountViewAllTableViewCell
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath))
            {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }
            else
            {
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            if indexPath.row == selectedIndex{
                selectedIndex = -1
                
            }else{
                selectedIndex = indexPath.row
                
            }
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableViewAll
        {
            if indexPath.row == selectedIndex
            {
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    return 193
                } else { //IPAD
                    return 227
                }
                
            }else{
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    return 98
                } else { //IPAD
                    return 115
                }
            }
        }
        return 0
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}
