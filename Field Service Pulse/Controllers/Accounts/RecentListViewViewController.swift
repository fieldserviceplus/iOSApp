//
//  RecentListViewViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 15/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class RecentListViewViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, passingDataDelegate {
    
    //MARK: Variables
    
    private let CELL_ID = "cell_listview"
    
    private let SEGUE_AC_DETAILS = "segueAccountDetails"
    private let SEGUE_CREATE_AC = "segue_CreateAC"
    
    var getAccountViewsArr:[GetAccountViewsData]? = []
    var listArr:[[String:String]] = []
    var arrTag:Int?
    
    var AccountViewID = ""
    var AccountViewName = ""
    var city:String?
    var state:String?
    var count:Int?
    var sortingFlag = 0
    var flag = 0
    var lastContentOffset: CGFloat = 0
    var addressLabel:PaddingLabel?
    var flagForDelegate = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var tableListView: UITableView!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var btnSorting: UIButton!
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var btnMoreMenu: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       customViewWillAppear()
    }

    func customViewWillAppear() {
        if flagForDelegate == 0 {
            webserviceCallForAccountsListViews()
            webserviceCallForGetAccountsViews()
        } else {
            flagForDelegate = 0
        }
        
        lblDropdown.text = "  " + AccountViewName + "  "
        if AccountViewID == "MyAccounts" || AccountViewID == "AllAccounts" || AccountViewID == "NewAccountsThisWeek"
        {
            btnMoreMenu.isEnabled = false
            btnSorting.isEnabled = false
        }
        else
        {
            btnMoreMenu.isEnabled = true
            btnSorting.isEnabled = true
        }
        
    }
    //MARK: Functions
    

    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        tableListView.tableFooterView = UIView()
        viewDropdown.dropShadow()
    }
    
    func sendData(array: [[String : String]], flag: Int) {
        
        listArr = array
        flagForDelegate = flag
        self.tableListView.reloadData()
    }
    
    func  webserviceCallForGetAccountsViews()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAccountView(urlString: API.getAccountViewsURL, parameters: parameters, headers: headers, vc: self) { (response:GetAccountViews) in
            
            if response.Result == "True"
            {
                self.getAccountViewsArr = response.data
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForDeleteCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"Account",
                          "ViewID":AccountViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForCopyCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"Account",
                          "ViewID":AccountViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.copyCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    func  webserviceCallForAccountsListViews()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "AccountViewID":AccountViewID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountListView(urlString: API.accountListViewsURL, parameters: parameters, headers: headers, vc: self) { (response:AccountListView) in
            
            if response.Result == "True"
            {
                //self.accountListViewArr = response.data
                self.listArr = response.data ?? []
                
                self.tableListView.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_AC_DETAILS
        {
            let detailVC = segue.destination as! AccountDetailsViewController
            
            let account = sender as? [String:String]
            User.instance.accountID = account?["AccountID"] ?? ""
            User.instance.accountName = account?["AccountName"] ?? ""
            
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == tableListView
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
            }
            else
            {
                // didn't move
            }
        }
    }
    
    @objc func btnCreateView() {
        
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "Account"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    @objc func btnRenameView() {
        
        let renameViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RenameView_ViewController") as!  RenameView_ViewController
        renameViewVC.object = "Account"
        renameViewVC.ViewID = AccountViewID
        self.navigationController?.pushViewController(renameViewVC, animated: true)
    }
    @objc func btnEditSharing() {
        
        let editSharingVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditSharingViewController") as!  EditSharingViewController
        editSharingVC.object = "Account"
        editSharingVC.ViewID = AccountViewID
        self.navigationController?.pushViewController(editSharingVC, animated: true)
    }
    @objc func btnCopy() {
        
        webserviceCallForCopyCustomView()
    }
    @objc func btnDeleteRow() {
        
        webserviceCallForDeleteCustomView()
    }
    @objc func btnEditColumns() {
        
        let editColumnsVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DisplayedColumnsViewController") as!  DisplayedColumnsViewController
        editColumnsVC.flagSpecifyAction = "Edit"
        editColumnsVC.object = "Account"
        editColumnsVC.ViewID = AccountViewID
        self.navigationController?.pushViewController(editColumnsVC, animated: true)
    }
    @objc func btnEditFilter() {
        
        let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
        
        editFilterVC.object = "Account"
        editFilterVC.ViewID = AccountViewID
        self.navigationController?.pushViewController(editFilterVC, animated: true)
    }
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableListView
        {
            return self.listArr.count
        }
        return getAccountViewsArr!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableListView
        {
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! RecentListViewTableViewCell
            
            for subview in cell.stackview.subviews {
                subview.removeFromSuperview()
            }
            
            cell.view_back.dropShadow()
            
            if AccountViewName == "My Accounts" || AccountViewName == "All Accounts" || AccountViewName == "New Accounts This Week"
            {
                let myArr = ["AssignedTo","AccountName","AccountType","PhoneNo","CityName","StateName"]
                
                if listArr.count > 0
                {
                        let dict = listArr[indexPath.row]
                        for j in 0..<myArr.count
                        {
                            if myArr[j] == "CityName"
                            {
                                city = dict[myArr[j]]
                            }
                            else if myArr[j] == "StateName"
                            {
                                state = dict[myArr[j]]
                                
                                let label = PaddingLabel()
                                label.textAlignment = .left
                                
                                let address = (city ?? "")  + "," + (state ?? "")
                                label.text = " Address" + ": " + address
                                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                                    count = count! + 20
                                    label.font = label.font.withSize(15)
                                } else { //IPAD
                                    count = count! + 25
                                    label.font = label.font.withSize(20)
                                }
                                
                                label.textColor = UIColor.darkGray
                                cell.stackview.addArrangedSubview(label)
                            }
                            else
                            {
                                let label = PaddingLabel()
                                label.textAlignment = .left
                                
                                label.text = myArr[j].camelCaseToWords()  + ": " + (dict[myArr[j]] ?? "")
                                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                                    count = count! + 20
                                    label.font = label.font.withSize(15)
                                } else { //IPAD
                                    count = count! + 25
                                    label.font = label.font.withSize(20)
                                }
                                
                                label.textColor = UIColor.darkGray
                                cell.stackview.addArrangedSubview(label)
                            }
                        }
                }
            }
            else
            {
                for (key, value) in listArr[indexPath.row] {
                    // here use key and value
                    
                    print(key)
                    print(value)
                    
                    if key == "AccountType" || key == "AccountName" || key == "AssignedToName" || key == "PhoneNo" || key == "LastServiceDate" || key == "PreferredTechnician" || key == "Notes"
                    {
                        
                        let label = PaddingLabel()
                        label.textAlignment = .left
                        
                        label.text = key.camelCaseToWords()  + ": " + value
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            label.font = label.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            label.font = label.font.withSize(20)
                        }
                        
                        label.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(label)
                    }
                    
                    if key == "BillingCity" || key == "CityName"
                    {
                        if addressLabel?.text != nil
                        {
                            addressLabel?.text = ""
                            addressLabel?.text = " Address: " + value + ", " + (state ?? "")
                        }
                        else
                        {
                            city = value
                        }
                    }
                    
                    if key == "BillingState" || key == "StateName"
                    {
                        state = value
                        addressLabel = PaddingLabel()
                        addressLabel?.textAlignment = .left
                        addressLabel?.text = " Address: " + (city ?? "") + ", " + value
                        
                        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                            count = count! + 20
                            addressLabel?.font = addressLabel?.font.withSize(15)
                        } else { //IPAD
                            count = count! + 25
                            addressLabel?.font = addressLabel?.font.withSize(20)
                        }
                        addressLabel?.textColor = UIColor.darkGray
                        cell.stackview.addArrangedSubview(addressLabel!)
                        
                    }
                }
            }
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getAccountViewsArr?[indexPath.row].AccountViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableListView
        {
            self.performSegue(withIdentifier: SEGUE_AC_DETAILS, sender: listArr[indexPath.row])
        }
        else
        {
            AccountViewID = (getAccountViewsArr?[indexPath.row].AccountViewID)!
            AccountViewName = (getAccountViewsArr?[indexPath.row].AccountViewName)!
            flag = 0
            webserviceCallForAccountsListViews()
            lblDropdown.text = "  " + AccountViewName + "  "
            
            if AccountViewID == "MyAccounts" || AccountViewID == "AllAccounts" || AccountViewID == "NewAccountsThisWeek"
            {
                btnMoreMenu.isEnabled = false
                btnSorting.isEnabled = false
            }
            else
            {
                btnMoreMenu.isEnabled = true
                btnSorting.isEnabled = true
            }
            viewDropdown.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableListView
        {
            return CGFloat(count! + 10)
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 40
            } else { //IPAD
                return 50
            }
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    @IBAction func btnMore(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_CREATE_AC, sender: self)
    }
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        
        if AccountViewID != "MyAccounts" && AccountViewID != "AllAccounts" && AccountViewID != "NewAccountsThisWeek" {
            if let customView = Bundle.main.loadNibNamed("CustomViewMenu", owner: self, options: nil)?.first as? CustomViewMenu {
                
                customView.tag = 101
                customView.btnCreateView.addTarget(self, action: #selector(btnCreateView), for: .touchUpInside)
                customView.btnRename.addTarget(self, action: #selector(btnRenameView), for: .touchUpInside)
                customView.btnEditSharing.addTarget(self, action: #selector(btnEditSharing), for: .touchUpInside)
                customView.btnCopy.addTarget(self, action: #selector(btnCopy), for: .touchUpInside)
                customView.btnDeleteRow.addTarget(self, action: #selector(btnDeleteRow), for: .touchUpInside)
                customView.btnEditColumns.addTarget(self, action: #selector(btnEditColumns), for: .touchUpInside)
                customView.btnEditFilter.addTarget(self, action: #selector(btnEditFilter), for: .touchUpInside)
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            }
        }
        
    }
    
    @IBAction func btnCreateNewView(_ sender: Any) {
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "Account"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    @IBAction func btnFilter(_ sender: Any) {
        
        if AccountViewID != "MyAccounts" && AccountViewID != "AllAccounts" && AccountViewID != "NewAccountsThisWeek" {
            let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
            
            editFilterVC.object = "Account"
            editFilterVC.ViewID = AccountViewID
            self.navigationController?.pushViewController(editFilterVC, animated: true)
        }
        else{
            let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountFilterViewController") as! AccountFilterViewController
            filterVC.AccountViewID = AccountViewID
            filterVC.delegate = self
            self.navigationController?.pushViewController(filterVC, animated: true)
        }
        
    }
    
    @IBAction func btnSorting(_ sender: Any) {
        
        let sortVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SortViewController") as! SortViewController
        sortVC.viewID = AccountViewID
        sortVC.object = "Account"
        self.navigationController?.pushViewController(sortVC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        view.viewWithTag(101)?.removeFromSuperview()
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}




