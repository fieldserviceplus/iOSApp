//
//  CreateAccountViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 04/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateAccountViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, searchDelegate {
    
    //MARK: Variables
    
    var accountID:String?
    var pickerView = UIPickerView()
    
    var ownerID:String?
    var typeID:String?
    var preferredTechID:String?
    var accountTypeArr:[AccountTypeData] = []
    
    var textfieldTag = 0
    var btnGetLocationTag = 0
    var latBillingAddress:String?
    var longBillingAddress:String?
    var latShippingAddress:String?
    var longShippingAddress:String?
    var isActive:String?
    var flagResponse = "0"
    var response:accountDetailsResponse?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    @IBOutlet weak var txtAccountOwner: UITextField!
    @IBOutlet weak var txtName: AkiraTextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtAccessNote: AkiraTextField!
    @IBOutlet weak var txtPopupReminder: AkiraTextField!
    @IBOutlet weak var txtNotes: AkiraTextField!
    @IBOutlet weak var txtAccountTypeDropdown: UITextField!
    @IBOutlet weak var txtPreferredTech: UITextField!
    @IBOutlet weak var txtBillingAddress: UITextField!
    @IBOutlet weak var txtBillingCity: AkiraTextField!
    @IBOutlet weak var txtBillingState: AkiraTextField!
    @IBOutlet weak var txtBillingCountry: AkiraTextField!
    @IBOutlet weak var txtBillingPostalCode: AkiraTextField!
    @IBOutlet weak var txtShippingAddress: UITextField!
    @IBOutlet weak var txtShoppingCity: AkiraTextField!
    @IBOutlet weak var txtShoppingState: AkiraTextField!
    @IBOutlet weak var txtShoppingCountry: AkiraTextField!
    @IBOutlet weak var txtShoppingPostalCode: AkiraTextField!
    
    //UIButton
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var btnShippingAddress: UIButton!
    @IBOutlet weak var btnBillingAddress: UIButton!
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
        webserviceCallForAccountType()
        webserviceCallForCustomFields()
    }

    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        
        txtAccountTypeDropdown.inputView = pickerView
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnShippingAddress.giveBorderToButton()
        btnBillingAddress.giveBorderToButton()
        self.txtAccountTypeDropdown.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtPreferredTech.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtAccountOwner.setRightImage(name: "search_small", placeholder: "--None--")
        
        self.txtShippingAddress.layer.borderWidth = 1.5
        self.txtShippingAddress.layer.borderColor = UIColor.darkGray.cgColor
        self.txtBillingAddress.layer.borderWidth = 1.5
        self.txtBillingAddress.layer.borderColor = UIColor.darkGray.cgColor
        
        if flagResponse == "1" {
            responseAccountDetails()
            flagResponse = "0"
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC: SearchAccountViewController) {
        
        if textfieldTag == 1 {
            txtAccountOwner.text = searchVC.fullName
            ownerID = searchVC.userID
        }
        else if textfieldTag == 4 {
            txtPreferredTech.text = searchVC.fullName
            preferredTechID = searchVC.userID
        }
    }
    
    func webserviceCall()
    {
        var parameters:[String:Any] = [:]
        

        if ownerID == nil || txtName.text! == "" || typeID == nil || txtPhone.text! == "" || txtBillingAddress.text! == "" || txtBillingCity.text! == "" || txtBillingState.text! == "" || txtBillingCountry.text! == "" || txtBillingPostalCode.text! == "" || txtShippingAddress.text == "" || txtShoppingCity.text! == "" || txtShoppingState.text! == "" || txtShoppingCountry.text! == "" || txtShoppingPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        print(parameters)
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["AssignedTo"] = ownerID
        parameters["AccountName"] = txtName.text!
        parameters["PhoneNo"] = txtPhone.text ?? ""
        parameters["AccessNotes"] = txtAccessNote.text ?? ""
        parameters["PopUpReminder"] = txtPopupReminder.text ?? ""
        parameters["Notes"] = txtNotes.text ?? ""
        parameters["AccountType"] = typeID
        parameters["IsActive"] = isActive ?? "1"
        parameters["PreferredTechnician"] = preferredTechID
        parameters["BillingAddress"] = txtBillingAddress.text ?? ""
        parameters["BillingCity"] = txtBillingCity.text ?? ""
        parameters["BillingPostalCode"] = txtBillingPostalCode.text ?? ""
        parameters["BillingState"] = txtBillingState.text ?? ""
        parameters["BillingCountry"] = txtBillingCountry.text ?? ""
        parameters["BillingLatitude"] = latBillingAddress ?? ""
        parameters["BillingLongitude"] = longBillingAddress ?? ""
        parameters["ShippingLatitude"] = latShippingAddress ?? ""
        parameters["ShippingLongitude"] = longShippingAddress ?? ""
        parameters["ShippingAddress"] = txtShippingAddress.text ?? ""
        parameters["ShippingCity"] = txtShoppingCity.text ?? ""
        parameters["ShippingState"] = txtShoppingState.text ?? ""
        parameters["ShippingCountry"] = txtShoppingCountry.text ?? ""
        parameters["ShippingPostalCode"] = txtShoppingPostalCode.text ?? ""
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createAccount(urlString: API.createAccountURL, parameters: parameters , headers: headers, vc: self) { (response:CreateAccountResponse) in
            
            if response.Result == "True"
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountDetailsViewController") as! AccountDetailsViewController
                User.instance.accountID = String(response.AccountID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForAccountType()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountType(urlString: API.accountTypeURL, parameters: parameters , headers: headers, vc: self) { (response:AccountTypeResponse) in
            
            if response.Result == "True"
            {
                self.accountTypeArr = response.TypeData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
   func responseAccountDetails()
    {
        self.txtAccountOwner.text = response?.data?.AssignedToName
        self.txtName.text = response?.data?.AccountName
        self.txtNotes.text = response?.data?.Notes
        self.txtPhone.text = response?.data?.PhoneNo
        self.txtAccessNote.text = response?.data?.AccessNotes
        self.txtPopupReminder.text = response?.data?.PopUpReminder
        self.txtAccountTypeDropdown.text = (response?.data?.AccountTypeName)
        self.txtPreferredTech.text = (response?.data?.PreferredTechnicianName) ?? ""
        self.ownerID = response?.data?.AssignedTo
        self.typeID = response?.data?.AccountType
        self.isActive = response?.data?.IsActive
        self.preferredTechID = response?.data?.PreferredTechnician
        self.txtBillingAddress.text = response?.data?.BillingAddress
        self.txtBillingCity.text = response?.data?.BillingCity
        self.txtBillingState.text = response?.data?.BillingState
        self.txtBillingCountry.text = response?.data?.BillingCountry
        self.txtBillingPostalCode.text = response?.data?.BillingPostalCode
        self.txtShippingAddress.text = response?.data?.ShippingAddress
        self.txtShoppingCity.text = response?.data?.ShippingCity
        self.txtShoppingState.text = response?.data?.ShippingState
        self.txtShoppingCountry.text = response?.data?.ShippingCountry
        self.txtShoppingPostalCode.text = response?.data?.ShippingPostalCode
                
        if self.isActive == "1"
        {
            self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
        }
        else
        {
            self.btnIsActive.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "Object":"Account",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                
                var viewFromConstrain:Any = self.txtShoppingPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtShoppingPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShoppingPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShoppingPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                           customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                customView.txtviewLabelField.textColor = UIColor.lightGray
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            self.checkboxValueArr.append("")
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                    
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.btnSave, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
    
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(value + "," + sender.accessibilityLabel!, at: index)
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: ",\(sender.accessibilityLabel!)", with: "")

                    
               checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxKey:\(checkboxValueArr)")
        
    
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 8
        {
            return accountTypeArr.count + 1
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 8
        {
            return row == 0 ? "--None--" : accountTypeArr[row-1].AccountType
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 8
        {
            if row == 0
            {
                txtAccountTypeDropdown.text = ""
            }
            else
            {
                txtAccountTypeDropdown.text = accountTypeArr[row-1].AccountType
                typeID = accountTypeArr[row-1].AccountTypeID
            }
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        
        //
        if textField == txtAccountOwner || textField == txtPreferredTech || textField == txtAccountTypeDropdown
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtName.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtPreferredTech.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtAccessNote.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtPopupReminder.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtNotes.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtAccountTypeDropdown.becomeFirstResponder()
        }
        else if textField.tag == 12
        {
            txtBillingAddress.becomeFirstResponder()
        }
        else if textField.tag == 14
        {
            txtBillingCity.becomeFirstResponder()
        }
        else if textField.tag == 15
        {
            txtBillingState.becomeFirstResponder()
        }
        else if textField.tag == 16
        {
            txtBillingCountry.becomeFirstResponder()
        }
        else if textField.tag == 17
        {
            txtBillingPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 18
        {
            txtShippingAddress.becomeFirstResponder()
        }
        else if textField.tag == 20
        {
            txtShoppingCity.becomeFirstResponder()
        }
        else if textField.tag == 21
        {
            txtShoppingState.becomeFirstResponder()
        }
        else if textField.tag == 22
        {
            txtShoppingCountry.becomeFirstResponder()
        }
        else if textField.tag == 23
        {
            txtShoppingPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 24
        {
            txtShoppingPostalCode.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1 || textField.tag == 4
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 13 || textField.tag == 19
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        
    }
    
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    
    
    //MARK: TextView Delegate Methods
    
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
                
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBillingAddress(_ sender: Any) {
        
        btnGetLocationTag = 0
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "createAC"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func btnShippingAddress(_ sender: Any) {
        btnGetLocationTag = 1
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "createAC"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        if customFieldKeyArr.count != 0 {
            
            if lastTextfieldTapped != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                    customFieldValueArr.append(lastTextfieldTapped.text ?? "")
                }
            }
            
        }
        
        
        //CustomTextView
        if customFieldKeyArr.count != 0 {
            
            if lastTappedTextView != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                    customFieldValueArr.append(lastTappedTextView.text ?? "")
                }
            }
            
        }
        
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropFirst()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
       
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        
        webserviceCall()
    }
    
    @IBAction func btnIsActive(_ sender: Any) {
        
        if (btnIsActive.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isActive = "0"
        }
        else{
            btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            isActive = "1"
        }
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        if btnGetLocationTag == 0
        {
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtBillingAddress.text = SelectLocationVC.address
                txtBillingCity.text = SelectLocationVC.city
                txtBillingState.text = SelectLocationVC.state
                txtBillingCountry.text = SelectLocationVC.country
                txtBillingPostalCode.text = SelectLocationVC.postalCode
                self.latBillingAddress = SelectLocationVC.latitude
                self.longBillingAddress = SelectLocationVC.longitude
            }
        }
        else{
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtShippingAddress.text = SelectLocationVC.address
                txtShoppingCity.text = SelectLocationVC.city
                txtShoppingState.text = SelectLocationVC.state
                txtShoppingCountry.text = SelectLocationVC.country
                txtShoppingPostalCode.text = SelectLocationVC.postalCode
                self.latShippingAddress = SelectLocationVC.latitude
                self.longShippingAddress = SelectLocationVC.longitude
            }
        }
    }
}

extension CreateAccountViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
//            if component.type == "route" {
//                print(component.name)
//                area = component.name
//            }
            
            if textfieldTag == 13
            {
                latBillingAddress = String(place.coordinate.latitude)
                longBillingAddress = String(place.coordinate.longitude)
                //if component.type == "sublocality_level_1" {
                    print(component.name)
                    //txtBillingAddress.text = place.name + " " + (area ?? "") + " " + component.name
                    txtBillingAddress.text = place.name
                //}
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtBillingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtBillingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtBillingState.text = component.name
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtBillingCountry.text = component.name
                }
            }
            else
            {
                latShippingAddress = String(place.coordinate.latitude)
                longShippingAddress = String(place.coordinate.longitude)
                //if component.type == "sublocality_level_1" {
                    print(component.name)
                    //txtShippingAddress.text = place.name + " " + (area ?? "") + " " + component.name
                txtShippingAddress.text = place.name
                //}
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtShoppingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtShoppingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtShoppingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtShoppingCountry.text = component.name
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


