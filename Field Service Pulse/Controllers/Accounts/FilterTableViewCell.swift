//
//  FilterTableViewCell.swift
//  Field Service Pulse
//
//  Created by Apple on 02/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var btnDropdown2: UIButton!
    @IBOutlet weak var btnDropdown1: UIButton!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var lblDropdown1: UILabel!
    @IBOutlet weak var lblDropdown2: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
