//
//  RecenetAccountsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 01/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class RecenetAccountsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    //MARK: VAriables
    
    private let CELL_ACCOUNT = "cell_account"
    
    private let SEGUE_CREATE_AC = "segue_CreateAC"
    private let SEGUE_AC_DETAILS = "segueAccountDetails"
    private let SEGUE_LIST_VIEW = "segue_RecentListView"
    
    var getAccountViewsArr:[GetAccountViewsData]? = []
    var listArr:[RecentAccountsData] = []
    var arrTag = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var tableAccount: UITableView!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForGetAccountsViews()
        webserviceCallForRecentAccounts()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        tableAccount.tableFooterView = UIView()
        lblDropdown.giveBorderToLabel()
        viewDropdown.dropShadow()
        btnDropdown.giveBorderToButton()
        
        tableAccount.rowHeight = UITableView.automaticDimension
        tableAccount.estimatedRowHeight = 176
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            tableDropdown.rowHeight = 40
        } else { //IPAD
            tableDropdown.rowHeight = 50
        }
    }
    
    func  webserviceCallForGetAccountsViews()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAccountView(urlString: API.getAccountViewsURL, parameters: parameters, headers: headers, vc: self) { (response:GetAccountViews) in
            
            if response.Result == "True"
            {
                self.getAccountViewsArr = response.data
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
   
    func  webserviceCallForRecentAccounts()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentAccounts(urlString: API.recentAccountsURL, parameters: parameters, headers: headers, vc: self) { (response:RecentAccounts) in
            
            if response.Result == "True"
            {
                self.listArr = response.data!
                self.tableAccount.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_LIST_VIEW
        {
            let listVC = segue.destination as! RecentListViewViewController
            let account = sender as! GetAccountViewsData
            listVC.AccountViewID = account.AccountViewID!
            listVC.AccountViewName = account.AccountViewName!
        }
        else if segue.identifier == SEGUE_AC_DETAILS
        {
            let detailVC = segue.destination as! AccountDetailsViewController
            
            let account = sender as? RecentAccountsData
            User.instance.accountID = account?.AccountID ?? ""
            User.instance.accountName = account?.AccountName ?? ""
        }
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableAccount
        {
            return listArr.count
        }
        return getAccountViewsArr!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableAccount
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ACCOUNT) as! RecentAccountTableViewCell
            
            cell.lblAccountName.text = listArr[indexPath.row].AccountName ?? ""
            cell.lblPhoneNo.text = listArr[indexPath.row].PhoneNo
            cell.lblType.text = listArr[indexPath.row].AccountType
            cell.lblCity.text = listArr[indexPath.row].BillingCity
            cell.lblState.text = listArr[indexPath.row].BillingState
            cell.lblAssignedTo.text = listArr[indexPath.row].AssignedTo
            
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getAccountViewsArr?[indexPath.row].AccountViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableDropdown
        {
            self.performSegue(withIdentifier: SEGUE_LIST_VIEW, sender: getAccountViewsArr?[indexPath.row])
            
        }
        else if tableView == tableAccount
        {
            self.performSegue(withIdentifier: SEGUE_AC_DETAILS, sender: listArr[indexPath.row])
        }
        
        self.viewDropdown.isHidden = true
    }
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    @IBAction func btnNewAccount(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_CREATE_AC, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
