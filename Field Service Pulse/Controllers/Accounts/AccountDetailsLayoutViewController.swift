//
//  AccountDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 24/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class AccountDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {

    //MARK: Variables
    
    var lastContentOffset: CGFloat = 0
    var contactID:String?
    var assignedToID:String?
    
    
    
    //MARK: IBOutlets
    @IBOutlet weak var lblAccountOwner: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblPreferredTech: UILabel!
    @IBOutlet weak var lblAccessNotes: UILabel!
    @IBOutlet weak var lblPopupReminder: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblAC_Type: UILabel!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var lblLastActivity: UILabel!
    @IBOutlet weak var lblLastServiceDate: UILabel!
    @IBOutlet weak var lblPrimaryContact: UILabel!
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblBillingDetailAddress: UILabel!
    @IBOutlet weak var lblBillingCountry: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblShippingDetailAddress: UILabel!
    @IBOutlet weak var lblShippingCountry: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblLastModifiedDate: UILabel!
    @IBOutlet weak var lblLastModifiedBy: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewShippingAddress: UIView!
    @IBOutlet weak var lblSystemInfo: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        webserviceCall()
        webserviceCallForCustomFields()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":User.instance.accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.lblAccountOwner.text = response.data?.AssignedToName
                self.lblName.text = response.data?.AccountName
                self.lblNotes.text = response.data?.Notes
                self.lblPhone.text = response.data?.PhoneNo
                self.lblAccessNotes.text = response.data?.AccessNotes
                self.lblLastActivity.text = response.data?.LastActivityDate
                self.lblPopupReminder.text = response.data?.PopUpReminder
                self.lblPrimaryContact.text = response.data?.PrimaryContactName
                self.lblLastServiceDate.text = response.data?.LastServiceDate
                self.lblAC_Type.text = (response.data?.AccountTypeName)
                self.lblPreferredTech.text = (response.data?.PreferredTechnicianName)!
                
                self.lblBillingAddress.text = (response.data?.BillingAddress)!
                self.lblBillingDetailAddress.text = (response.data?.BillingCity)! + ", " + (response.data?.BillingState)! + ", " + (response.data?.BillingPostalCode)!
                self.lblBillingCountry.text = response.data?.BillingCountry
                
                self.lblShippingAddress.text = (response.data?.ShippingAddress)!
                self.lblShippingDetailAddress.text = (response.data?.ShippingCity)! + ", "  + (response.data?.ShippingState)! + ", " + (response.data?.ShippingPostalCode)!
                self.lblShippingCountry.text = response.data?.ShippingCountry
                self.lblCreatedBy.text = response.data?.CreatedBy
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblLastModifiedBy.text = response.data?.LastModifiedBy
                self.lblLastModifiedDate.text = response.data?.LastModifiedDate
                
                if response.data?.IsActive == "1"
                {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                
                self.contactID = response.data?.PrimaryContact
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.accountID,
                          "Object":"Account",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayOptionValues:[String]?
                var arrayFieldValue:[String]?
                
                var viewFromConstrain = self.viewShippingAddress
                for i in 0..<(response.data?.count)!  {
                    print(response.data?[i].FieldType)
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 5
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 85
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 70
                        case "LongText":
                            constantHeight = 110
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.viewShippingAddress)
                    }
                   
                    let myView = UIView()
                    
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    print(constant)
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewShippingAddress, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewShippingAddress, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        if let customView = Bundle.main.loadNibNamed("DefaultSizeView", owner: self, options: nil)?.first as? DefaultSizeView {
                            
                            customView.lblFieldLabel.text = response.data?[i].FieldLabel
                            customView.lblFieldValue.text = response.data?[i].FieldValue
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextSizeView", owner: self, options: nil)?.first as? LongTextSizeView {
                            
                            customView.txtview.text = response.data?[i].FieldValue
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            for i in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                
                                if ((arrayFieldValue?.contains((arrayOptionValues?[i])!))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![i]
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    
                    if let parent = self.parent as? AccountDetailsViewController
                    {
                        parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                    }
                    
                }, completion: { _ in
                    
                    if let parent = self.parent as? AccountDetailsViewController
                    {
                        parent.view_BottomContainer.isHidden = true
                        parent.actionSheetAccountView.isHidden = true
                        // Here you hide it when animation done
                    }
                    
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    if let parent = self.parent as? AccountDetailsViewController
                    {
                        parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                    }
                    
                    
                }, completion: { _ in
                    if let parent = self.parent as? AccountDetailsViewController
                    {
                        parent.view_BottomContainer.isHidden = false
                        parent.actionSheetAccountView.isHidden = true
                        // Here you hide it when animation done
                    }
                    
                })
                
            }
            else
            {
                // didn't move
            }
        
    }

    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
        } else if sender.tag == 2 {
            if self.contactID != nil && self.contactID != "" {
                let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                User.instance.contactID = contactID!
                User.instance.contactName = self.lblPrimaryContact.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
