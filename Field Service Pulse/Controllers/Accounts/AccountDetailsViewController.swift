//
//  AccountDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 07/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces
import MessageUI
import Fusuma

class AccountDetailsViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MFMessageComposeViewControllerDelegate, searchDelegate {

    //MARK: VAriables
    private let SEGUE_LAYOUT = "segueViewLayout"
    var lastContentOffset: CGFloat = 0
    
    var contactsArr:[ContactsData] = []
    var contactID:String?
    var allUsersArr:[UsersData] = []
    var pickerView = UIPickerView()
   
    var ownerID:String?
    var typeID:String?
    var preferredTechID:String?
    var accountTypeArr:[AccountTypeData] = []
    
    var textfieldTag = 0
    var btnGetLocationTag = 0
    var latBillingAddress:String?
    var longBillingAddress:String?
    var latShippingAddress:String?
    var longShippingAddress:String?
    var isActive:String?
    
    var responseAC_Details:accountDetailsResponse?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    @IBOutlet weak var txtAccountOwner: UITextField!
    @IBOutlet weak var txtName: AkiraTextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtAccessNote: AkiraTextField!
    @IBOutlet weak var txtPopupReminder: AkiraTextField!
    @IBOutlet weak var txtNotes: AkiraTextField!
    @IBOutlet weak var txtLastActivity: AkiraTextField!
    @IBOutlet weak var txtLastServiceDate: AkiraTextField!
    @IBOutlet weak var txtPrimaryContact: UITextField!
    @IBOutlet weak var txtPreferredTech: UITextField!
    @IBOutlet weak var txtBillingAddress: UITextField!
    @IBOutlet weak var txtBillingCountry: AkiraTextField!
    @IBOutlet weak var txtBillingPostalCode: AkiraTextField!
    @IBOutlet weak var txtShippingAddress: UITextField!
    @IBOutlet weak var txtShippingCity: AkiraTextField!
    @IBOutlet weak var txtShippingState: AkiraTextField!
    @IBOutlet weak var txtShippingCountry: AkiraTextField!
    @IBOutlet weak var txtShippingPostalCode: AkiraTextField!
    @IBOutlet weak var txtBillingCity: AkiraTextField!
    @IBOutlet weak var txtBillingState: AkiraTextField!
    @IBOutlet weak var txtAccountTypeDropdown: UITextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    
    
    //UIButton
    @IBOutlet weak var btnRelated: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var btnShippingAddress: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    //UILabel
    @IBOutlet weak var lblType1: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var view_BottomContainer: UIView!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var containerViewLayout: UIView!
    @IBOutlet weak var actionSheetAccountView: UIView!
    @IBOutlet weak var heightConstraintActionSheet: NSLayoutConstraint!
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        pickerView.delegate = self
        txtAccountOwner.inputView = pickerView
        txtPreferredTech.inputView = pickerView
        txtAccountTypeDropdown.inputView = pickerView
        txtPrimaryContact.inputView = pickerView
        setupUI()
        webserviceCall()
        webserviceCallForAccountType()
        webserviceCallForGetAllUsers()
        webserviceCallForCustomFields()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        if let actionsheetView = Bundle.main.loadNibNamed("ActionMenuAccountView", owner: self, options: nil)?.first as? ActionSheetAccount
        {
            actionsheetView.btnClose.addTarget(self, action: #selector(btnClose), for: .touchUpInside)
            actionsheetView.btnNewContact.addTarget(self, action: #selector(btnNewContact), for: .touchUpInside)
            actionsheetView.btnNewWO.addTarget(self, action: #selector(btnNewWO), for: .touchUpInside)
            actionsheetView.btnNewEstimate.addTarget(self, action: #selector(btnNewEstimate), for: .touchUpInside)
            actionsheetView.btnNewInvoice.addTarget(self, action: #selector(btnNewInvoice), for: .touchUpInside)
            actionsheetView.btnNewFile.addTarget(self, action: #selector(btnNewFile), for: .touchUpInside)
            actionsheetView.btnNewTask.addTarget(self, action: #selector(btnNewTask), for: .touchUpInside)
            actionsheetView.btnCall.addTarget(self, action: #selector(btnCall1), for: .touchUpInside)
            actionsheetView.btnText.addTarget(self, action: #selector(btnMessage1), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnCopyAC.addTarget(self, action: #selector(btnCloneAC), for: .touchUpInside)
            actionsheetView.btnNewEvent.addTarget(self, action: #selector(btnNewEvent), for: .touchUpInside)
            actionsheetView.btnDeleteAC.addTarget(self, action: #selector(btnDeleteAC), for: .touchUpInside)
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetAccountView.bounds
            self.actionSheetAccountView.addSubview(actionsheetView)
        }
        
        self.containerview.isHidden = true
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnDetails.giveCornerRadius()
        btnRelated.giveCornerRadius()
        btnRelated.giveBorderToButton()
        btnDetails.giveBorderToButton()
        btnBillingAddress.giveBorderToButton()
        btnShippingAddress.giveBorderToButton()
        self.txtAccountTypeDropdown.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtPreferredTech.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtPrimaryContact.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtPrimaryContact.attributedPlaceholder = NSAttributedString(string: "Search Contact", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.txtAccountOwner.setRightImage(name: "search_small", placeholder: "--None--")
        
        heightConstraintActionSheet.constant =  User.instance.screenHeight * (0.50)
    }
    func loadNIB()
    {
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse
        {
            customView.btnAttachFile.addTarget(self, action: #selector(btnAttachFile), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(btnGallery), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 44 + UIApplication.shared.statusBarFrame.height, width: User.instance.screenWidth, height: User.instance.screenHeight-(44 + UIApplication.shared.statusBarFrame.height))
            self.view.addSubview(customView)
        }
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        if textfieldTag == 1 {
            txtAccountOwner.text = searchVC.fullName
            ownerID = searchVC.userID
        }
        else if textfieldTag == 4 {
            txtPreferredTech.text = searchVC.fullName
            preferredTechID = searchVC.userID
        }
        else if textfieldTag == 12 {
            txtPrimaryContact.text = searchVC.relatedContactName
            contactID = searchVC.relatedContactID
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @objc func btnClose()
    {
        self.view_back.isHidden = true
        self.actionSheetAccountView.isHidden = true
        self.view_BottomContainer.isHidden = false
    }
    @objc func btnNewEvent()
    {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "Account"
        createEventVC.objectID = User.instance.accountID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerViewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionSheetAccountView.isHidden = true
        self.view_BottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    @objc func btnCall1()
    {
        if  let number = self.txtPhone.text {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    @objc func btnMessage1()
    {
        if let number = self.txtPhone.text {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
        
    }
    @objc func btnCloneAC()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        vc.flagResponse = "1"
        vc.response = responseAC_Details
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnDeleteAC()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Account",
                          "What":User.instance.accountID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    @objc func btnNewContact()
    {
        let vc = UIStoryboard(name: "Contact", bundle: nil).instantiateViewController(withIdentifier: "CreateContactViewController") as? CreateContactViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewWO()
    {
        let vc = UIStoryboard(name: "WorkOrder", bundle: nil).instantiateViewController(withIdentifier: "CreateWOViewController") as? CreateWOViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewEstimate()
    {
        let vc = UIStoryboard(name: "Estimate", bundle: nil).instantiateViewController(withIdentifier: "CreateEstimateViewController") as? CreateEstimateViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnNewInvoice()
    {
        let vc = UIStoryboard(name: "Invoice", bundle: nil).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as? CreateInvoiceViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewFile()
    {
        loadNIB()
        
    }
    @objc func btnNewTask()
    {
        let vc = UIStoryboard(name: "Task", bundle: nil).instantiateViewController(withIdentifier: "CreateTaskViewController") as? CreateTaskViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnAttachFile()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: false) {
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = true
            }
        }
    }
    @objc func btnGallery()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        //imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        //fusuma.hasVideo = true //To allow for video capturing with .library and .camera available by default
        fusuma.cropHeightRatio = 0.6 // Height-to-width ratio. The default value is 1, which means a squared-size photo.
        fusuma.allowMultipleSelection = true // You can select multiple photos from the camera roll. The default value is false.
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":User.instance.accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.responseAC_Details = response
                self.txtAccountOwner.text = response.data?.AssignedToName
                self.txtName.text = response.data?.AccountName
                self.txtNotes.text = response.data?.Notes
                self.txtPhone.text = response.data?.PhoneNo
                self.txtAccessNote.text = response.data?.AccessNotes
                self.txtLastActivity.text = response.data?.LastActivityDate
                self.txtPopupReminder.text = response.data?.PopUpReminder
                self.txtPrimaryContact.text = response.data?.PrimaryContactName
                self.txtLastServiceDate.text = response.data?.LastServiceDate
                self.txtAccountTypeDropdown.text = (response.data?.AccountTypeName)
                self.txtPreferredTech.text = (response.data?.PreferredTechnicianName) ?? ""
                self.ownerID = response.data?.AssignedTo
                self.typeID = response.data?.AccountType
                self.contactID = response.data?.PrimaryContact
                self.isActive = response.data?.IsActive
                self.preferredTechID = response.data?.PreferredTechnician
                self.txtBillingAddress.text = response.data?.BillingAddress
                self.txtBillingCity.text = response.data?.BillingCity
                self.txtBillingState.text = response.data?.BillingState
                self.txtBillingCountry.text = response.data?.BillingCountry
                self.txtBillingPostalCode.text = response.data?.BillingPostalCode
                self.txtShippingAddress.text = response.data?.ShippingAddress
                self.txtShippingCity.text = response.data?.ShippingCity
                self.txtShippingState.text = response.data?.ShippingState
                self.txtShippingCountry.text = response.data?.ShippingCountry
                self.txtShippingPostalCode.text = response.data?.ShippingPostalCode
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtCreatedBy.text = response.data?.CreatedBy
                self.txtLastModifiedDate.text = response.data?.LastModifiedDate
                self.txtLastModifiedBy.text = response.data?.LastModifiedBy
                
                self.lblPhone.text = "Phone: " + (self.txtPhone.text ?? "")
                self.lblType1.text = "Type: " + ((response.data?.AccountTypeName) ?? "")
                self.lblHeaderTitle.text = response.data?.AccountName
                User.instance.accountName = (response.data?.AccountName)!
                if self.isActive == "1"
                {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                if response.data?.PopUpReminder != "" {
                    Helper.instance.showAlertView(message: (response.data?.PopUpReminder)!, vc: self)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.allUsersArr = response.UsersData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForAccountType()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountType(urlString: API.accountTypeURL, parameters: parameters , headers: headers, vc: self) { (response:AccountTypeResponse) in
            
            if response.Result == "True"
            {
                
                self.accountTypeArr = response.TypeData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.accountID,
                          "Object":"Account",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                var arrayFieldValue:[String]?
                
                var viewFromConstrain:Any = self.txtShippingPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtShippingPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShippingPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShippingPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            //
                            customView.txtLabelField.text = response.data?[i].FieldValue
                            
                            //
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtLabelField.text ?? "")
                            
                            //
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                           
                            if response.data?[i].IsRequired == "1" {
                                
                                if response.data?[i].FieldValue == "" {
                                    customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                    customView.txtviewLabelField.textColor = UIColor.lightGray
                                } else {
                                    customView.txtviewLabelField.text = response.data?[i].FieldValue
                                    
                                }
                                
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtviewLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtviewLabelField.text ?? "")
                            
                            //
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            //Edit
                            if response.data?[i].FieldValue == "" {
                                self.checkboxValueArr.append("")
                            } else{
                                self.checkboxValueArr.append("\((response.data?[i].FieldValue ?? "")),")
                            }
                            //
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                if ((arrayFieldValue?.contains((arrayCheckboxValues[j])))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                
                //Edit
                if value == "," {
                    checkboxValueArr.insert("\(sender.accessibilityLabel!),", at: index)
                } else {
                    checkboxValueArr.insert(value + "\(sender.accessibilityLabel!),", at: index)
                }
                //
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: "\(sender.accessibilityLabel!),", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.view_BottomContainer.isHidden = true
                    self.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.view_BottomContainer.isHidden = false
                    self.actionSheetAccountView.isHidden = true
                    // Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            
            view_back.isHidden = true
            actionSheetAccountView.isHidden = true
            
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 8
        {
            return accountTypeArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 8
        {
            return row == 0 ? "--None--":accountTypeArr[row-1].AccountType
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 8
        {
            if row == 0
            {
                txtAccountTypeDropdown.text = ""
            }
            else
            {
                txtAccountTypeDropdown.text = accountTypeArr[row-1].AccountType
                typeID = accountTypeArr[row-1].AccountTypeID
            }
        }
        
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        //
        if textField == txtAccountOwner || textField == txtPreferredTech || textField == txtAccountTypeDropdown
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtName.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtPreferredTech.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtAccessNote.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtPopupReminder.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtNotes.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtAccountTypeDropdown.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtLastActivity.becomeFirstResponder()
        }
        else if textField.tag == 10
        {
            txtLastServiceDate.becomeFirstResponder()
        }
        else if textField.tag == 11
        {
            txtPrimaryContact.becomeFirstResponder()
        }
        else if textField.tag == 12
        {
            txtBillingAddress.becomeFirstResponder()
        }
        else if textField.tag == 14
        {
            txtBillingCity.becomeFirstResponder()
        }
        else if textField.tag == 15
        {
            txtBillingState.becomeFirstResponder()
        }
        else if textField.tag == 16
        {
            txtBillingCountry.becomeFirstResponder()
        }
        else if textField.tag == 17
        {
            txtBillingPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 18
        {
            txtShippingAddress.becomeFirstResponder()
        }
        else if textField.tag == 20
        {
            txtShippingCity.becomeFirstResponder()
        }
        else if textField.tag == 21
        {
            txtShippingState.becomeFirstResponder()
        }
        else if textField.tag == 22
        {
            txtShippingCountry.becomeFirstResponder()
        }
        else if textField.tag == 23
        {
            txtShippingPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 24
        {
            txtShippingPostalCode.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1 || textField.tag == 4
        {
            textfieldTag = textField.tag
            textField.resignFirstResponder()
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 12
        {
            textfieldTag = textField.tag
            textField.resignFirstResponder()
            textfieldTapped(object: "RelatedContact")
        }
        if textField.tag == 13 || textField.tag == 19
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    
    //MARK: TextView Delegate Methods
    
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnDetails(_ sender: Any) {
       
        self.containerview.isHidden = true
        self.scrollview.isHidden = true
        containerViewLayout.isHidden = false
        self.btnDetails.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func btnRelated(_ sender: Any) {
        
        self.containerview.isHidden = false
        self.scrollview.isHidden = true
        containerViewLayout.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCall(_ sender: Any) {
        
        if let number = self.txtPhone.text {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    
    @IBAction func btnIsActive(_ sender: Any) {
        
        if (btnIsActive.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isActive = "0"
        }
        else{
            btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            isActive = "1"
        }
    }
    @IBAction func btnMessage(_ sender: Any) {
        if let number = self.txtPhone.text {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnCalender(_ sender: Any) {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "Account"
        createEventVC.objectID = User.instance.accountID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    
    @IBAction func txtLastActivity(_ sender: Any) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForLastActivity), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForLastActivity(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        txtLastActivity.text = dateFormatter.string(from: sender.date)
        
    }
    
    @objc func datePickerValueChangedForLastServiceDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        txtLastServiceDate.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func txtLastServiceDate(_ sender: Any) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForLastServiceDate), for: UIControl.Event.valueChanged)
    }
    
    func editWebserviceCall() {
        
        if ownerID == nil || txtName.text! == "" || typeID == nil || txtPhone.text! == "" || txtBillingAddress.text! == "" || txtBillingCity.text! == "" || txtBillingState.text! == "" || txtBillingCountry.text! == "" || txtBillingPostalCode.text! == "" || txtShippingAddress.text == "" || txtShippingCity.text! == "" || txtShippingState.text! == "" || txtShippingCountry.text! == "" || txtShippingPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        self.showHUD()
        var parameters:[String:String] = [:]
        
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["AssignedTo"] = ownerID
        parameters["AccountID"] = User.instance.accountID
        parameters["AccountName"] = txtName.text!
        parameters["PhoneNo"] = txtPhone.text ?? ""
        parameters["BillingAddress"] = txtBillingAddress.text ?? ""
        parameters["BillingCity"] = txtBillingCity.text ?? ""
        parameters["BillingPostalCode"] = txtBillingPostalCode.text ?? ""
        parameters["BillingState"] = txtBillingState.text ?? ""
        parameters["BillingCountry"] = txtBillingCountry.text ?? ""
        parameters["BillingLatitude"] = latBillingAddress ?? ""
        parameters["BillingLongitude"] = longBillingAddress ?? ""
        parameters["ShippingLatitude"] = latShippingAddress ?? ""
        parameters["ShippingLongitude"] = longShippingAddress ?? ""
        parameters["ShippingAddress"] = txtShippingAddress.text ?? ""
        parameters["ShippingCity"] = txtShippingCity.text ?? ""
        parameters["ShippingState"] = txtShippingState.text ?? ""
        parameters["ShippingCountry"] = txtShippingCountry.text ?? ""
        parameters["ShippingPostalCode"] = txtShippingPostalCode.text ?? ""
        parameters["AccessNotes"] = txtAccessNote.text ?? ""
        parameters["PopUpReminder"] = txtPopupReminder.text ?? ""
        parameters["Notes"] = txtNotes.text ?? ""
        parameters["AccountType"] = typeID
        parameters["LastActivityDate"] = txtLastActivity.text!
        parameters["LastServiceDate"] = txtLastServiceDate.text!
        parameters["PrimaryContact"] = contactID
        parameters["IsActive"] = isActive ?? ""
        parameters["PreferredTechnician"] = preferredTechID
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editAccountURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.webserviceCall()
                
                self.containerViewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[1] as! AccountDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        
        
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        if customFieldKeyArr.count != 0 {
            
            if lastTextfieldTapped != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                    customFieldValueArr.append(lastTextfieldTapped.text ?? "")
                }
            }
            
        }
        
        
        //CustomTextView
        if customFieldKeyArr.count != 0 {
            
            if lastTappedTextView != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                    customFieldValueArr.append(lastTappedTextView.text ?? "")
                }
            }
            
        }
        
        checkboxEditedValueArr.removeAll()
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropLast()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        
        editWebserviceCall()
    }
    @IBAction func btnMore(_ sender: Any) {
        self.actionSheetAccountView.isHidden = false
        self.view_BottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerViewLayout.isHidden = true
        scrollview.isHidden = false
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        self.btnCancel.isHidden = true
        self.btnSave.isHidden = true
        containerViewLayout.isHidden = false
        scrollview.isHidden = true
        
        let vc  = self.children[1] as! AccountDetailsLayoutViewController
        vc.viewWillAppear(true)
    }
    
    
    @IBAction func btnBillingAddress(_ sender: Any) {
        
        btnGetLocationTag = 0
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "AC_Details"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func btnShippingAddress(_ sender: Any) {
        
        btnGetLocationTag = 1
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "AC_Details"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        if btnGetLocationTag == 0
        {
            if let SelectLocationVC = sender.source as? SelectLocationViewController{
                
                txtBillingAddress.text = SelectLocationVC.address
                txtBillingCity.text = SelectLocationVC.city
                txtBillingState.text = SelectLocationVC.state
                txtBillingCountry.text = SelectLocationVC.country
                txtBillingPostalCode.text = SelectLocationVC.postalCode
                self.latBillingAddress = SelectLocationVC.latitude
                self.longBillingAddress = SelectLocationVC.longitude
            }
        }else{
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtShippingAddress.text = SelectLocationVC.address
                txtShippingCity.text = SelectLocationVC.city
                txtShippingState.text = SelectLocationVC.state
                txtShippingCountry.text = SelectLocationVC.country
                txtShippingPostalCode.text = SelectLocationVC.postalCode
                self.latShippingAddress = SelectLocationVC.latitude
                self.longShippingAddress = SelectLocationVC.longitude
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension AccountDetailsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            
            if textfieldTag == 13
            {
                latBillingAddress = String(place.coordinate.latitude)
                longBillingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtBillingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtBillingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtBillingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtBillingState.text = component.name
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtBillingCountry.text = component.name
                }
            }
            else
            {
                latShippingAddress = String(place.coordinate.latitude)
                longShippingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtShippingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtShippingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtShippingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtShippingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtShippingCountry.text = component.name
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension AccountDetailsViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtName.text ?? "",
                          "AssignedTo":self.ownerID!,
                          "RelatedTo":"Account",
                          "What":User.instance.accountID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.uploadDocs(urlString: API.createNewFileAllURL, fileName: "FileName", URLs: urls as [NSURL], parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
}

extension AccountDetailsViewController:FusumaDelegate {
    
    // Return the image which is selected from camera roll or is taken via the camera.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(image: UIImage, source: FusumaMode) {
        
        print("Called just after FusumaViewController is dismissed.")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        print("Called just after a video has been selected.")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // Return selected images when you allow to select multiple photos.
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtName.text ?? "",
                          "AssignedTo":self.ownerID!,
                          "RelatedTo":"Account",
                          "What":User.instance.accountID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createNewFileAll(urlString: API.createNewFileAllURL, pickedImages: images, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // Return an image and the detailed information.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
    }
}
