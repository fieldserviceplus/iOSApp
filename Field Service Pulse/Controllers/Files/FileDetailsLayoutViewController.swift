//
//  FileDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 25/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class FileDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var assignedToID:String?
    var relatedToID:String?
    var RelatedToName:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var lblFileSize: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblLastModifiedDate: UILabel!
    @IBOutlet weak var lblLastModifiedBy: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewLast: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        webserviceCall()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "FileID":User.instance.fileID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.fileDetails(urlString: API.fileDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:FileDetails) in
            
            if response.Result == "True"
            {
                self.lblAssignedTo.text =  response.data?.AssignedToName
                self.lblSubject.text = response.data?.Subject
                self.lblRelatedTo.text = (response.data?.RelatedTo ?? "") 
                self.txtviewDescription.text = response.data?.Description
                self.lblFileName.text = response.data?.FileName
                self.lblFileSize.text = (response.data?.FileSize ?? "") + " MB"
                self.lblLastModifiedBy.text = (response.data?.LastModifiedBy ?? "")
                self.lblLastModifiedDate.text = (response.data?.LastModifiedDate ?? "")
                self.lblCreatedBy.text = (response.data?.CreatedBy ?? "")
                self.lblCreatedDate.text = (response.data?.CreatedDate ?? "")
                
                self.assignedToID = response.data?.AssignedTo
                self.relatedToID = response.data?.What
                self.RelatedToName = response.data?.RelatedToName
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
        
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? FileDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? FileDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = true
                    parent.actionSheetView.isHidden = true
                    parent.btnClose.isHidden = true// Here you hide it when animation done
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? FileDetailsViewController
                {
                    parent.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? FileDetailsViewController
                {
                    parent.viewBottomContainer.isHidden = false // Here you hide it when animation done
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnFileName(_ sender: Any) {
        
        if let parent = self.parent as? FileDetailsViewController {
            
            if parent.FileURL?.range(of: "zip") != nil {
                
                let activityViewController = UIActivityViewController(activityItems: ["File", parent.FileURL], applicationActivities: nil)
                present(activityViewController, animated: true, completion: nil)
            }
            else
            {
                let docVC = self.storyboard?.instantiateViewController(withIdentifier: "PreviewDocViewController") as! PreviewDocViewController
                docVC.fileURL = parent.FileURL ?? ""
                docVC.fileName = parent.FileName ?? ""
                present(docVC, animated: true, completion: nil)
            }
        }
        
        
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
            
        } else if sender.tag == 2 {
            
            if self.relatedToID != nil && self.relatedToID != "" {
                
                if RelatedToName == "Account" {
                    
                    let acVC = UIStoryboard(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController")  as! AccountDetailsViewController
                    User.instance.accountID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Contact" {
                    
                    let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                    User.instance.contactID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "WorkOrder" {
                    
                    let acVC = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController")  as! WorkOrdersDetailsViewController
                    User.instance.workorderID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Estimate" {
                    
                    let acVC = UIStoryboard(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController")  as! EstimateDetailsViewController
                    User.instance.estimateID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                if RelatedToName == "Invoice" {
                    
                    let acVC = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController")  as! InvoiceDetailsViewController
                    User.instance.invoiceID = relatedToID!
                    self.navigationController?.pushViewController(acVC, animated: true)
                }
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
