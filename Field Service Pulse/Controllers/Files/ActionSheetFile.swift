//
//  ActionSheetFile.swift
//  Field Service Pulse
//
//  Created by Apple on 30/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ActionSheetFile: UIView {

    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCloneFile: UIButton!
    @IBOutlet weak var btnDeleteFile: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var heightBackView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        self.btnOpen.leftImage(image: #imageLiteral(resourceName: "file"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnShare.leftImage(image: #imageLiteral(resourceName: "share"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEdit.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnCloneFile.leftImage(image: #imageLiteral(resourceName: "copy"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnDeleteFile.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: UIImage.RenderingMode.alwaysOriginal)
    }
    
}
