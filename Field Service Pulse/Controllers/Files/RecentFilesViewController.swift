//
//  RecentFilesViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentFilesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    
    private let SEGUE_DETAILS = "segueDetails"
    private let SEGUE_NEW_FILE = "segueNewFile"
    private let SEGUE_FILE_LIST = "segueFileList"
    
    private let CELL_RECENT_FILE = "cell_recentFile"
    
    var getViewsArr:[GetViewsFileData] = []
    var recentFilesArr:[RecentFilesData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown:UIButton!
    
    @IBOutlet weak var table_getViews: UITableView!
    @IBOutlet weak var table_recentFile:UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForRecentTasks()
        webserviceCallForGetViews()
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        table_recentFile.tableFooterView = UIView()
        
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    func  webserviceCallForRecentTasks()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentFiles(urlString: API.recentFilesURL, parameters: parameters, headers: headers, vc: self) { (response:RecentFiles) in
            
            if response.Result == "True"
            {
                self.recentFilesArr = response.data!
                self.table_recentFile.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getViewsFile(urlString: API.getViewsFileURL, parameters: parameters, headers: headers, vc: self) { (response:GetViewsFile
            ) in
            
            if response.Result == "True"
            {
                
                self.getViewsArr = response.data!
                self.table_getViews.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_FILE_LIST
        {
            let listVC = segue.destination as! FilesListViewController
            
            let file = sender as? GetViewsFileData
            listVC.FileViewID = file?.FileViewID ?? ""
            listVC.FileViewName = file?.FileViewName ?? ""
            
        }
        else if segue.identifier == SEGUE_DETAILS
        {
            let detailVC = segue.destination as! FileDetailsViewController
            
            let file = sender as? RecentFilesData
            User.instance.fileID = file?.FileID ?? ""
        }
        
    }
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == table_recentFile
        {
            return recentFilesArr.count
        }
        return getViewsArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == table_recentFile
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_RECENT_FILE) as! RecentFilesTableViewCell
            
            cell.lblSubject.text = recentFilesArr[indexPath.row].Subject
            cell.lblRelatedTo.text = recentFilesArr[indexPath.row].RelatedTo
            cell.lblFileName.text = recentFilesArr[indexPath.row].FileName
            cell.lblContentType.text = recentFilesArr[indexPath.row].ContentType
            cell.lblDescription.text = recentFilesArr[indexPath.row].Description
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getViewsArr[indexPath.row].FileViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_getViews
        {
            self.performSegue(withIdentifier: SEGUE_FILE_LIST, sender: getViewsArr[indexPath.row])
            
        }
        else if tableView == table_recentFile
        {
            self.performSegue(withIdentifier: SEGUE_DETAILS, sender: recentFilesArr[indexPath.row])
        }
        
    }
    
    
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func btnNewFile(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_NEW_FILE, sender: self)
    }
    
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
