//
//  FileDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Photos

class FileDetailsViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, searchDelegate, searchRelatedToDelegate {
    
    
    //MARK: Variables
    private let SEGUE_RECURRING = "segue_recurring"
    private let SEGUE_LAYOUT = "segueDetailsLayout"
    
    var lastContentOffset: CGFloat = 0
    
    var textfieldTag = 0
    var imagePicker = UIImagePickerController()
    var pickedImage:UIImage?
    var relatedObj:String?
    var assignedToID:String?
    var relatedToID:String?
    var relatedObjListArr:[GetRelatedObjListData] = []
    var pickerView = UIPickerView()
    var flag = ""
    var cico:URL?
    var menuView:ActionSheetFile?
    var responseFile_Details:FileDetails?
    var FileURL:String?
    var FileName:String?
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtOwner: UITextField!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtRelatedTo: UITextField!
    @IBOutlet weak var txtWhat: UITextField!
    @IBOutlet weak var txtFileName: AkiraTextField!
    @IBOutlet weak var txtFileSize: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    //UILabel
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var btnClose: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    
    
    //MARK: Functions
    
    func setupUI()
    {
        webserviceCallForFileDetails()
        webserviceCallForGetRelatedObjList()
        
        if let actionsheetView = Bundle.main.loadNibNamed("ActionSheetFileView", owner: self, options: nil)?.first as? ActionSheetFile
        {
            menuView = actionsheetView
            
            actionsheetView.btnClose.addTarget(self, action: #selector(btnCancelMenu), for: .touchUpInside)
            actionsheetView.btnOpen.addTarget(self, action: #selector(btnOpen1), for: .touchUpInside)
            actionsheetView.btnShare.addTarget(self, action: #selector(btnShare1(sender:)), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnCloneFile.addTarget(self, action: #selector(btnCloneFile), for: .touchUpInside)
            actionsheetView.btnDeleteFile.addTarget(self, action: #selector(btnDeleteFile), for: .touchUpInside)
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetView.bounds
            self.actionSheetView.addSubview(actionsheetView)
            
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetView.bounds
            self.actionSheetView.addSubview(actionsheetView)
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        txtRelatedTo.inputView = pickerView
        txtviewDescription.giveBorder()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtOwner.setRightImage(name: "search_small", placeholder: "--None--")
        txtWhat.setRightImage(name: "search_small", placeholder: "--None--")
        txtRelatedTo.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtWhatTapped))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        txtWhat.addGestureRecognizer(tap)
        
    }
    
    func sendData(relatedToId: String, relatedToName: String) {
        
        relatedToID = relatedToId
        txtWhat.text = relatedToName
        
    }
    
    func sendData(searchVC: SearchAccountViewController) {
        
        txtOwner.text = searchVC.fullName
        assignedToID = searchVC.userID
        
    }
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @objc func txtWhatTapped()
    {
        if txtRelatedTo.text == "--None--" || txtRelatedTo.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.enterRelatedTo, vc: self)
            return
        }
        let searchVC = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchRelatedToViewController") as! SearchRelatedToViewController
        searchVC.delegate = self
        searchVC.relatedTo = relatedObj
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @objc func btnCloneFile()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateFileViewController") as! CreateFileViewController
        vc.flagResponse = "1"
        vc.response = responseFile_Details
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func btnDeleteFile()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"File",
                          "What":User.instance.fileID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    
    @objc func btnCancelMenu()
    {
        self.actionSheetView.isHidden = true
        self.btnCancel.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionSheetView.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
        
    }
    @objc func btnOpen1()
    {
        if FileURL?.range(of: "zip") != nil {
            
            let activityViewController = UIActivityViewController(activityItems: ["File", FileURL], applicationActivities: nil)
            present(activityViewController, animated: true, completion: nil)
        }
        else
        {
            let docVC = self.storyboard?.instantiateViewController(withIdentifier: "PreviewDocViewController") as! PreviewDocViewController
            docVC.fileURL = FileURL ?? ""
            docVC.fileName = FileName ?? ""
            present(docVC, animated: true, completion: nil)
        }
        
        
    }
    @objc func btnShare1(sender:UIButton)
    {
        let firstActivityItem = FileName
        let secondActivityItem : NSURL = NSURL(string: FileURL!)!
        
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem!, secondActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender )
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func webserviceCallForGetRelatedObjList()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getRelatedObjList(urlString: API.getRelatedObjListFileURL, parameters: parameters , headers: headers, vc: self) { (response:GetRelatedObjList) in
            
            if response.Result == "True"
            {
                
                self.relatedObjListArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    func webserviceCallForFileDetails()
    {
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "FileID":User.instance.fileID]

        let headers = ["key":User.instance.key,
                       "token":User.instance.token]

        NetworkManager.sharedInstance.fileDetails(urlString: API.fileDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:FileDetails) in

            if response.Result == "True"
            {
                self.responseFile_Details = response
                self.txtOwner.text =  response.data?.AssignedToName
                self.txtSubject.text = response.data?.Subject
                self.txtRelatedTo.text = response.data?.RelatedToName
                self.txtWhat.text = response.data?.WhatName
                self.txtviewDescription.text = response.data?.Description
                self.txtFileName.text = response.data?.FileName
                self.txtFileSize.text = (response.data?.FileSize ?? "") + "MB"
                self.txtLastModifiedBy.text = (response.data?.LastModifiedBy ?? "")
                self.txtLastModifiedDate.text = (response.data?.LastModifiedDate ?? "")
                self.txtCreatedBy.text = (response.data?.CreatedBy ?? "")
                self.txtCreatedDate.text = (response.data?.CreatedDate ?? "")

                self.assignedToID = (response.data?.AssignedTo ?? "")
                self.relatedToID = (response.data?.What ?? "")
                self.relatedObj = (response.data?.RelatedToName ?? "")

                self.FileURL = (response.data?.FileURL ?? "")
                self.FileName = response.data?.FileName
                
                self.lblAssignedTo.text = "Assigned To: " + ((response.data?.AssignedToName) ?? "")
                self.lblType.text = "Related To: " + (( response.data?.RelatedToName) ?? "")
                self.lblHeaderTitle.text = response.data?.Subject

            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }

        }
    }
    
    func webserviceCallForEditWithoutFile()
    {
        if relatedToID == nil || assignedToID == nil || relatedObj == nil || txtSubject.text == "" || txtFileName.text == "" || txtFileSize.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["FileID"] = User.instance.fileID
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        parameters["FileName"] = ""
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editFileURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.webserviceCallForFileDetails()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[0] as! FileDetailsLayoutViewController
                vc.viewWillAppear(true)
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForEditFile()
    {
        if relatedToID == nil || assignedToID == nil || relatedObj == nil || txtSubject.text == "" || txtFileName.text == "" || txtFileSize.text == "" || pickedImage == nil
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["FileID"] = User.instance.fileID
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        let imgData = pickedImage!.jpegData(compressionQuality: 0.2)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "FileName",fileName: (self.txtFileName.text! + ".jpg"), mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to:API.editFileURL, method: .post, headers: headers)
            
            
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    let userData:[String:Any] = response.result.value as! [String : Any]
                    print(userData)
                    
                    if userData["Result"] as! String == "True"
                    {
                        self.webserviceCallForFileDetails()
                        self.containerviewLayout.isHidden = false
                        self.scrollview.isHidden = true
                        
                        let vc  = self.children[0] as! FileDetailsLayoutViewController
                        vc.viewWillAppear(true)
                    }
                    else
                    {
                        Helper.instance.showAlertNotification(message: userData["ResponseMsg"] as! String, vc: self)
                    }
                    SVProgressHUD.dismiss()
                }
                
            case .failure(let encodingError):
                print(encodingError)
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
            }
            else
            {
                // didn't move
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            view_back.isHidden = true
            actionSheetView.isHidden = true
            btnCancel.isHidden = true
            //view_backNewFile.isHidden = true
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 3
        {
            return relatedObjListArr.count + 1
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 3
        {
            return row == 0 ? "--None--":relatedObjListArr[row-1].Name
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 3
        {
            if row == 0
            {
                txtRelatedTo.text = ""
            }
            else{
                txtRelatedTo.text = relatedObjListArr[row-1].Name
                relatedObj = txtRelatedTo.text
            }
        }
        txtWhat.text = ""
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtOwner || textField == txtRelatedTo || textField == txtWhat
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.tag == 1
        {
            txtSubject.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtRelatedTo.becomeFirstResponder()
            
        }
        else if textField.tag == 3
        {
            txtWhat.becomeFirstResponder()
        }
        else if textField.tag == 4
        {
            txtFileName.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtFileSize.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtFileSize.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        
        pickerView.reloadAllComponents()
    }
    
    //MARK: UITextview Delegate Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description"
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtviewDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        if flag == "Photo" {
            
            webserviceCallForEditFile()
        } else if flag == "Document"  {
            
            if cico != nil {
                self.downloadfile(URL: cico! as NSURL)
            }
        }
        else
        {
            webserviceCallForEditWithoutFile()
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
        
        txtviewDescription.isUserInteractionEnabled = true
        
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        btnCancel.isHidden = false
        btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
    }
    
    
    
    
    @IBAction func btnOpen(_ sender: Any) {
        if FileURL?.range(of: "zip") != nil {
            
            let activityViewController = UIActivityViewController(activityItems: ["File", FileURL], applicationActivities: nil)
            present(activityViewController, animated: true, completion: nil)
        }
        else
        {
            let docVC = self.storyboard?.instantiateViewController(withIdentifier: "PreviewDocViewController") as! PreviewDocViewController
            docVC.fileURL = FileURL ?? ""
            docVC.fileName = FileName ?? ""
            present(docVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let firstActivityItem = FileName
        let secondActivityItem : NSURL = NSURL(string: FileURL!)!
        
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem!, secondActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func btnMore(_ sender: Any) {
        
        //self.btnCancel.isHidden = false
        self.actionSheetView.isHidden = false
        self.viewBottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
    
    @IBAction func btnBrowse(_ sender: Any) {
        
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse {
            
            customView.tag = 101
            customView.btnAttachFile.addTarget(self, action: #selector(openFiles), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(openGallary), for: .touchUpInside)
            customView.btnTakePhotos.addTarget(self, action: #selector(openCamera), for: .touchUpInside)
            customView.frame = CGRect(x: 0, y: 44 + UIApplication.shared.statusBarFrame.height, width: self.view.frame.width, height: self.view.frame.height-(44 + UIApplication.shared.statusBarFrame.height))
            self.view.addSubview(customView)
        }
        
    }
    
    @objc func openCamera()
    {
        
        flag = "Photo"
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            checkPermission()
        }
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    @objc func openGallary()
    {
        flag = "Photo"
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        checkPermission()
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            present(imagePicker, animated: true, completion: nil)
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    self.present(self.imagePicker, animated: true, completion: nil)
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if let asset = info["UIImagePickerControllerPHAsset"] as? PHAsset{
//            if let fileName = asset.value(forKey: "filename") as? String{
//                print(fileName)
//                txtFileName.text = fileName
//            }
            let assetResources = PHAssetResource.assetResources(for: asset)
            
            print(assetResources.first!.originalFilename)
            txtFileName.text = assetResources.first!.originalFilename ?? "unknown"
        }
        
        if info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage != nil {
            pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
            
            let imgData: NSData = NSData(data: (pickedImage)!.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.length
            print("size of image in KB: \(Double(imageSize) / 1024.0)")
            
            txtFileSize.text = String(format: "%.2f", Double(imageSize) / 1024.0) + "KB"
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func openFiles()
    {
        flag = "Document"
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    
    //After having the URL you can download file and then upload the file.
    func downloadfile(URL: NSURL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL as URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = response?.mimeType
                print("Success: \(String(describing: statusCode))")
                DispatchQueue.main.async(execute: {
                    self.uploadDocument(data!, filename: URL.lastPathComponent!)
                })
                
                // This is your file-variable:
                // data
            }
            else {
                // Failure
                print("Failure: %@", error!.localizedDescription)
            }
        })
        task.resume()
    }
    
    func uploadDocument(_ file: Data,filename : String) {
        
        if relatedToID == nil || assignedToID == nil || relatedObj == nil || txtSubject.text == "" || txtFileName.text == "" || txtFileSize.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        self.showHUD()
        
        var parameters:[String:String] = [:]
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        parameters["FileID"] = User.instance.fileID
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        let fileData = file
        let URL2 = try! URLRequest(url: API.editFileURL, method: .post, headers: headers)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(fileData as Data, withName: "FileName", fileName: filename, mimeType: "text/plain")
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, with: URL2 , encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                print("success")
                upload.responseJSON {
                    response in
                    if let JSON = response.result.value as? [String : Any]{
                        print(JSON)
                        
                        if JSON["Result"] as! String == "True"
                        {
                            self.webserviceCallForFileDetails()
                            self.containerviewLayout.isHidden = false
                            self.scrollview.isHidden = true
                            
                            let vc  = self.children[0] as! FileDetailsLayoutViewController
                            vc.viewWillAppear(true)
                        }
                        else
                        {
                            Helper.instance.showAlertNotification(message: JSON["ResponseMsg"] as! String, vc: self)
                        }
                        SVProgressHUD.dismiss()
                    }else {
                        //error hanlding
                        SVProgressHUD.dismiss()
                    }
                    
                }
            case .failure(_):
                print("Failure")
                SVProgressHUD.dismiss()
            }
        })
    }
}
extension FileDetailsViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        txtFileName.text = url.lastPathComponent ?? ""
        print(url.pathExtension)
        
        do
        {
            
            let fileDictionary = try FileManager.default.attributesOfItem(atPath: (cico?.path)!)
            let fileSize = fileDictionary[FileAttributeKey.size] as! Double
            print ("\(fileSize)")
            txtFileSize.text = String(format: "%.2f", (fileSize / 1000.0)) + "KB"
            
        }
        catch{}
        
    }
    
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
