//
//  CreateFileViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Photos

class CreateFileViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, searchRelatedToDelegate, searchDelegate {
    
    //MARK: Variables
    
    var textfieldTag = 0
    var imagePicker = UIImagePickerController()
    var pickedImage:UIImage?
    var relatedObj:String?
    var assignedToID:String?
    var relatedToID:String?
    var relatedObjListArr:[GetRelatedObjListData] = []
    var pickerView = UIPickerView()
    var flag = ""
    var cico:URL?
    var flagResponse = "0"
    var response:FileDetails?
    var accountName:String?
    var workOrderSubject:String?
    var estimateName:String?
    var contactName:String?
    var invoiceNo:String?
    
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtOwner: UITextField!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtRelatedTo: UITextField!
    @IBOutlet weak var txtWhat: UITextField!
    @IBOutlet weak var txtFileName: AkiraTextField!
    @IBOutlet weak var txtFileSize: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
   
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var viewBackground: UIView!
    //UILabel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetRelatedObjList()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        imagePicker.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        txtWhat.inputView = pickerView
        txtRelatedTo.inputView = pickerView
        txtviewDescription.giveBorder()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtOwner.setRightImage(name: "search_small", placeholder: "--None--")
        txtWhat.setRightImage(name: "search_small", placeholder: "--None--")
        txtRelatedTo.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        if flagResponse == "1" {
            responseFileDetails()
            flagResponse = "0"
        }
        if accountName != nil {
            self.txtRelatedTo.text = "Account"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = accountName
        }
        if workOrderSubject != nil {
            self.txtRelatedTo.text = "WorkOrder"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = workOrderSubject
        }
        if estimateName != nil {
            self.txtRelatedTo.text = "Estimate"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = estimateName
        }
        if contactName != nil {
            self.txtRelatedTo.text = "Contact"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = contactName
        }
        if invoiceNo != nil {
            self.txtRelatedTo.text = "Invoice"
            relatedObj = self.txtRelatedTo.text
            self.txtWhat.text = invoiceNo
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtWhatTapped))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        txtWhat.addGestureRecognizer(tap)
        
    }
    func sendData(relatedToId: String, relatedToName: String) {
        
        relatedToID = relatedToId
        txtWhat.text = relatedToName
    }
    
    func sendData(searchVC: SearchAccountViewController) {
        
        txtOwner.text = searchVC.fullName
        assignedToID = searchVC.userID
    }
    
    @objc func txtWhatTapped()
    {
        if txtRelatedTo.text == "--None--" || txtRelatedTo.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.enterRelatedTo, vc: self)
            return
        }
        let searchVC = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchRelatedToViewController") as! SearchRelatedToViewController
        searchVC.delegate = self
        searchVC.relatedTo = relatedObj
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func webserviceCallForGetRelatedObjList()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getRelatedObjList(urlString: API.getRelatedObjListFileURL, parameters: parameters , headers: headers, vc: self) { (response:GetRelatedObjList) in
            
            if response.Result == "True"
            {
                self.relatedObjListArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    func webserviceCall()
    {
        if relatedToID == nil || assignedToID == nil || relatedObj == nil || txtSubject.text == "" || txtFileName.text == "" || txtFileSize.text == "" || pickedImage == nil
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        let imgData = pickedImage!.jpegData(compressionQuality: 0.2)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "FileName",fileName: (self.txtFileName.text! + ".jpg"), mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to:API.createFileURL, method: .post, headers: headers)
            
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    let userData:[String:Any] = response.result.value as! [String : Any]
                    print(userData)
                    
                    if userData["Result"] as! String == "True"
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                        User.instance.fileID = String(userData["FileID"] as! Int)
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    else
                    {
                        Helper.instance.showAlertNotification(message: userData["ResponseMsg"] as! String, vc: self)
                    }
                    SVProgressHUD.dismiss()
                }
                
            case .failure(let encodingError):
                print(encodingError)
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func responseFileDetails()
    {
        self.txtOwner.text =  response?.data?.AssignedToName
        self.txtSubject.text = response?.data?.Subject
        self.txtRelatedTo.text = response?.data?.RelatedToName
        self.txtWhat.text = response?.data?.WhatName
        self.txtviewDescription.text = response?.data?.Description
        self.txtFileName.text = ""
        self.txtFileSize.text = ""
        
        self.assignedToID = (response?.data?.AssignedTo ?? "")
        self.relatedToID = (response?.data?.What ?? "")
        self.relatedObj = (response?.data?.RelatedToName ?? "")
    }
    
    
    
    
    
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 3
        {
            return relatedObjListArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 3
        {
            return row == 0 ? "--None--":relatedObjListArr[row-1].Name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 3
        {
            if row == 0
            {
                txtRelatedTo.text = ""
            }
            else{
                txtRelatedTo.text = relatedObjListArr[row-1].Name
                relatedObj = txtRelatedTo.text
            }
        }
        txtWhat.text = ""
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        
        //
        if textField == txtOwner || textField == txtRelatedTo || textField == txtWhat 
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.tag == 1
        {
            txtSubject.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtRelatedTo.becomeFirstResponder()
            
        }
        else if textField.tag == 3
        {
            txtWhat.becomeFirstResponder()
        }
        else if textField.tag == 4
        {
            txtFileName.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtFileSize.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtFileSize.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 4
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 6
        {
            textfieldTag = textField.tag
        }
        pickerView.reloadAllComponents()
        
        
    }
    
    
    
    
    //MARK: UITextview Delegate Methods
    var lastTappedTextView:UITextView!
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description"
        {
            textView.text = ""
        }
        
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description"
        }
        
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtviewDescription.resignFirstResponder()
            return false
        }
        return true
    }
    //MARK: IBActions
    
   
    @IBAction func btnSave(_ sender: Any) {
        
        if flag == "Photo" {
            
            webserviceCall()
        } else {
            
            if cico != nil {
                self.downloadfile(URL: cico! as NSURL)
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBrowse(_ sender: Any) {
        
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse {
            
            customView.tag = 101
            customView.btnAttachFile.addTarget(self, action: #selector(openFiles), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(openGallary), for: .touchUpInside)
            customView.btnTakePhotos.addTarget(self, action: #selector(openCamera), for: .touchUpInside)
            customView.frame = CGRect(x: 0, y: 44 + UIApplication.shared.statusBarFrame.height, width: self.view.frame.width, height: self.view.frame.height-(44 + UIApplication.shared.statusBarFrame.height))
            self.view.addSubview(customView)
        }
    }
    
    @objc func openCamera()
    {
       
        flag = "Photo"
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            
            checkPermission()
        }
        
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    @objc func openGallary()
    {
        flag = "Photo"
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        checkPermission()
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            present(imagePicker, animated: true, completion: nil)
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    self.present(self.imagePicker, animated: true, completion: nil)
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if let asset = info["UIImagePickerControllerPHAsset"] as? PHAsset{
//            if let fileName = asset.value(forKey: "filename") as? String{
//                print(fileName)
//                txtFileName.text = fileName
//            }
            let assetResources = PHAssetResource.assetResources(for: asset)
            
            print(assetResources.first!.originalFilename)
            txtFileName.text = assetResources.first!.originalFilename ?? "unknown"
            
        }
        
        if info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage != nil {
            pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
            
            let imgData: NSData = NSData(data: (pickedImage)!.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.length
            print("size of image in KB: \(Double(imageSize) / 1024.0)")
            
            txtFileSize.text = String(format: "%.2f", Double(imageSize) / 1024.0) + "KB"
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func openFiles()
    {
        flag = "Document"
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    
    //After having the URL you can download file and then upload the file.
    func downloadfile(URL: NSURL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL as URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = response?.mimeType
                print("Success: \(String(describing: statusCode))")
                DispatchQueue.main.async(execute: {
                    self.uploadDocument(data!, filename: URL.lastPathComponent!)
                })
                
                // This is your file-variable:
                // data
            }
            else {
                // Failure
                print("Failure: %@", error!.localizedDescription)
            }
        })
        task.resume()
    }
    
    func uploadDocument(_ file: Data,filename : String) {
        
        if relatedToID == nil || assignedToID == nil || relatedObj == nil || txtSubject.text == "" || txtFileName.text == "" || txtFileSize.text == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        self.showHUD()
        
        var parameters:[String:String] = [:]
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["RelatedTo"] = relatedObj
        parameters["AssignedTo"] = assignedToID ?? ""
        parameters["What"] = relatedToID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["Subject"] = txtSubject.text ?? ""
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        let fileData = file
        let URL2 = try! URLRequest(url: API.createFileURL, method: .post, headers: headers)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(fileData as Data, withName: "FileName", fileName: filename, mimeType: "text/plain")
            
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, with: URL2 , encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                print("success")
                upload.responseJSON {
                    response in
                    if let JSON = response.result.value as? [String : Any]{
                        print(JSON)
                        
                        if JSON["Result"] as! String == "True"
                        {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                            User.instance.fileID = String(JSON["FileID"] as! Int)
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                        else
                        {
                            Helper.instance.showAlertNotification(message: JSON["ResponseMsg"] as! String, vc: self)
                        }
                        SVProgressHUD.dismiss()
                    }else {
                        //error hanlding
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(_):
                print("Failure")
                SVProgressHUD.dismiss()
            }
    })
}
}

extension CreateFileViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        txtFileName.text = url.lastPathComponent ?? ""
        print(url.pathExtension)
        
        do
        {
            let fileDictionary = try FileManager.default.attributesOfItem(atPath: (cico?.path)!)
            let fileSize = fileDictionary[FileAttributeKey.size] as! Double
            print ("\(fileSize)")
            txtFileSize.text = String(format: "%.2f", (fileSize / 1000.0)) + "KB"
        }
        catch{}
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
