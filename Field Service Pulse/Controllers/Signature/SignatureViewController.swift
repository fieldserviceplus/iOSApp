//
//  SignatureViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 15/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SignatureViewController: UIViewController, YPSignatureDelegate {

    //MARK: Variables
    
    var base64:String?
    var flag :String?
    
    //MARK: IBOutlets
    
        @IBOutlet weak var signatureView: YPDrawSignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        signatureView.delegate = self
    }

    //MARK: Function
    
    func  webserviceCallForSaveSignatureWO()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID ,
                          "WorkOrderID": User.instance.workorderID ,
                          "OrganizationID":User.instance.OrganizationID,
                          "Signature": "data:image/png;base64," + base64!]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.saveSignatureURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForSaveSignatureEstimate()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID": User.instance.estimateID ,
                          "Signature": "data:image/png;base64," + base64!]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.saveEstimateSignatureURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    //MARK: Landscape Mode Methods
    
        override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (self.isMovingFromParent) {
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
        
    }
    
     @objc func canRotate() -> Void {}
    
    //MARK: IBAction
    
    @IBAction func btnSave(_ sender: Any) {
        
        if let signatureImage = self.signatureView.getSignature(scale: 1) {
            
            // Saving signatureImage from the line above to the Photo Roll.
            // The first time you do this, the app asks for access to your pictures.
            //UIImageWriteToSavedPhotosAlbum(signatureImage, nil, nil, nil)
            
            // Since the Signature is now saved to the Photo Roll, the View can be cleared anyway.
            
            
            let imageData = signatureImage.resizeImage(newWidth: 349.0).pngData()
            base64 = imageData?.base64EncodedString(options: .init(rawValue: 0))
            
            if flag == "0"
            {
                webserviceCallForSaveSignatureWO()
            }
            else if flag == "1"
            {
                webserviceCallForSaveSignatureEstimate()
            }
            
            self.signatureView.clear()
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnClear(_ sender: Any) {
        // This is how the signature gets cleared
        self.signatureView.clear()
    }
    @IBAction func btnBack(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Delegate Methods
    
    func didStart() {
        print("Started Drawing")
    }
    
    func didFinish() {
        print("Finished Drawing")
    }
    

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
