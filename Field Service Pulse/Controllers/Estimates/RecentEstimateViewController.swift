//
//  RecentEstimateViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentEstimateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    
    private let SEGUE_DETAILS = "segueDetails"
    private let SEGUE_NEW_ESTIMATE = "segueNewESTIMATE"
    private let SEGUE_ESTIMATE_LIST = "segueEstimateList"
    private let CELL_RECENT_ESTIMATE = "cell_recentEstimate"
    
    var getViewsArr:[GetEstimateViewsData] = []
    var recentEstimatesArr:[RecentEstimatesData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown:UIButton!
    @IBOutlet weak var table_getViews: UITableView!
    @IBOutlet weak var table_recentEstimates:UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForRecentEstimates()
        webserviceCallForGetViews()
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        table_recentEstimates.tableFooterView = UIView()
        
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    func  webserviceCallForRecentEstimates()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentEstimates(urlString: API.RecentEstimatesURL, parameters: parameters, headers: headers, vc: self) { (response:RecentEstimates) in
            
            if response.Result == "True"
            {
                self.recentEstimatesArr = response.data!
                self.table_recentEstimates.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func  webserviceCallForGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEstimateViews(urlString: API.getEstimateViewsURL, parameters: parameters, headers: headers, vc: self) { (response:GetEstimateViews) in
            
            if response.Result == "True"
            {
                
                self.getViewsArr = response.data!
                self.table_getViews.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_ESTIMATE_LIST
        {
            let listVC = segue.destination as! EstimateListViewController
            
            let estimate = sender as? GetEstimateViewsData
            listVC.EstimateViewID = estimate?.EstimateViewID ?? ""
            listVC.EstimateViewName = estimate?.EstimateViewName ?? ""
            
        }
        else if segue.identifier == SEGUE_DETAILS
        {
            let detailVC = segue.destination as! EstimateDetailsViewController
            
            let estimate = sender as? RecentEstimatesData
            User.instance.estimateID = estimate?.EstimateID ?? ""
            User.instance.estimateName = estimate?.EstimateName ?? ""
        }
        
    }
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == table_recentEstimates
        {
            return recentEstimatesArr.count
        }
        return getViewsArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == table_recentEstimates
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_RECENT_ESTIMATE) as! RecentEstimateTableViewCell
            
            cell.lblAccountName.text = recentEstimatesArr[indexPath.row].AccountName
            cell.lblEstimateName.text = recentEstimatesArr[indexPath.row].EstimateName
            cell.lblOwner.text = recentEstimatesArr[indexPath.row].Owner
            cell.lblStatus.text = recentEstimatesArr[indexPath.row].Status
            cell.lblEstimateNo.text = recentEstimatesArr[indexPath.row].ET
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getViewsArr[indexPath.row].EstimateViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_getViews
        {
            self.performSegue(withIdentifier: SEGUE_ESTIMATE_LIST, sender: getViewsArr[indexPath.row])
            
        }
        else if tableView == table_recentEstimates
        {
            self.performSegue(withIdentifier: SEGUE_DETAILS, sender: recentEstimatesArr[indexPath.row])
        }
        
    }
    
    
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func btnNewEstimate(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_NEW_ESTIMATE, sender: self)
    }
    
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
