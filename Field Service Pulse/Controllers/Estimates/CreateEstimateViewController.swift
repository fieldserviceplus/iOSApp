//
//  CreateEstimateViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 20/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateEstimateViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchDelegate {
    
    //MARK: Variables
    var btnGetLocationTag = 0
    var latBillingAddress:String?
    var longBillingAddress:String?
    var latShippingAddress:String?
    var longShippingAddress:String?
    var textfieldTag = 0
    
    var accountID:String?
    var estimateStatusID:String?
    var contactID:String?
    var ownerID:String?
    var estimateStatusArr:[GetEstimateStatusData] = []
    
    var pickerView = UIPickerView()
    var flagResponse = "0"
    var response:EstimateDetails?
    var accountName:String?
    var contactName:String?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtEstimateName: AkiraTextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtOwner: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtExpirationDate: AkiraTextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtBillingName: AkiraTextField!
    @IBOutlet weak var txtBillingAddress: UITextField!
    @IBOutlet weak var txtBillingCity: AkiraTextField!
    @IBOutlet weak var txtBillingState: AkiraTextField!
    @IBOutlet weak var txtBillingCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    @IBOutlet weak var txtShippingName: AkiraTextField!
    @IBOutlet weak var txtShippingAddress: UITextField!
    @IBOutlet weak var txtShippingCity: AkiraTextField!
    @IBOutlet weak var txtSHippingState: AkiraTextField!
    @IBOutlet weak var txtShippingCountry: AkiraTextField!
    @IBOutlet weak var txtShippingPostalCode: AkiraTextField!
    
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnShippingAddress: UIButton!
    @IBOutlet weak var btnBillingAddress: UIButton!
    
    @IBOutlet weak var viewBackground: UIView!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetEstimateStatus()
        webserviceCallForCustomFields()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        pickerView.delegate = self
        pickerView.dataSource = self
        txtStatus.inputView = pickerView
        txtviewDescription.giveBorder()
        btnShippingAddress.giveBorderToButton()
        btnBillingAddress.giveBorderToButton()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtContact.setRightImage(name: "search_small", placeholder: "--None--")
        txtOwner.setRightImage(name: "search_small", placeholder: "--None--")
        txtStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtShippingAddress.layer.borderWidth = 1.5
        self.txtShippingAddress.layer.borderColor = UIColor.darkGray.cgColor
        self.txtBillingAddress.layer.borderWidth = 1.5
        self.txtBillingAddress.layer.borderColor = UIColor.darkGray.cgColor
        if flagResponse == "1" {
            responseEstimateDetails()
            flagResponse = "0"
        }
        if accountName != nil {
            self.txtAccount.text = accountName
        }
        if contactName != nil {
            self.txtContact.text = contactName
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        txtExpirationDate.text = formatter.string(from: Date())
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        if textfieldTag == 2 {
            
            txtBillingName.text = searchVC.accountName
            txtAccount.text = searchVC.accountName
            accountID = searchVC.accountID
            webserviceCallForAccountDetails()
        } else if textfieldTag == 3 {
            
            txtContact.text = searchVC.contactName
            contactID = searchVC.contactID
            webserviceCallContactDetails()
            
        }  else if textfieldTag == 4 {
            
            txtOwner.text = searchVC.fullName
            ownerID = searchVC.userID
        }
    }
    
    func webserviceCallForGetEstimateStatus()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEstimateStatus(urlString: API.estimateStatusURL, parameters: parameters , headers: headers, vc: self) { (response:GetEstimateStatus) in
            
            if response.Result == "True"
            {
                
                self.estimateStatusArr = response.data!
                self.txtStatus.text = self.estimateStatusArr[0].Status
                self.estimateStatusID = self.estimateStatusArr[0].EstimateStatusID
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    func webserviceCall()
    {
        if accountID == nil || txtEstimateName.text! == "" || estimateStatusID == nil || txtviewDescription.text! == "Description*" || ownerID == nil || txtExpirationDate.text! == "" || txtBillingName.text == "" || txtBillingAddress.text! == "" || txtBillingCity.text! == "" || txtBillingState.text! == "" || txtBillingCountry.text! == "" || txtPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        var parameters:[String:String] = [:]
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["Account"] = accountID
        parameters["Owner"] = ownerID ?? ""
        parameters["EstimateName"] = txtEstimateName.text ?? ""
        parameters["Contact"] = contactID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["ExpirationDate"] = txtExpirationDate.text ?? ""
        parameters["EstimateStatus"] = estimateStatusID ?? ""
        parameters["Phone"] = txtPhone.text ?? ""
        parameters["Email"] = txtEmail.text ?? ""
        
        parameters["BillingName"] = txtBillingName.text ?? ""
        parameters["BillingAddress"] = txtBillingAddress.text ?? ""
        parameters["BillingCity"] = txtBillingCity.text ?? ""
        parameters["BillingPostalCode"] = txtPostalCode.text ?? ""
        parameters["BillingState"] = txtBillingState.text ?? ""
        parameters["BillingCountry"] = txtBillingCountry.text ?? ""
        parameters["BillingLatitude"] = latBillingAddress ?? ""
        parameters["BillingLongitude"] = longBillingAddress ?? ""
        parameters["ShippingName"] = txtShippingName.text ?? ""
        parameters["ShippingLatitude"] = latShippingAddress ?? ""
        parameters["ShippingLongitude"] = longShippingAddress ?? ""
        parameters["ShippingAddress"] = txtShippingAddress.text ?? ""
        parameters["ShippingCity"] = txtShippingCity.text ?? ""
        parameters["ShippingState"] = txtSHippingState.text ?? ""
        parameters["ShippingCountry"] = txtShippingCountry.text ?? ""
        parameters["ShippingPostalCode"] = txtShippingPostalCode.text ?? ""
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createEstimate(urlString: API.createEstimatesURL, parameters: parameters , headers: headers, vc: self) { (response:CreateEstimate) in
            
            if response.Result == "True"
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EstimateDetailsViewController") as! EstimateDetailsViewController
                User.instance.estimateID = String(response.EstimateID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtBillingAddress.text = response.data?.BillingAddress
                self.txtBillingCity.text = response.data?.BillingCity
                self.txtBillingState.text = response.data?.BillingState
                self.txtBillingCountry.text = response.data?.BillingCountry
                self.txtPostalCode.text = response.data?.BillingPostalCode
                self.latBillingAddress = response.data?.BillingLatitude
                self.longBillingAddress = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func responseEstimateDetails()
    {
        
        self.txtAccount.text  = response?.data?.AccountName
        self.txtOwner.text = response?.data?.OwnerName
        self.txtEstimateName.text = response?.data?.EstimateName
        
        self.txtviewDescription.text = (response?.data?.Description ?? "")
        self.txtExpirationDate.text = response?.data?.ExpirationDate
        self.txtPhone.text = response?.data?.Phone
        self.txtEmail.text = response?.data?.Email
        self.txtStatus.text = response?.data?.Status
        
        self.accountID = response?.data?.Account
        self.estimateStatusID = response?.data?.EstimateStatus
        self.contactID = response?.data?.Contact
        self.ownerID = response?.data?.Owner
        
        self.txtBillingName.text = response?.data?.BillingName
        self.txtBillingAddress.text = response?.data?.BillingAddress
        self.txtBillingCity.text = response?.data?.BillingCity
        self.txtPostalCode.text =  response?.data?.BillingPostalCode
        self.txtBillingState.text = response?.data?.BillingState
        self.txtBillingCountry.text = (response?.data?.BillingCountry) ?? ""
        self.txtShippingName.text = (response?.data?.ShippingName) ?? ""
        self.txtShippingAddress.text = (response?.data?.ShippingAddress)!
        self.txtShippingCity.text = (response?.data?.ShippingCity)!
        self.txtSHippingState.text = (response?.data?.ShippingState) ?? ""
        self.txtShippingCountry.text = response?.data?.ShippingCountry
        self.txtShippingPostalCode.text = (response?.data?.ShippingPostalCode ?? "")
        self.latShippingAddress = (response?.data?.ShippingLatitude ?? "")
        self.longShippingAddress = (response?.data?.ShippingLongitude ?? "")
        self.latBillingAddress = (response?.data?.BillingLatitude ?? "")
        self.longBillingAddress = (response?.data?.BillingLongitude ?? "")
        
        self.txtContact.text = response?.data?.PrimaryContactName
                
            
    }
    func webserviceCallContactDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID!,
                          "OrganizationID":User.instance.OrganizationID]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactDetails(urlString: API.contactDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:ContactDetails) in
            
            if response.Result == "True"
            {
                self.txtPhone.text = response.data?.PhoneNo ?? ""
                self.txtEmail.text = response.data?.Email ?? ""
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "Object":"Estimate",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                
                var viewFromConstrain:Any = self.txtShippingPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtShippingPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShippingPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShippingPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                customView.txtviewLabelField.textColor = UIColor.lightGray
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            self.checkboxValueArr.append("")
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.btnSave, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(value + "," + sender.accessibilityLabel!, at: index)
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: ",\(sender.accessibilityLabel!)", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxKey:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    @IBAction func txtExpirationDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        txtExpirationDate.text = dateFormatter.string(from: sender.date)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 5
        {
            return estimateStatusArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 5
        {
            return row == 0 ? "--None--":estimateStatusArr[row-1].Status
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 5
        {
            if row == 0
            {
                txtStatus.text = ""
            }
            else{
                txtStatus.text = estimateStatusArr[row-1].Status
                estimateStatusID = estimateStatusArr[row-1].EstimateStatusID
            }
            
        }
        
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        //
        if textField == txtAccount || textField == txtContact || textField == txtOwner || textField == txtStatus 
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtContact.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtviewDescription.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtStatus.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtExpirationDate.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtEmail.becomeFirstResponder()
        }
        
        else if textField.tag == 15
        {
            txtBillingName.becomeFirstResponder()
        }
        else if textField.tag == 16
        {
            txtBillingAddress.becomeFirstResponder()
        }
        
        else if textField.tag == 18
        {
            txtBillingCity.becomeFirstResponder()
        }
        else if textField.tag == 19
        {
            txtBillingState.becomeFirstResponder()
        }
        else if textField.tag == 20
        {
            txtBillingCountry.becomeFirstResponder()
        }
        else if textField.tag == 21
        {
            txtPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 22
        {
            txtShippingName.becomeFirstResponder()
        }
        else if textField.tag == 23
        {
            txtShippingAddress.becomeFirstResponder()
        }
        else if textField.tag == 24
        {
            txtShippingPostalCode.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Account")
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 4
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 12
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 17 || textField.tag == 24
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self as GMSAutocompleteViewControllerDelegate
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    
    //MARK: UITextview Delegate Methods
    var lastTappedTextView:UITextView!
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description*"
        {
            textView.text = ""
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description*"
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtviewDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        
        if lastTextfieldTapped != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                customFieldValueArr.append(lastTextfieldTapped.text ?? "")
            }
        }
        
        
        
        //CustomTextView
        
        if lastTappedTextView != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                customFieldValueArr.append(lastTappedTextView.text ?? "")
            }
        }
        
        
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropFirst()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCall()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBillingAddress(_ sender: Any) {
        
        btnGetLocationTag = 0
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "createEstimate"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func btnShippingAddress(_ sender: Any) {
        
        btnGetLocationTag = 1
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "createEstimate"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        if btnGetLocationTag == 0
        {
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtBillingAddress.text = SelectLocationVC.address
                txtBillingCity.text = SelectLocationVC.city
                txtBillingState.text = SelectLocationVC.state
                txtBillingCountry.text = SelectLocationVC.country
                txtPostalCode.text = SelectLocationVC.postalCode
                self.latBillingAddress = SelectLocationVC.latitude
                self.longBillingAddress = SelectLocationVC.longitude
            }
        }
            
        else{
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtShippingAddress.text = SelectLocationVC.address
                txtShippingCity.text = SelectLocationVC.city
                txtSHippingState.text = SelectLocationVC.state
                txtShippingCountry.text = SelectLocationVC.country
                txtShippingPostalCode.text = SelectLocationVC.postalCode
                self.latShippingAddress = SelectLocationVC.latitude
                self.longShippingAddress = SelectLocationVC.longitude
            }
        }
        
    }
    
    
}

extension CreateEstimateViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            
            if textfieldTag == 13
            {
                latBillingAddress = String(place.coordinate.latitude)
                longBillingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtBillingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtBillingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtBillingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtBillingCountry.text = component.name
                }
            }
            else
            {
                latShippingAddress = String(place.coordinate.latitude)
                longShippingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtShippingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtShippingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtShippingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtSHippingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtShippingCountry.text = component.name
                }
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
