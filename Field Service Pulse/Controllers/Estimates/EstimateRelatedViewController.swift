//
//  EstimateRelatedViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 04/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EstimateRelatedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    //MARK: VAriables
    
    private let CELL_Estimate_RELATED = "cell_EstimateRelated"
    private let CELL_SUB_Estimate_RELATED = "cell_subEstimateRelated"
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var estimateID:String?
    
    var titleArr:NSMutableArray = []
    
    var lineItemsArr:[EstimateRelatedLineItemData] = []
    var eventArr:[EstimateRelatedEventData] = []
    var taskArr:[EstimateRelatedTaskData] = []
    var fileArr:[EstimateRelatedFileData] = []
    var noteArr:[EstimateRelatedNoteData] = []
    
    
    //MARK: IBOutlets
    
    @IBOutlet weak var tableEstimate: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        titleArr.removeAllObjects()
        if let _ = self.parent as? EstimateDetailsViewController {
            
            self.estimateID = User.instance.estimateID
            print(self.estimateID ?? "")
            webseriviceCallForRelatedList()
            webseriviceCallForLineItems()
            webseriviceCallForEstimateRelatedTask()
            webseriviceCallForEstimateRelatedFile()
            webseriviceCallForEstimateRelatedNote()
            webseriviceCallForEstimateRelatedEvent()
        }
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        tableEstimate.rowHeight = 45
        tableEstimate.tableFooterView = UIView()
    }
    
    func webseriviceCallForRelatedList()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":estimateID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateRelatedList(urlString: API.estimateRelatedListURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EstimateRelatedList?) in
            
            if response?.Result == "True"
            {
                print(self.titleArr)
                self.titleArr.removeAllObjects()
                self.titleArr.add(response?.data?.EstimateLine?.title ?? "")
                self.titleArr.add(response?.data?.Event?.title ?? "")
                self.titleArr.add(response?.data?.Task?.title ?? "")
                self.titleArr.add(response?.data?.File?.title ?? "")
                self.titleArr.add(response?.data?.Note?.title ?? "")
                
                self.tableEstimate.reloadData()
                
                if let parent = self.parent as? EstimateDetailsViewController {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        parent.heightConstraintActionSheet.constant = User.instance.screenHeight * (0.50)
                        parent.menuView?.heightBackView.constant = 540
                    } else { //IPAD
                        parent.heightConstraintActionSheet.constant = User.instance.screenHeight * (0.50)
                        parent.menuView?.heightBackView.constant = 770
                    }
                    
                }
                    if (self.titleArr[0] as? String == "Estimate Line Items (0)")
                    {
                        if let parent = self.parent as? EstimateDetailsViewController {
                            
                            parent.menuView?.heightBackView.constant = (parent.menuView?.heightBackView.constant)! - 35
                            parent.menuView?.heightConstraintBtnEditLineItems.constant = 0
                            parent.menuView?.heightConstraintBtnNewLineItems.constant = 35
                            parent.heightConstraintActionSheet.constant = parent.heightConstraintActionSheet.constant + 35
                            parent.menuView?.btnEditLines.isHidden = true
                            parent.menuView?.btnNewLines.isHidden = false
                        }
                    }
                    else {
                        if (self.titleArr[0] as? String)?.range(of:"Estimate Line Items") != nil {
                            if let parent = self.parent as? EstimateDetailsViewController {
                                parent.menuView?.heightBackView.constant = (parent.menuView?.heightBackView.constant)! - 35
                                parent.menuView?.heightConstraintBtnEditLineItems.constant = 35
                                parent.menuView?.heightConstraintBtnNewLineItems.constant = 0
                                parent.heightConstraintActionSheet.constant = parent.heightConstraintActionSheet.constant + 35
                                    parent.menuView?.btnEditLines.isHidden = false
                                parent.menuView?.btnNewLines.isHidden = true
                            }
                        }
                }
                
                print(self.titleArr)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForLineItems()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":estimateID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateRelatedLineItem(urlString: API.estimateRelatedLineItemURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EstimateRelatedLineItem?) in
            
            if response?.Result == "True"
            {
                self.lineItemsArr = (response?.data)!
                
                User.instance.productIDArr3.removeAll()
                User.instance.productNameArr3.removeAll()
                User.instance.listPriceArr3.removeAll()
                User.instance.discountArr3.removeAll()
                User.instance.listPriceEditableArr3.removeAll()
                User.instance.quantityEditableArr3.removeAll()
                User.instance.taxArr3.removeAll()
                User.instance.taxableArr3.removeAll()
                User.instance.quantityArr3.removeAll()
                
                for i in 0..<self.lineItemsArr.count
                {
                    User.instance.productNameArr3.append(self.lineItemsArr[i].ProductName!)
                    User.instance.productIDArr3.append(self.lineItemsArr[i].Product!)
                    User.instance.listPriceArr3.append(self.lineItemsArr[i].ListPrice!)
                    User.instance.discountArr3.append(self.lineItemsArr[i].Discount!)
                User.instance.listPriceEditableArr3.append(self.lineItemsArr[i].IsListPriceEditable!)
                User.instance.quantityEditableArr3.append(self.lineItemsArr[i].IsQuantityEditable!)
                    User.instance.taxArr3.append(self.lineItemsArr[i].Tax!)
                    User.instance.taxableArr3.append(self.lineItemsArr[i].Taxable!)
                    User.instance.quantityArr3.append(self.lineItemsArr[i].Quantity!)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForEstimateRelatedEvent()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":estimateID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateRelatedEvent(urlString: API.estimateRelatedEventURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EstimateRelatedEvent?) in
            
            if response?.Result == "True"
            {
                self.eventArr = (response?.data)!
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForEstimateRelatedTask()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":estimateID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateRelatedTask(urlString: API.estimateRelatedTaskURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EstimateRelatedTask?) in
            
            if response?.Result == "True"{
                self.taskArr = (response?.data)!
            }else{
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForEstimateRelatedFile()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":estimateID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateRelatedFile(urlString: API.estimateRelatedFileURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EstimateRelatedFile?) in
            
            if response?.Result == "True"{
                self.fileArr = (response?.data)!
            }else{
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForEstimateRelatedNote(){
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":estimateID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateRelatedNote(urlString: API.estimateRelatedNoteURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EstimateRelatedNote?) in
            
            if response?.Result == "True"{
                self.noteArr = (response?.data)!
                
//                User.instance.chemicalArr.removeAll()
//                User.instance.tested1Arr.removeAll()
//                User.instance.tested2Arr.removeAll()
//                User.instance.app1Arr.removeAll()
//                User.instance.app2Arr.removeAll()
//                User.instance.appAreaArr.removeAll()
//                User.instance.appNoteArr.removeAll()
//
//                for i in 0..<self.chemicalArr.count
//                {
//                    User.instance.chemicalArr.append(self.chemicalArr[i].ProductName!)
//                    User.instance.tested1Arr.append(self.chemicalArr[i].TestConcentration ?? "")
//                    User.instance.tested2Arr.append(self.chemicalArr[i].TestedUnitOfMeasure ?? "")
//                    User.instance.app1Arr.append(self.chemicalArr[i].ApplicationAmount ?? "")
//                    User.instance.app2Arr.append(self.chemicalArr[i].ApplicationUnitOfMeasure ?? "")
//                    User.instance.appAreaArr.append(self.chemicalArr[i].ApplicationArea ?? "")
//                    User.instance.appNoteArr.append(self.chemicalArr[i].AdditionalNotes ?? "")
//                }
            }else{
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnViewAll(sender:UIButton)
    {
        
        let viewAllVC = self.storyboard?.instantiateViewController(withIdentifier: "EstimateRelatedViewAllViewController") as! EstimateRelatedViewAllViewController
        if let parent = self.parent as? EstimateDetailsViewController {
            
            viewAllVC.headerTitle = parent.lblHeaderTitle.text
        }
        
        viewAllVC.titleLabel = titleArr[sender.tag] as? String
        viewAllVC.tag = sender.tag
        if sender.tag == 0
        {
            viewAllVC.lineItemsArr =  lineItemsArr
        }
        else if sender.tag == 1
        {
            viewAllVC.eventArr = eventArr
        }
        else if sender.tag == 2
        {
            viewAllVC.taskArr = taskArr
        }
        else if sender.tag == 3
        {
            viewAllVC.fileArr = fileArr
        }
        else if sender.tag == 4
        {
            viewAllVC.noteArr = noteArr
        }
        
        self.navigationController?.pushViewController(viewAllVC, animated: true)
    }
    
    @objc func btnAdd(sender:UIButton)
    {
        if sender.tag == 0
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchEstimateViewController") as! SearchEstimateViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
        if sender.tag == 1 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
            vc.relatedTo = "Estimate"
            vc.objectID = User.instance.estimateID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 2 {
            
            let vc = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
            vc.relatedToID = User.instance.estimateID
            vc.estimateName = User.instance.estimateName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 3{
            
            let vc = UIStoryboard(name: "File", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateFileViewController") as! CreateFileViewController
            
            vc.relatedToID = User.instance.estimateID
            vc.estimateName = User.instance.estimateName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 4 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateNoteViewController") as! CreateNoteViewController
            vc.relatedTo = "Estimate"
            vc.objectID = User.instance.estimateID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = tableEstimate.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < tableEstimate.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = true
                }
            })
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? EstimateDetailsViewController{
                    parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = false // Here you hide it when animation done
                }
            })
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableEstimate{
            return titleArr.count
        } else {
            if selectedIndex == 0 {
                
                return lineItemsArr.count
            } else if selectedIndex == 1 {
                
                return eventArr.count
            } else if selectedIndex == 2 {
                
                return taskArr.count
            } else if selectedIndex == 3 {
                
                return fileArr.count
            } else if selectedIndex == 4 {
                
                return noteArr.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableEstimate
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_Estimate_RELATED) as! EstimateRelatedTableViewCell
            cell.lblTitle.text = titleArr[indexPath.row] as? String
            cell.btnViewAll.tag = indexPath.row
            cell.btnAdd.giveBorderToButton()
            cell.btnAdd.tag = indexPath.row
            cell.btnAdd.addTarget(self, action: #selector(btnAdd(sender:)), for: .touchUpInside)
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(sender:)), for: .touchUpInside)
            
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell.subTable.rowHeight = 70
            } else { //IPAD
                cell.subTable.rowHeight = 100
            }
            cell.subTable.tableFooterView = UIView()
            cell.subTable.reloadData()
            
            if (self.titleArr[indexPath.row] as! String).range(of: "(0)") != nil
            {
                cell.btnViewAll.isHidden = true
                cell.subTable.isHidden = true
                
            } else {
                cell.btnViewAll.isHidden = false
                cell.subTable.isHidden = false
            }
            
            if(selectedArray.contains(indexPath)){
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }else{
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                cell.btnAdd.isHidden = true
            }
            
            return cell
        }else{
            let cell2 = tableView.dequeueReusableCell(withIdentifier: CELL_SUB_Estimate_RELATED) as! EstimateRelatedSubTableViewCell
            
            if selectedIndex == 0 {
                
                cell2.lblFirst?.text = "Estimate Line No: " + (lineItemsArr[indexPath.row].EstimateLineNo ?? "")
                cell2.lblSecond?.text = "Product Name: " +  (lineItemsArr[indexPath.row].ProductName ?? "")
                cell2.lblThird?.text = "Total Price: " + (lineItemsArr[indexPath.row].TotalPrice ?? "").convertToCurrencyFormat()
                
            } else if selectedIndex == 1 {
                
                cell2.lblFirst?.text = "Event Status: " + (eventArr[indexPath.row].EventStatus ?? "")
                cell2.lblSecond?.text = "Event Type Name: " +  (eventArr[indexPath.row].EventTypeName ?? "")
                cell2.lblThird?.text = "Event Start Time: " + (eventArr[indexPath.row].EventStartTime ?? "")
                
            } else if selectedIndex == 2 {
                
                cell2.lblFirst?.text = "Subject: " + (taskArr[indexPath.row].Subject ?? "")
                cell2.lblSecond?.text = "Task Status: " +  (taskArr[indexPath.row].TaskStatus ?? "")
                cell2.lblThird?.text = "Priority: " + (taskArr[indexPath.row].Priority ?? "")
                
            } else if selectedIndex == 3 {
                
                cell2.lblFirst?.text = "File Name: " + (fileArr[indexPath.row].FileName ?? "")
                cell2.lblSecond?.text = "Content Type: " +  (fileArr[indexPath.row].ContentType ?? "")
                cell2.lblThird?.text = "Subject: " + (fileArr[indexPath.row].Subject ?? "")
                
            } else if selectedIndex == 4 {
                
                cell2.lblFirst?.text = "Owner Name: " + (noteArr[indexPath.row].OwnerName ?? "")
                cell2.lblSecond?.text = "Subject: " + (noteArr[indexPath.row].Subject ?? "")
                cell2.lblThird?.text = "Created Date: " + (noteArr[indexPath.row].CreatedDate ?? "")
                
            }
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableEstimate{
            let cell = tableView.cellForRow(at: indexPath) as! EstimateRelatedTableViewCell
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath)) {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }else{
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            if indexPath.row == selectedIndex {
                selectedIndex = -1
            } else {
                selectedIndex = indexPath.row
            }
            
            if (self.titleArr[indexPath.row] as? String == "Estimate Line Items (0)") || (self.titleArr[indexPath.row] as? String == "Events (0)") || (self.titleArr[indexPath.row] as? String == "Tasks (0)") || (self.titleArr[indexPath.row] as? String == "Notes (0)") || (self.titleArr[indexPath.row] as? String == "Files (0)") {
                
                if (self.titleArr[indexPath.row] as? String == "Estimate Line Items (0)") {
                    cell.btnAdd.setTitle("Add Line Items", for: .normal)
                    
                } else {
                    let newText = "Add " + ((self.titleArr[indexPath.row]) as! String).replacingOccurrences(of: " (0)", with: "", options: NSString.CompareOptions.literal, range:nil)
                    cell.btnAdd.setTitle(newText, for: .normal)
                }
                cell.btnAdd.isHidden = false
            } else {
                cell.btnAdd.isHidden = true
            }
            tableView.reloadData()
        } else {
            
            
            if selectedIndex == 0
            {
                let productVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
                productVC.navigationController?.isNavigationBarHidden = true
                productVC.productID = lineItemsArr[indexPath.row].Product!
                productVC.prouctType = "Product"
                productVC.what = User.instance.estimateID
                productVC.relatedTo = "Estimate"
                self.navigationController?.pushViewController(productVC, animated: true)
            }
            else if selectedIndex == 1
            {
                let eventVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                eventVC.navigationController?.isNavigationBarHidden = true
                User.instance.eventID = eventArr[indexPath.row].EventID!
                self.navigationController?.pushViewController(eventVC, animated: true)
            }
            else if selectedIndex == 2
            {
                let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                taskVC.navigationController?.isNavigationBarHidden = true
                User.instance.taskID = taskArr[indexPath.row].TaskID!
                self.navigationController?.pushViewController(taskVC, animated: true)
            }
            else if selectedIndex == 3
            {
                let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.fileID = fileArr[indexPath.row].FileID!
                self.navigationController?.pushViewController(fileVC, animated: true)
            }
            else if selectedIndex == 4
            {
                let noteVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "NoteDetailsViewController") as! NoteDetailsViewController
                noteVC.navigationController?.isNavigationBarHidden = true
                User.instance.noteID = noteArr[indexPath.row].NoteID!
                noteVC.relatedTo = "Estimate"
                noteVC.objectID = User.instance.estimateID
                self.navigationController?.pushViewController(noteVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableEstimate {
            if indexPath.row == selectedIndex {
                return 290
            }else{
                return 45
            }
        } else {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 70
            } else { //IPAD
                return 100
            }
        }
    }
}



