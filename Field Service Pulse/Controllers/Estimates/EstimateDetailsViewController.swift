//
//  EstimateDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 25/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces
import MessageUI
import Fusuma

class EstimateDetailsViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIPickerViewDelegate,UIPickerViewDataSource, searchDelegate, MFMessageComposeViewControllerDelegate {
    
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var btnGetLocationTag = 0
    var latBillingAddress:String?
    var longBillingAddress:String?
    var latShippingAddress:String?
    var longShippingAddress:String?
    var textfieldTag = 0
    
    var accountID:String?
    var estimateStatusID:String?
    var contactID:String?
    var ownerID:String?
    var estimateStatusArr:[GetEstimateStatusData] = []
    
    var pickerView = UIPickerView()
    var menuView:ActionSheetEstimateView?
    var flag = 0
    var responseEstimate_Details:EstimateDetails?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    
    @IBOutlet weak var txtEstimateNo: AkiraTextField!
    @IBOutlet weak var txtEstimateName: AkiraTextField!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtOwner: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtExpirationDate: AkiraTextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtSubTotal: AkiraTextField!
    @IBOutlet weak var txtDiscount: AkiraTextField!
    @IBOutlet weak var txtTotalPrice: AkiraTextField!
    @IBOutlet weak var txtShippingHandling: AkiraTextField!
    @IBOutlet weak var txtTax: AkiraTextField!
    @IBOutlet weak var txtGrandTotal: AkiraTextField!
    @IBOutlet weak var txtBillingName: AkiraTextField!
    @IBOutlet weak var txtBillingAddress: UITextField!
    @IBOutlet weak var txtBillingCity: AkiraTextField!
    @IBOutlet weak var txtBillingState: AkiraTextField!
    @IBOutlet weak var txtBillingCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    @IBOutlet weak var txtShippingName: AkiraTextField!
    @IBOutlet weak var txtShippingAddress: UITextField!
    @IBOutlet weak var txtShippingCity: AkiraTextField!
    @IBOutlet weak var txtSHippingState: AkiraTextField!
    @IBOutlet weak var txtShippingCountry: AkiraTextField!
    @IBOutlet weak var txtShippingPostalCode: AkiraTextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    
    
    
    //UIButton
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnShippingAddress: UIButton!
    @IBOutlet weak var btnBillingAddress: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnRelated: UIButton!
    
    //UILabel
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var view_BottomContainer: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var topConstraintActionView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintActionSheet: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
        webserviceCallForGetEstimateStatus()
        webserviceCallForEstimateDetails()
        webserviceCallForCustomFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if flag == 1
        {
            let vc = self.children[0] as! EstimateRelatedViewController
            vc.viewWillAppear(true)
            flag = 0
        }
    }

    //MARK: Functions
    
    func setupUI()
    {
        
        if let actionsheetView = Bundle.main.loadNibNamed("ActionSheetEstimate", owner: self, options: nil)?.first as? ActionSheetEstimateView
        {
            menuView = actionsheetView
            actionsheetView.btnSignature.addTarget(self, action: #selector(btnSignature), for: .touchUpInside)
            actionsheetView.btnClose.addTarget(self, action: #selector(btnCloseMenu), for: .touchUpInside)
            actionsheetView.btnNewLines.addTarget(self, action: #selector(btnNewLineItems), for: .touchUpInside)
            actionsheetView.btnEditLines.addTarget(self, action: #selector(btnEditLineItems), for: .touchUpInside)
            actionsheetView.btnCall.addTarget(self, action: #selector(btnCall1), for: .touchUpInside)
            actionsheetView.btnText.addTarget(self, action: #selector(btnMessage1), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnNewInvoice.addTarget(self, action: #selector(btnNewInvoice), for: .touchUpInside)
            actionsheetView.btnNewFile.addTarget(self, action: #selector(btnNewFile), for: .touchUpInside)
            actionsheetView.btnNewTask.addTarget(self, action: #selector(btnNewTask), for: .touchUpInside)
            actionsheetView.btnCloneEstimate.addTarget(self, action: #selector(btnCloneEstimate), for: .touchUpInside)
            actionsheetView.btnNewEvents.addTarget(self, action: #selector(btnNewEvent), for: .touchUpInside)
            actionsheetView.btnNewNote.addTarget(self, action: #selector(btnNewNote), for: .touchUpInside)
            actionsheetView.btnDeleteEstimate.addTarget(self, action: #selector(btnDeleteEstimate), for: .touchUpInside)
            actionsheetView.btnConvertToWO.addTarget(self, action: #selector(btnConvertWorkOrder), for: .touchUpInside)
            actionsheetView.btnGenerateDoc.addTarget(self, action: #selector(btnGenerateDoc), for: .touchUpInside)
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionSheetView.bounds
            self.actionSheetView.addSubview(actionsheetView)
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        txtStatus.inputView = pickerView
        txtviewDescription.giveBorder()
        btnShippingAddress.giveBorderToButton()
        btnBillingAddress.giveBorderToButton()
        btnDetails.giveCornerRadius()
        btnRelated.giveCornerRadius()
        btnRelated.giveBorderToButton()
        btnDetails.giveBorderToButton()
        
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtContact.setRightImage(name: "search_small", placeholder: "--None--")
        txtOwner.setRightImage(name: "search_small", placeholder: "--None--")
        txtStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtShippingAddress.layer.borderWidth = 1.5
        self.txtShippingAddress.layer.borderColor = UIColor.darkGray.cgColor
        self.txtBillingAddress.layer.borderWidth = 1.5
        self.txtBillingAddress.layer.borderColor = UIColor.darkGray.cgColor
        heightConstraintActionSheet.constant = User.instance.screenHeight * (0.50)
    }
    func loadNIB()
    {
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse
        {
            customView.tag = 101
            customView.btnAttachFile.addTarget(self, action: #selector(btnAttachFile), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(btnGallery), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    @objc func btnAttachFile()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: false) {
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = true
            }
        }
    }
    @objc func btnGallery()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        //imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        //fusuma.hasVideo = true //To allow for video capturing with .library and .camera available by default
        fusuma.cropHeightRatio = 0.6 // Height-to-width ratio. The default value is 1, which means a squared-size photo.
        fusuma.allowMultipleSelection = true // You can select multiple photos from the camera roll. The default value is false.
        self.present(fusuma, animated: true, completion: nil)
    }
    @objc func btnNewEvent()
    {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "Estimate"
        createEventVC.objectID = User.instance.estimateID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    @objc func btnSignature()
    {
        actionSheetView.isHidden = true
        btnClose.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        //view_backNewFile.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as? SignatureViewController
        vc?.flag = "1"
        self.present(vc!, animated: true, completion: nil)
    }
    
    @objc func btnNewLineItems()
    {
        actionSheetView.isHidden = true
        btnClose.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        //view_backNewFile.isHidden = true

        let vc = UIStoryboard.init(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchEstimateViewController") as? SearchEstimateViewController
        vc?.selectedProducts = User.instance.productNameArr3
        vc?.selectedQuantity = User.instance.quantityArr3
        vc?.selectedListPriceArr = User.instance.listPriceArr3
        vc?.selectedTaxableArr = User.instance.taxableArr3
        vc?.selectedTaxArr = User.instance.taxArr3
        vc?.selectedQuantityEditableArr = User.instance.quantityEditableArr3
        vc?.selectedListPriceEditableArr = User.instance.listPriceEditableArr3
        vc?.selectedProductIDArr = User.instance.productIDArr3
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnEditLineItems()
    {
        actionSheetView.isHidden = true
        btnClose.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        //view_backNewFile.isHidden = true

        let vc = UIStoryboard.init(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchEstimateViewController") as? SearchEstimateViewController
        
        vc?.selectedProducts = User.instance.productNameArr3
        vc?.selectedQuantity = User.instance.quantityArr3
        vc?.selectedListPriceArr = User.instance.listPriceArr3
        vc?.selectedTaxableArr = User.instance.taxableArr3
        vc?.selectedTaxArr = User.instance.taxArr3
        vc?.selectedQuantityEditableArr = User.instance.quantityEditableArr3
        vc?.selectedListPriceEditableArr = User.instance.listPriceEditableArr3
        vc?.selectedProductIDArr = User.instance.productIDArr3
        User.instance.flagSkipEstimateProduct = "1"

        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @objc func btnCloseMenu()
    {
        self.actionSheetView.isHidden = true
        self.btnClose.isHidden = true
        self.view_BottomContainer.isHidden = false
        self.view_back.isHidden = true
        //self.view_backNewFile.isHidden = true
    }
    
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionSheetView.isHidden = true
        self.viewBottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    @objc func btnCall1()
    {
        if let number = self.txtPhone.text {
            
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    @objc func btnMessage1()
    {
        if let number = self.txtPhone.text {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
        
    }
    @objc func btnNewInvoice()
    {
        let vc = UIStoryboard(name: "Invoice", bundle: nil).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as? CreateInvoiceViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnNewFile()
    {
        loadNIB()
        
    }
    @objc func btnNewTask()
    {
        let vc = UIStoryboard(name: "Task", bundle: nil).instantiateViewController(withIdentifier: "CreateTaskViewController") as? CreateTaskViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @objc func btnCloneEstimate()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateEstimateViewController") as! CreateEstimateViewController
        vc.flagResponse = "1"
        vc.response = responseEstimate_Details
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func btnGenerateDoc()
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenerateDocumentViewController") as? GenerateDocumentViewController
        vc?.object = "Estimate"
        vc?.objectID = User.instance.estimateID
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @objc func btnConvertWorkOrder() {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":User.instance.estimateID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.convertToWO(urlString: API.convertToWorkOrderURL, parameters: parameters , headers: headers, vc: self) { (response:ConvertToWO) in
            
            if response.Result == "True"
            {
                let vc = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
                User.instance.workorderID = String(response.WorkOrderID!)
                self.navigationController?.pushViewController(vc, animated: true)
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    @objc func btnNewNote()
    {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateNoteViewController") as! CreateNoteViewController
        vc.relatedTo = "Estimate"
        vc.objectID = User.instance.estimateID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnDeleteEstimate()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"Estimate",
                          "What":User.instance.estimateID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
        
    }
    func sendData(searchVC:SearchAccountViewController) {
        
        if textfieldTag == 2 {
            
            txtBillingName.text = searchVC.accountName
            txtAccount.text = searchVC.accountName
            accountID = searchVC.accountID
            webserviceCallForAccountDetails()
        } else if textfieldTag == 3 {
            
            txtContact.text = searchVC.contactName
            contactID = searchVC.contactID
            webserviceCallContactDetails()
            
        }  else if textfieldTag == 4 {
            
            txtOwner.text = searchVC.fullName
            ownerID = searchVC.userID
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtBillingAddress.text = response.data?.BillingAddress
                self.txtBillingCity.text = response.data?.BillingCity
                self.txtBillingState.text = response.data?.BillingState
                self.txtBillingCountry.text = response.data?.BillingCountry
                self.txtPostalCode.text = response.data?.BillingPostalCode
                self.latBillingAddress = response.data?.BillingLatitude
                self.longBillingAddress = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForGetEstimateStatus()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEstimateStatus(urlString: API.estimateStatusURL, parameters: parameters , headers: headers, vc: self) { (response:GetEstimateStatus) in
            
            if response.Result == "True"
            {
                
                self.estimateStatusArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    
    func webserviceCallForEstimateDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":User.instance.estimateID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateDetails(urlString: API.estimateDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:EstimateDetails) in
            
            if response.Result == "True"
            {
                self.responseEstimate_Details = response
                self.txtEstimateNo.text = response.data?.EstimateNo
                self.txtAccount.text  = response.data?.AccountName
                self.txtOwner.text = response.data?.OwnerName
                self.txtEstimateName.text = response.data?.EstimateName
                self.txtBillingName.text = response.data?.BillingName
                self.txtBillingAddress.text = response.data?.BillingAddress
                self.txtBillingCity.text = response.data?.BillingCity
                self.txtPostalCode.text =  response.data?.BillingPostalCode
                self.txtBillingState.text = response.data?.BillingState
                self.txtBillingCountry.text = (response.data?.BillingCountry) ?? ""
                self.txtShippingName.text = (response.data?.ShippingName) ?? ""
                self.txtShippingAddress.text = (response.data?.ShippingAddress)!
                self.txtShippingCity.text = (response.data?.ShippingCity)!
                self.txtSHippingState.text = (response.data?.ShippingState) ?? ""
                self.txtShippingCountry.text = response.data?.ShippingCountry
                self.txtShippingPostalCode.text = (response.data?.ShippingPostalCode ?? "")
                self.latShippingAddress = (response.data?.ShippingLatitude ?? "")
                self.longShippingAddress = (response.data?.ShippingLongitude ?? "")
                self.latBillingAddress = (response.data?.BillingLatitude ?? "")
                self.longBillingAddress = (response.data?.BillingLongitude ?? "")
                self.txtviewDescription.text = (response.data?.Description ?? "")
                self.txtExpirationDate.text = response.data?.ExpirationDate
                self.txtPhone.text = response.data?.Phone
                self.txtEmail.text = response.data?.Email
                self.txtStatus.text = response.data?.Status
                self.lblStatus.text = "Status: " + (response.data?.Status ?? "")!
                self.lblPhone.text = "Phone: " + (response.data?.Phone ?? "")!
                self.lblHeaderTitle.text = response.data?.EstimateName
                User.instance.estimateName = (response.data?.EstimateName)!
                self.accountID = response.data?.Account
                self.estimateStatusID = response.data?.EstimateStatus
                self.contactID = response.data?.Contact
                self.ownerID = response.data?.Owner
                self.txtSubTotal.text = response.data?.SubTotal
                self.txtDiscount.text = response.data?.Discount
                self.txtTotalPrice.text = response.data?.TotalPrice
                self.txtShippingHandling.text = response.data?.ShippingHandling
                self.txtTax.text = response.data?.Tax
                self.txtGrandTotal.text = response.data?.GrandTotal
                self.txtContact.text = response.data?.PrimaryContactName
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtCreatedBy.text = response.data?.CreatedBy
                self.txtLastModifiedDate.text = response.data?.LastModifiedDate
                self.txtLastModifiedBy.text = response.data?.LastModifiedBy
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForEditEstimate()
    {
        if accountID == nil || txtEstimateName.text! == "" || estimateStatusID == nil || contactID == nil || txtviewDescription.text! == "Description*" || ownerID == nil || txtExpirationDate.text! == "" || txtBillingName.text == "" || txtBillingAddress.text! == "" || txtBillingCity.text! == "" || txtBillingState.text! == "" || txtBillingCountry.text! == "" || txtPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        
        
        var parameters:[String:String] = [:]
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["EstimateID"] = User.instance.estimateID
        parameters["Account"] = accountID
        parameters["Owner"] = ownerID ?? ""
        parameters["EstimateName"] = txtEstimateName.text ?? ""
        parameters["BillingName"] = txtBillingName.text ?? ""
        parameters["BillingAddress"] = txtBillingAddress.text ?? ""
        parameters["BillingCity"] = txtBillingCity.text ?? ""
        parameters["BillingPostalCode"] = txtPostalCode.text ?? ""
        parameters["BillingState"] = txtBillingState.text ?? ""
        parameters["BillingCountry"] = txtBillingCountry.text ?? ""
        parameters["BillingLatitude"] = latBillingAddress ?? ""
        parameters["BillingLongitude"] = longBillingAddress ?? ""
        parameters["ShippingName"] = txtShippingName.text ?? ""
        parameters["ShippingLatitude"] = latShippingAddress ?? ""
        parameters["ShippingLongitude"] = longShippingAddress ?? ""
        parameters["ShippingAddress"] = txtShippingAddress.text ?? ""
        parameters["ShippingCity"] = txtShippingCity.text ?? ""
        parameters["ShippingState"] = txtSHippingState.text ?? ""
        parameters["ShippingCountry"] = txtShippingCountry.text ?? ""
        parameters["ShippingPostalCode"] = txtShippingPostalCode.text ?? ""
        parameters["Contact"] = contactID ?? ""
        parameters["Description"] = txtviewDescription.text ?? ""
        parameters["ExpirationDate"] = txtExpirationDate.text ?? ""
        parameters["EstimateStatus"] = estimateStatusID ?? ""
        parameters["Phone"] = txtPhone.text ?? ""
        parameters["Email"] = txtEmail.text ?? ""
        
        print(parameters)
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editEstimateURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.webserviceCallForEstimateDetails()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[1] as! EstimateDetailsLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    func webserviceCallContactDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID!,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactDetails(urlString: API.contactDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:ContactDetails) in
            
            if response.Result == "True"
            {
                self.txtPhone.text = response.data?.PhoneNo ?? ""
                self.txtEmail.text = response.data?.Email ?? ""
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.estimateID,
                          "Object":"Estimate",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                var arrayFieldValue:[String]?
                
                var viewFromConstrain:Any = self.txtShippingPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtShippingPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShippingPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtShippingPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            //
                            customView.txtLabelField.text = response.data?[i].FieldValue
                            
                            //
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtLabelField.text ?? "")
                            
                            //
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            
                            if response.data?[i].IsRequired == "1" {
                                
                                if response.data?[i].FieldValue == "" {
                                    customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                    customView.txtviewLabelField.textColor = UIColor.lightGray
                                } else {
                                    customView.txtviewLabelField.text = response.data?[i].FieldValue
                                    
                                }
                                
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtviewLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtviewLabelField.text ?? "")
                            
                            //
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            //Edit
                            if response.data?[i].FieldValue == "" {
                                self.checkboxValueArr.append("")
                            } else{
                                self.checkboxValueArr.append("\((response.data?[i].FieldValue ?? "")),")
                            }
                            //
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                if ((arrayFieldValue?.contains((arrayCheckboxValues[j])))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                
                //Edit
                if value == "," {
                    checkboxValueArr.insert("\(sender.accessibilityLabel!),", at: index)
                } else {
                    checkboxValueArr.insert(value + "\(sender.accessibilityLabel!),", at: index)
                }
                //
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: "\(sender.accessibilityLabel!),", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == scrollview
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
                
            }
            else
            {
                // didn't move
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            
            view_back.isHidden = true
            actionSheetView.isHidden = true
            btnClose.isHidden = true
            //view_backNewFile.isHidden = true
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 5
        {
            return estimateStatusArr.count + 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 5
        {
            return row == 0 ? "--None--":estimateStatusArr[row-1].Status
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 5
        {
            if row == 0
            {
                txtStatus.text = ""
            }
            else{
                txtStatus.text = estimateStatusArr[row-1].Status
                estimateStatusID = estimateStatusArr[row-1].EstimateStatusID
            }
            
        }
        
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        //
        if textField == txtAccount || textField == txtContact || textField == txtOwner || textField == txtStatus 
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtContact.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtviewDescription.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtStatus.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtExpirationDate.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField.tag == 8
        {
            txtSubTotal.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtDiscount.becomeFirstResponder()
        }
        else if textField.tag == 10
        {
            txtTotalPrice.becomeFirstResponder()
        }
        else if textField.tag == 11
        {
            txtShippingHandling.becomeFirstResponder()
        }
            
        else if textField.tag == 12
        {
            txtTax.becomeFirstResponder()
        }
        else if textField.tag == 13
        {
            txtGrandTotal.becomeFirstResponder()
        }
        
        else if textField.tag == 15
        {
            txtBillingName.becomeFirstResponder()
        }
        else if textField.tag == 16
        {
            txtBillingAddress.becomeFirstResponder()
        }
        
        else if textField.tag == 18
        {
            txtBillingCity.becomeFirstResponder()
        }
        else if textField.tag == 19
        {
            txtBillingState.becomeFirstResponder()
        }
        else if textField.tag == 20
        {
            txtBillingCountry.becomeFirstResponder()
        }
        else if textField.tag == 21
        {
            txtPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 22
        {
            txtShippingName.becomeFirstResponder()
        }
        else if textField.tag == 23
        {
            txtShippingAddress.becomeFirstResponder()
        }
        else if textField.tag == 24
        {
            txtShippingPostalCode.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 2
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Account")
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        
        if textField.tag == 4
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 8
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 12
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 17 || textField.tag == 24
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    
    //MARK: UITextview Delegate Methods
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtviewDescription.resignFirstResponder()
            
            return false
        }
        
        return true
    }
    
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        if customFieldKeyArr.count != 0 {
            
            if lastTextfieldTapped != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                    customFieldValueArr.append(lastTextfieldTapped.text ?? "")
                }
            }
            
        }
        
        
        //CustomTextView
        if customFieldKeyArr.count != 0 {
            
            if lastTappedTextView != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                    customFieldValueArr.append(lastTappedTextView.text ?? "")
                }
            }
            
        }
        
        checkboxEditedValueArr.removeAll()
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropLast()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCallForEditEstimate()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
        
        txtviewDescription.isUserInteractionEnabled = true
        
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        btnCancel.isHidden = false
        btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        txtStatus.layer.borderColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnBillingAddress(_ sender: Any) {
        
        btnGetLocationTag = 0
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "estimateDetails"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    @IBAction func btnShippingAddress(_ sender: Any) {
        
        btnGetLocationTag = 1
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "estimateDetails"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    
    @IBAction func txtExpirationDate(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
   
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        txtExpirationDate.text = dateFormatter.string(from: sender.date)
        
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        if btnGetLocationTag == 0
        {
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtBillingAddress.text = SelectLocationVC.address
                txtBillingCity.text = SelectLocationVC.city
                txtBillingState.text = SelectLocationVC.state
                txtBillingCountry.text = SelectLocationVC.country
                txtPostalCode.text = SelectLocationVC.postalCode
                self.latBillingAddress = SelectLocationVC.latitude
                self.longBillingAddress = SelectLocationVC.longitude
            }
        }
            
        else{
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtShippingAddress.text = SelectLocationVC.address
                txtShippingCity.text = SelectLocationVC.city
                txtSHippingState.text = SelectLocationVC.state
                txtShippingCountry.text = SelectLocationVC.country
                txtShippingPostalCode.text = SelectLocationVC.postalCode
                self.latShippingAddress = SelectLocationVC.latitude
                self.longShippingAddress = SelectLocationVC.longitude
            }
        }
        if let _ = sender.source as? EditLinesEstimateViewController
        {
            self.flag = 1
        }
        
    }
    
    @IBAction func btnDetails(_ sender: Any) {
        containerviewLayout.isHidden = false
        self.containerview.isHidden = true
        self.scrollview.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func btnRelated(_ sender: Any) {
        containerviewLayout.isHidden = true
        self.containerview.isHidden = false
        self.scrollview.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnCall(_ sender: Any) {
        
        if let number = self.txtPhone.text {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func btnMessage(_ sender: Any) {
        if let number = self.txtPhone.text {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnCalender(_ sender: Any) {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "Estimate"
        createEventVC.objectID = User.instance.estimateID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    
    @IBAction func btnMore(_ sender: Any) {
        
        //self.btnClose.isHidden = false
        self.actionSheetView.isHidden = false
        self.view_BottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
}

extension EstimateDetailsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            
            if textfieldTag == 13
            {
                latBillingAddress = String(place.coordinate.latitude)
                longBillingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtBillingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtBillingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtBillingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtBillingCountry.text = component.name
                }
            }
            else
            {
                latShippingAddress = String(place.coordinate.latitude)
                longShippingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtShippingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtShippingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtShippingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtSHippingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtShippingCountry.text = component.name
                }
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension EstimateDetailsViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtEstimateName.text ?? "",
                          "AssignedTo":self.ownerID!,
                          "RelatedTo":"Estimate",
                          "What":User.instance.estimateID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.uploadDocs(urlString: API.createNewFileAllURL, fileName: "FileName", URLs: urls as [NSURL], parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
}

extension EstimateDetailsViewController:FusumaDelegate {
    
    // Return the image which is selected from camera roll or is taken via the camera.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(image: UIImage, source: FusumaMode) {
        
        print("Called just after FusumaViewController is dismissed.")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        print("Called just after a video has been selected.")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // Return selected images when you allow to select multiple photos.
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtEstimateName.text ?? "",
                          "AssignedTo":self.ownerID!,
                          "RelatedTo":"Estimate",
                          "What":User.instance.estimateID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createNewFileAll(urlString: API.createNewFileAllURL, pickedImages: images, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // Return an image and the detailed information.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
    }
}
