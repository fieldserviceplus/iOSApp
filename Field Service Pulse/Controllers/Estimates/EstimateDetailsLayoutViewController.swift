//
//  EstimateDetailsLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 25/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EstimateDetailsLayoutViewController: UIViewController, UIScrollViewDelegate {

    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var accountID:String?
    var contactID:String?
    var assignedToID:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblEstimateName: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblExpirationDate: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblShippingHandling: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var imgviewSignature: UIImageView!
    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblBillingCity: UILabel!
    @IBOutlet weak var lblBillingCountry: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblShippingCity: UILabel!
    @IBOutlet weak var lblShippingCountry: UILabel!
    @IBOutlet weak var lblEstimateNo: UILabel!
    @IBOutlet weak var viewShippingAddress: UIView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblSystemInfo: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        webserviceCallForEstimateDetails()
        webserviceCallForCustomFields()
    }

    //MARK: Function
    
    func webserviceCallForEstimateDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "EstimateID":User.instance.estimateID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.estimateDetails(urlString: API.estimateDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:EstimateDetails) in
            
            if response.Result == "True"
            {
                self.lblEstimateNo.text = response.data?.EstimateNo
                self.lblAccount.text  = response.data?.AccountName
                User.instance.accountNameForProductUI = response.data?.AccountName ?? ""
                self.lblOwner.text = response.data?.OwnerName
                self.lblEstimateName.text = response.data?.EstimateName
                
                self.lblBillingAddress.text = response.data?.BillingAddress
                self.lblBillingCity.text = (response.data?.BillingCity)! + ", " + (response.data?.BillingState)! + ", " + (response.data?.BillingPostalCode)!
                self.lblBillingCountry.text = (response.data?.BillingCountry) ?? ""
                
                self.lblShippingAddress.text = (response.data?.ShippingAddress)!
                self.lblShippingCity.text = (response.data?.ShippingCity)! + "," + (response.data?.ShippingState)! + ", " + (response.data?.ShippingPostalCode)!
                self.lblShippingCountry.text = response.data?.ShippingCountry
                self.txtviewDescription.text = (response.data?.Description ?? "")
                self.lblExpirationDate.text = response.data?.ExpirationDate
                self.lblPhone.text = response.data?.Phone
                self.lblEmail.text = response.data?.Email
                self.lblStatus.text = response.data?.Status
                self.lblSubTotal.text = (response.data?.SubTotal)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblDiscount.text = response.data?.Discount
                self.lblTotalPrice.text = (response.data?.TotalPrice)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblShippingHandling.text = (response.data?.ShippingHandling)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblTax.text = (response.data?.Tax)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblGrandTotal.text = (response.data?.GrandTotal)?.convertToCurrencyFormat()  ?? "$0.00"
                self.lblContact.text = response.data?.PrimaryContactName
                if let range = response.data?.Signature?.range(of: "data:image/png;base64,") {
                    response.data?.Signature?.removeSubrange(range)
                }
                if let decodedImageData = Data(base64Encoded: response.data?.Signature ?? "", options: .ignoreUnknownCharacters) {
                    
                    self.imgviewSignature.image = UIImage(data: decodedImageData)
                }
                
                self.accountID = response.data?.Account
                self.contactID = response.data?.Contact
                self.assignedToID = response.data?.Owner
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.estimateID,
                          "Object":"Estimate",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayOptionValues:[String]?
                var arrayFieldValue:[String]?
                
                var viewFromConstrain = self.viewShippingAddress
                for i in 0..<(response.data?.count)!  {
                    print(response.data?[i].FieldType)
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 5
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 85
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 70
                        case "LongText":
                            constantHeight = 110
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.viewShippingAddress)
                    }
                    
                    let myView = UIView()
                    
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    print(constant)
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewShippingAddress, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewShippingAddress, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        if let customView = Bundle.main.loadNibNamed("DefaultSizeView", owner: self, options: nil)?.first as? DefaultSizeView {
                            
                            customView.lblFieldLabel.text = response.data?[i].FieldLabel
                            customView.lblFieldValue.text = response.data?[i].FieldValue
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextSizeView", owner: self, options: nil)?.first as? LongTextSizeView {
                            
                            customView.txtview.text = response.data?[i].FieldValue
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            for i in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                
                                if ((arrayFieldValue?.contains((arrayOptionValues?[i])!))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![i]
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = true
                    //parent.actionsheetWOView.isHidden = true
                    parent.btnClose.isHidden = true// Here you hide it when animation done
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? EstimateDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = false // Here you hide it when animation done
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if self.accountID != nil && self.accountID != "" {
                let acVC = UIStoryboard(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController")  as! AccountDetailsViewController
                User.instance.accountID = accountID!
                User.instance.accountName = self.lblAccount.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        } else if sender.tag == 2 {
            if self.contactID != nil && self.contactID != "" {
                let acVC = UIStoryboard(name: "Contact", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactDetailsViewController")  as! ContactDetailsViewController
                User.instance.contactID = contactID!
                User.instance.contactName = self.lblContact.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        } else if sender.tag == 3 {
            
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}
