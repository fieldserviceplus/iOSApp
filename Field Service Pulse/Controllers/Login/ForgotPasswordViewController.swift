//
//  ForgotPasswordViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 04/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    //MARK: Variables
    
    
    
    //MARK: IBOutlets
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var btnSend: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnSend.giveCornerRadius()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSend(_ sender: Any) {
        
        self.showHUD()
        
        let parameters = ["Email":txtEmail.text!]
        let headers = ["key":User.instance.key]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.forgotPasswordURL, parameters: parameters, headers: headers, vc: self) {(response:ResponseDict?) in
            
            if response?.Result == "True"
            {
                
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
        
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
}
