//
//  LoginViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 05/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {

    //MARK: Variables
    
    let SEGUE_SIGNUP = "segueSignUp"
    let SEGUE_FORGOT_PASSWORD = "segueForgotPassword"
    //MARK: IBOutlets
    
    @IBOutlet weak var txtUsername: AkiraTextField!
    @IBOutlet weak var txtPassword: AkiraTextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if Helper.instance.getUserDefaultsValue(key: "userID") as? String != "" && Helper.instance.getUserDefaultsValue(key: "userID") as? String != nil
        {
            User.instance.UserID = (Helper.instance.getUserDefaultsValue(key: "userID") as? String)!
            User.instance.token = (Helper.instance.getUserDefaultsValue(key: "token") as? String)!
            User.instance.OrganizationID = (Helper.instance.getUserDefaultsValue(key: "organizationID") as? String)!
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: false)
        }
        setupUI()
    }

    func setupUI()
    {
        btnLogin.giveCornerRadius()
    }
    
    //MARK: UITextfield Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1{
            txtPassword.becomeFirstResponder()
        }else
        {
            txtPassword.resignFirstResponder()
        }
        return true
    }
    
    //AMRK: IBActions
    
    @IBAction func btnLogin(_ sender: Any) {
        
        
        if (txtUsername?.text?.isValidEmail() == false){
            Helper.instance.showAlertNotification(message: Message.validEmail, vc: self)
            
            return
        }

        if (txtPassword?.text?.isPasswordValid() == false){
            Helper.instance.showAlertNotification(message: Message.validPassword, vc: self)
            return
        }
        
        self.showHUD()
        
        let parameters = ["Email":txtUsername.text!,
                          "Password":txtPassword.text!,
                          "DeviceUDID":User.instance.deviceID,
                          "DeviceType":"iOS"]
        let headers = ["key":User.instance.key]
        
        NetworkManager.sharedInstance.login(urlString: API.loginURL, parameters: parameters, headers: headers, vc: self) {(response:LoginResponse?) in
            
            if response?.Result == "True"
            {
                self.txtPassword.text = ""
                self.txtUsername.text = ""
                User.instance.UserID = (response?.data?.UserID)!
                User.instance.token = (response?.data?.token)!
                User.instance.OrganizationID = (response?.data?.OrganizationID)!
                
                Helper.instance.saveToUserDefaults(key: "userID", value: User.instance.UserID)
                Helper.instance.saveToUserDefaults(key: "token", value: User.instance.token)
                Helper.instance.saveToUserDefaults(key: "organizationID", value: User.instance.OrganizationID)
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: true)
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnForgotPassword(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_FORGOT_PASSWORD, sender: self)
    }
    
    @IBAction func btnSignUP(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_SIGNUP, sender: self)
    }
    
    @IBAction func unwindToLogin(_ sender: UIStoryboardSegue){}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
