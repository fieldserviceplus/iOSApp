//
//  PreLoginViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 08/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class PreLoginViewController: UIViewController {

    
    //MARK: Variables
    
    private let SEGUE_LOGIN = "segueLogin"
    private let SEGUE_SIGNUP = "segueRegister"
    
    //MARK: IBOutlets
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if Helper.instance.getUserDefaultsValue(key: "userID") as? String != "" && Helper.instance.getUserDefaultsValue(key: "userID") as? String != nil
        {
            User.instance.UserID = (Helper.instance.getUserDefaultsValue(key: "userID") as? String)!
            User.instance.token = (Helper.instance.getUserDefaultsValue(key: "token") as? String)!
            User.instance.OrganizationID = (Helper.instance.getUserDefaultsValue(key: "organizationID") as? String)!
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(loginVC, animated: false)
        }
        setupUI()
        
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        btnLogin.giveCornerRadius()
        btnSignUp.giveCornerRadius()
    }

    //MARK: IBActions
    
    @IBAction func btnLogin(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_LOGIN, sender: self)
    }
    
    @IBAction func btnSIgnUp(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_SIGNUP, sender: self)
    }
    
    @IBAction func unwindToLoginSignUP(sender:UIStoryboardSegue){}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
