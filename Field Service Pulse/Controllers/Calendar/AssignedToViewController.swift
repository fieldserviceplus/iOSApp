//
//  AssignedToViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AssignedToViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    var allUsersArr:[UsersData] = []
    var selectedDate:String?
    
    var wholeDayArr:[String] = []
    var fixedTimeArr:[String] = []
    var wholeDayEventArr:[String] = []
    var fixedTimeEventArr:[String] = []
    var wholeDayColorArr:[String] = []
    var fixedTimeColorArr:[String] = []
    var wholeDayIDArr:[String] = []
    var fixedTimeIDArr:[String] = []
    var wholeDayTypeArr:[String] = []
    var fixedTimeTypeArr:[String] = []
    
    //MARK: IBOutlet
    
    private let CELL_ASSIGNED_TO = "cellAssignedTo"
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var tableAssignedTo:UITableView!
    @IBOutlet weak var btnSelectAll:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        webserviceCallForGetAllUsers()
        setupUI()
    }

    //MARK: Function
    
    func setupUI()
    {
        btnApply.giveCornerRadius()
        btnClose.giveCornerRadius()
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            tableAssignedTo.rowHeight = 50
        } else { //IPAD
            tableAssignedTo.rowHeight = 60
        }
        
        if User.instance.flagSelectAll == "1"
        {
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
        }
        else
        {
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.allUsersArr = response.UsersData!
                
                if User.instance.flagForGetUsers == "1"
                {
                    User.instance.selectedUsersArr.removeAll()
                    User.instance.selectedUsersIDArr.removeAll()
                    User.instance.checkboxArr.removeAll()
                    
                    for i in 0..<self.allUsersArr.count
                    {
                        User.instance.checkboxArr.append("1")
                        User.instance.selectedUsersArr.append(self.allUsersArr[i].FullName ?? "")
                        User.instance.selectedUsersIDArr.append(self.allUsersArr[i].UserID ?? "")
                    }
                    User.instance.flagForGetUsers = "0"
                }
                
                
                self.tableAssignedTo.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForApplyAssignedTo()
    {
        User.instance.allUsersArr.removeAll()
        User.instance.allUsersArr = User.instance.selectedUsersIDArr
        self.showHUD()
        
        var parameters:[String : Any] = [:]
        if User.instance.statusIDArr.count == 0 {
            
            parameters["WOStatus"] = ""
            
        } else {
            parameters["WOStatus"] = User.instance.statusIDArr
        }
        if User.instance.typeIDArr.count == 0 {
            
            parameters["WorkOrderType"] = ""
            
        } else {
            parameters["WorkOrderType"] = User.instance.typeIDArr
        }
        if User.instance.priorityIDArr.count == 0 {
            
            parameters["WOPriority"] = ""
            
        } else {
            parameters["WOPriority"] = User.instance.priorityIDArr
        }
        if User.instance.allUsersArr.count == 0 {
            
            parameters["AssignedTo"] = ""
            
        } else {
            parameters["AssignedTo"] = User.instance.allUsersArr
        }
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["FromDate"] = selectedDate!
        parameters["ToDate"] = selectedDate!
        parameters["ShowTasks"] = User.instance.ShowTasks
        

        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                let responseArr:[GetEventTaskByDatesData] = response.data!
                
                self.wholeDayArr.removeAll()
                self.wholeDayEventArr.removeAll()
                self.fixedTimeArr.removeAll()
                self.fixedTimeEventArr.removeAll()
                self.wholeDayColorArr.removeAll()
                self.fixedTimeColorArr.removeAll()
                self.wholeDayIDArr.removeAll()
                self.fixedTimeIDArr.removeAll()
                self.wholeDayTypeArr.removeAll()
                self.fixedTimeTypeArr.removeAll()
                
                print(responseArr.count)
                for i in 0..<responseArr.count
                {
                    let dict = responseArr[i]
                    if dict.Time == "All-Day" && dict.Start != ""
                    {
                        self.wholeDayArr.append(dict.Time!)
                        self.wholeDayEventArr.append(responseArr[i].Title ?? "")
                        
                        self.wholeDayColorArr.append(responseArr[i].ColorCode ?? "")
                        self.wholeDayIDArr.append(responseArr[i].ID ?? "")
                        self.wholeDayTypeArr.append(responseArr[i].type ?? "")
                    }
                    else{
                        self.fixedTimeArr.append(dict.Time!)
                        self.fixedTimeEventArr.append(responseArr[i].Title ?? "")
                        
                        self.fixedTimeColorArr.append(responseArr[i].ColorCode ?? "")
                        self.fixedTimeIDArr.append(responseArr[i].ID ?? "")
                        self.fixedTimeTypeArr.append(responseArr[i].type ?? "")
                    }
                    
                }
                
                User.instance.eventTitleArr.removeAll()
                User.instance.eventTimeArr.removeAll()
                User.instance.eventColorArr.removeAll()
                User.instance.eventIDArr.removeAll()
                User.instance.eventTypeArr.removeAll()
                
                User.instance.eventTitleArr = self.wholeDayEventArr + self.fixedTimeEventArr
                User.instance.eventTimeArr = self.wholeDayArr + self.fixedTimeArr
                User.instance.eventColorArr = self.wholeDayColorArr + self.fixedTimeColorArr
                User.instance.eventIDArr = self.wholeDayIDArr + self.fixedTimeIDArr
                User.instance.eventTypeArr = self.wholeDayTypeArr + self.fixedTimeTypeArr
                
                User.instance.flag = "0"
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    @objc func btnCheckbox(sender:UIButton)
    {
        guard let cell = sender.superview?.superview as? AssignedToTableViewCell else {
            return // or fatalError() or whatever
        }
        
        let indexPath = tableAssignedTo.indexPath(for: cell)
        
        let index = User.instance.selectedUsersIDArr.index(of: cell.lblName.accessibilityLabel!)
        
        if (sender.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            sender.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            
            User.instance.selectedUsersArr.remove(at: index!)
            User.instance.selectedUsersIDArr.remove(at: index!)
            
            User.instance.checkboxArr.remove(at: (indexPath?.row)!)
            User.instance.checkboxArr.insert("0", at: (indexPath?.row)!)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            User.instance.selectedUsersArr.append(cell.lblName.text!)
            User.instance.selectedUsersIDArr.append(cell.lblName.accessibilityLabel!)
            
            User.instance.checkboxArr.remove(at: (indexPath?.row)!)
            User.instance.checkboxArr.insert("1", at: (indexPath?.row)!)
        }
        
        print("Name:\(User.instance.selectedUsersArr)")
        print("ID:\(User.instance.selectedUsersIDArr)")
        print("Checkbox:\(User.instance.checkboxArr)")
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allUsersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ASSIGNED_TO) as! AssignedToTableViewCell
        cell.btnCheckbox.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
        cell.btnCheckbox.tag = indexPath.row
        cell.lblName.text = allUsersArr[indexPath.row].FullName
        
        cell.lblName.accessibilityLabel = allUsersArr[indexPath.row].UserID ?? ""
        
        if User.instance.checkboxArr[indexPath.row] == "1"
        {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
        }
        else{
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
        
        return cell
    }
    
    
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApply(_ sender: Any) {
       
        webserviceCallForApplyAssignedTo()
    }
    
    @IBAction func btnSelectAll(_ sender: Any) {
        
        
        User.instance.selectedUsersArr.removeAll()
        User.instance.selectedUsersIDArr.removeAll()
        User.instance.checkboxArr.removeAll()
        
        if (btnSelectAll.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            for _ in 0..<self.allUsersArr.count
            {
                User.instance.checkboxArr.append("0")
                
            }
            User.instance.flagSelectAll = "0"
        }
        else{
             btnSelectAll.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            
            for i in 0..<self.allUsersArr.count
            {
                User.instance.checkboxArr.append("1")
                User.instance.selectedUsersArr.append(self.allUsersArr[i].FullName ?? "")
                User.instance.selectedUsersIDArr.append(self.allUsersArr[i].UserID ?? "")
            }
            User.instance.flagSelectAll = "1"
        }
        
        self.tableAssignedTo.reloadData()
        print(User.instance.selectedUsersArr)
        print(User.instance.selectedUsersIDArr)
        print(User.instance.checkboxArr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
