//
//  MapAssignedToViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MapAssignedToViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    
    var allUsersArr:[UsersData] = []
    var selectedDate:String?
    var selectedFromDate:String?
    var selectedToDate:String?
    var flag:Int?
    
    //MARK: IBOutlet
    
    private let CELL_ASSIGNED_TO = "cellMapAssignedTo"
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var tableAssignedTo:UITableView!
    @IBOutlet weak var btnSelectAll: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        webserviceCallForGetAllUsers()
        setupUI()
    }
    
    //MARK: Function
    
    func setupUI()
    {
        btnApply.giveCornerRadius()
        btnClose.giveCornerRadius()
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            tableAssignedTo.rowHeight = 50
        } else { //IPAD
            tableAssignedTo.rowHeight = 60
        }
        if User.instance.flagSelectAllMap == "1"
        {
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
        }
        else
        {
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
    }
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                self.allUsersArr = response.UsersData!
                
                if User.instance.flagForGetUsersMap == "1"
                {
                    User.instance.selectedUsersMapArr.removeAll()
                    User.instance.selectedUsersIDMapArr.removeAll()
                    User.instance.checkboxMapArr.removeAll()
                    
                    for i in 0..<self.allUsersArr.count
                    {
                        User.instance.checkboxMapArr.append("1")
                        User.instance.selectedUsersMapArr.append(self.allUsersArr[i].FullName ?? "")
                        User.instance.selectedUsersIDMapArr.append(self.allUsersArr[i].UserID ?? "")
                    }
                    User.instance.flagForGetUsersMap = "0"
                }
                
                self.tableAssignedTo.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForApplyAssignedTo()
    {
        User.instance.allUsersMapArr.removeAll()
        User.instance.allUsersMapArr = User.instance.selectedUsersIDMapArr
        self.showHUD()
        
        var parameters:[String : Any] = [:]
        if User.instance.statusIDMapArr.count == 0 {
            
            parameters["WOStatus"] = ""
            
        } else {
            parameters["WOStatus"] = User.instance.statusIDMapArr
        }
        if User.instance.typeIDMapArr.count == 0 {
            
            parameters["WorkOrderType"] = ""
            
        } else {
            parameters["WorkOrderType"] = User.instance.typeIDMapArr
        }
        if User.instance.priorityIDMapArr.count == 0 {
            
            parameters["WOPriority"] = ""
            
        } else {
            parameters["WOPriority"] = User.instance.priorityIDMapArr
        }
        if User.instance.allUsersMapArr.count == 0 {
            
            parameters["AssignedTo"] = ""
            
        } else {
            parameters["AssignedTo"] = User.instance.allUsersMapArr
        }
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        if flag == 0 {
            
            parameters["FromDate"] = selectedDate!
            parameters["ToDate"] = selectedDate!
        } else {
            parameters["FromDate"] = selectedFromDate!
            parameters["ToDate"] = selectedToDate!
        }
        

        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                let responseArr:[GetEventTaskByDatesData] = response.data!
                
                print(responseArr.count)
                
                User.instance.workOrderByDateArr.removeAll()
                User.instance.workOrderByDateArr = responseArr
                
                User.instance.flagMap = "0"
                User.instance.flagMapWO = "0"
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckbox(sender:UIButton)
    {
        guard let cell = sender.superview?.superview as? MapAssignedToTableViewCell else {
            return // or fatalError() or whatever
        }
        
        let indexPath = tableAssignedTo.indexPath(for: cell)
        
        let index = User.instance.selectedUsersIDMapArr.index(of: cell.lblName.accessibilityLabel!)
        
        if (sender.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            sender.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            
            User.instance.selectedUsersMapArr.remove(at: index!)
            User.instance.selectedUsersIDMapArr.remove(at: index!)
            
            User.instance.checkboxMapArr.remove(at: (indexPath?.row)!)
            User.instance.checkboxMapArr.insert("0", at: (indexPath?.row)!)
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            User.instance.selectedUsersMapArr.append(cell.lblName.text!)
            User.instance.selectedUsersIDMapArr.append(cell.lblName.accessibilityLabel!)
            
            User.instance.checkboxMapArr.remove(at: (indexPath?.row)!)
            User.instance.checkboxMapArr.insert("1", at: (indexPath?.row)!)
        }
        
        print("Name:\(User.instance.selectedUsersMapArr)")
        print("ID:\(User.instance.selectedUsersIDMapArr)")
        print("Checkbox:\(User.instance.checkboxMapArr)")
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allUsersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ASSIGNED_TO) as! MapAssignedToTableViewCell
        cell.btnCheckbox.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
        cell.btnCheckbox.tag = indexPath.row
        cell.lblName.text = allUsersArr[indexPath.row].FullName
        
        
        
        cell.lblName.accessibilityLabel = allUsersArr[indexPath.row].UserID ?? ""
        
        if User.instance.checkboxMapArr[indexPath.row] == "1"
        {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
        }
        else{
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
        
        return cell
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApply(_ sender: Any) {
        
        webserviceCallForApplyAssignedTo()
    }
    
    @IBAction func btnSelectAll(_ sender: Any) {
        
        User.instance.selectedUsersMapArr.removeAll()
        User.instance.selectedUsersIDMapArr.removeAll()
        User.instance.checkboxMapArr.removeAll()
        
        if (btnSelectAll.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            for _ in 0..<self.allUsersArr.count
            {
                User.instance.checkboxMapArr.append("0")
                
            }
            User.instance.flagSelectAllMap = "0"
        }
        else{
            btnSelectAll.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            
            for i in 0..<self.allUsersArr.count
            {
                User.instance.checkboxMapArr.append("1")
                User.instance.selectedUsersMapArr.append(self.allUsersArr[i].FullName ?? "")
                User.instance.selectedUsersIDMapArr.append(self.allUsersArr[i].UserID ?? "")
            }
            User.instance.flagSelectAllMap = "1"
        }
        
        self.tableAssignedTo.reloadData()
        print(User.instance.selectedUsersMapArr)
        print(User.instance.selectedUsersIDMapArr)
        print(User.instance.checkboxMapArr)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
