//
//  ActionSheetCalendar.swift
//  Field Service Pulse
//
//  Created by Apple on 02/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ActionSheetCalendar: UIView {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var btnNewTask: UIButton!
    @IBOutlet weak var btnNewEvent: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    override func awakeFromNib() {
        
        viewBackground.giveBorderToView()
        btnNewTask.leftImage(image: #imageLiteral(resourceName: "task"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        btnNewEvent.leftImage(image: #imageLiteral(resourceName: "calendar"), renderMode: UIImage.RenderingMode.alwaysOriginal)
    }
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
