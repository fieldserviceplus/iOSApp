//
//  ViewController.swift
//  DemoCalendar
//
//  Created by Apple on 19/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    
    private let CELL_EVENTS = "cell_events"
    var flag = 0
    var firstDay:String?
    var lastDay:String?
    
    
    var eventsByDatesArr:[GetEventTaskByDatesData]?
    var wholeMonthEventArr:[String] = []
    
    var allEventsArr:[String] = []
    var selectedDate:String?
    
    var eventTimeArr:[String] = []
    var eventTitleArr:[String] = []
    var eventColorArr:[String] = []
    var eventIDArr:[String] = []
    var eventTypeArr:[String] = []
    var tableFromDate:String?
    var tableToDate:String?
    
    var previouslySelectedDate:Date!
    
    var myDate1:Date! = nil
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate weak var calendar: FSCalendar!
    
    //MARK: IBOutlets
    
    @IBOutlet weak var constraintTableTop: NSLayoutConstraint!
    @IBOutlet weak var table_events: UITableView!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var lblNoEvents: UILabel!
    @IBOutlet weak var lblTodayDate: UILabel!
    @IBOutlet weak var btnTodayDate: UIButton!
    @IBOutlet weak var viewNavigationBar: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Uncomment this to perform an 'initial-week-scope'
        // self.calendar.scope = FSCalendarScopeWeek;
        User.instance.flagForGetUsers = "1"
        User.instance.flag = "1"
        User.instance.flagType = "1"
        User.instance.flagStatus = "1"
        User.instance.flagPriority = "1"
        User.instance.ShowTasks = "1"
        setupUI()
        tableFromDate = getTodayDate(format: "MM/dd/yyyy")
        tableToDate = tableFromDate
        webserviceCallForGetAllUsers()
        webserviceCallForSelectedDateEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.table_events.reloadData()
    }
    
    func setupUI()
    {
        self.navigationController?.navigationBar.isHidden = true
        table_events.rowHeight = 44
        table_events.rowHeight = UITableView.automaticDimension
        table_events.tableFooterView = UIView()
        designCalendar()
    }
    
    func designCalendar()
    {
        //let height: CGFloat = UIDevice.current.model.hasPrefix("iPad") ? 400 : 300
        let height: CGFloat!
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            height = 300
            self.table_events.rowHeight = 40
        } else { //IPAD
            height = 400
            self.table_events.rowHeight = 60
        }
        
        let calendar = FSCalendar(frame: CGRect(x: 0, y: self.viewNavigationBar.frame.height + UIApplication.shared.statusBarFrame.height, width: view.frame.size.width, height: height))
        calendar.dataSource = self
        calendar.delegate = self
        calendar.allowsMultipleSelection = false
        self.view.addSubview(calendar)
        self.calendar = calendar
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            calendar.appearance.weekdayFont = UIFont.systemFont(ofSize: 15)
            calendar.appearance.headerTitleFont = UIFont.systemFont(ofSize: 17)
        } else { //IPAD
            calendar.appearance.weekdayFont = UIFont.systemFont(ofSize: 20)
            calendar.appearance.headerTitleFont = UIFont.systemFont(ofSize: 22)
        }
        calendar.calendarHeaderView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.calendarWeekdayView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendar.appearance.eventSelectionColor = UIColor.black
        calendar.appearance.eventOffset = CGPoint(x: 0, y: -5)
        
        calendar.appearance.eventDefaultColor = UIColor.black
        calendar.appearance.titleWeekendColor = UIColor.red
        calendar.appearance.weekdayTextColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
        calendar.appearance.headerTitleColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
        calendar.appearance.headerMinimumDissolvedAlpha = 0.0
        calendar.today = nil // Hide the today circle
        
        calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        //        calendar.clipsToBounds = true // Remove top/bottom line
        
        calendar.swipeToChooseGesture.isEnabled = true // Swipe-To-Choose
        
        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
        calendar.addGestureRecognizer(scopeGesture)
        let dates = [
            self.gregorian.date(byAdding: .day, value: -1, to: Date()),
            Date(),
            self.gregorian.date(byAdding: .day, value: 1, to: Date())
        ]
        dates.forEach { (date) in
            self.calendar.select(date, scrollToDate: false)
        }
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        
        getCurrentMonth()
        lblTodayDate.text = getTodayDate(format:"dd")
    }
    
    //MARK: Function
    func  webserviceCallForGetEventTaskByDates()
    {
        wholeMonthEventArr.removeAll()
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "FromDate": firstDay!,
                          "ToDate": lastDay!]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                self.eventsByDatesArr = response.data
                
                for i in 0..<(self.eventsByDatesArr?.count as! Int)
                {
                    let dict = self.eventsByDatesArr![i]
                    self.wholeMonthEventArr.append(dict.Start!)
                    
                }
                
                if self.flag == 0
                {
                    self.allEventsArr.removeAll()
                    self.allEventsArr = self.wholeMonthEventArr
                    self.calendar.reloadData()
                    self.flag = 1
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForSelectedDateEvents()
    {
        
        var fixedTimeEventArr:[String] = []
        var fixedTimeEventTitleArr:[String] = []
        var fixedTimeEventColorArr:[String] = []
        var fixedTimeEventIDArr:[String] = []
        var fixedTimeEventTypeArr:[String] = []
        
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "FromDate": tableFromDate!,
                          "ToDate": tableToDate!]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                let responseArr = response.data
                self.eventTimeArr.removeAll()
                self.eventTitleArr.removeAll()
                self.eventColorArr.removeAll()
                self.eventIDArr.removeAll()
                self.eventTypeArr.removeAll()
                
                if (responseArr?.count)!>0
                {
                    for i in 0..<(responseArr?.count as! Int)
                    {
                        
                        let dict = responseArr![i]
                        if dict.Time == "All-Day" && dict.Start != ""
                        {
                            self.eventTimeArr.append(dict.Time ?? "")
                            self.eventTitleArr.append(dict.Title ?? "")
                            self.eventColorArr.append(dict.ColorCode ?? "")
                            self.eventIDArr.append(dict.ID ?? "")
                            self.eventTypeArr.append(dict.type ?? "")
                        }
                        else{
                            fixedTimeEventArr.append(dict.Time ?? "")
                            fixedTimeEventTitleArr.append(dict.Title ?? "")
                            fixedTimeEventColorArr.append(dict.ColorCode ?? "")
                            fixedTimeEventIDArr.append(dict.ID ?? "")
                            fixedTimeEventTypeArr.append(dict.type ?? "")
                        }
                    }
                    
                    self.eventTimeArr = self.eventTimeArr + fixedTimeEventArr
                    self.eventTitleArr = self.eventTitleArr + fixedTimeEventTitleArr
                    self.eventColorArr = self.eventColorArr + fixedTimeEventColorArr
                    self.eventIDArr = self.eventIDArr + fixedTimeEventIDArr
                    self.eventTypeArr = self.eventTypeArr + fixedTimeEventTypeArr
                    
                    self.table_events.reloadData()
                }
                else{
                    self.lblNoEvents.isHidden = false
                    self.table_events.isHidden = true
                }
                
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    func webserviceCallForGetAllUsers()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getAllUsers(urlString: API.getAllUsersURL, parameters: parameters , headers: headers, vc: self) { (response:GetAllUsersResponse) in
            
            if response.Result == "True"
            {
                
                let allUsersArr = response.UsersData!
                
                
                User.instance.allUsersArr.removeAll()
                if User.instance.allUsersMapArr.count>0
                {
                    User.instance.allUsersMapArr.removeAll()
                }
                
                for i in 0..<allUsersArr.count
                {
                    User.instance.allUsersArr.append(allUsersArr[i].UserID!)
                    User.instance.allUsersMapArr.append(allUsersArr[i].UserID!)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func getCurrentMonth()
    {
        print(calendar.currentPage.getMonthName())
        print(calendar.currentPage.getYear())
        if calendar.currentPage.getMonthName() == "01" || calendar.currentPage.getMonthName() == "03" || calendar.currentPage.getMonthName() == "05" || calendar.currentPage.getMonthName() == "07" || calendar.currentPage.getMonthName() == "08" || calendar.currentPage.getMonthName() == "10" || calendar.currentPage.getMonthName() == "12"
        {
            
            lastDay = calendar.currentPage.getMonthName() + "/" + "31" + "/" + calendar.currentPage.getYear()
            
        }
        else if calendar.currentPage.getMonthName() == "04" || calendar.currentPage.getMonthName() == "06" || calendar.currentPage.getMonthName() == "09" || calendar.currentPage.getMonthName() == "11"
        {
            
            lastDay = calendar.currentPage.getMonthName() + "/" + "30" + "/" + calendar.currentPage.getYear()
        }
        else if calendar.currentPage.getMonthName() == "02"{
            
            lastDay = calendar.currentPage.getMonthName() + "/" + "28" + "/" + calendar.currentPage.getYear()
        }
        firstDay = calendar.currentPage.getMonthName() + "/" + "01" + "/" +  calendar.currentPage.getYear()
        
        webserviceCallForGetEventTaskByDates()
    }
    
    func getTodayDate(format:String)->String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let result = formatter.string(from: date)
        
        return result
    }
    
    @objc func btnCalendarWeek(sender:UIButton)
    {
        self.calendar.scope = FSCalendarScope.week
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            constraintTableTop.constant = 156
        } else { //IPAD
            constraintTableTop.constant = 180
        }
        
        self.view.viewWithTag(1002)?.removeFromSuperview()
    }
    @objc func btnCalendarMonth(sender:UIButton)
    {
        self.calendar.scope = FSCalendarScope.month
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            constraintTableTop.constant = 344
        } else { //IPAD
            constraintTableTop.constant = 444
        }
        self.view.viewWithTag(1002)?.removeFromSuperview()
    }
    @objc func btnMapWeek(sender:UIButton)
    {
        
        let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "CalendarMapViewController") as! CalendarMapViewController
        let todayDate = getTodayDate(format:"MM/dd/yyyy")
        mapVC.selectedDate = selectedDate ?? todayDate
        
        let startWeek = (myDate1 ?? Date()).startOfWeek
        let endWeek = (myDate1 ?? Date()).endOfWeek
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        mapVC.selectedFromDate = formatter.string(from: startWeek!)
        mapVC.selectedToDate = formatter.string(from: endWeek!)
        mapVC.flag = 1
        self.navigationController?.pushViewController(mapVC, animated: true)
        self.view.viewWithTag(1002)?.removeFromSuperview()
    }
    @objc func btnMapDay(sender:UIButton)
    {
        let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "CalendarMapViewController") as! CalendarMapViewController
        let todayDate = getTodayDate(format:"MM/dd/yyyy")
        mapVC.selectedDate = selectedDate ?? todayDate
        mapVC.flag = 0
        self.navigationController?.pushViewController(mapVC, animated: true)
        self.view.viewWithTag(1002)?.removeFromSuperview()
    }
    
    @objc func btnNewTask(sender:UIButton)
    {
        let vc = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
        self.navigationController?.pushViewController(vc, animated: true)
        self.view.viewWithTag(1003)?.removeFromSuperview()
    }
    
    @objc func btnNewEvent(sender:UIButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        self.navigationController?.pushViewController(vc, animated: true)
        self.view.viewWithTag(1003)?.removeFromSuperview()
    }
    
    // MARK:- FSCalendarDataSource
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        ////////////
        calendar.visibleCells().forEach { (cell) in
            
            let date = calendar.date(for: cell)
            
            cell.backgroundView?.alpha = 0
            cell.backgroundView?.backgroundColor = UIColor.clear
            cell.titleLabel.textColor = UIColor.black
        }
        
        ///////////
        
        print(calendar.currentPage.getMonthName())
        print(calendar.currentPage.getYear())
        if calendar.currentPage.getMonthName() == "01" || calendar.currentPage.getMonthName() == "03" || calendar.currentPage.getMonthName() == "05" || calendar.currentPage.getMonthName() == "07" || calendar.currentPage.getMonthName() == "08" || calendar.currentPage.getMonthName() == "10" || calendar.currentPage.getMonthName() == "12"
        {
            
            lastDay = calendar.currentPage.getMonthName() + "/" + "31" + "/" + calendar.currentPage.getYear()
            
        }
        else if calendar.currentPage.getMonthName() == "04" || calendar.currentPage.getMonthName() == "06" || calendar.currentPage.getMonthName() == "09" || calendar.currentPage.getMonthName() == "11"
        {
            
            lastDay = calendar.currentPage.getMonthName() + "/" + "30" + "/" + calendar.currentPage.getYear()
        }
        else if calendar.currentPage.getMonthName() == "02"{
            
            lastDay = calendar.currentPage.getMonthName() + "/" + "28" + "/" + calendar.currentPage.getYear()
        }
        firstDay = calendar.currentPage.getMonthName() + "/" + "01" + "/" +  calendar.currentPage.getYear()
        flag = 0
        webserviceCallForGetEventTaskByDates()
        
    }
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position) as! DIYCalendarCell
        
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.black.cgColor
        
        
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 1
    }
    
    
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        return UIColor.black
        
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {

        if (allEventsArr.contains(formatter.string(from: date))) {
            print("yes")
            return UIColor.green
            
        }
        else
        {
            return UIColor.black
            
        }
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventSelectionColorsFor date: Date) -> [UIColor]? {
        

        if (allEventsArr.contains(formatter.string(from: date))) {
            print("yes")
            calendar.appearance.eventSelectionColor = UIColor.green
            return [UIColor.green]
        }
        else
        {
            calendar.appearance.eventSelectionColor = UIColor.black
            return [UIColor.clear]
        }
    }
    
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        
        self.calendar.frame.size.height = bounds.height
        self.constraintTableTop.constant = bounds.height + 44
        
        calendar.visibleCells().forEach { (cell) in
            
            let date = calendar.date(for: cell)
            
            cell.backgroundView?.alpha = 0
            cell.backgroundView?.backgroundColor = UIColor.clear
            cell.titleLabel.textColor = UIColor.black
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        // User.instance.flag = "1"
       // print("did select date \(self.formatter.string(from: date))")
        selectedDate = Helper.instance.convertDateFormat(date: formatter.string(from: date), fromFormat: "yyyy-MM-dd", toFormat: "MM/dd/yyyy")
        myDate1 = date
        //Edited
        self.configureVisibleCells()
        //
        if (allEventsArr.contains(formatter.string(from: self.myDate1))) {
            //print("did select date \(self.formatter.string(from: date))")
            tableFromDate = Helper.instance.convertDateFormat(date: formatter.string(from: self.myDate1), fromFormat: "yyyy-MM-dd", toFormat: "MM/dd/yyyy")
            tableToDate = tableFromDate
            
            if User.instance.flag == "1" {
                webserviceCallForSelectedDateEvents()
            } else {
                webserviceCallForApplyAssignedTo()
            }
            //webserviceCallForSelectedDateEvents()
            self.configureVisibleCells()
            self.lblNoEvents.isHidden = true
            self.table_events.isHidden = false
        }
        else
        {
            self.lblNoEvents.isHidden = false
            self.table_events.isHidden = true
        }
        
        if monthPosition == .current
        {
            
            let cell = calendar.cell(for: date, at: .current) as! DIYCalendarCell
            cell.backgroundView?.alpha = 1
            cell.backgroundView?.backgroundColor = UIColor.black
            cell.titleLabel.textColor = UIColor.white
            if self.gregorian.isDateInToday(date)
            {
                cell.circleImageView.isHidden = false
            }
            else
            {
                cell.circleImageView.isHidden = true
            }
            
            //This code is for only if incase diddeselect method doesnt call
            if previouslySelectedDate != nil {
                let cell = calendar.cell(for: previouslySelectedDate, at: .current)
                cell?.backgroundView?.alpha = 0
                cell?.backgroundView?.backgroundColor = UIColor.clear
                cell?.titleLabel.textColor = UIColor.black
            }
            previouslySelectedDate = date
            
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        //print("did deselect date \(self.formatter.string(from: date))")
        if (allEventsArr.contains(formatter.string(from: date))) {
            print("yes")
            self.configureVisibleCells()
        }
        let cell = calendar.cell(for: date, at: .current)
        cell?.backgroundView?.alpha = 0
        cell?.backgroundView?.backgroundColor = UIColor.clear
        cell?.titleLabel.textColor = UIColor.black
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        
        let myDate = formatter.string(from: date)
        //print(myDate)
        

        if (allEventsArr.contains(myDate)) {
            print("yes")
            calendar.appearance.eventSelectionColor = UIColor.green
            return [UIColor.green]
        }
        else
        {
            calendar.appearance.eventSelectionColor = UIColor.clear
            return [UIColor.clear]
        }
        
    }
    
    // MARK: - Private functions
    
    private func configureVisibleCells() {
        calendar.visibleCells().forEach { (cell) in
            let date = calendar.date(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        if position == .current
        {
            if Calendar.current.isDate((date), inSameDayAs:Date()) == true
            {
                diyCell.titleLabel.textColor = UIColor.green
            }
            else{
                diyCell.titleLabel.textColor = UIColor.black
            }
            
           
        }
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE

            cell.titleLabel.font = UIFont.systemFont(ofSize: 15)
        } else { //IPAD

            cell.titleLabel.font = UIFont.systemFont(ofSize: 20)
        }
        
        // Custom today circle
        //diyCell.circleImageView.isHidden = !self.gregorian.isDateInToday(date)
        diyCell.circleImageView.isHidden = true
        
    }
    
    //MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if User.instance.flag == "1"
        {
            return eventTitleArr.count
        }
        else{
            return User.instance.eventTitleArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_EVENTS) as! EventsTableViewCell
        
        if User.instance.flag == "1"
        {
            if self.eventTitleArr.count>0
            {
                cell.lblTime.text = eventTimeArr[indexPath.row]
                cell.lblTitle.text = eventTitleArr[indexPath.row]
                cell.viewLine.backgroundColor = UIColor(hexString: eventColorArr[indexPath.row])
            }
        }
        else {
            if User.instance.eventTitleArr.count>0
            {
                cell.lblTime.text = User.instance.eventTimeArr[indexPath.row]
                cell.lblTitle.text = User.instance.eventTitleArr[indexPath.row]
                cell.viewLine.backgroundColor = UIColor(hexString: User.instance.eventColorArr[indexPath.row])
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if User.instance.flag == "1" {
            
            if eventTypeArr[indexPath.row] == "Task" {
                
                let taskVC = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                User.instance.taskID = eventIDArr[indexPath.row]
                self.navigationController?.pushViewController(taskVC, animated: true)
                
            } else if eventTypeArr[indexPath.row] == "Event" {
                
                let eventVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                User.instance.eventID = eventIDArr[indexPath.row]
                self.navigationController?.pushViewController(eventVC, animated: true)
            }
        } else {
            
            if User.instance.eventTypeArr[indexPath.row] == "Task" {
                
                let taskVC = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                User.instance.taskID = User.instance.eventIDArr[indexPath.row]
                self.navigationController?.pushViewController(taskVC, animated: true)
                
            } else if User.instance.eventTypeArr[indexPath.row] == "Event" {
                
                let eventVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                User.instance.eventID = User.instance.eventIDArr[indexPath.row]
                self.navigationController?.pushViewController(eventVC, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: IBActions
    
    @IBAction func btnLeftMenu(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    @IBAction func btnMore(_ sender: Any) {
        
        self.view.viewWithTag(1002)?.removeFromSuperview()
        if let customView = Bundle.main.loadNibNamed("ActionSheetCalendar", owner: self, options: nil)?.first as? ActionSheetCalendar
        {
            customView.tag = 1003
            
            customView.btnNewTask.addTarget(self, action: #selector(btnNewTask(sender:)), for: .touchUpInside)
            customView.btnNewEvent.addTarget(self, action: #selector(btnNewEvent(sender:)), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    
    @IBAction func btnTodayDate(_ sender: Any) {
        
        calendar.setCurrentPage(Date(), animated: true)
    }
    
    @IBAction func btnViewMenu(_ sender: Any) {
        
        self.view.viewWithTag(1003)?.removeFromSuperview()
        if let customView = Bundle.main.loadNibNamed("CalendarMenu", owner: self, options: nil)?.first as? CalendarMenuView
        {
            customView.tag = 1002
            
            customView.btnCalendarWeek.addTarget(self, action: #selector(btnCalendarWeek(sender:)), for: .touchUpInside)
            customView.btnCalendarMonth.addTarget(self, action: #selector(btnCalendarMonth(sender:)), for: .touchUpInside)
            customView.btnMapWeek.addTarget(self, action: #selector(btnMapWeek(sender:)), for: .touchUpInside)
            customView.btnMapDay.addTarget(self, action: #selector(btnMapDay(sender:)), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "EditFilterViewController")as! EditFilterViewController
        let todayDate = getTodayDate(format:"MM/dd/yyyy")
        filterVC.selectedDate = selectedDate ?? todayDate
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    @IBAction func btnAssigndTo(_ sender: Any) {
        
        let assignedToVC = self.storyboard?.instantiateViewController(withIdentifier: "AssignedToViewController")as! AssignedToViewController
        let todayDate = getTodayDate(format:"MM/dd/yyyy")
        assignedToVC.selectedDate = selectedDate ?? todayDate
        self.navigationController?.pushViewController(assignedToVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webserviceCallForApplyAssignedTo()
    {
        
        self.showHUD()
        
        var parameters:[String : Any] = [:]
        if User.instance.statusIDArr.count == 0 {
            
            parameters["WOStatus"] = ""
            
        } else {
            parameters["WOStatus"] = User.instance.statusIDArr
        }
        if User.instance.typeIDArr.count == 0 {
            
            parameters["WorkOrderType"] = ""
            
        } else {
            parameters["WorkOrderType"] = User.instance.typeIDArr
        }
        if User.instance.priorityIDArr.count == 0 {
            
            parameters["WOPriority"] = ""
            
        } else {
            parameters["WOPriority"] = User.instance.priorityIDArr
        }
        if User.instance.allUsersArr.count == 0 {
            
            parameters["AssignedTo"] = ""
            
        } else {
            parameters["AssignedTo"] = User.instance.allUsersArr
        }
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["FromDate"] = selectedDate!
        parameters["ToDate"] = selectedDate!
        parameters["ShowTasks"] = User.instance.ShowTasks
        
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                let responseArr:[GetEventTaskByDatesData] = response.data!
                
                self.wholeDayArr.removeAll()
                self.wholeDayEventArr.removeAll()
                self.fixedTimeArr.removeAll()
                self.fixedTimeEventArr.removeAll()
                self.wholeDayColorArr.removeAll()
                self.fixedTimeColorArr.removeAll()
                self.wholeDayIDArr.removeAll()
                self.fixedTimeIDArr.removeAll()
                self.wholeDayTypeArr.removeAll()
                self.fixedTimeTypeArr.removeAll()
                
                print(responseArr.count)
                for i in 0..<responseArr.count
                {
                    let dict = responseArr[i]
                    if dict.Time == "All-Day" && dict.Start != ""
                    {
                        self.wholeDayArr.append(dict.Time!)
                        self.wholeDayEventArr.append(responseArr[i].Title ?? "")
                        
                        self.wholeDayColorArr.append(responseArr[i].ColorCode ?? "")
                        self.wholeDayIDArr.append(responseArr[i].ID ?? "")
                        self.wholeDayTypeArr.append(responseArr[i].type ?? "")
                    }
                    else{
                        self.fixedTimeArr.append(dict.Time!)
                        self.fixedTimeEventArr.append(responseArr[i].Title ?? "")
                        
                        self.fixedTimeColorArr.append(responseArr[i].ColorCode ?? "")
                        self.fixedTimeIDArr.append(responseArr[i].ID ?? "")
                        self.fixedTimeTypeArr.append(responseArr[i].type ?? "")
                    }
                    
                }
                
                User.instance.eventTitleArr.removeAll()
                User.instance.eventTimeArr.removeAll()
                User.instance.eventColorArr.removeAll()
                User.instance.eventIDArr.removeAll()
                User.instance.eventTypeArr.removeAll()
                
                User.instance.eventTitleArr = self.wholeDayEventArr + self.fixedTimeEventArr
                User.instance.eventTimeArr = self.wholeDayArr + self.fixedTimeArr
                User.instance.eventColorArr = self.wholeDayColorArr + self.fixedTimeColorArr
                User.instance.eventIDArr = self.wholeDayIDArr + self.fixedTimeIDArr
                User.instance.eventTypeArr = self.wholeDayTypeArr + self.fixedTimeTypeArr
                
                User.instance.flag = "0"
                self.table_events.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    var wholeDayArr:[String] = []
    var fixedTimeArr:[String] = []
    var wholeDayEventArr:[String] = []
    var fixedTimeEventArr:[String] = []
    var wholeDayColorArr:[String] = []
    var fixedTimeColorArr:[String] = []
    var wholeDayIDArr:[String] = []
    var fixedTimeIDArr:[String] = []
    var wholeDayTypeArr:[String] = []
    var fixedTimeTypeArr:[String] = []
    
    
}

extension Date {
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    func getYear() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        return dateFormatter.string(from: self)
    }
    
    
    
}

