//
//  CalendarMenuView.swift
//  Field Service Pulse
//
//  Created by Apple on 25/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CalendarMenuView: UIView {

    @IBOutlet weak var btnCalendarWeek: UIButton!
    @IBOutlet weak var btnCalendarMonth: UIButton!
    @IBOutlet weak var btnMapDay: UIButton!
    @IBOutlet weak var btnMapWeek: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    
    override func awakeFromNib() {
        
        viewBackground.giveBorderToView()
        self.btnCalendarWeek.contentHorizontalAlignment = .left
        self.btnCalendarMonth.contentHorizontalAlignment = .left
        self.btnMapWeek.contentHorizontalAlignment = .left
        self.btnMapDay.contentHorizontalAlignment = .left
     }
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    

}
