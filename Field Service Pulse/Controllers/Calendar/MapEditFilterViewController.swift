//
//  MapEditFilterViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MapEditFilterViewController: UIViewController {
    
    //MARK: Variables
    
    var WOTypeArr:[WOTypeData] = []
    var WOStatusArr:[WOStatusData] = []
    var WOPriorityArr:[WOPriorityData] = []
    var yForFirstStackview = 0
    var yForSecondStackview = 0
    var yForThirdStackview = 0
    var heightFirstStackview = 0
    var heightSecondStackview = 0
    var heightThirdStackview = 0
    var selectedDate:String!
    var selectedFromDate:String?
    var selectedToDate:String?
    var flag:Int?
    
    
    //MARK: IBOutlet
    
    @IBOutlet weak var stackviewType: UIStackView!
    @IBOutlet weak var stackviewStatus: UIStackView!
    @IBOutlet weak var stackviewPriority: UIStackView!
    @IBOutlet weak var btnArrowType: UIButton!
    @IBOutlet weak var btnArrowStatus: UIButton!
    @IBOutlet weak var btnArrowPriority: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var heightStackviewType: NSLayoutConstraint!
    @IBOutlet weak var heightStackviewStatus: NSLayoutConstraint!
    @IBOutlet weak var heightStackviewPriority: NSLayoutConstraint!
    @IBOutlet weak var heightViewType: NSLayoutConstraint!
    @IBOutlet weak var heightViewStatus: NSLayoutConstraint!
    @IBOutlet weak var heightViewPriority: NSLayoutConstraint!
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var viewPriority: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForCalendarFilterData()
    }
    
    //MARK: Function
    
    func setupUI()
    {
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        viewType.giveBorderToView()
        viewStatus.giveBorderToView()
        viewPriority.giveBorderToView()
        
        
    }
    
    func webserviceCallForApplyFilter()
    {
        self.showHUD()
        
        var parameters:[String : Any] = [:]
        if User.instance.statusIDMapArr.count == 0 {
            
            parameters["WOStatus"] = ""
            
        } else {
            parameters["WOStatus"] = User.instance.statusIDMapArr
        }
        if User.instance.typeIDMapArr.count == 0 {
            
            parameters["WorkOrderType"] = ""
            
        } else {
            parameters["WorkOrderType"] = User.instance.typeIDMapArr
        }
        if User.instance.priorityIDMapArr.count == 0 {
            
            parameters["WOPriority"] = ""
            
        } else {
            parameters["WOPriority"] = User.instance.priorityIDMapArr
        }
        if User.instance.allUsersMapArr.count == 0 {
            
            parameters["AssignedTo"] = ""
            
        } else {
            parameters["AssignedTo"] = User.instance.allUsersMapArr
        }
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        if flag == 0 {
            
            parameters["FromDate"] = selectedDate!
            parameters["ToDate"] = selectedDate!
        } else {
            parameters["FromDate"] = selectedFromDate!
            parameters["ToDate"] = selectedToDate!
        }

        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                let responseArr:[GetEventTaskByDatesData] = response.data!
                
                print(responseArr.count)
                
                User.instance.workOrderByDateArr.removeAll()
                User.instance.workOrderByDateArr = responseArr
                
                User.instance.flagMap = "0"
                User.instance.flagMapWO = "0"
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webserviceCallForCalendarFilterData()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.calendarFilterData(urlString: API.calendarFilterDataURL, parameters: parameters , headers: headers, vc: self) { (response:CalendarFilterData) in
            
            if response.Result == "True"
            {
                
                let data:CalendarFilterDataDict = response.data!
                self.WOPriorityArr = data.WOPriority!
                self.WOStatusArr = data.WOStatus!
                self.WOTypeArr = data.WOType!
                
                if User.instance.flagTypeMap == "1"
                {
                    User.instance.checkboxTypeMapArr.removeAll()
                    User.instance.typeIDMapArr.removeAll()
                    
                    for i in 0..<self.WOTypeArr.count
                    {
                        User.instance.checkboxTypeMapArr.append("1")
                        User.instance.typeIDMapArr.append(self.WOTypeArr[i].WorkOrderTypeID!)
                    }
                    User.instance.flagTypeMap = "0"
                }
                if User.instance.flagStatusMap == "1"
                {
                    User.instance.checkboxStatusMapArr.removeAll()
                    User.instance.statusIDMapArr.removeAll()
                    
                    for i in 0..<self.WOStatusArr.count
                    {
                        User.instance.checkboxStatusMapArr.append("1")
                        User.instance.statusIDMapArr.append(self.WOStatusArr[i].WOStatusID!)
                    }
                    User.instance.flagStatusMap = "0"
                }
                if User.instance.flagPriorityMap == "1"
                {
                    User.instance.checkboxPriorityMapArr.removeAll()
                    User.instance.priorityIDMapArr.removeAll()
                    
                    for i in 0..<self.WOPriorityArr.count
                    {
                        User.instance.checkboxPriorityMapArr.append("1")
                        User.instance.priorityIDMapArr.append(self.WOPriorityArr[i].WOPriorityID!)
                    }
                    User.instance.flagPriorityMap = "0"
                }
                
                for i in 0..<self.WOTypeArr.count
                {
                    
                    self.heightStackviewType.constant = self.heightStackviewType.constant + 40
                    let button = UIButton(frame: CGRect(x: 0, y: self.yForFirstStackview, width: Int(self.stackviewType.frame.width), height: 25))
                    button.tag = 1
                    button.contentHorizontalAlignment = .left
                    let spacing = 10 // the amount of spacing to appear between image and title
                    button.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
                    button.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
                    if User.instance.checkboxTypeMapArr[i] == "1"
                    {
                        button.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                    }
                    else{
                        button.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                    }
                    
                    button.accessibilityLabel = self.WOTypeArr[i].WorkOrderTypeID
                    button.setTitle(self.WOTypeArr[i].WorkOrderType, for: .normal)
                    button.accessibilityIdentifier = String(i)
                    button.setTitleColor(UIColor.black, for: .normal)
                    button.titleLabel?.font = button.titleLabel?.font.withSize(20)
                    button.addTarget(self, action: #selector(self.btnCheckbox(sender:)), for: .touchUpInside)
                    self.stackviewType.addSubview(button)
                    self.heightFirstStackview = self.heightFirstStackview + 40
                    self.yForFirstStackview = self.yForFirstStackview + 40
                    
                }
                
                for i in 0..<self.WOStatusArr.count
                {
                    
                    self.heightStackviewStatus.constant = self.heightStackviewStatus.constant + 40
                    let button = UIButton(frame: CGRect(x: 0, y: self.yForSecondStackview, width: Int(self.stackviewStatus.frame.width), height: 25))
                    button.tag = 2
                    button.contentHorizontalAlignment = .left
                    let spacing = 10 // the amount of spacing to appear between image and title
                    button.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
                    button.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
                    if User.instance.checkboxStatusMapArr[i] == "1"
                    {
                        button.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                    }
                    else{
                        button.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                    }
                    button.setTitle(self.WOStatusArr[i].Status, for: .normal)
                    button.accessibilityLabel = self.WOStatusArr[i].WOStatusID
                    button.accessibilityIdentifier = String(i)
                    button.setTitleColor(UIColor.black, for: .normal)
                    button.titleLabel?.font = button.titleLabel?.font.withSize(20)
                    button.addTarget(self, action: #selector(self.btnCheckbox(sender:)), for: .touchUpInside)
                    self.stackviewStatus.addSubview(button)
                    self.heightSecondStackview = self.heightSecondStackview + 40
                    
                    self.yForSecondStackview = self.yForSecondStackview + 40
                    
                }
                
                for i in 0..<self.WOPriorityArr.count
                {
                    
                    self.heightStackviewPriority.constant = self.heightStackviewPriority.constant + 40
                    let button = UIButton(frame: CGRect(x: 0, y: self.yForThirdStackview, width: Int(self.stackviewPriority.frame.width), height: 25))
                    button.tag = 3
                    button.contentHorizontalAlignment = .left
                    let spacing = 10 // the amount of spacing to appear between image and title
                    button.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
                    button.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
                    if User.instance.checkboxPriorityMapArr[i] == "1"
                    {
                        button.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                    }
                    else{
                        button.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                    }
                    button.setTitle(self.WOPriorityArr[i].Priority, for: .normal)
                    button.accessibilityIdentifier = String(i)
                    button.accessibilityLabel = self.WOPriorityArr[i].WOPriorityID
                    
                    button.setTitleColor(UIColor.black, for: .normal)
                    button.titleLabel?.font = button.titleLabel?.font.withSize(20)
                    button.addTarget(self, action: #selector(self.btnCheckbox(sender:)), for: .touchUpInside)
                    self.stackviewPriority.addSubview(button)
                    self.heightThirdStackview = self.heightThirdStackview + 40
                    
                    self.yForThirdStackview = self.yForThirdStackview + 40
                    
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    @objc func btnCheckbox(sender:UIButton)
    {
        if sender.tag == 1
        {
            if (sender.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
            {
                sender.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                let index = User.instance.typeIDMapArr.index(of: sender.accessibilityLabel!)
                
                User.instance.typeIDMapArr.remove(at: index!)
                User.instance.checkboxTypeMapArr.remove(at: Int(sender.accessibilityIdentifier!)!)
                User.instance.checkboxTypeMapArr.insert("0", at: Int(sender.accessibilityIdentifier!)!)
            }
            else{
                sender.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                
                User.instance.typeIDMapArr.append(sender.accessibilityLabel!)
                User.instance.checkboxTypeMapArr.remove(at: Int(sender.accessibilityIdentifier!)!)
                User.instance.checkboxTypeMapArr.insert("1", at: Int(sender.accessibilityIdentifier!)!)
            }
        }
        else if sender.tag == 2
        {
            if (sender.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
            {
                sender.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                let index = User.instance.statusIDMapArr.index(of: sender.accessibilityLabel!)
                
                User.instance.statusIDMapArr.remove(at: index!)
                User.instance.checkboxStatusMapArr.remove(at: Int(sender.accessibilityIdentifier!)!)
                User.instance.checkboxStatusMapArr.insert("0", at: Int(sender.accessibilityIdentifier!)!)
                
            }
            else{
                sender.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                
                User.instance.statusIDMapArr.append(sender.accessibilityLabel!)
                User.instance.checkboxStatusMapArr.remove(at: Int(sender.accessibilityIdentifier!)!)
                User.instance.checkboxStatusMapArr.insert("1", at: Int(sender.accessibilityIdentifier!)!)
            }
        }
        else if sender.tag == 3
        {
            if (sender.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
            {
                sender.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                let index = User.instance.priorityIDMapArr.index(of: sender.accessibilityLabel!)
                
                User.instance.priorityIDMapArr.remove(at: index!)
                User.instance.checkboxPriorityMapArr.remove(at: Int(sender.accessibilityIdentifier!)!)
                User.instance.checkboxPriorityMapArr.insert("0", at: Int(sender.accessibilityIdentifier!)!)
            }
            else{
                sender.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                
                User.instance.priorityIDMapArr.append(sender.accessibilityLabel!)
                User.instance.checkboxPriorityMapArr.remove(at: Int(sender.accessibilityIdentifier!)!)
                User.instance.checkboxPriorityMapArr.insert("1", at: Int(sender.accessibilityIdentifier!)!)
            }
        }
    }
    
    
    
    //MARK: IBAction
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        webserviceCallForApplyFilter()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnExpandCollapse(_ sender: Any) {
        
        let btnArr = [btnArrowType,btnArrowStatus,btnArrowPriority]
        let heightViewArr = [heightViewType,heightViewStatus,heightViewPriority]
        let stackviewArr = [stackviewType,stackviewStatus,stackviewPriority]
        let heightStackviewArr = [heightFirstStackview,heightSecondStackview,heightThirdStackview]
        
        for i in 0..<btnArr.count
        {
            if (sender as AnyObject).tag == btnArr[i]?.tag
            {
                if (btnArr[i]?.currentImage?.isEqual(#imageLiteral(resourceName: "arrow")))!
                {
                    btnArr[i]?.setImage(#imageLiteral(resourceName: "down-arrow"), for: .normal)
                    heightViewArr[i]?.constant = CGFloat(heightStackviewArr[i]) + 50
                    stackviewArr[i]?.isHidden = false
                }
                else{
                    
                    for j in 0..<btnArr.count
                    {
                        btnArr[j]?.setImage(#imageLiteral(resourceName: "arrow"), for: .normal)
                        heightViewArr[j]?.constant = 50
                        stackviewArr[j]?.isHidden = true
                    }
                }
            }
            else{
                btnArr[i]?.setImage(#imageLiteral(resourceName: "arrow"), for: .normal)
                heightViewArr[i]?.constant = 50
                stackviewArr[i]?.isHidden = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
