//
//  CalendarMapViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 25/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleMaps

class CalendarMapViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    //MARK: Variables
    
    private let CELL_ID = "cell_mapWO"
    var selectedDate:String!
    var datePickerContainer:UIView!
    
    var selectedFromDate:String?
    var selectedToDate:String?
    var flag:Int?
    
    //MARK: IBOutlets
    
    @IBOutlet weak var mapview: GMSMapView!
    @IBOutlet weak var lblSelectedDate: UILabel!
    @IBOutlet weak var tableMapWO: UITableView!
    @IBOutlet weak var lblNoWO: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        User.instance.flagMapWO = "1"
        webserviceCallForGetWorkOrderByDate()
        lblSelectedDate.text = Helper.instance.convertDateFormat(date: selectedDate, fromFormat: "MM/dd/yyyy", toFormat: "dd")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if User.instance.flagMapWO == "0"
        {
            showMarkersOnMap()
            self.tableMapWO.reloadData()
        }
        
    }

    //MARK: Function
    
    func  webserviceCallForGetWorkOrderByDate()
    {
        self.showHUD()
        
        var parameters:[String:Any] = [:]
        if flag == 0 {
            parameters = ["UserID":User.instance.UserID,
                              "OrganizationID":User.instance.OrganizationID,
                              "FromDate":selectedDate!,
                              "ToDate":selectedDate!]
        } else {
            parameters = ["UserID":User.instance.UserID,
                              "OrganizationID":User.instance.OrganizationID,
                              "FromDate":selectedFromDate!,
                              "ToDate":selectedToDate!]
        }
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEventTaskByDates(urlString: API.getEventTaskByDatesURL, parameters: parameters, headers: headers, vc: self) { (response:GetEventTaskByDates) in
            
            if response.Result == "True"
            {
                if User.instance.workOrderByDateArr.count>0 {
                    User.instance.workOrderByDateArr.removeAll()
                    self.lblNoWO.isHidden = true
                    self.tableMapWO.isHidden = false
                } else {
                    self.lblNoWO.isHidden = false
                    self.tableMapWO.isHidden = true
                }
                User.instance.workOrderByDateArr = response.data!
                self.showMarkersOnMap()
                self.tableMapWO.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }

    //MARK: Google Maps
    
    func showMarkersOnMap()
    {
        var bounds = GMSCoordinateBounds()

        self.mapview.clear()
        
        if User.instance.workOrderByDateArr.count > 0
        {
            for i in 0..<User.instance.workOrderByDateArr.count
            {
                if User.instance.workOrderByDateArr[i].Latitude != "0" &&  User.instance.workOrderByDateArr[i].Longitude != "0" && User.instance.workOrderByDateArr[i].ShapeType != "" {
                    
                    let marker = GMSMarker()
                    marker.zIndex = Int32(i)
                    var markerView:UIImageView!
                    
                    if User.instance.workOrderByDateArr[i].ShapeType == "SQUARE"
                    {
                        markerView = UIImageView(image: #imageLiteral(resourceName: "square-map").withRenderingMode(.alwaysTemplate))
                    }
                    else if User.instance.workOrderByDateArr[i].ShapeType == "CIRCLE"
                    {
                        markerView = UIImageView(image: #imageLiteral(resourceName: "circle-map").withRenderingMode(.alwaysTemplate))
                    }
                    else if User.instance.workOrderByDateArr[i].ShapeType == "STAR"
                    {
                        markerView = UIImageView(image: #imageLiteral(resourceName: "star-map").withRenderingMode(.alwaysTemplate))
                    }
                    
                    markerView.tintColor = UIColor(hexString: User.instance.workOrderByDateArr[i].ColorCode!)
                    
                    marker.iconView = markerView
                    
                    marker.position = CLLocationCoordinate2D(latitude:CLLocationDegrees(User.instance.workOrderByDateArr[i].Latitude ?? "")!, longitude:CLLocationDegrees(User.instance.workOrderByDateArr[i].Longitude ?? "")!)
                    marker.snippet = User.instance.workOrderByDateArr[i].Title ?? ""
                    marker.map = self.mapview
                    let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(User.instance.workOrderByDateArr[i].Latitude!) as! CLLocationDegrees, longitude: CLLocationDegrees(User.instance.workOrderByDateArr[i].Longitude!) as! CLLocationDegrees, zoom: 4.0)
                    self.mapview.camera = camera
                    self.mapview.animate(to: camera)
                }
                
            }
            self.lblNoWO.isHidden = true
            self.tableMapWO.isHidden = false
        } else {
            self.lblNoWO.isHidden = false
            self.tableMapWO.isHidden = true
        }
        
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User.instance.workOrderByDateArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! mapWOTableViewCell
        
        cell.lblTitle.text = User.instance.workOrderByDateArr[indexPath.row].Title ?? ""
        if User.instance.workOrderByDateArr[indexPath.row].ShapeType == "SQUARE"
        {
            cell.imgShape.image = #imageLiteral(resourceName: "square-map").withRenderingMode(.alwaysTemplate)
        }
        else if User.instance.workOrderByDateArr[indexPath.row].ShapeType == "CIRCLE"
        {
            cell.imgShape.image = #imageLiteral(resourceName: "circle-map").withRenderingMode(.alwaysTemplate)
        }
        else if User.instance.workOrderByDateArr[indexPath.row].ShapeType == "STAR"
        {
            cell.imgShape.image = #imageLiteral(resourceName: "star-map").withRenderingMode(.alwaysTemplate)
        }
        
        cell.imgShape.tintColor = UIColor(hexString: User.instance.workOrderByDateArr[indexPath.row].ColorCode!)
        
        cell.lblTime.text = (User.instance.workOrderByDateArr[indexPath.row].StartTime ?? "") + "-" + (User.instance.workOrderByDateArr[indexPath.row].EndTime ?? "")
        cell.viewLine.backgroundColor = UIColor(hexString: User.instance.workOrderByDateArr[indexPath.row].ColorCode!)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = User.instance.workOrderByDateArr[indexPath.row]
            
            if dict.type == "Task" {
                
                let taskVC = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                User.instance.taskID = dict.ID ?? "1"
                self.navigationController?.pushViewController(taskVC, animated: true)
                
            } else if dict.type == "Event" {
                
                let eventVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                User.instance.eventID = dict.ID ?? "1"
                self.navigationController?.pushViewController(eventVC, animated: true)
            }
        
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        
        let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "MapEditFilterViewController") as! MapEditFilterViewController
        filterVC.selectedDate = selectedDate
        filterVC.flag = flag
        if flag == 1{
            filterVC.selectedFromDate = selectedFromDate
            filterVC.selectedToDate = selectedToDate
        }
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    @IBAction func btnAssignedTo(_ sender: Any) {
        let assignedToVC = self.storyboard?.instantiateViewController(withIdentifier: "MapAssignedToViewController") as! MapAssignedToViewController
        assignedToVC.selectedDate = selectedDate
        assignedToVC.flag = flag
        if flag == 1{
            assignedToVC.selectedFromDate = selectedFromDate
            assignedToVC.selectedToDate = selectedToDate
        }
        self.navigationController?.pushViewController(assignedToVC, animated: true)
    }
    
    @IBAction func btnMore(_ sender: Any) {
        if let customView = Bundle.main.loadNibNamed("ActionSheetCalendar", owner: self, options: nil)?.first as? ActionSheetCalendar
        {
            customView.tag = 1003
            
            customView.btnNewTask.addTarget(self, action: #selector(btnNewTask(sender:)), for: .touchUpInside)
            customView.btnNewEvent.addTarget(self, action: #selector(btnNewEvent(sender:)), for: .touchUpInside)
            
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    @objc func btnNewTask(sender:UIButton)
    {
        let vc = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
        self.navigationController?.pushViewController(vc, animated: true)
        self.view.viewWithTag(1003)?.removeFromSuperview()
    }
    
    @objc func btnNewEvent(sender:UIButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        self.navigationController?.pushViewController(vc, animated: true)
        self.view.viewWithTag(1003)?.removeFromSuperview()
    }
    @IBAction func btnMenu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectDate(_ sender: Any) {
        datePickerContainer = UIView()
        
        datePickerContainer.frame = CGRect(x:0.0, y:self.view.frame.height-300, width:self.view.frame.width, height:300.0)
        datePickerContainer.backgroundColor = UIColor.white
        
        let picker : UIDatePicker = UIDatePicker()
        picker.datePickerMode = UIDatePicker.Mode.date
        picker.addTarget(self, action: #selector(selectDate(sender:)), for: UIControl.Event.valueChanged)
        picker.frame = CGRect(x: 0.0, y: 20, width: self.view.frame.width, height: 280)
        picker.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        datePickerContainer.addSubview(picker)
        
        let doneButton = UIButton()
        doneButton.setTitle("Done", for: UIControl.State.normal)
        doneButton.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
        doneButton.addTarget(self, action: #selector(dismissPicker), for: UIControl.Event.touchUpInside)
        
        doneButton.frame = CGRect(x: self.view.frame.width-80, y: 5, width: 70, height: 37)
        datePickerContainer.addSubview(doneButton)
        self.view.addSubview(datePickerContainer)
        
    }
    
    @objc func selectDate(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        selectedDate = dateFormatter.string(from: sender.date)
        lblSelectedDate.text = Helper.instance.convertDateFormat(date: dateFormatter.string(from: sender.date), fromFormat: "MM/dd/yyyy", toFormat: "dd")

        if flag == 1 {
            let startWeek = (sender.date).startOfWeek
            let endWeek = (sender.date).endOfWeek
            
            selectedFromDate = dateFormatter.string(from: startWeek!)
            selectedToDate = dateFormatter.string(from: endWeek!)
        }
        
    }
    
    @objc func dismissPicker()
    {
        User.instance.flagMapWO = "1"
        webserviceCallForGetWorkOrderByDate()
        datePickerContainer.removeFromSuperview()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
