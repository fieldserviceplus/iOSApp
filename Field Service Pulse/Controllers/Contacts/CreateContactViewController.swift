//
//  CreateContactViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 12/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateContactViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, searchDelegate {
    
    
    //MARK: Variables
    
    var accountID:String?
    var accountName:String?
    var pickerView = UIPickerView()
    
    var assignedToID:String?
    var typeID:String?
    var leadSourceID:String?
    var preferredTechID:String?
    var leadSourcesArr:[GetLeadSourcesData] = []
    
    var textfieldTag = 0
    var latMailingAddress:String?
    var longMailingAddress:String?
    
    var isActive:String?
    var dontCall:String?
    var solutationArr:[GetSalutationsData] = []
    var flagResponse = "0"
    var response:ContactDetails?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtFirstName: AkiraTextField!
    @IBOutlet weak var txtLastName: AkiraTextField!
    @IBOutlet weak var txtTitle: AkiraTextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtMobile: AkiraTextField!
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var txtMailingCountry: AkiraTextField!
    @IBOutlet weak var txtMailingPostalCode: AkiraTextField!
    @IBOutlet weak var txtMailingAddress: UITextField!
    @IBOutlet weak var txtMailingCity: AkiraTextField!
    @IBOutlet weak var txtMailingState: AkiraTextField!
    @IBOutlet weak var txtLeadSource: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtNotes: AkiraTextField!
    
    
    //UIButton
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var btnMailingAddress: UIButton!
    @IBOutlet weak var btnDontCall: UIButton!
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        webserviceCallForLeadSource()
        webserviceCallForGetSalutations()
        webserviceCallForCustomFields()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnMailingAddress.giveBorderToButton()
        self.txtAssignedTo.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtLeadSource.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtFullName.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        self.txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtMailingAddress.layer.borderWidth = 1.5
        self.txtMailingAddress.layer.borderColor = UIColor.darkGray.cgColor
        
        if flagResponse == "1" {
            responseContactDetails()
            flagResponse = "0"
        }
        if accountName != nil {
            self.txtAccount.text = accountName
        }
        pickerView.delegate = self
        
        txtLeadSource.inputView = pickerView
        txtFullName.inputView = pickerView
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        if textfieldTag == 1 {
            
            txtAssignedTo.text = searchVC.fullName
            assignedToID = searchVC.userID
        } else if textfieldTag == 7 {
            
            txtAccount.text = searchVC.accountName
            accountID = searchVC.accountID
            webserviceCallForAccountDetails()
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":accountID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtMailingAddress.text = response.data?.BillingAddress
                self.txtMailingCity.text = response.data?.BillingCity
                self.txtMailingState.text = response.data?.BillingState
                self.txtMailingCountry.text = response.data?.BillingCountry
                self.txtMailingPostalCode.text = response.data?.BillingPostalCode
                self.latMailingAddress = response.data?.BillingLatitude
                self.longMailingAddress = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCall()
    {
        if assignedToID == nil || accountID == nil || txtFirstName.text! == "" || txtLastName.text! == "" || txtTitle.text! == "" || txtMailingCity.text! == "" || txtMailingState.text! == "" || txtMailingAddress.text! == "" || txtMailingCountry.text! == "" || txtMailingPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        self.showHUD()
        var parameters:[String:String] = [:]
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["AssignedTo"] = assignedToID
        parameters["Account"] = accountID
        parameters["PhoneNo"] = txtPhone.text ?? ""
        parameters["MailingAddress"] = txtMailingAddress.text ?? ""
        parameters["MailingCity"] = txtMailingCity.text ?? ""
        parameters["MailingPostalCode"] = txtMailingPostalCode.text ?? ""
        parameters["MailingState"] = txtMailingState.text ?? ""
        parameters["MailingCountry"] = txtMailingCountry.text ?? ""
        parameters["MailingLatitude"] = latMailingAddress ?? ""
        parameters["MailingLongitude"] = longMailingAddress ?? ""
        parameters["Title"] = txtTitle.text ?? ""
        parameters["Salutation"] = txtFullName.text ?? ""
        parameters["IsActive"] = isActive ?? "1"
        parameters["DoNotCall"] = dontCall ?? "0"
        parameters["FirstName"] = txtFirstName.text ?? ""
        parameters["LastName"] = txtLastName.text ?? ""
        parameters["BirthDate"] = txtDOB.text ?? ""
        parameters["LeadSource"] = leadSourceID ?? ""
        parameters["Notes"] = txtNotes.text ?? ""
        parameters["Email"] = txtEmail.text ?? ""
        parameters["MobileNo"] = txtMobile.text ?? ""
        
    
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createContact(urlString: API.createContactURL, parameters: parameters , headers: headers, vc: self) { (response:CreateContact) in
            
            if response.Result == "True"
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsViewController") as! ContactDetailsViewController
                User.instance.contactID = String(response.ContactID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    func responseContactDetails()
    {
        
        self.txtAssignedTo.text = response?.data?.AssignedToName
        self.assignedToID = response?.data?.AssignedTo
        self.txtAccount.text = response?.data?.AccountName
        self.accountID = response?.data?.Account
        self.txtPhone.text = response?.data?.PhoneNo
        self.txtMailingAddress.text = response?.data?.MailingAddress
        self.txtMailingCity.text = response?.data?.MailingCity
        self.txtMailingPostalCode.text = response?.data?.MailingPostalCode
        self.txtMailingState.text = response?.data?.MailingState
        self.txtMailingCountry.text = response?.data?.MailingCountry
        self.latMailingAddress = response?.data?.MailingLatitude
        self.longMailingAddress = response?.data?.MailingLongitude
        self.txtTitle.text = response?.data?.Title
        self.txtFullName.text = response?.data?.Salutation
        self.isActive = response?.data?.IsActive
        self.dontCall = response?.data?.DoNotCall
        self.txtFirstName.text = response?.data?.FirstName
        self.txtLastName.text = response?.data?.LastName
        self.txtDOB.text = response?.data?.BirthDate
        self.leadSourceID = response?.data?.LeadSource
        self.txtLeadSource.text = response?.data?.LeadSourceName
        self.txtNotes.text = response?.data?.Notes
        self.txtEmail.text = response?.data?.Email
        self.txtMobile.text = response?.data?.MobileNo
                
        if self.isActive == "1"
        {
            self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
        }
        else{
            self.btnIsActive.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
        if self.dontCall == "1"
        {
            self.btnDontCall.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
        }
        else
        {
            self.btnDontCall.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
    }
    
    func webserviceCallForLeadSource()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getLeadSources(urlString: API.getLeadSourcesURL, parameters: parameters , headers: headers, vc: self) { (response:GetLeadSources) in
            
            if response.Result == "True"
            {
                
                self.leadSourcesArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetSalutations()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getSalutations(urlString: API.getSalutationsURL, parameters: parameters , headers: headers, vc: self) { (response:GetSalutations) in
            
            if response.Result == "True"
            {
                
                self.solutationArr = response.data!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "Object":"Contact",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                
                var viewFromConstrain:Any = self.txtMailingPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtMailingPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtMailingPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtMailingPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                customView.txtviewLabelField.textColor = UIColor.lightGray
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            self.checkboxValueArr.append("")
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.btnSave, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(value + "," + sender.accessibilityLabel!, at: index)
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: ",\(sender.accessibilityLabel!)", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxKey:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 2
        {
            return solutationArr.count + 1
        }
        else if textfieldTag == 13
        {
            return leadSourcesArr.count + 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 2
        {
            return row == 0 ? "--None--" : solutationArr[row-1].Salutation
        }
        else if textfieldTag == 13
        {
            return row == 0 ? "--None--" : leadSourcesArr[row-1].LeadSource
        }
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 2
        {
            if row == 0
            {
                txtFullName.text = ""
            }
            else{
                txtFullName.text = solutationArr[row-1].Salutation
            }
        }
        if textfieldTag == 13
        {
            if row == 0
            {
                txtLeadSource.text = ""
            }
            else
            {
                txtLeadSource.text = leadSourcesArr[row-1].LeadSource
                leadSourceID = leadSourcesArr[row-1].LeadSourceID
            }
            
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
        
    }
    
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        
        //
        if textField == txtAssignedTo || textField == txtFullName || textField == txtAccount || textField == txtDOB || textField == txtLeadSource
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtFullName.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtFirstName.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtLastName.becomeFirstResponder()
            return false
        }
        else if textField.tag == 4
        {
            txtTitle.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 12
        {
            txtMobile.becomeFirstResponder()
        }
        else if textField.tag == 15
        {
            txtEmail.becomeFirstResponder()
        }
        
        else if textField.tag == 17
        {
            txtLeadSource.becomeFirstResponder()
        }
        else if textField.tag == 20
        {
            txtDOB.becomeFirstResponder()
        }
        else if textField.tag == 21
        {
            txtNotes.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 5
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 7
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Account")
        }
        if textField.tag == 13
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 16
        {
            textfieldTag = textField.tag
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    
    
    //MARK: TextView Delegate Methods
    
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMailingAddress(_ sender: Any) {
        
        
        let selectVC = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        selectVC.isUnwindTo = "createAC"
        self.navigationController?.pushViewController(selectVC, animated: true)
    }
    
    
    
    @IBAction func btnSave(_ sender: Any) {
        
        
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        
//        if self.requiredCustomFieldsArr.count != 0 {
//            
//            for i in 0..<self.requiredCustomFieldsArr.count {
//                
//                
//                if self.requiredCustomFieldsArr[i] is UITextView {
//                    let charset = CharacterSet(charactersIn: "*")
//                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
//                        
//                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
//                        return
//                        
//                    }
//                }
//                
//                
//                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
//                    
//                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
//                    return
//                }
//            }
//        }
        
        //CustomTextField
        
        
        if lastTextfieldTapped != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                customFieldValueArr.append(lastTextfieldTapped.text ?? "")
            }
        }
        
        
        
        //CustomTextView
        
        if lastTappedTextView != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                customFieldValueArr.append(lastTappedTextView.text ?? "")
            }
        }
        
        
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropFirst()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCall()
    }
    
    @IBAction func btnIsActive(_ sender: Any) {
        
        if (btnIsActive.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnIsActive.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            isActive = "0"
        }
        else{
            btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            isActive = "1"
        }
        
        
    }
    
    @IBAction func btnDontCall(_ sender: Any) {
        
        if (btnDontCall.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnDontCall.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            dontCall = "0"
        }
        else{
            btnDontCall.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            dontCall = "1"
        }
        
        
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        
        
            if let SelectLocationVC = sender.source as? SelectLocationViewController {
                
                txtMailingAddress.text = SelectLocationVC.address
                txtMailingCity.text = SelectLocationVC.city
                txtMailingState.text = SelectLocationVC.state
                txtMailingCountry.text = SelectLocationVC.country
                txtMailingPostalCode.text = SelectLocationVC.postalCode
                self.latMailingAddress = SelectLocationVC.latitude
                self.longMailingAddress = SelectLocationVC.longitude
            }
        
        
    }
    
    @IBAction func txtDOB(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDOB), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDOB(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtDOB.text = dateFormatter.string(from: sender.date)
        
    }
}

extension CreateContactViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            
            
                latMailingAddress = String(place.coordinate.latitude)
                longMailingAddress = String(place.coordinate.longitude)
                if component.type == "sublocality_level_1" {
                    print(component.name)
                    txtMailingAddress.text = "\(place.name) \(area ?? "") \(component.name)"
                    
                }
                if component.type == "postal_code"
                {
                    print("Code: \(component.name)")
                    txtMailingPostalCode.text = component.name
                }
                if component.type == "administrative_area_level_2" {
                    print("City: \(component.name)")
                    txtMailingCity.text = component.name
                }
                if component.type == "administrative_area_level_1"
                {
                    print("State: \(component.name)")
                    txtMailingState.text = component.name
                    
                }
                if component.type == "country"
                {
                    print("Country: \(component.name)")
                    txtMailingCountry.text = component.name
                }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


