//
//  ContactLayoutViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD

class ContactLayoutViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    var lastContentOffset: CGFloat = 0
    var isActive:String?
    var dontCall:String?
    var accountID:String?
    var assignedToID:String?
    
    //MARK: IBOutlet
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var btnDontCall: UIButton!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblLeadSource: UILabel!
    @IBOutlet weak var btnIsActive: UIButton!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblLastModifiedDate: UILabel!
    @IBOutlet weak var lblLastModifiedBy: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblSystemInfo: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        webserviceCall()
        webserviceCallForCustomFields()
    }
    
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":User.instance.contactID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactDetails(urlString: API.contactDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:ContactDetails) in
            
            if response.Result == "True"
            {
                self.lblAssignedTo.text = response.data?.AssignedToName
                self.lblAccount.text = response.data?.AccountName
                self.lblPhone.text = response.data?.PhoneNo
                self.lblAddress.text = response.data?.MailingAddress
                self.lblCity.text = (response.data?.MailingCity)! + "," + (response.data?.MailingState)! + ", " + (response.data?.MailingPostalCode)!
                self.lblCountry.text = response.data?.MailingCountry
                self.lblTitle.text = response.data?.Title
                self.lblName.text = response.data?.Name
                self.isActive = response.data?.IsActive
                self.dontCall = response.data?.DoNotCall
                self.lblDOB.text = response.data?.BirthDate
                self.lblLeadSource.text = response.data?.LeadSourceName
                self.lblNotes.text = response.data?.Notes
                self.lblEmail.text = response.data?.Email
                self.lblMobile.text = response.data?.MobileNo
                self.lblCreatedBy.text = response.data?.CreatedBy
                self.lblCreatedDate.text = response.data?.CreatedDate
                self.lblLastModifiedBy.text = response.data?.LastModifiedBy
                self.lblLastModifiedDate.text = response.data?.LastModifiedDate
                self.accountID = (response.data?.Account)!
                self.assignedToID = (response.data?.AssignedTo)!
                
                if self.isActive == "1"
                {
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                else{
                    self.btnIsActive.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                }
                if self.dontCall == "1"
                {
                    self.btnDontCall.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                else
                {
                    self.btnDontCall.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.contactID,
                          "Object":"Contact",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayOptionValues:[String]?
                var arrayFieldValue:[String]?
                
                var viewFromConstrain = self.viewAddress
                for i in 0..<(response.data?.count)!  {
                    print(response.data?[i].FieldType)
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 5
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 85
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 70
                        case "LongText":
                            constantHeight = 110
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayOptionValues = (response.data?[i].OptionValues)?.components(separatedBy: ",")
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.viewAddress)
                    }
                    
                    let myView = UIView()
                    
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    print(constant)
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewAddress, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewAddress, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        if let customView = Bundle.main.loadNibNamed("DefaultSizeView", owner: self, options: nil)?.first as? DefaultSizeView {
                            
                            customView.lblFieldLabel.text = response.data?[i].FieldLabel
                            customView.lblFieldValue.text = response.data?[i].FieldValue
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextSizeView", owner: self, options: nil)?.first as? LongTextSizeView {
                            
                            customView.txtview.text = response.data?[i].FieldValue
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            customView.lblLabelField.text = response.data?[i].FieldLabel
                            
                            for i in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                
                                if ((arrayFieldValue?.contains((arrayOptionValues?[i])!))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![i]
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = true
                    parent.actionSheetAccountView.isHidden = true
                    //parent.btnClose.isHidden = true// Here you hide it when animation done
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = false // Here you hide it when animation done
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnClicked(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            if self.assignedToID != nil && self.assignedToID != "" {
                let userVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserDetailsViewController")  as! UserDetailsViewController
                userVC.viewUserID = assignedToID!
                self.navigationController?.pushViewController(userVC, animated: true)
            }
            
        } else if sender.tag == 2 {
            
            if self.accountID != nil && self.accountID != "" {
                let acVC = UIStoryboard(name: "Account", bundle: Bundle.main).instantiateViewController(withIdentifier: "AccountDetailsViewController")  as! AccountDetailsViewController
                User.instance.accountID = accountID!
                User.instance.accountName = self.lblAccount.text ?? ""
                self.navigationController?.pushViewController(acVC, animated: true)
            }
            
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
