//
//  ContactViewAllViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ContactViewAllViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    
    private let CELL_VIEWALL = "cellViewAll"
    var tag:Int?
    var titleLabel:String?
    var headerTitle:String?
    var workorderArr:[ContactRelatedWorkOrderData] = []
    var estimateArr:[ContactRelatedEstimateData] = []
    var invoiceArr:[ContactRelatedInvoiceData] = []
    var eventArr:[ContactRelatedEventData] = []
    var taskArr:[ContactRelatedTaskData] = []
    var fileArr:[ContactRelatedFileData] = []
    var noteArr:[ContactRelatedNoteData] = []
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    //MARK: IBOutlets
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var tableViewAll: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
    }
    func setupUI()
    {
        tableViewAll.tableFooterView = UIView()
        lblTitle.text = titleLabel
        lblHeaderTitle.text = headerTitle
    }
    @objc func btnDetails(sender:UIButton) {
        
        if tag == 0
        {
            let woVC = UIStoryboard(name: "WorkOrder", bundle:  Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
            woVC.navigationController?.isNavigationBarHidden = true
            User.instance.workorderID = workorderArr[sender.tag].WorkOrderID!
            User.instance.workorderSubject = workorderArr[sender.tag].Subject!
            self.navigationController?.pushViewController(woVC, animated: true)
        }
        else if tag == 1
        {
            let estimateVC = UIStoryboard(name: "Estimate", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController") as! EstimateDetailsViewController
            estimateVC.navigationController?.isNavigationBarHidden = true
            User.instance.estimateID = estimateArr[sender.tag].EstimateID!
            User.instance.estimateName = estimateArr[sender.tag].EstimateName!
            self.navigationController?.pushViewController(estimateVC, animated: true)
        }
        else if tag == 2
        {
            let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
            invoiceVC.navigationController?.isNavigationBarHidden = true
            User.instance.invoiceID = invoiceArr[sender.tag].InvoiceID!
            User.instance.invoiceNo = invoiceArr[sender.tag].InvoiceNumber!
            self.navigationController?.pushViewController(invoiceVC, animated: true)
        }
        else if tag == 3
        {
            let eventVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            eventVC.navigationController?.isNavigationBarHidden = true
            User.instance.eventID = eventArr[sender.tag].EventID!
            self.navigationController?.pushViewController(eventVC, animated: true)
        }
        else if tag == 4
        {
            let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
            taskVC.navigationController?.isNavigationBarHidden = true
            User.instance.taskID = taskArr[sender.tag].TaskID!
            self.navigationController?.pushViewController(taskVC, animated: true)
        }
        else if tag == 5
        {
            let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
            fileVC.navigationController?.isNavigationBarHidden = true
            User.instance.fileID = fileArr[sender.tag].FileID!
            self.navigationController?.pushViewController(fileVC, animated: true)
        }
        else if tag == 6
        {
            let noteVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "NoteDetailsViewController") as! NoteDetailsViewController
            noteVC.navigationController?.isNavigationBarHidden = true
            User.instance.noteID = noteArr[sender.tag].NoteID!
            noteVC.relatedTo = "Contact"
            noteVC.objectID = User.instance.contactID
            self.navigationController?.pushViewController(noteVC, animated: true)
        }
    }
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tag == 0
        {
            return workorderArr.count
        }
        else if tag == 1
        {
            return estimateArr.count
        }
        else if tag == 2
        {
            return invoiceArr.count
        }
        else if tag == 3
        {
            return eventArr.count
        }
        else if tag == 4
        {
            return taskArr.count
        }
        else if tag == 5
        {
            return fileArr.count
        }
        else if tag == 6
        {
            return noteArr.count
        }
        
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_VIEWALL) as! ContactViewAllTableViewCell
        cell.btnDetail.addTarget(self, action: #selector(btnDetails(sender:)), for: .touchUpInside)
        cell.btnDetail.tag = indexPath.row
        cell.view_back.dropShadow()
        if tag == 0
        {
            cell.lblFirst?.text = "WorkOrder No: " + (workorderArr[indexPath.row].WorkOrderNo ?? "")
            cell.lblSecond?.text = "Subject: " +  (workorderArr[indexPath.row].Subject ?? "").convertToCurrencyFormat()
            cell.lblThird?.text = "Start Date: " + (workorderArr[indexPath.row].StartDate ?? "")
            cell.lblFourth?.text = "Status: " + (workorderArr[indexPath.row].Status ?? "")
            cell.lblFifth?.text = "Priority: " + (workorderArr[indexPath.row].Priority ?? "")
            cell.lblSixth?.text = "Category Name: " + (workorderArr[indexPath.row].CategoryName ?? "")
        }
        else if tag == 1
        {
            cell.lblFirst?.text = "Estimate Name: " + (estimateArr[indexPath.row].EstimateName ?? "")
            cell.lblSecond?.text = "Created Date: " +  (estimateArr[indexPath.row].CreatedDate ?? "")
            cell.lblThird?.text = "Expiration Date: " + (estimateArr[indexPath.row].ExpirationDate ?? "")
            cell.lblFourth?.text = "Owner Name: " + (estimateArr[indexPath.row].OwnerName ?? "")
            cell.lblFifth?.text = "Status: " +  (estimateArr[indexPath.row].Status ?? "")
            cell.lblSixth?.text = "Grand Total: " + (estimateArr[indexPath.row].GrandTotal ?? "").convertToCurrencyFormat()
        }
        else if tag == 2
        {
            cell.lblFirst?.text = "Invoice Status: " + (invoiceArr[indexPath.row].InvoiceStatus ?? "")
            cell.lblSecond?.text = "DueDate: " +  (invoiceArr[indexPath.row].DueDate ?? "")
            cell.lblThird?.text = "Subject: " + (invoiceArr[indexPath.row].Subject ?? "")
            cell.lblFourth?.text = "Sub Total: " + (invoiceArr[indexPath.row].SubTotal ?? "").convertToCurrencyFormat()
            cell.lblFifth?.text = "TotalPrice: " +  (invoiceArr[indexPath.row].TotalPrice ?? "").convertToCurrencyFormat()
            cell.lblSixth?.text = ""
        }
        else if tag == 3
        {
            cell.lblFirst?.text = "Assigned To: " + (eventArr[indexPath.row].AssignedTo ?? "")
            cell.lblSecond?.text = "Created By: " +  (eventArr[indexPath.row].CreatedBy ?? "")
            cell.lblThird?.text = "Name: " + (eventArr[indexPath.row].Name ?? "")
            cell.lblFourth?.text = "Created Date: " + (eventArr[indexPath.row].CreatedDate ?? "")
            cell.lblFifth?.text = "Event End Date: " +  (eventArr[indexPath.row].EventEndDate ?? "")
            cell.lblSixth?.text = "Subject: " + (eventArr[indexPath.row].Subject ?? "")
        }
            
        else if tag == 4
        {
            cell.lblFirst?.text = "TaskType: " + (taskArr[indexPath.row].TaskType ?? "")
            cell.lblSecond?.text = "Task Status: " + (taskArr[indexPath.row].TaskStatus ?? "")
            cell.lblThird?.text = "Priority: " + (taskArr[indexPath.row].Priority ?? "")
            cell.lblFourth?.text = "Subject: " + (taskArr[indexPath.row].Subject ?? "")
            cell.lblFifth?.text = "Name: " + (taskArr[indexPath.row].Name ?? "")
            cell.lblSixth?.text = "Date: " + (taskArr[indexPath.row].Date ?? "")
        }
        else if tag == 5
        {
            cell.lblFirst?.text = "File Name: " + (fileArr[indexPath.row].FileName ?? "")
            cell.lblSecond?.text = "Content Type: " +  (fileArr[indexPath.row].ContentType ?? "")
            cell.lblThird?.text = "Subject: " + (fileArr[indexPath.row].Subject ?? "")
            cell.lblFourth?.text = ""
            cell.lblFifth?.text = ""
            cell.lblSixth?.text = ""
        }
            
        else if tag == 6
        {
            cell.lblFirst?.text = "Owner Name: " + (noteArr[indexPath.row].OwnerName ?? "")
            cell.lblSecond?.text = "Subject: " +  (noteArr[indexPath.row].Subject ?? "")
            cell.lblThird?.text = "Created Date: " + (noteArr[indexPath.row].CreatedDate ?? "")
            cell.lblFourth?.text = ""
            cell.lblFifth?.text = ""
            cell.lblSixth?.text = ""
        }
        if(selectedArray.contains(indexPath))
        {
            // use selected image
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        }
        else
        {
            // use normal image
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewAll
        {
            let cell = tableView.cellForRow(at: indexPath) as! ContactViewAllTableViewCell
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath))
            {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }
            else
            {
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            
            if indexPath.row == selectedIndex{
                selectedIndex = -1
                
            }else{
                selectedIndex = indexPath.row
                
            }
            tableView.reloadData()
        }
        else
        {
            //self.performSegue(withIdentifier: SEGUE_AC_DETAILS, sender: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableViewAll
        {
            if indexPath.row == selectedIndex
            {
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    return 193
                } else { //IPAD
                    return 227
                }
                
            }else{
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    return 98
                } else { //IPAD
                    return 115
                }
            }
        }
        return 0
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}

