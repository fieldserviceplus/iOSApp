//
//  RecentContactsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 12/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK: VAriables
    
    private let CELL_CONTACTS = "cell_contacts"
    
    private let SEGUE_CREATE_CONTACT = "segue_CreateContact"
    private let SEGUE_CONTACT_DETAILS = "segueContactDetails"
    private let SEGUE_LIST_VIEW = "segue_RecentListView"
    
    var getContactViewsArr:[ContactGetViewsData]? = []
    var listArr:[RecentContactsData] = []
    var arrTag = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var lblTableHeader: UILabel!
    @IBOutlet weak var tableContact: UITableView!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var viewDropdown: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForGetContactsViews()
        webserviceCallForRecentContacts()
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        tableContact.tableFooterView = UIView()
        lblDropdown.giveBorderToLabel()
        viewDropdown.dropShadow()
        btnDropdown.giveBorderToButton()
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            tableDropdown.rowHeight = 40
        } else { //IPAD
            tableDropdown.rowHeight = 50
        }
        
    }
    
    
    func  webserviceCallForGetContactsViews()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactGetViews(urlString: API.contactGetViewsURL, parameters: parameters, headers: headers, vc: self) { (response:ContactGetViews) in
            
            if response.Result == "True"
            {
                self.getContactViewsArr = response.data
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    func  webserviceCallForRecentContacts()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentContacts(urlString: API.recentContactsURL, parameters: parameters, headers: headers, vc: self) { (response:RecentContacts) in
            
            if response.Result == "True"
            {
                
                self.listArr = response.data!
                self.tableContact.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_LIST_VIEW
        {
            let listVC = segue.destination as! ContactListViewController
            let contact = sender as! ContactGetViewsData
            listVC.ContactViewID = contact.ContactViewID!
            listVC.ContactViewName = contact.ContactViewName!
        }
        else if segue.identifier == SEGUE_CONTACT_DETAILS
        {
            let detailVC = segue.destination as! ContactDetailsViewController
            
            let contact = sender as? RecentContactsData
            User.instance.contactID = contact?.ContactID ?? ""
            User.instance.contactName = contact?.ContactName ?? ""
        }
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableContact
        {
            return listArr.count
        }
        return getContactViewsArr!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == tableContact
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_CONTACTS) as! RecentContactsTableViewCell
            
            cell.lblContactName.text = listArr[indexPath.row].ContactName ?? ""
            cell.lblPhoneNo.text = listArr[indexPath.row].PhoneNo
            cell.lblAccountName.text = listArr[indexPath.row].AccountName
            cell.lblCity.text = listArr[indexPath.row].MailingCity
            cell.lblState.text = listArr[indexPath.row].MailingState
            cell.lblAssignedTo.text = listArr[indexPath.row].AssignedTo
            
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = getContactViewsArr?[indexPath.row].ContactViewName ?? ""
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(15)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(20)
            }
            return cell1
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == tableDropdown
        {
            self.performSegue(withIdentifier: SEGUE_LIST_VIEW, sender: getContactViewsArr?[indexPath.row])
            
        }
        else if tableView == tableContact
        {
            self.performSegue(withIdentifier: SEGUE_CONTACT_DETAILS, sender: listArr[indexPath.row])
        }
        
        
    }
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    @IBAction func btnNewContact(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_CREATE_CONTACT, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
