//
//  COntactFilterViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol sendingContactDataDelegate {
    
    func sendData(array:[[String:String]], flag:Int)
    
}

class ContactFilterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //MARK: Variables
    
    var senderTag = 0
    var name:String?
    var isCancel = false
    var dropdown2 = ["Equals","Contains","StartsWith","DoesNotContain","NotEqualTo","LessThan","GreaterThan","LessOREqualTo","GreaterOREqualTo"]
    var contactFieldsArr:[String] = []
    var contactConditionsArr:[String] = []
    var picker = UIPickerView()
    var pickerInputField: UITextField!
    var contactValuesArr:[String] = []
    var cell:FilterTableViewCell?
    var cell1:FilterTableViewCell?
    var ContactViewID:String?
    var estimateFieldsArr:[GetEstimateViewFieldsData] = []
    var contactFilterArr:[[String:String]] = []
    var SortByValue:String?
    var indexOfWOField:Int?
    var indexOfDropdown2:Int?
    var delegate:sendingContactDataDelegate?
    var textFieldIndexPath:IndexPath?
    var count = 0
    
    //MARK: IBOutlet
    
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnClearAll: UIButton!
    @IBOutlet weak var btnAddFilter: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
            table.rowHeight = 110
        } else { //IPAD
            table.rowHeight = 171
        }
        webserviceCallForGetContactViewFields()
        pickerKeyboard()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnSave.isEnabled = false
    }
    
    //MARK: Function
    
    func setupUI()
    {
        table.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "id")
        viewBackground.giveBorderToView()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnClearAll.giveCornerRadius()
        btnAddFilter.giveCornerRadius()
    }
    
    func  webserviceCallForGetContactViewFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getEstimateViewFields(urlString: API.getContactViewFieldsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:GetEstimateViewFields) in
            
            if response.Result == "True"
            {
                self.estimateFieldsArr = response.data!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForContactFilter()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "ContactViewID":ContactViewID ?? "",
                          "SortByField":"ContactName",
                          "SortByValue":SortByValue ?? "",
                          "FilterFields":contactFieldsArr,
                          "FilterConditions":contactConditionsArr,
                          "FilterValues":contactValuesArr] as [String : Any]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactFilter(urlString: API.contactFilterURL, parameters: parameters, headers: headers, vc: self) { (response:ContactFilter) in
            
            if response.Result == "True"
            {
                
                self.contactFilterArr = response.data!
                
                self.delegate?.sendData(array: self.contactFilterArr, flag: 1)
                self.navigationController?.popViewController(animated: true)
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func pickerKeyboard()
    {
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        
        pickerInputField = {
            let field = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            self.view.addSubview(field)
            field.delegate = self
            return field
        }()
        pickerInputField.inputView = picker
    }
    
    @objc func btnDropdown1(sender:UIButton)
    {
        senderTag = 1
        picker.reloadAllComponents()
        
        pickerInputField.becomeFirstResponder()
        //Get Button cell position.
        let ButtonPosition = (sender as AnyObject).convert(CGPoint.zero, to: table)
        let indexPath = table.indexPathForRow(at: ButtonPosition)
        indexOfWOField = indexPath?.row
        if indexPath != nil {
            
            cell = table.cellForRow(at: indexPath!) as? FilterTableViewCell
            print("Cell indexPath: \(String(describing: indexPath?.row))")
        }
    }
    
    @objc func btnDropdown2(sender:UIButton)
    {
        senderTag = 2
        picker.reloadAllComponents()
        if pickerInputField.isFirstResponder {
            pickerInputField.resignFirstResponder()
        }
        
        pickerInputField.inputView = picker
        pickerInputField.becomeFirstResponder()
        //Get Button cell position.
        let ButtonPosition = (sender as AnyObject).convert(CGPoint.zero, to: table)
        let indexPath = table.indexPathForRow(at: ButtonPosition)
        indexOfDropdown2 = indexPath?.row
        if indexPath != nil {
            
            cell1 = table.cellForRow(at: indexPath!) as? FilterTableViewCell
            
            
            print("Cell indexPath: \(String(describing: indexPath?.row))")
        }
    }
    
    @objc func btnCancel(sender:UIButton)
    {
        isCancel = true
        if textFieldIndexPath != nil
        {
            let cell3 = table.cellForRow(at: textFieldIndexPath!) as! FilterTableViewCell
            contactValuesArr[cell3.txtTitle.tag] = cell3.txtTitle.text ?? ""
        }
        contactFieldsArr.remove(at: sender.tag)
        contactConditionsArr.remove(at: sender.tag)
        contactValuesArr.remove(at: sender.tag)
        count -= 1
        self.viewWillLayoutSubviews()
        self.table.reloadData()
        textFieldIndexPath = nil
    }
    
    //MARK: IBAction
    
    @IBAction func btnAddFilter(_ sender: Any) {
        
        isCancel = false
        for i in 0..<contactFieldsArr.count
        {
            if contactFieldsArr[i] == "--None--"
            {
                Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                return
            }
        }
        for i in 0..<contactConditionsArr.count
        {
            if contactConditionsArr[i] == "--None--"
            {
                Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                return
            }
        }
        
        
        count += 1
        btnSave.isEnabled = true
        contactFieldsArr.append("--None--")
        contactConditionsArr.append("--None--")
        contactValuesArr.append("")
        print(contactFieldsArr)
        print(contactConditionsArr)
        print(contactValuesArr)
        table.reloadData()
    }
    
    
    @IBAction func btnSave(_ sender: Any) {
        
        
        if count == 0
        {
            return
            
        }
        
        if count == 1
        {
            let firstIndex = IndexPath(row:0,section:0)
            let firstCell = table.cellForRow(at: firstIndex) as! FilterTableViewCell
            if firstCell.lblDropdown1.text == "--None--" || firstCell.lblDropdown2.text == "--None--"
            {
                Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                return
            }
            
            
        }
        
        let index = IndexPath(row:count-1,section:0)
        let cell = table.cellForRow(at: index) as! FilterTableViewCell
        
        if cell.lblDropdown1.text == "--None--" || cell.lblDropdown2.text == "--None--"
        {
            contactValuesArr.remove(at: count-1)
            contactFieldsArr.remove(at: count-1)
            contactConditionsArr.remove(at: count-1)
        }
        
        //contactValuesArr[cell.txtTitle.tag] = cell.txtTitle.text ?? ""
        if textFieldIndexPath != nil
        {
            let cell3 = table.cellForRow(at: textFieldIndexPath!) as! FilterTableViewCell
            contactValuesArr[cell3.txtTitle.tag] = cell3.txtTitle.text ?? ""
        }
        
        webserviceCallForContactFilter()
        print(contactValuesArr)
        print(contactFieldsArr)
        print(contactConditionsArr)
        
        
    }
    
    
    
    
    @IBAction func btnCancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnClearAll(_ sender: Any) {
        
        count = 0
        self.tableHeightConstraint?.constant = 10
        contactFieldsArr.removeAll()
        contactValuesArr.removeAll()
        contactConditionsArr.removeAll()
        table.reloadData()
    }
    
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "id") as! FilterTableViewCell
        
        cell.txtTitle.delegate = self
        cell.txtTitle.tag = indexPath.row
        cell.btnCancel.tag = indexPath.row
        cell.txtTitle.text = contactValuesArr[indexPath.row]
        cell.lblDropdown1.text = contactFieldsArr[indexPath.row]
        cell.lblDropdown2.text = contactConditionsArr[indexPath.row]
        
        cell.btnDropdown1.addTarget(self, action: #selector(btnDropdown1(sender:)), for: .touchUpInside)
        cell.btnDropdown2.addTarget(self, action: #selector(btnDropdown2(sender:)), for: .touchUpInside)
        cell.btnCancel.addTarget(self, action: #selector(btnCancel(sender:)), for: .touchUpInside)
        cell.lblDropdown1.giveBorderToLabel()
        cell.lblDropdown2.giveBorderToLabel()
        cell.btnDropdown1.giveBorderToButton()
        cell.btnDropdown2.giveBorderToButton()
        return cell
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text != ""
        {
            if isCancel
            {
                isCancel = false
            }
            else
            {
                if textField.tag < contactValuesArr.count
                {
                    contactValuesArr[textField.tag] = textField.text!
                }
            }
            
        }
        
        print(contactValuesArr)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let pointInTable = textField.convert(textField.bounds.origin, to: self.table)
        textFieldIndexPath = self.table.indexPathForRow(at: pointInTable)
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        self.viewWillLayoutSubviews()
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        if count > 0
        {
            if self.table.contentSize.height <= self.view.frame.height*(0.50)
            {
                self.tableHeightConstraint?.constant = self.table.contentSize.height
            }
            else
            {
                self.tableHeightConstraint?.constant = self.view.frame.height*(0.50)
            }
        }
        else
        {
            self.tableHeightConstraint?.constant = 10
        }
        
        //self.tableHeightConstraint?.constant = self.table.contentSize.height
    }
    
    //MARK: UIPickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if senderTag == 1
        {
            return estimateFieldsArr.count + 1
        }
        return dropdown2.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if senderTag == 1
        {
            return row == 0 ? "--None--":estimateFieldsArr[row-1].FieldName
        }
        
        return row == 0 ? "--None--":dropdown2[row-1]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if senderTag == 1
        {
            if row == 0
            {
                cell?.lblDropdown1.text = ""
            }
            else
            {
                name = estimateFieldsArr[row-1].FieldName
                cell?.lblDropdown1.text = name
                
                contactFieldsArr.remove(at: indexOfWOField!)
                
                contactFieldsArr.insert(cell?.lblDropdown1.text ?? "", at: indexOfWOField!)
            }
            
        }
        else
        {
            if row == 0
            {
                cell1?.lblDropdown2.text = ""
            }
            else
            {
                name = dropdown2[row-1]
                cell1?.lblDropdown2.text = name
                
                
                contactConditionsArr.remove(at: indexOfDropdown2!)
                
                contactConditionsArr.insert(cell1?.lblDropdown2.text ?? "", at: indexOfDropdown2!)
            }
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

