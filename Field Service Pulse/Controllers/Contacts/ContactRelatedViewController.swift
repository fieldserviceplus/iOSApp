//
//  ContactRelatedViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ContactRelatedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    //MARK: VAriables
    
    private let CELL_CONTACT_RELATED = "cell_ContactRelated"
    private let CELL_SUB_CONTACT_RELATED = "cell_subContactRelated"
    
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var contactID:String?
    
    var titleArr:NSMutableArray = []
    
    var workOrdersArr:[ContactRelatedWorkOrderData] = []
    var tasksArr:[ContactRelatedTaskData] = []
    var estimateArr:[ContactRelatedEstimateData] = []
    var eventArr:[ContactRelatedEventData] = []
    var invoiceArr:[ContactRelatedInvoiceData] = []
    var fileArr:[ContactRelatedFileData] = []
    var noteArr:[ContactRelatedNoteData] = []
    
    //MARK: IBOutlets
    
    @IBOutlet weak var tableRelated: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        titleArr.removeAllObjects()
        if let parent = self.parent as? ContactDetailsViewController {
            
            self.contactID = User.instance.contactID
            print(self.contactID)
            webseriviceCall()
            webseriviceCallForWorkOrders()
            webseriviceCallForTasks()
            webseriviceCallForNote()
            webseriviceCallForFile()
            webseriviceCallForEstimate()
            webseriviceCallForInvoice()
            webseriviceCallForEvent()
        }
        
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        tableRelated.rowHeight = 45
        tableRelated.tableFooterView = UIView()
        
    }
    
    func webseriviceCall()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID!,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedList(urlString: API.contactRelatedListURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedList?) in
            
            if response?.Result == "True"
            {
                
                self.titleArr.add(response?.data?.WorkOrder?.title)
                self.titleArr.add(response?.data?.Estimate?.title)
                self.titleArr.add(response?.data?.Invoice?.title)
                self.titleArr.add(response?.data?.Event?.title)
                self.titleArr.add(response?.data?.Task?.title)
                self.titleArr.add(response?.data?.File?.title)
                self.titleArr.add(response?.data?.Note?.title)
                self.tableRelated.reloadData()
                print(self.titleArr)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    
    func webseriviceCallForWorkOrders()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedWorkOrder(urlString: API.contactRelatedWorkOrderURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedWorkOrder?) in
            
            if response?.Result == "True"
            {
                self.workOrdersArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForTasks()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedTask(urlString: API.contactRelatedTaskURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedTask?) in
            
            if response?.Result == "True"
            {
                self.tasksArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForEstimate()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedEstimate(urlString: API.contactRelatedEstimateURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedEstimate?) in
            
            if response?.Result == "True"
            {
                self.estimateArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForInvoice()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedInvoice(urlString: API.contactRelatedInvoiceURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedInvoice?) in
            
            if response?.Result == "True"
            {
                self.invoiceArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForEvent()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedEvent(urlString: API.contactRelatedEventURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedEvent?) in
            
            if response?.Result == "True"
            {
                self.eventArr = (response?.data)!
                self.tableRelated.reloadData()
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForFile()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedFile(urlString: API.contactRelatedFileURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedFile?) in
            
            if response?.Result == "True"
            {
                self.fileArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    func webseriviceCallForNote()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ContactID":contactID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.contactRelatedNote(urlString: API.contactRelatedNoteURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ContactRelatedNote?) in
            
            if response?.Result == "True"
            {
                self.noteArr = (response?.data)!
                self.tableRelated.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    
    @objc func btnViewAll(sender:UIButton)
    {
        let viewAllVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewAllViewController") as! ContactViewAllViewController
        
        if let parent = self.parent as? ContactDetailsViewController {
            
            viewAllVC.headerTitle = parent.lblHeaderTitle.text
            
        }
        
        viewAllVC.titleLabel = titleArr[(sender as AnyObject).tag] as? String
        viewAllVC.tag = (sender as AnyObject).tag
        
        if (sender as AnyObject).tag == 0
        {
            viewAllVC.workorderArr = workOrdersArr
        }
        else if (sender as AnyObject).tag == 1
        {
            viewAllVC.estimateArr = estimateArr
        }
        else if (sender as AnyObject).tag == 2
        {
            viewAllVC.invoiceArr = invoiceArr
        }
        else if (sender as AnyObject).tag == 3
        {
            viewAllVC.eventArr = eventArr
        }
        else if (sender as AnyObject).tag == 4
        {
            viewAllVC.taskArr = tasksArr
        }
        else if (sender as AnyObject).tag == 5
        {
            viewAllVC.fileArr = fileArr
        }
        else if (sender as AnyObject).tag == 6
        {
            viewAllVC.noteArr = noteArr
        }
        
        self.navigationController?.pushViewController(viewAllVC, animated: true)
    }
    
    @objc func btnAdd(sender:UIButton)
    {
        if sender.tag == 0
        {
            let vc = UIStoryboard(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateWOViewController") as! CreateWOViewController
            vc.contactID = User.instance.contactID
            vc.contactName = User.instance.contactName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 1
        {
            let vc = UIStoryboard(name: "Estimate", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEstimateViewController") as! CreateEstimateViewController
            vc.contactID = User.instance.contactID
            vc.contactName = User.instance.contactName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 2 {
            
            let vc = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as! CreateInvoiceViewController
            vc.contactID = User.instance.contactID
            vc.contactName = User.instance.contactName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 3 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
            vc.relatedTo = "Contact"
            vc.objectID = User.instance.contactID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 4 {
            
            let vc = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
            vc.relatedToID = User.instance.contactID
            vc.contactName = User.instance.contactName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 5{
            
            let vc = UIStoryboard(name: "File", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateFileViewController") as! CreateFileViewController
            
            vc.relatedToID = User.instance.contactID
            vc.contactName = User.instance.contactName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 6 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateNoteViewController") as! CreateNoteViewController
            vc.relatedTo = "Contact"
            vc.objectID = User.instance.contactID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = tableRelated.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = true
                    
                }
                
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }
                
                
            }, completion: { _ in
                if let parent = self.parent as? ContactDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = false
                    
                }
                
            })
            
        }
        else
        {
            // didn't move
        }
        
    }
    
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableRelated
        {
            return titleArr.count
        }
        else
        {
            
            if selectedIndex == 0
            {
                return workOrdersArr.count
            }
            else if selectedIndex == 1
            {
                return estimateArr.count
            }
            else if selectedIndex == 2
            {
                return invoiceArr.count
            }
            else if selectedIndex == 3
            {
                return eventArr.count
            }
            else if selectedIndex == 4
            {
                return tasksArr.count
            }
            else if selectedIndex == 5
            {
                return fileArr.count
            }
            else if selectedIndex == 6
            {
                return noteArr.count
            }
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableRelated
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_CONTACT_RELATED) as! ContactRelatedTableViewCell
            cell.lblTitle.text = titleArr[indexPath.row] as? String
            cell.btnViewAll.tag = indexPath.row
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(sender:)), for: .touchUpInside)
            cell.btnAdd.giveBorderToButton()
            cell.btnAdd.tag = indexPath.row
            cell.btnAdd.addTarget(self, action: #selector(btnAdd(sender:)), for: .touchUpInside)
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell.subTable.rowHeight = 70
            } else { //IPAD
                cell.subTable.rowHeight = 100
            }
            
            cell.subTable.tableFooterView = UIView()
            cell.subTable.reloadData()
            
            if (self.titleArr[indexPath.row] as! String).range(of: "(0)") != nil
            {
                cell.btnViewAll.isHidden = true
                cell.subTable.isHidden = true
                
            } else {
                cell.btnViewAll.isHidden = false
                cell.subTable.isHidden = false
            }
            
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                cell.btnAdd.isHidden = true
            }
            
            return cell
        }
        else
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: CELL_SUB_CONTACT_RELATED) as! ContactSubRelatedTableViewCell
            
            if selectedIndex == 0
            {
                cell2.lblFirst?.text = "Work Order No: " + workOrdersArr[indexPath.row].WorkOrderNo!
                cell2.lblSecond?.text = "Status: " + workOrdersArr[indexPath.row].Status!
                cell2.lblThird?.text = "Start Date: " + workOrdersArr[indexPath.row].StartDate!
                
            }
            else if selectedIndex == 1
            {
                cell2.lblFirst?.text = "Estimate Name: " + estimateArr[indexPath.row].EstimateName!
                cell2.lblSecond?.text = "Status: " + estimateArr[indexPath.row].Status!
                cell2.lblThird?.text = "Grand Total: " + estimateArr[indexPath.row].GrandTotal!
                
            }
            else if selectedIndex == 2
            {
                cell2.lblFirst?.text = "Invoice Number: " + invoiceArr[indexPath.row].InvoiceNumber!
                cell2.lblSecond?.text = "Invoice Status: " + invoiceArr[indexPath.row].InvoiceStatus!
                cell2.lblThird?.text = "DueDate: " + invoiceArr[indexPath.row].DueDate!
                
            }
            else if selectedIndex == 3
            {
                cell2.lblFirst?.text = "Assigned To: " + eventArr[indexPath.row].AssignedTo!
                cell2.lblSecond?.text = "Name: " + eventArr[indexPath.row].Name!
                cell2.lblThird?.text = "Event Start Date: " + eventArr[indexPath.row].EventStartDate!
                
            }
            else if selectedIndex == 4
            {
                cell2.lblFirst?.text = "Assigned To: " + tasksArr[indexPath.row].AssignedTo!
                cell2.lblSecond?.text = "Task Status: " + tasksArr[indexPath.row].TaskStatus!
                cell2.lblThird?.text = "Priority: " + tasksArr[indexPath.row].Priority!
                
            }
            else if selectedIndex == 5
            {
                
                cell2.lblFirst?.text = "File Name: " + fileArr[indexPath.row].FileName!
                cell2.lblSecond?.text = "Content Type: " + fileArr[indexPath.row].ContentType!
                cell2.lblThird?.text = "Subject: " + fileArr[indexPath.row].Subject!
            }
            else if selectedIndex == 6
            {
                cell2.lblFirst?.text = "Owner Name: " + noteArr[indexPath.row].OwnerName!
                cell2.lblSecond?.text = "Subject: " + noteArr[indexPath.row].Subject!
                cell2.lblThird?.text = "Created Date: " + noteArr[indexPath.row].CreatedDate!
                
            }
            
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableRelated
        {
            let cell = tableView.cellForRow(at: indexPath) as! ContactRelatedTableViewCell
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath))
            {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }
            else
            {
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            if indexPath.row == selectedIndex{
                selectedIndex = -1
                
            }else{
                selectedIndex = indexPath.row
                
            }
            
            if (self.titleArr[indexPath.row] as? String == "Work Orders (0)") || (self.titleArr[indexPath.row] as? String == "Estimates (0)") || (self.titleArr[indexPath.row] as? String == "Invoices (0)") || (self.titleArr[indexPath.row] as? String == "Events (0)") || (self.titleArr[indexPath.row] as? String == "Tasks (0)") || (self.titleArr[indexPath.row] as? String == "Notes (0)") || (self.titleArr[indexPath.row] as? String == "Files (0)")
            {
                
                let newText = "Add " + ((self.titleArr[indexPath.row]) as! String).replacingOccurrences(of: " (0)", with: "", options: NSString.CompareOptions.literal, range:nil)
                cell.btnAdd.setTitle(newText, for: .normal)
                
                cell.btnAdd.isHidden = false
                
            }
            else
            {
                cell.btnAdd.isHidden = true
            }
            
            tableView.reloadData()
        }
        else
        {
            
            if selectedIndex == 0
            {
                let woVC = UIStoryboard(name: "WorkOrder", bundle:  Bundle.main).instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
                woVC.navigationController?.isNavigationBarHidden = true
                User.instance.workorderID = workOrdersArr[indexPath.row].WorkOrderID!
                User.instance.workorderSubject = workOrdersArr[indexPath.row].Subject!
                self.navigationController?.pushViewController(woVC, animated: true)
            }
            else if selectedIndex == 1
            {
                let estimateVC = UIStoryboard(name: "Estimate", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EstimateDetailsViewController") as! EstimateDetailsViewController
                estimateVC.navigationController?.isNavigationBarHidden = true
                User.instance.estimateID = estimateArr[indexPath.row].EstimateID!
                User.instance.estimateName = estimateArr[indexPath.row].EstimateName!
                self.navigationController?.pushViewController(estimateVC, animated: true)
            }
            else if selectedIndex == 2
            {
                let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
                invoiceVC.navigationController?.isNavigationBarHidden = true
                User.instance.invoiceID = invoiceArr[indexPath.row].InvoiceID!
                User.instance.invoiceNo = invoiceArr[indexPath.row].InvoiceNumber!
                self.navigationController?.pushViewController(invoiceVC, animated: true)
            }
            else if selectedIndex == 3
            {
                let eventVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                eventVC.navigationController?.isNavigationBarHidden = true
                User.instance.eventID = eventArr[indexPath.row].EventID!
                self.navigationController?.pushViewController(eventVC, animated: true)
            }
            else if selectedIndex == 4
            {
                let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                taskVC.navigationController?.isNavigationBarHidden = true
                User.instance.taskID = tasksArr[indexPath.row].TaskID!
                self.navigationController?.pushViewController(taskVC, animated: true)
            }
            else if selectedIndex == 5
            {
                let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.fileID = fileArr[indexPath.row].FileID!
                self.navigationController?.pushViewController(fileVC, animated: true)
            }
            else if selectedIndex == 6
            {
                let noteVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "NoteDetailsViewController") as! NoteDetailsViewController
                noteVC.navigationController?.isNavigationBarHidden = true
                User.instance.noteID = noteArr[indexPath.row].NoteID!
                noteVC.relatedTo = "Contact"
                noteVC.objectID = User.instance.contactID
                self.navigationController?.pushViewController(noteVC, animated: true)
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableRelated
        {
            if indexPath.row == selectedIndex
            {
                return 286
            }else{
                return 45
            }
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 70
            } else { //IPAD
                return 100
            }
        }
    }
    
}
