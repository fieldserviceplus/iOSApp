//
//  ActionSheetContactView.swift
//  Field Service Pulse
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ActionSheetContactView: UIView {

    @IBOutlet weak var btnCall:UIButton!
    @IBOutlet weak var btnText:UIButton!
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnNewWO:UIButton!
    @IBOutlet weak var btnNewEvent:UIButton!
    @IBOutlet weak var btnNewEstimate:UIButton!
    @IBOutlet weak var btnNewInvoice:UIButton!
    @IBOutlet weak var btnNewFile:UIButton!
    @IBOutlet weak var btnNewTask:UIButton!
    @IBOutlet weak var btnCopyContact:UIButton!
    @IBOutlet weak var btnDeleteContact:UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var back_view: UIView!
    @IBOutlet weak var btnNewNote: UIButton!
    
    @IBOutlet weak var scrollview:UIScrollView!

    override func awakeFromNib() {
        
        self.btnCall.leftImage(image: #imageLiteral(resourceName: "phone-1"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnText.leftImage(image: #imageLiteral(resourceName: "message"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEmail.leftImage(image: #imageLiteral(resourceName: "email"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnEdit.leftImage(image: #imageLiteral(resourceName: "edit"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewWO.leftImage(image: #imageLiteral(resourceName: "orders"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewEstimate.leftImage(image: #imageLiteral(resourceName: "estimate"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewEvent.leftImage(image: #imageLiteral(resourceName: "calendar_black"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewInvoice.leftImage(image: #imageLiteral(resourceName: "invoice"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewFile.leftImage(image: #imageLiteral(resourceName: "file"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewTask.leftImage(image: #imageLiteral(resourceName: "task"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnNewNote.leftImage(image: #imageLiteral(resourceName: "signature"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnCopyContact.leftImage(image: #imageLiteral(resourceName: "copy"), renderMode: UIImage.RenderingMode.alwaysOriginal)
        self.btnDeleteContact.leftImage(image: #imageLiteral(resourceName: "delete"), renderMode: UIImage.RenderingMode.alwaysOriginal)
    }
}
