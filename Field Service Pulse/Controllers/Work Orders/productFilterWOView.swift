//
//  productFilterWOView.swift
//  Field Service Pulse
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class productFilterWOView: UIView {

    @IBOutlet weak var txtProductFamily: UITextField!
    @IBOutlet weak var txtProductCode: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var viewProductFilter: UIView!
    
    override func awakeFromNib() {
        
        btnApply.giveCornerRadius()
        btnClear.giveCornerRadius()
        viewProductFilter.giveBorderToView()
        
    }
    
    @IBAction func btnTransparent(_ sender: Any) {
        
        self.removeFromSuperview()
    }
    

}
