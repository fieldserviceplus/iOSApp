//
//  RecurrenceDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecurrenceDetailsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    
    //MARK: Variables
    
    private let SEGUE_WO_DETAILS = "segue_WO_Details"
    private let SEGUE_UNWIND = "unwindDetails"
    
    var EndDate:String?
    var Ends:String?
    var EndsAfterOccurrences:String?
    var EndsOnDate:String?
    var EndTime:String?
    var IntervalEvery:String?
    var IsRecurring:String?
    var RepeatEvery:String?
    var RepeatOn:String?
    var StartDate:String?
    var StartTime:String?
    var WORecurrenceID:String?
    var StartOn:String?
    
    var textfieldTag = 0
    
    var btnWeekArr:[UIButton]?
    var btnArr:[UIButton]?
    var intervalArr = ["1","2","3","4","5"]
    var repeatsArr = ["Daily","Weekly","Monthly","Yearly","Periodically"]
    
    var weekArr:[String] = []
    
    let pickerView = UIPickerView()
    var ends:String?
    
    //MARK: IBOutlets
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtRepeats: UITextField!
    @IBOutlet weak var txtInterval: UITextField!
    @IBOutlet weak var btnNever: UIButton!
    @IBOutlet weak var btnOn: UIButton!
    @IBOutlet weak var btnAfter: UIButton!
    @IBOutlet weak var txtOn: UITextField!
    @IBOutlet weak var txtAfter: UITextField!
    @IBOutlet weak var txtStartTime: AkiraTextField!
    @IBOutlet weak var txtEndTime: AkiraTextField!
    
    @IBOutlet weak var btnSun: UIButton!
    @IBOutlet weak var btnMon: UIButton!
    @IBOutlet weak var btnTue: UIButton!
    @IBOutlet weak var btnWed: UIButton!
    @IBOutlet weak var btnThurs: UIButton!
    @IBOutlet weak var btnFri: UIButton!
    @IBOutlet weak var btnSat: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var txtStartsOn: AkiraTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        btnArr = [btnNever, btnOn, btnAfter]
        btnWeekArr = [btnSun,btnMon,btnWed,btnThurs,btnTue,btnFri,btnSat] as? [UIButton]
        pickerView.delegate = self
        txtInterval.inputView = pickerView
        txtRepeats.inputView = pickerView
        txtAfter.inputView = pickerView
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        heightConstraint.constant
            = 0
        backView.translatesAutoresizingMaskIntoConstraints = false
        
        txtRepeats.text = RepeatEvery
        txtInterval.text = IntervalEvery
        txtStartTime.text = StartTime
        txtEndTime.text = EndTime
        txtStartsOn.text = StartOn
        
        if Ends == "Never"
        {
            btnNever.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
        }
        if Ends == "On"
        {
            btnOn.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            txtOn.text = EndsOnDate
        }
        if Ends == "After"
        {
            btnAfter.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            txtAfter.text = EndsAfterOccurrences
        }
        
    
        if txtRepeats.text != ""
        {
            backView.isHidden = false
            heightConstraint.constant
                = 110
            let RepeatOnArr = RepeatOn?.components(separatedBy: ",")
            
            for i in 0..<RepeatOnArr!.count
            {
                if RepeatOnArr![i] == "Sun"
                {
                    btnSun.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                if RepeatOnArr![i] == "Mon"
                {
                    btnMon.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                if RepeatOnArr![i] == "Tue"
                {
                    btnTue.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                if RepeatOnArr![i] == "Wed"
                {
                    btnWed.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                if RepeatOnArr![i] == "Thurs"
                {
                    btnThurs.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                if RepeatOnArr![i] == "Fri"
                {
                    btnFri.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                if RepeatOnArr![i] == "Sat"
                {
                    btnSat.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
            }
        }
    }
    
    func setupUI()
    {
        txtAfter.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtRepeats.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtInterval.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        txtOn.attributedPlaceholder = NSAttributedString(string: "--None--", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        txtOn.layer.borderWidth = 1.5
        txtOn.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    //UITextfieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
            
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 3
        {
            textfieldTag = textField.tag
        }
        pickerView.reloadAllComponents()
    }
    
    //Pickerview
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if textfieldTag == 1
        {
            return repeatsArr.count + 1
        }
        if textfieldTag == 2
        {
            return intervalArr.count + 1
        }
        if textfieldTag == 3
        {
            return intervalArr.count + 1
        }
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if textfieldTag == 1
        {
            return row == 0 ? "--None--":repeatsArr[row-1]
        }
        if textfieldTag == 2
        {
            return row == 0 ? "--None--":intervalArr[row-1]
        }
        if textfieldTag == 3
        {
            return row == 0 ? "--None--":intervalArr[row-1]
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 1
        {
            if row == 0
            {
                txtRepeats.text = ""
            }
            else
            {
                txtRepeats.text  = repeatsArr[row-1]
                if txtRepeats.text == "Weekly"
                {
                    backView.isHidden = false
                    
                    heightConstraint.constant
                        = 110
                }
                else{
                    backView.isHidden = true
                    
                    heightConstraint.constant
                        = 0
                }
            }
        }
        if textfieldTag == 2
        {
            if row == 0
            {
                txtInterval.text = ""
            }
            else
            {
                txtInterval.text = intervalArr[row-1]
            }
        }
        if textfieldTag == 3
        {
            if row == 0
            {
                txtAfter.text = ""
            }
            else
            {
                txtAfter.text = intervalArr[row-1]
            }
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: IBActions
    
    @IBAction func btnSave(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_UNWIND, sender: self)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEnds(_ sender: Any) {
        
        for btn in btnArr!{
            if btn == sender as! UIButton
            {
                btn.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
                ends = btn.accessibilityLabel
            }
            else
            {
                btn.setImage(#imageLiteral(resourceName: "blankRadio"), for: .normal)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func txtOn(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForOnTime), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtStartTime(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.time
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStartTime), for: UIControl.Event.valueChanged)
    }
    @IBAction func txtEndTime(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.time
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForEndTime), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtStartsOn(_ sender: Any) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForStartsOn), for: UIControl.Event.valueChanged)
    }
    
    
    @IBAction func btnWeek(_ sender: Any) {
        
        for btn in btnWeekArr!
        {
            if btn == sender as! UIButton
            {
                if (btn.currentImage?.isEqual(UIImage(named: "green_checked-box")))!
                {
                    btn.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                }
                else{
                    btn.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
                }
                
                if weekArr.contains(btn.accessibilityIdentifier!) {
                    if let index = weekArr.index(of: btn.accessibilityIdentifier!) {
                        weekArr.remove(at: index)
                    }
                }
                else{
                    weekArr.append(btn.accessibilityIdentifier!)
                }
            }
        }
    }
    
    @objc func datePickerValueChangedForOnTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        txtOn.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func datePickerValueChangedForStartTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeStyle = .short
        
        txtStartTime.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func datePickerValueChangedForEndTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeStyle = .short
        
        txtEndTime.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func datePickerValueChangedForStartsOn(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtStartsOn.text = dateFormatter.string(from: sender.date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
