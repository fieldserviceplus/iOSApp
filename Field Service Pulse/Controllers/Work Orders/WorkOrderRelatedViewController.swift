//
//  WorkOrderRelatedViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 14/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class WorkOrderRelatedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    //MARK: VAriables
    
    private let CELL_WORK_RELATED = "cell_WorkRelated"
    private let CELL_SUB_WORK_RELATED = "cell_subWorkRelated"
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var orderID:String?
    var accountName:String?
    
    var titleArr:NSMutableArray = []
    
    var eventArr:[EventData] = []
    var taskArr:[TaskData] = []
    var fileArr:[FileData] = []
    var lineItemsArr:[LineItemsData] = []
    var chemicalArr:[ChemicalData] = []
    var noteArr:[NoteData] = []
    var invoiceArr:[InvoiceData] = []
    
    
    //MARK: IBOutlets
    
    @IBOutlet weak var tableWorkOrder: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        titleArr.removeAllObjects()
        if let _ = self.parent as? WorkOrdersDetailsViewController {
            
            self.orderID = User.instance.workorderID
            print(self.orderID ?? "")
            webseriviceCall()
            webseriviceCallForEvent()
            webseriviceCallForTask()
            webseriviceCallForFile()
            webseriviceCallForNote()
            webseriviceCallForInvoice()
            webseriviceCallForChemical()
            webseriviceCallForLineItems()
        }
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        tableWorkOrder.rowHeight = 45
        tableWorkOrder.tableFooterView = UIView()
    }
    
    func webseriviceCall()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.orderRelatedList(urlString: API.orderRelatedListURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:OrderRelatedListResponse?) in
            
            if response?.Result == "True"
            {
                self.titleArr.removeAllObjects()
                self.titleArr.add(response?.data?.WOLineItem?.title ?? "")
                self.titleArr.add(response?.data?.Chemical?.title ?? "")
                self.titleArr.add(response?.data?.Invoice?.title ?? "")
                self.titleArr.add(response?.data?.Event?.title ?? "")
                self.titleArr.add(response?.data?.Task?.title ?? "")
                self.titleArr.add(response?.data?.Note?.title ?? "")
                self.titleArr.add(response?.data?.File?.title ?? "")
                self.tableWorkOrder.reloadData()

                if let parent = self.parent as? WorkOrdersDetailsViewController {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        parent.menuView?.heightBackView.constant = 640
                    } else { //IPAD
                        parent.menuView?.heightBackView.constant = 920
                    }
                    parent.topConstraintActionView.constant = 30
                }
                    if (self.titleArr[0] as? String == "Work Order Line Items (0)")
                    {
                        if let parent = self.parent as? WorkOrdersDetailsViewController {
                            
                            parent.menuView?.heightBackView.constant = (parent.menuView?.heightBackView.constant)! - 35
                            parent.menuView?.heightConstraintBtnEditLineItems.constant = 0
                            parent.menuView?.heightConstraintBtnNewLineItems.constant = 35
                            parent.topConstraintActionView.constant = parent.topConstraintActionView.constant + 35
                             parent.menuView?.btnEditLines.isHidden = true
                            parent.menuView?.btnNewLines.isHidden = false
                        }
                    }
                    else {
                        if (self.titleArr[0] as? String)?.range(of:"Work Order Line Items") != nil {
                        if let parent = self.parent as? WorkOrdersDetailsViewController {
                            parent.menuView?.heightBackView.constant = (parent.menuView?.heightBackView.constant)! - 35
                            parent.menuView?.heightConstraintBtnEditLineItems.constant = 35
                            parent.menuView?.heightConstraintBtnNewLineItems.constant = 0
                            parent.topConstraintActionView.constant = parent.topConstraintActionView.constant + 35
                            parent.menuView?.btnEditLines.isHidden = false
                            parent.menuView?.btnNewLines.isHidden = true
                            
                            }
                }
                }
                
                if (self.titleArr[1] as? String == "Chemicals (0)")
                    {
                        if let parent = self.parent as? WorkOrdersDetailsViewController {
                            print(parent.menuView?.heightBackView.constant)
                            print(parent.topConstraintActionView.constant)
                            parent.menuView?.heightBackView.constant = (parent.menuView?.heightBackView.constant)! - 35
                            parent.menuView?.heightBtnEditChemical.constant = 0
                            parent.menuView?.heightBtnNewChemical.constant = 35
                            parent.topConstraintActionView.constant = parent.topConstraintActionView.constant + 35
                            
                            parent.menuView?.btnNewChemicals.isHidden = false
                            parent.menuView?.btnEditChemical.isHidden = true
                        }
                    }
                else {
                    if (self.titleArr[1] as? String)?.range(of:"Chemicals") != nil {
                        if let parent = self.parent as? WorkOrdersDetailsViewController {
                            parent.menuView?.heightBackView.constant = (parent.menuView?.heightBackView.constant)! - 35
                            parent.topConstraintActionView.constant = parent.topConstraintActionView.constant + 35
                            parent.menuView?.heightBtnEditChemical.constant = 35
                            parent.menuView?.heightBtnNewChemical.constant = 0
                            parent.menuView?.btnNewChemicals.isHidden = true
                            parent.menuView?.btnEditChemical.isHidden = false
                            
                        }
                    }
                }
                print(self.titleArr)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForEvent()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.events(urlString: API.eventURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:EventResponse?) in
            
            if response?.Result == "True"
            {
                
                self.eventArr = (response?.data)!
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForTask()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.task(urlString: API.taskURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:TaskResponse?) in
            
            if response?.Result == "True"
            {
                self.taskArr = (response?.data)!
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForFile()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.file(urlString: API.fileURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:FileResponse?) in
            
            if response?.Result == "True"
            {
                self.fileArr = (response?.data)!
                
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForLineItems()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.lineItems(urlString: API.lineItemsURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:LineItemsResponse?) in
            
            if response?.Result == "True"
            {
                self.lineItemsArr = (response?.data)!
                
                if User.instance.productIDArr.count>0
                {
                    User.instance.productIDArr.removeAll()
                    User.instance.productNameArr.removeAll()
                    User.instance.listPriceArr.removeAll()
                    User.instance.discountArr.removeAll()
                    User.instance.listPriceEditableArr.removeAll()
                    User.instance.quantityEditableArr.removeAll()
                    User.instance.taxArr.removeAll()
                    User.instance.taxableArr.removeAll()
                    User.instance.quantityArr.removeAll()
                }
                
                for i in 0..<self.lineItemsArr.count
                {
                    User.instance.productNameArr.append(self.lineItemsArr[i].ProductName!)
                    User.instance.productIDArr.append(self.lineItemsArr[i].Product!)
                    User.instance.listPriceArr.append(self.lineItemsArr[i].ListPrice!)
                    User.instance.discountArr.append(self.lineItemsArr[i].Discount!)
                    User.instance.listPriceEditableArr.append(self.lineItemsArr[i].IsListPriceEditable!)
                    User.instance.quantityEditableArr.append(self.lineItemsArr[i].IsQuantityEditable!)
                    User.instance.taxArr.append(self.lineItemsArr[i].Tax!)
                    User.instance.taxableArr.append(self.lineItemsArr[i].Taxable!)
                    User.instance.quantityArr.append(self.lineItemsArr[i].Quantity!)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForChemical()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.chemical(urlString: API.chemicalURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:ChemicalResponse?) in
            
            if response?.Result == "True"
            {
                self.chemicalArr = (response?.data)!
                
                User.instance.chemicalArr.removeAll()
                User.instance.tested1Arr.removeAll()
                User.instance.tested2Arr.removeAll()
                User.instance.app1Arr.removeAll()
                User.instance.app2Arr.removeAll()
                User.instance.appAreaArr.removeAll()
                User.instance.appNoteArr.removeAll()
                
                for i in 0..<self.chemicalArr.count
                {
                    User.instance.chemicalArr.append(self.chemicalArr[i].ProductName!)
                    User.instance.tested1Arr.append(self.chemicalArr[i].TestConcentration ?? "")
                    User.instance.tested2Arr.append(self.chemicalArr[i].TestedUnitOfMeasure ?? "")
                    User.instance.app1Arr.append(self.chemicalArr[i].ApplicationAmount ?? "")
                    User.instance.app2Arr.append(self.chemicalArr[i].ApplicationUnitOfMeasure ?? "")
                    User.instance.appAreaArr.append(self.chemicalArr[i].ApplicationArea ?? "")
                    User.instance.appNoteArr.append(self.chemicalArr[i].AdditionalNotes ?? "")
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForNote()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.note(urlString: API.noteURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:NoteResponse?) in
            
            if response?.Result == "True"
            {
                self.noteArr = (response?.data)!
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webseriviceCallForInvoice()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":orderID,
                          "OrganizationID":User.instance.OrganizationID]
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.invoice(urlString: API.invoiceURL, parameters: parameters as? [String : String], headers: headers, vc: self) {(response:InvoiceResponse?) in
            
            if response?.Result == "True"
            {
                self.invoiceArr = (response?.data)!
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnViewAll(sender:UIButton)
    {
        
        let viewAllVC = self.storyboard?.instantiateViewController(withIdentifier: "WORelatedViewAllViewController") as! WORelatedViewAllViewController
        if let parent = self.parent as? WorkOrdersDetailsViewController {
        
            viewAllVC.headerTitle = parent.lblHeadetTitle.text
        }
        
        viewAllVC.titleLabel = titleArr[sender.tag] as? String
        viewAllVC.tag = sender.tag
        if sender.tag == 0
        {
            viewAllVC.lineItemsArr =  lineItemsArr
        }
        else if sender.tag == 1
        {
            viewAllVC.chemicalArr = chemicalArr
        }
        else if sender.tag == 2
        {
            viewAllVC.invoiceArr = invoiceArr
        }
        else if sender.tag == 3
        {
            viewAllVC.eventArr = eventArr
        }
        else if sender.tag == 4
        {
            viewAllVC.taskArr = taskArr
        }
        else if sender.tag == 5
        {
            viewAllVC.noteArr = noteArr
        }
        else if sender.tag == 6
        {
            viewAllVC.fileArr = fileArr
        }
        
        self.navigationController?.pushViewController(viewAllVC, animated: true)
    }
    
    @objc func btnAdd(sender:UIButton)
    {
        if sender.tag == 0
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchProductViewController") as! SearchProductViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
        if sender.tag == 1
        {
            let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchChemicalViewController") as! SearchChemicalViewController
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
        if sender.tag == 2 {
            
            let vc = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as! CreateInvoiceViewController
            vc.WOID = User.instance.workorderID
            vc.workOrderSubject = User.instance.workorderSubject
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 3 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
            vc.relatedTo = "WorkOrder"
            vc.objectID = User.instance.workorderID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 4 {
            
            let vc = UIStoryboard(name: "Task", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateTaskViewController") as! CreateTaskViewController
            vc.relatedToID = User.instance.workorderID
            vc.workOrderSubject = User.instance.workorderSubject
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 5 {
            
            let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateNoteViewController") as! CreateNoteViewController
            vc.relatedTo = "WorkOrder"
            vc.objectID = User.instance.workorderID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 6{
            
            let vc = UIStoryboard(name: "File", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateFileViewController") as! CreateFileViewController
            
            vc.relatedToID = User.instance.workorderID
            vc.workOrderSubject = User.instance.workorderSubject
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = tableWorkOrder.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < tableWorkOrder.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? WorkOrdersDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 0 // Here you will get the animation you want
                }
                
            }, completion: { _ in
                
                if let parent = self.parent as? WorkOrdersDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = true
                    
                }
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                
                if let parent = self.parent as? WorkOrdersDetailsViewController
                {
                    parent.view_BottomContainer.alpha = 1 // Here you will get the animation you want
                }
            }, completion: { _ in
                
                if let parent = self.parent as? WorkOrdersDetailsViewController
                {
                    parent.view_BottomContainer.isHidden = false // Here you hide it when animation done
                }
            })
        }
        else
        {
            // didn't move
        }
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableWorkOrder
        {
            return titleArr.count
        }
        else
        {
            if selectedIndex == 0
            {
                return lineItemsArr.count
            }
            else if selectedIndex == 1
            {
                return chemicalArr.count
            }
            else if selectedIndex == 2
            {
                return invoiceArr.count
            }
            else if selectedIndex == 3
            {
                return eventArr.count
            }
            else if selectedIndex == 4
            {
                return taskArr.count
            }
            else if selectedIndex == 5
            {
                return noteArr.count
            }
            else if selectedIndex == 6
            {
                return fileArr.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableWorkOrder
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_WORK_RELATED) as! WorkOrderRelatedTableViewCell
            cell.lblTitle.text = titleArr[indexPath.row] as? String
            cell.btnViewAll.tag = indexPath.row
            cell.btnAdd.giveBorderToButton()
            cell.btnAdd.tag = indexPath.row
            cell.btnAdd.addTarget(self, action: #selector(btnAdd(sender:)), for: .touchUpInside)
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(sender:)), for: .touchUpInside)
            
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell.subTable.rowHeight = 70
            } else { //IPAD
                cell.subTable.rowHeight = 100
            }
            cell.subTable.tableFooterView = UIView()
            cell.subTable.reloadData()
            
            if (self.titleArr[indexPath.row] as! String).range(of: "(0)") != nil
            {
                cell.btnViewAll.isHidden = true
                cell.subTable.isHidden = true
                
            } else {
                cell.btnViewAll.isHidden = false
                cell.subTable.isHidden = false
            }
            
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                cell.btnAdd.isHidden = true
            }
            return cell
        }
        else
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: CELL_SUB_WORK_RELATED) as! SubWorkOrderRelatedTableViewCell
            
            if selectedIndex == 0
            {
                cell2.lblNumber?.text = "Line Item No: " + (lineItemsArr[indexPath.row].LineItemNo ?? "")
                cell2.lblProduct?.text = "Product Name: " +  (lineItemsArr[indexPath.row].ProductName ?? "")
                cell2.lblQuantity?.text = "Net Total: " + (lineItemsArr[indexPath.row].NetTotal ?? "").convertToCurrencyFormat()
            }
            else if selectedIndex == 1
            {
                cell2.lblNumber?.text = "Chemical No: " + (chemicalArr[indexPath.row].ChemicalNo ?? "")
                cell2.lblProduct?.text = "Product Name: " + (chemicalArr[indexPath.row].ProductName ?? "")
                cell2.lblQuantity?.text = "Tested Concentration: " +  (chemicalArr[indexPath.row].TestConcentration ?? "")
                
            }
            else if selectedIndex == 2
            {
                cell2.lblNumber?.text = "Invoice Number: " + (invoiceArr[indexPath.row].InvoiceNumber ?? "")
                cell2.lblProduct?.text = "Invoice Status: " +  (invoiceArr[indexPath.row].InvoiceStatus ?? "")
                cell2.lblQuantity?.text = "WorkOrderName: " + (invoiceArr[indexPath.row].WorkOrderName ?? "")
            }
            else if selectedIndex == 3
            {
                cell2.lblNumber?.text = "Event Status: " + (eventArr[indexPath.row].EventStatus ?? "")
                cell2.lblProduct?.text = "Event Type Name: " +  (eventArr[indexPath.row].EventTypeName ?? "")
                cell2.lblQuantity?.text = "Subject: " + (eventArr[indexPath.row].Subject ?? "")
            }
            else if selectedIndex == 4
            {
                cell2.lblNumber?.text = "Subject: " + (taskArr[indexPath.row].Subject ?? "")
                cell2.lblProduct?.text = "Task Status: " + (taskArr[indexPath.row].TaskStatus ?? "")
                cell2.lblQuantity?.text = "Priority: " + (taskArr[indexPath.row].Priority ?? "")
            }
            else if selectedIndex == 5
            {
                cell2.lblNumber?.text = "Note ID: " + (noteArr[indexPath.row].NoteID  ?? "")
                cell2.lblProduct?.text = "Subject: " + (noteArr[indexPath.row].Subject ?? "")
                cell2.lblQuantity?.text = "Created Date: " + (noteArr[indexPath.row].CreatedDate ?? "")
            }
            else if selectedIndex == 6
            {
                cell2.lblNumber?.text = "Subject: " + (fileArr[indexPath.row].Subject ?? "")
                cell2.lblProduct?.text = "File Name: " + (fileArr[indexPath.row].FileName ?? "")
                cell2.lblQuantity?.text = "Content Type: " + (fileArr[indexPath.row].ContentType ?? "")
            }
            
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableWorkOrder
        {
            let cell = tableView.cellForRow(at: indexPath) as! WorkOrderRelatedTableViewCell
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            tableView.deselectRow(at: indexPath, animated: true)
            
            if(!selectedArray.contains(indexPath))
            {
                selectedArray.removeAll()
                selectedArray.append(indexPath)
            }
            else
            {
                selectedArray = selectedArray.filter{$0 != indexPath}
                // remove from array here if required
            }
            
            if indexPath.row == selectedIndex{
                selectedIndex = -1
                
                
            }else{
                selectedIndex = indexPath.row
                
            }
            
            if (self.titleArr[indexPath.row] as? String == "Work Order Line Items (0)") || (self.titleArr[indexPath.row] as? String == "Chemicals (0)") || (self.titleArr[indexPath.row] as? String == "Invoices (0)") || (self.titleArr[indexPath.row] as? String == "Events (0)") || (self.titleArr[indexPath.row] as? String == "Tasks (0)") || (self.titleArr[indexPath.row] as? String == "Notes (0)") || (self.titleArr[indexPath.row] as? String == "Files (0)")
            {
                if (self.titleArr[indexPath.row] as? String == "Work Order Line Items (0)")
                {
                    cell.btnAdd.setTitle("Add Line Items", for: .normal)
                }
                else{
                    let newText = "Add " + ((self.titleArr[indexPath.row]) as! String).replacingOccurrences(of: " (0)", with: "", options: NSString.CompareOptions.literal, range:nil)
                    cell.btnAdd.setTitle(newText, for: .normal)
                }
                cell.btnAdd.isHidden = false
                
            }
            else
            {
                cell.btnAdd.isHidden = true
            }
            tableView.reloadData()
        }
        else
        {
            if selectedIndex == 0
            {
                let productVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
                productVC.navigationController?.isNavigationBarHidden = true
                productVC.productID = lineItemsArr[indexPath.row].ProductID!
                productVC.prouctType = "Product"
                productVC.what = User.instance.workorderID
                productVC.relatedTo = "WorkOrder"
                self.navigationController?.pushViewController(productVC, animated: true)
            }
            else if selectedIndex == 1
            {
                let productVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
                productVC.navigationController?.isNavigationBarHidden = true
                productVC.productID = chemicalArr[indexPath.row].ProductID!
                productVC.prouctType = "Chemical"
                productVC.what = User.instance.workorderID
                productVC.relatedTo = "WorkOrder"
                self.navigationController?.pushViewController(productVC, animated: true)
            }
            else if selectedIndex == 2
            {
                let invoiceVC = UIStoryboard(name: "Invoice", bundle:  Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
                invoiceVC.navigationController?.isNavigationBarHidden = true
                User.instance.invoiceID = invoiceArr[indexPath.row].InvoiceID!
                User.instance.invoiceNo = invoiceArr[indexPath.row].InvoiceNumber!
                self.navigationController?.pushViewController(invoiceVC, animated: true)
            }
            else if selectedIndex == 3
            {
                let eventVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                eventVC.navigationController?.isNavigationBarHidden = true
                User.instance.eventID = eventArr[indexPath.row].EventID!
                self.navigationController?.pushViewController(eventVC, animated: true)
            }
            else if selectedIndex == 4
            {
                let taskVC = UIStoryboard(name: "Task", bundle:  Bundle.main).instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                taskVC.navigationController?.isNavigationBarHidden = true
                User.instance.taskID = taskArr[indexPath.row].TaskID!
                self.navigationController?.pushViewController(taskVC, animated: true)
            }
            else if selectedIndex == 5
            {
                let noteVC = UIStoryboard(name: "Main", bundle:  Bundle.main).instantiateViewController(withIdentifier: "NoteDetailsViewController") as! NoteDetailsViewController
                noteVC.navigationController?.isNavigationBarHidden = true
                User.instance.noteID = noteArr[indexPath.row].NoteID!
                noteVC.relatedTo = "WorkOrder"
                noteVC.objectID = User.instance.workorderID
                self.navigationController?.pushViewController(noteVC, animated: true)
            }
            else if selectedIndex == 6
            {
                let fileVC = UIStoryboard(name: "File", bundle:  Bundle.main).instantiateViewController(withIdentifier: "FileDetailsViewController") as! FileDetailsViewController
                fileVC.navigationController?.isNavigationBarHidden = true
                User.instance.fileID = fileArr[indexPath.row].FileID!
                self.navigationController?.pushViewController(fileVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableWorkOrder
        {
            if indexPath.row == selectedIndex
            {
                return 290
            }else{
                return 45
            }
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 70
            } else { //IPAD
                return 100
            }
        }
    }
}
