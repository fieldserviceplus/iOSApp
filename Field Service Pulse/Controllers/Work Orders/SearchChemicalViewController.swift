//
//  SearchChemicalViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 23/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol searchChemicalDelegate {
    
    func sendData(productID:String, productName:String)
}

class SearchChemicalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: Variables
    
    private let CELL_ID = "cell_searchChemical"

    var filteredArray:[GetChemicalsData] = []
    var chemicalArr:[GetChemicalsData] = []
    var isSeaching = false
    var accountID = ""
    var accountName = ""
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    var flag = "0"
    var delegate:searchChemicalDelegate!
    
    //MARK: IBOutlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        webserviceCallForGetChemicals()
    }
    
    //MARK: Function
    
    func setupUI()
    {
        self.table.tableFooterView = UIView()
        self.table.rowHeight = 50
    }
    
    func webserviceCallForGetChemicals()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]

        let headers = ["key":User.instance.key,
                       "token":User.instance.token]

        NetworkManager.sharedInstance.getChemicals(urlString: API.getChemicals, parameters: parameters , headers: headers, vc: self) { (response:GetChemicals) in

            if response.Result == "True"
            {

                self.chemicalArr = response.data!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnEditChemical(sender:UIButton)
    {
        guard let cell = sender.superview?.superview as? SearchChemicalTableViewCell
            else {
            return // or fatalError() or whatever
        }
        if flag == "1" {
            
            delegate.sendData(productID: cell.lblChemicalName.accessibilityIdentifier!, productName: cell.lblChemicalName.text!)
            flag = "0"
            self.dismiss(animated: true, completion: nil)
        } else {
            
            let editChemicalVC = self.storyboard?.instantiateViewController(withIdentifier: "EditChemicalViewController") as! EditChemicalViewController
            editChemicalVC.chemicalArr.append(cell.lblChemicalName.text ?? "")
            editChemicalVC.chemicalListArr.removeAll()
            editChemicalVC.chemicalListArr = chemicalArr
            self.navigationController?.pushViewController(editChemicalVC, animated: true)
        }
        
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSeaching
        {
            return filteredArray.count
        }
        return chemicalArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! SearchChemicalTableViewCell
        cell.textview.giveBorder()
        if isSeaching
        {
            cell.lblChemicalName.text = filteredArray[indexPath.row].ProductName
            cell.lblChemicalName.accessibilityIdentifier = filteredArray[indexPath.row].ProductID
            cell.textview.text = filteredArray[indexPath.row].Description
            cell.btnEditChemical.addTarget(self, action: #selector(btnEditChemical(sender:)), for: .touchUpInside)
            
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                //cell.btnAdd.isHidden = true
            }
        }
        else
        {
            cell.lblChemicalName?.text = chemicalArr[indexPath.row].ProductName
            cell.lblChemicalName?.accessibilityIdentifier = chemicalArr[indexPath.row].ProductID
            cell.textview?.text = chemicalArr[indexPath.row].Description
            cell.btnEditChemical.addTarget(self, action: #selector(btnEditChemical(sender:)), for: .touchUpInside)
            
            if(selectedArray.contains(indexPath))
            {
                // use selected image
                cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            }
            else
            {
                // use normal image
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
                //cell.btnAdd.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSeaching
        {
            
            let _ = filteredArray[indexPath.row].ProductName
        }
        else
        {
            
            let _ = chemicalArr[indexPath.row].ProductName
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! SearchChemicalTableViewCell
        cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(!selectedArray.contains(indexPath))
        {
            selectedArray.removeAll()
            selectedArray.append(indexPath)
        }
        else
        {
            selectedArray = selectedArray.filter{$0 != indexPath}
            // remove from array here if required
        }
        
        if indexPath.row == selectedIndex{
            selectedIndex = -1
            
        }else{
            selectedIndex = indexPath.row
            
        }
        self.table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == selectedIndex
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 140
            } else { //IPAD
                return 206
            }
        }else{
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 50
            } else { //IPAD
                return 63
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredArray = chemicalArr.filter({ (text) -> Bool in
            let tmp: NSString = text.ProductName! as NSString
            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(filteredArray.count == 0){
            isSeaching = false;
        } else {
            isSeaching = true;
        }
        self.table.reloadData()
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
