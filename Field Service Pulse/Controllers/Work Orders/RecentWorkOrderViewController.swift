//
//  RecentWorkOrderViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 09/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentWorkOrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    //MARK: Variables
    
    private let CELL_WORK_ORDERS = "cell_orders"
    private let SEGUE_ORDER_DETAILS = "segueDetails"
    private let SEGUE_LIST_ORDERS = "segue_listOrders"
    private let SEGUE_CREATE_ORDER = "segue_createWO"
    
    var recentWOArr:[RecentWOData]? = []
    var woGetViewsArr:[WOGetViewsData]? = []
    var allOrdersArr:[AllOrdersData]? = []
  
    //MARK: IBOutlets
    
    @IBOutlet weak var tableWorkOrders: UITableView!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var viewDropdown: UIView!
    @IBOutlet weak var tableDrodown: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        webserviceCallForRecentWO()
        webserviceCallForWOGetViews()
        webserviceCallForAllOrders()
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        tableWorkOrders.tableFooterView = UIView()
        
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    
    func  webserviceCallForRecentWO()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.recentWO(urlString: API.recentWOURL, parameters: parameters, headers: headers, vc: self) { (response:RecentWO) in
            
            if response.Result == "True"
            {
                self.recentWOArr = response.data
                self.tableWorkOrders.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForWOGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.woGetViews(urlString: API.woGetViewsURL, parameters: parameters, headers: headers, vc: self) { (response:WOGetViews) in
            
            if response.Result == "True"
            {
                self.woGetViewsArr = response.data
                self.tableDrodown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForAllOrders()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.allOrders(urlString: API.allOrdersURL, parameters: parameters, headers: headers, vc: self) { (response:AllOrdersResponse) in
            
            if response.Result == "True"
            {
                self.allOrdersArr = response.data
                self.tableWorkOrders.reloadData()
                User.instance.allOrdersArr = response.data
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_LIST_ORDERS
        {
            let listVC = segue.destination as! RecentOrderListViewController
            
            let wo = sender as? WOGetViewsData
            listVC.WorkOrderViewID = wo?.WorkOrderViewID ?? ""
            listVC.WorkOrderViewName = wo?.WorkOrderViewName ?? ""
        }
        else if segue.identifier == SEGUE_ORDER_DETAILS
        {
            let detailVC = segue.destination as! WorkOrdersDetailsViewController
            
            let wo = sender as? RecentWOData
            User.instance.workorderID = wo?.WorkOrderID ?? ""
            User.instance.workorderSubject = wo?.Subject ?? ""
        }
    }
    
    //MARK: UITableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableWorkOrders {
            return recentWOArr!.count
        } else {
            return woGetViewsArr!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableWorkOrders
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_WORK_ORDERS) as! RecentWorkOrderTableViewCell
            
            cell.lblWO_No.text = recentWOArr?[indexPath.row].WorkOrderNo
            cell.lblSubject.text = recentWOArr?[indexPath.row].Subject
            cell.lblAccountName.text = recentWOArr?[indexPath.row].AccountName
            cell.lblStatus.text = recentWOArr?[indexPath.row].Status
            cell.lblPriority.text = recentWOArr?[indexPath.row].Priority
            cell.lblAssignedTo.text = recentWOArr?[indexPath.row].AssignedTo
            cell.lblAccountType.text = recentWOArr?[indexPath.row].AccountType
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            cell1.textLabel?.text = woGetViewsArr?[indexPath.row].WorkOrderViewName
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(12)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(17)
            }
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableDrodown
        {
            self.performSegue(withIdentifier: SEGUE_LIST_ORDERS, sender: woGetViewsArr?[indexPath.row])
        }
        else
        {
            self.performSegue(withIdentifier: SEGUE_ORDER_DETAILS, sender: recentWOArr?[indexPath.row])
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnOpenMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    @IBAction func btnNewOrder(_ sender: Any) {
        
        self.performSegue(withIdentifier: SEGUE_CREATE_ORDER, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func segueToObjectHomeUI(segue:UIStoryboardSegue){}
    override func viewWillDisappear(_ animated: Bool) {
        
        viewDropdown.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
