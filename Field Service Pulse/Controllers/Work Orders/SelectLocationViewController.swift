//
//  SelectLocationViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SelectLocationViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    //MARK: Variables
    
    private let SEGUE_UNWIND = "segueUnWind"
    
    
    var didFindLocation:Bool?
    var locationManager = CLLocationManager()
    
    var isUnwindTo:String?
    var address:String?
    var city:String?
    var state:String?
    var country:String?
    var postalCode:String?
    var latitude:String?
    var longitude:String?
    
    
    //MARK: IBOutlets
    
     @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mapView?.isMyLocationEnabled = true
        locationManager.delegate = self
        //Location Manager code to fetch current location
        self.locationManager.startUpdatingLocation()
        didFindLocation = false
    }

    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 10.0)
        
        self.mapView?.animate(to: camera)
        
        if didFindLocation == false 
        {
            let marker = GMSMarker()
            marker.isDraggable = true
            marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
            
            
            geocode(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!) { placemark, error in
                guard let placemark = placemark, error == nil else { return }
                // you should always update your UI in the main thread
                DispatchQueue.main.async {
                    //  update UI here
                    print("address1:", placemark.thoroughfare ?? "")
                    print("address2:", placemark.subThoroughfare ?? "")
                    print("city:",     placemark.locality ?? "")
                    print("state:",    placemark.administrativeArea ?? "")
                    print("zip code:", placemark.postalCode ?? "")
                    print("country:",  placemark.country ?? "")
                    
                    self.address = (placemark.subThoroughfare ?? "") + " " + (placemark.thoroughfare ?? "")
                    self.city = placemark.locality ?? ""
                    self.state = placemark.administrativeArea ?? ""
                    self.country = placemark.country ?? ""
                    self.postalCode = placemark.postalCode ?? ""
                    self.latitude = String(Double((location?.coordinate.latitude)!))
                    self.longitude = String(Double((location?.coordinate.longitude)!))
                    
                    marker.title = placemark.thoroughfare
                    marker.snippet = placemark.locality
                    marker.map = self.mapView
                    self.locationManager.stopUpdatingLocation()
                    self.locationManager.delegate = nil
                }
            }
            didFindLocation = true
        }
       
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
        
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        
        performSegue(withIdentifier: SEGUE_UNWIND, sender: self)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
        print(marker.position.latitude)
        getAddressFromLatLon(pdblLatitude: String(marker.position.latitude), withLongitude: String(marker.position.longitude))

    }
    
    //Reverse Geocoding
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        self.latitude = String(lat)
        self.longitude = String(lon)
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    
                        if pm.subThoroughfare != nil {
                            print("address1:", pm.subThoroughfare ?? "")
                            self.address = pm.subThoroughfare ?? ""
                        }
                        if pm.thoroughfare != nil {
                            print("address2:", pm.thoroughfare ?? "")
                            self.address = self.address! + " " + (pm.thoroughfare ?? "")
                        }
                        if pm.locality != nil {
                            print("city:", pm.locality ?? "")
                            self.city = pm.locality ?? ""
                        }
                        if pm.administrativeArea != nil {
                            print("state:", pm.administrativeArea ?? "")
                            self.state = pm.administrativeArea ?? ""
                        }
                        if pm.country != nil {
                            print("country:", pm.country ?? "")
                            self.country = pm.country ?? ""
                        }
                        if pm.postalCode != nil {
                            print("zip code:", pm.postalCode ?? "")
                            self.postalCode = pm.postalCode ?? ""
                        }
                    }
        })
    }
}
