//
//  EditChemicalViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 07/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EditChemicalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate, searchChemicalDelegate {
    
    

    //MARK: Variables
    private let CELL_ID = "editChemicalID"
    var count = 1
    
    var UnitOfMeasurementArr:[GetUnitOfMeasurementData] = []
    var UnitOfMeasurementIDArr:[String] = []
    var UnitOfMeasurementNameArr:[String] = []
    var pickerView = UIPickerView()
    
    var cellD1:EditChemicalTableViewCell!
    
    var textfieldTag = 0
    var txtviewTag = 0
    
    var chemicalArr:[String] = []
    var tested1Arr:[String] = []
    var tested2Arr:[String] = []
    var app1Arr:[String] = []
    var app2Arr:[String] = []
    
    var tested2IDArr:[String] = []
    var app2IDArr:[String] = []
    
    var appAreaArr:[String] = []
    var appNoteArr:[String] = []
    
    var index = 0
    
    var selectedIndex : NSInteger! = -1
    var lastContentOffset: CGFloat = 0
    var selectedArray : [IndexPath] = [IndexPath]()
    
    var chemicalName:String?
    var chemicalListArr:[GetChemicalsData] = []
    var chemicalNameArr:[String] = []
    var chemicalIDArr:[String] = []
    var productIDArr:[String] = []
    
    var editedCell:EditChemicalTableViewCell!
    
    var finalChemicalIDArr:[String] = []
    var finalTested1Arr:[String] = []
    var finalApp1Arr:[String] = []
    var finalTested2IDArr:[String] = []
    var finalApp2IDArr:[String] = []
    var finalAppAreaArr:[String] = []
    var finalAppNoteArr:[String] = []
    
    var checkboxArr:[String] = []
    
    //MARK: IBOutlet
    
    @IBOutlet weak var btnAddChemical: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var back_view: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnQuickSave: UIButton!
    @IBOutlet weak var btnDeleteLines: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var lblDropdown: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if User.instance.flagForEditChemical == "1"
        {
            tested1Arr = [""]
            tested2Arr = [""]
            app1Arr = [""]
            app2Arr = [""]
            appAreaArr = [""]
            appNoteArr = [""]
            checkboxArr = ["0"]
        }
        else{
            count = chemicalArr.count
            
            for _ in 0..<chemicalArr.count
            {
                checkboxArr.append("0")
            }
        }
        
        setupUI()
        webserviceCallForGetUnitOfMeasurement()
    }
    
    //MARK: Function
    
    func setupUI()
    {
        back_view.dropShadow()
        btnSave.contentHorizontalAlignment = .left
        btnQuickSave.contentHorizontalAlignment = .left
        btnDeleteLines.contentHorizontalAlignment = .left
        btnCancel.contentHorizontalAlignment = .left
        btnDropdown.giveBorderToButton()
        lblDropdown.giveBorderToLabel()
        
        pickerView.delegate = self
        
        //For selecting first row of table as default
        let index = IndexPath(row: 0, section: 0)
        self.table.selectRow(at: index, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
        self.tableView(self.table, didSelectRowAt: index)
        
        for i in 0..<chemicalListArr.count
        {
            chemicalIDArr.append(chemicalListArr[i].ProductID!)
            chemicalNameArr.append(chemicalListArr[i].ProductName!)
        }
    }
    
    func sendData(productID: String, productName: String) {
        
        cellD1.txtChemical.text = productName
        if cellD1.txtChemical.text != ""
        {
            chemicalArr.remove(at: index)
            chemicalArr.insert(cellD1.txtChemical.text!, at: index)
        }
    }
    
    @objc func txtChemicalTapped(sender:UITapGestureRecognizer)
    {
        let touch = sender.location(in: table)
        if let indexPath = table.indexPathForRow(at: touch) {
            cellD1 = table.cellForRow(at: indexPath) as! EditChemicalTableViewCell
            index = indexPath.row
        }
        
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchChemicalViewController") as! SearchChemicalViewController
        searchVC.delegate = self
        searchVC.flag = "1"
        self.navigationController?.present(searchVC, animated: true, completion: nil)
        
    }
    
    @objc func btnSelectChemical(sender:UIButton)
    {
        
        //Get Last Cell and Add last row's data into Array
                let lastRowIndex = self.table.numberOfRows(inSection: 0) - 1
                let indexPath1 = IndexPath(row: lastRowIndex, section: 0)
                let cell = self.table.cellForRow(at: indexPath1) as! EditChemicalTableViewCell
        
                if textfieldTag == 1
                {
                    chemicalArr.remove(at: lastRowIndex)
                    chemicalArr.insert(cell.txtChemical.text!, at: lastRowIndex)
        
                }
                if textfieldTag == 2
                {
                    tested1Arr.remove(at: lastRowIndex)
                    tested1Arr.insert(cell.txtTested1.text ?? "", at: lastRowIndex)
        
                }
                if textfieldTag == 3
                {
                    tested2Arr.remove(at: lastRowIndex)
                    tested2Arr.insert(cell.txtTested2.text ?? "", at: lastRowIndex)
        
                }
                if textfieldTag == 4
                {
                    app1Arr.remove(at: lastRowIndex)
                    app1Arr.insert(cell.txtApplication1.text ?? "", at: lastRowIndex)
        
                }
                if textfieldTag == 5
                {
                    app2Arr.remove(at: lastRowIndex)
                    app2Arr.insert(cell.txtApplication2.text ?? "", at: lastRowIndex)
        
                }
        
                if txtviewTag == 1
                {
                    appAreaArr.remove(at: lastRowIndex)
                    appAreaArr.insert(cell.txtviewAppArea.text ?? "", at: lastRowIndex)
                }
                if txtviewTag == 2
                {
                    appNoteArr.remove(at: lastRowIndex)
                    appNoteArr.insert(cell.txtviewAppNotes.text ?? "", at: lastRowIndex)
                }
        
        editedCell = sender.superview?.superview as? EditChemicalTableViewCell
        let indexPath = table.indexPath(for: editedCell)
        
        if (editedCell.btnCheckbox.currentImage?.isEqual(#imageLiteral(resourceName: "green_checked-box")))!
        {
            editedCell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
            
            let indexOfA = chemicalNameArr.index(of:chemicalArr[(indexPath?.row)!])
            let indexB = finalChemicalIDArr.index(of: chemicalIDArr[indexOfA!])
            
            finalChemicalIDArr.remove(at: indexB!)
            finalTested1Arr.remove(at: indexB!)
            finalApp1Arr.remove(at: indexB!)
            finalTested2IDArr.remove(at: indexB!)
            finalApp2IDArr.remove(at: indexB!)
            finalAppAreaArr.remove(at: indexB!)
            finalAppNoteArr.remove(at: indexB!)
            
            checkboxArr.remove(at: (indexPath?.row)!)
            checkboxArr.insert("0", at: (indexPath?.row)!)
            
        }
        else{
            editedCell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
            
            let indexOfA = chemicalNameArr.index(of: chemicalArr[(indexPath?.row)!])
            if indexOfA != nil{
                finalChemicalIDArr.append(chemicalIDArr[indexOfA!])
            }
            else
            {
                editedCell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
                Helper.instance.showAlertNotification(message: Message.enterChemicalName, vc: self)
                return
            }
            
            let indexOfB = UnitOfMeasurementNameArr.index(of: tested2Arr[(indexPath?.row)!])
            
            if indexOfB != nil{
                finalTested2IDArr.append(UnitOfMeasurementIDArr[indexOfB!])
            }
            else{
                finalTested2IDArr.append("")
            }
            
            let indexOfC = UnitOfMeasurementNameArr.index(of: app2Arr[(indexPath?.row)!])
            
            if indexOfC != nil{
                finalApp2IDArr.append(UnitOfMeasurementIDArr[indexOfC!])
            }
            else{
                finalApp2IDArr.append("")
            }
            finalTested1Arr.append(tested1Arr[(indexPath?.row)!])
            finalApp1Arr.append(app1Arr[(indexPath?.row)!])
            finalAppAreaArr.append(appAreaArr[(indexPath?.row)!])
            finalAppNoteArr.append(appNoteArr[(indexPath?.row)!])
            
            checkboxArr.remove(at: (indexPath?.row)!)
            checkboxArr.insert("1", at: (indexPath?.row)!)
        }
        
        print("finalChemicalIDArr:\(finalChemicalIDArr)")
        print("finalTested2IDArr:\(finalTested2IDArr)")
        print("finalApp2IDArr:\(finalApp2IDArr)")
        print("finalTested1Arr:\(finalTested1Arr)")
        print("finalApp1Arr:\(finalApp1Arr)")
        print("finalAppAreaArr:\(finalAppAreaArr)")
        print("finalAppNoteArr:\(finalAppNoteArr)")
        
    }
    
    func webserviceCallForGetUnitOfMeasurement()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getUnitOfMeasurement(urlString: API.getUnitOfMeasurementURL, parameters: parameters , headers: headers, vc: self) { (response:GetUnitOfMeasurement) in
            
            if response.Result == "True"
            {
                
                self.UnitOfMeasurementArr = response.data!
                
                for i in 0..<self.UnitOfMeasurementArr.count
                {
                    self.UnitOfMeasurementIDArr.append(self.UnitOfMeasurementArr[i].UnitOfMeasurementID!)
                    self.UnitOfMeasurementNameArr.append(self.UnitOfMeasurementArr[i].UnitOfMeasurement!)
                }
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForSaveWOChemicals()
    {
        self.showHUD()
        var parameters:[String : Any] = [:]
        
        parameters = ["UserID":User.instance.UserID,
                      "OrganizationID":User.instance.OrganizationID,
                      "WorkOrderID":User.instance.workorderID,
                      "Account":User.instance.acID,
                      "Product": finalChemicalIDArr,
                      "TestConcentration": tested1Arr,
                      "TestedUnitOfMeasure": finalTested2IDArr,
                      "ApplicationAmount": app1Arr,
                      "ApplicationUnitOfMeasure": finalApp2IDArr,
                      "ApplicationArea": appAreaArr,
                      "AdditionalNotes": appNoteArr] as [String : Any]
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.saveWOChemicalsURL
        , parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }

    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! EditChemicalTableViewCell
        cell.txtviewAppArea.giveBorder()
        cell.txtviewAppNotes.giveBorder()
        cell.view_back.dropShadow()
        cell.btnCheckbox.addTarget(self, action: #selector(btnSelectChemical(sender:)), for: .touchUpInside)
        cell.txtTested2.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        cell.txtApplication2.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        cell.txtChemical.setRightImage(name: "search_small", placeholder: "--None--")
        cell.txtTested2.inputView = pickerView
        cell.txtApplication2.inputView = pickerView
        
        cell.txtTested2.delegate = self
        cell.txtApplication2.delegate = self
        cell.txtTested2.delegate = self
        cell.txtApplication2.delegate = self
        cell.txtChemical.delegate = self
        cell.txtviewAppNotes.delegate = self
        cell.txtviewAppArea.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(txtChemicalTapped(sender:)))
        tap.numberOfTapsRequired = 1
        tap.delegate = self as? UIGestureRecognizerDelegate
        cell.txtChemical.isUserInteractionEnabled = true
        cell.txtChemical.addGestureRecognizer(tap)
        
        cell.txtChemical.text = chemicalArr[indexPath.row]
        cell.txtTested1.text = tested1Arr[indexPath.row]
        cell.txtTested2.text = tested2Arr[indexPath.row]
        cell.txtApplication1.text = app1Arr[indexPath.row]
        cell.txtApplication2.text = app2Arr[indexPath.row]
        cell.txtviewAppNotes.text = appNoteArr[indexPath.row]
        cell.txtviewAppArea.text = appAreaArr[indexPath.row]
        
        if checkboxArr[indexPath.row] == "0"
        {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_box"), for: .normal)
        }
        else{
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "green_checked-box"), for: .normal)
        }
        
        if(selectedArray.contains(indexPath))
        {
            // use selected image
            cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
            cell.txtChemical.isUserInteractionEnabled = true
        }
        else
        {
            // use normal image
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow")
            cell.txtChemical.isUserInteractionEnabled = false
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! EditChemicalTableViewCell
        cell.imgArrow.image = #imageLiteral(resourceName: "down-arrow")
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(!selectedArray.contains(indexPath))
        {
            selectedArray.removeAll()
            selectedArray.append(indexPath)
        }
        else
        {
            selectedArray = selectedArray.filter{$0 != indexPath}
            // remove from array here if required
        }
        
        if indexPath.row == selectedIndex{
            selectedIndex = -1
            
        }else{
            selectedIndex = indexPath.row
            
        }
        
        self.table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == selectedIndex
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 320
            } else { //IPAD
                return 490
            }
            
        }else{
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 55
            } else { //IPAD
                return 83
            }
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        return UnitOfMeasurementArr.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return row == 0 ? "--None--":UnitOfMeasurementArr[row-1].UnitOfMeasurement
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 1
        {
            
        }
        if textfieldTag == 2
        {
            
        }
        if textfieldTag == 3
        {
            if row == 0
            {
                cellD1.txtTested2.text = ""
            }
            else{
                cellD1.txtTested2.text = UnitOfMeasurementArr[row-1].UnitOfMeasurement
            }
            
        }
        if textfieldTag == 4
        {
            
        }
        if textfieldTag == 5
        {
            if row == 0
            {
                cellD1.txtApplication2.text = ""
            }
            else{
                cellD1.txtApplication2.text = UnitOfMeasurementArr[row-1].UnitOfMeasurement
            }
        }
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: UITextfield Delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        cellD1 = textField.superview?.superview as! EditChemicalTableViewCell
        
        if textfieldTag == 1
        {
            if cellD1.txtChemical.text != ""
            {
                chemicalArr.remove(at: index)
                chemicalArr.insert(cellD1.txtChemical.text!, at: index)
            }
        }
        if textfieldTag == 2
        {
            if cellD1.txtTested1.text != ""
            {
                tested1Arr.remove(at: index)
                tested1Arr.insert(cellD1.txtTested1.text ?? "", at: index)
            }
            
        }
        if textfieldTag == 3
        {
            if cellD1.txtTested2.text != ""
            {
                tested2Arr.remove(at: index)
                tested2Arr.insert(cellD1.txtTested2.text ?? "", at: index)
            }
            
        }
        if textfieldTag == 4
        {
            if cellD1.txtApplication1.text != ""
            {
                app1Arr.remove(at: index)
                app1Arr.insert(cellD1.txtApplication1.text ?? "", at: index)
            }
        }
        if textfieldTag == 5
        {
            if cellD1.txtApplication2.text != ""
            {
                app2Arr.remove(at: index)
                app2Arr.insert(cellD1.txtApplication2.text ?? "", at: index)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textfieldTag = textField.tag
        cellD1 = textField.superview?.superview as! EditChemicalTableViewCell
        let indexPath = table.indexPath(for: cellD1)
        index = (indexPath?.row)!
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textfieldTag ==  3 || textfieldTag ==  5
        {
            return false
        }
        return true
    }
    
    //MARK: UITextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        txtviewTag = textView.tag
        cellD1 = textView.superview?.superview as! EditChemicalTableViewCell
        let indexPath = table.indexPath(for: cellD1)
        index = (indexPath?.row)!
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        cellD1 = textView.superview?.superview as! EditChemicalTableViewCell
        
        if txtviewTag == 1
        {
            if cellD1.txtviewAppArea.text != ""
            {
                if appAreaArr.count>index{
                    appAreaArr.remove(at: index)
                    appAreaArr.insert(cellD1.txtviewAppArea.text ?? "", at: index)
                }
            }
        }
        if txtviewTag == 2
        {
            if cellD1.txtviewAppNotes.text != ""
            {
                if appNoteArr.count>index{
                    appNoteArr.remove(at: index)
                    appNoteArr.insert(cellD1.txtviewAppNotes.text ?? "", at: index)
                }
            }
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddChemical(_ sender: Any) {
        
        //Get Last Cell and Add last row's data into Array
        let lastRowIndex = self.table.numberOfRows(inSection: 0) - 1
        let indexPath = IndexPath(row: lastRowIndex, section: 0)
        let cell = self.table.cellForRow(at: indexPath) as! EditChemicalTableViewCell
        
        if textfieldTag == 1
        {
            chemicalArr.remove(at: lastRowIndex)
            chemicalArr.insert(cell.txtChemical.text!, at: lastRowIndex)
            
        }
        if textfieldTag == 2
        {
            tested1Arr.remove(at: lastRowIndex)
            tested1Arr.insert(cell.txtTested1.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 3
        {
            tested2Arr.remove(at: lastRowIndex)
            tested2Arr.insert(cell.txtTested2.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 4
        {
            app1Arr.remove(at: lastRowIndex)
            app1Arr.insert(cell.txtApplication1.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 5
        {
            app2Arr.remove(at: lastRowIndex)
            app2Arr.insert(cell.txtApplication2.text ?? "", at: lastRowIndex)
            
        }
        
        if txtviewTag == 1
        {
            appAreaArr.remove(at: lastRowIndex)
            appAreaArr.insert(cell.txtviewAppArea.text ?? "", at: lastRowIndex)
        }
        if txtviewTag == 2
        {
            appNoteArr.remove(at: lastRowIndex)
            appNoteArr.insert(cell.txtviewAppNotes.text ?? "", at: lastRowIndex)
        }
        
        print(chemicalArr)
        print(tested1Arr)
        print(tested2Arr)
        print(app1Arr)
        print(app2Arr)
        
        print(appAreaArr)
        print(appNoteArr)
        for i in 0..<chemicalArr.count
        {
            if chemicalArr[i] == ""
            {
                return
            }
        }
        
        count += 1
        
        chemicalArr.append("")
        tested1Arr.append("")
        tested2Arr.append("")
        app1Arr.append("")
        app2Arr.append("")
        
        appAreaArr.append("")
        appNoteArr.append("")
        
        checkboxArr.append("0")
        
        table.reloadData()
        //For selecting first row of table as default
        let index = IndexPath(row: lastRowIndex+1, section: 0)
        self.table.selectRow(at: index, animated: true, scrollPosition: UITableView.ScrollPosition.middle)
        self.tableView(self.table, didSelectRowAt: index)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        //Get Last Cell and Add last row's data into Array
        let lastRowIndex = self.table.numberOfRows(inSection: 0) - 1
        let indexPath1 = IndexPath(row: lastRowIndex, section: 0)
        let cell = self.table.cellForRow(at: indexPath1) as! EditChemicalTableViewCell
        
        if textfieldTag == 1
        {
            chemicalArr.remove(at: lastRowIndex)
            chemicalArr.insert(cell.txtChemical.text!, at: lastRowIndex)
        }
        if textfieldTag == 2
        {
            tested1Arr.remove(at: lastRowIndex)
            tested1Arr.insert(cell.txtTested1.text ?? "", at: lastRowIndex)
        }
        if textfieldTag == 3
        {
            tested2Arr.remove(at: lastRowIndex)
            tested2Arr.insert(cell.txtTested2.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 4
        {
            app1Arr.remove(at: lastRowIndex)
            app1Arr.insert(cell.txtApplication1.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 5
        {
            app2Arr.remove(at: lastRowIndex)
            app2Arr.insert(cell.txtApplication2.text ?? "", at: lastRowIndex)
            
        }
        
        if txtviewTag == 1
        {
            appAreaArr.remove(at: lastRowIndex)
            appAreaArr.insert(cell.txtviewAppArea.text ?? "", at: lastRowIndex)
        }
        if txtviewTag == 2
        {
            appNoteArr.remove(at: lastRowIndex)
            appNoteArr.insert(cell.txtviewAppNotes.text ?? "", at: lastRowIndex)
        }
        
        
        for i in 0..<chemicalArr.count {
            
            let indexOfA = chemicalNameArr.index(of: chemicalArr[i])
            
            finalChemicalIDArr.append(chemicalIDArr[indexOfA!])
            
            let indexOfB = UnitOfMeasurementNameArr.index(of: tested2Arr[i])
            if indexOfB != nil {
                finalTested2IDArr.append(UnitOfMeasurementIDArr[indexOfB!])
            }
            
            let indexOfC = UnitOfMeasurementNameArr.index(of: app2Arr[i])
            if indexOfC != nil {
                finalApp2IDArr.append(UnitOfMeasurementIDArr[indexOfC!])
            }
        }
        
        for i in 0..<chemicalArr.count
        {
            if chemicalArr[i] == ""
            {
                chemicalArr.remove(at: i)
                tested1Arr.remove(at: i)
                tested2Arr.remove(at: i)
                app1Arr.remove(at: i)
                app2Arr.remove(at: i)
                appAreaArr.remove(at: i)
                appNoteArr.remove(at: i)
                checkboxArr.remove(at: i)
            }
        }
        back_view.isHidden = true
        
        count = chemicalArr.count
        self.table.reloadData()
        
        if finalChemicalIDArr.count == 0
        {
            Helper.instance.showAlertNotification(message: Message.selectProduct, vc: self)
            return
        }
        webserviceCallForSaveWOChemicals()
        
        self.performSegue(withIdentifier: "segueWO_Details", sender: self)
    }
    
    @IBAction func btnQuickSave(_ sender: Any) {
        
        
        //Get Last Cell and Add last row's data into Array
        let lastRowIndex = self.table.numberOfRows(inSection: 0) - 1
        let indexPath1 = IndexPath(row: lastRowIndex, section: 0)
        let cell = self.table.cellForRow(at: indexPath1) as! EditChemicalTableViewCell
        
        if textfieldTag == 1
        {
            chemicalArr.remove(at: lastRowIndex)
            chemicalArr.insert(cell.txtChemical.text!, at: lastRowIndex)
            
        }
        if textfieldTag == 2
        {
            tested1Arr.remove(at: lastRowIndex)
            tested1Arr.insert(cell.txtTested1.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 3
        {
            tested2Arr.remove(at: lastRowIndex)
            tested2Arr.insert(cell.txtTested2.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 4
        {
            app1Arr.remove(at: lastRowIndex)
            app1Arr.insert(cell.txtApplication1.text ?? "", at: lastRowIndex)
            
        }
        if textfieldTag == 5
        {
            app2Arr.remove(at: lastRowIndex)
            app2Arr.insert(cell.txtApplication2.text ?? "", at: lastRowIndex)
            
        }
        
        if txtviewTag == 1
        {
            appAreaArr.remove(at: lastRowIndex)
            appAreaArr.insert(cell.txtviewAppArea.text ?? "", at: lastRowIndex)
        }
        if txtviewTag == 2
        {
            appNoteArr.remove(at: lastRowIndex)
            appNoteArr.insert(cell.txtviewAppNotes.text ?? "", at: lastRowIndex)
        }
        
        
        for i in 0..<chemicalArr.count {
            
            let indexOfA = chemicalNameArr.index(of: chemicalArr[i])
            
            finalChemicalIDArr.append(chemicalIDArr[indexOfA!])
            
            let indexOfB = UnitOfMeasurementNameArr.index(of: tested2Arr[i])
            if indexOfB != nil {
                finalTested2IDArr.append(UnitOfMeasurementIDArr[indexOfB!])
            }
            
            let indexOfC = UnitOfMeasurementNameArr.index(of: app2Arr[i])
            if indexOfC != nil {
                finalApp2IDArr.append(UnitOfMeasurementIDArr[indexOfC!])
            }
            
        }
        
        
        for i in 0..<chemicalArr.count
        {
            if chemicalArr[i] == ""
            {
                chemicalArr.remove(at: i)
                tested1Arr.remove(at: i)
                tested2Arr.remove(at: i)
                app1Arr.remove(at: i)
                app2Arr.remove(at: i)
                appAreaArr.remove(at: i)
                appNoteArr.remove(at: i)
                checkboxArr.remove(at: i)
                
            }
        }

        back_view.isHidden = true
        
        count = chemicalArr.count
        self.table.reloadData()
        
        if finalChemicalIDArr.count == 0
        {
            Helper.instance.showAlertNotification(message: Message.selectProduct, vc: self)
            return
        }
        webserviceCallForSaveWOChemicals()
    }
    
    @IBAction func btnDeleteLines(_ sender: Any) {
        back_view.isHidden = true
        
        if finalChemicalIDArr.count == 0
        {
            Helper.instance.showAlertNotification(message: Message.selectProduct, vc: self)
            return
        }
        
        for i in 0..<finalChemicalIDArr.count{
            
            let index = chemicalIDArr.index(of: finalChemicalIDArr[i])
            let name = chemicalNameArr[index!]
            let index1 = chemicalArr.index(of: name)
            
            chemicalArr.remove(at: index1!)
            tested1Arr.remove(at: index1!)
            tested2Arr.remove(at: index1!)
            app1Arr.remove(at: index1!)
            app2Arr.remove(at: index1!)
            appNoteArr.remove(at: index1!)
            appAreaArr.remove(at: index1!)
            checkboxArr.remove(at: index1!)
        }
        
        count = chemicalArr.count
        table.reloadData()
        
        
        finalChemicalIDArr.removeAll()
        finalTested1Arr.removeAll()
        finalApp1Arr.removeAll()
        finalTested2IDArr.removeAll()
        finalApp2IDArr.removeAll()
        finalAppAreaArr.removeAll()
        finalAppNoteArr.removeAll()
        
        for i in 0..<tested2Arr.count {
            let indexOfB = UnitOfMeasurementNameArr.index(of: tested2Arr[i])
            
            finalTested2IDArr.append(UnitOfMeasurementIDArr[indexOfB!])
        }
        
        for i in 0..<app2Arr.count {
            
            let indexOfC = UnitOfMeasurementNameArr.index(of: app2Arr[i])
            
            finalApp2IDArr.append(UnitOfMeasurementIDArr[indexOfC!])
        }
        
        for i in 0..<chemicalArr.count {
            
            let index = chemicalNameArr.index(of: chemicalArr[i])
            finalChemicalIDArr.append(chemicalIDArr[index!])
        }
        
        webserviceCallForSaveWOChemicals()
        
        finalChemicalIDArr.removeAll()
        finalTested2IDArr.removeAll()
        finalApp2IDArr.removeAll()
        
        if chemicalArr.count == 0 {
            self.performSegue(withIdentifier: "segueWO_Details", sender: self)
        }
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        back_view.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        
        if back_view.isHidden
        {
            back_view.isHidden = false
            
        }
        else{
            back_view.isHidden = true
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
