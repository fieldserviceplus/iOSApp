//
//  WorkOrdersDetailsViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 09/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GooglePlaces
import MessageUI
import Fusuma

class WorkOrdersDetailsViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, searchDelegate, MFMessageComposeViewControllerDelegate {

    //MARK: Variables
    
    private let SEGUE_RECURRING = "segue_recurring"
    private let SEGUE_LAYOUT = "segueDetailsLayout"
    
    var woTypesArr:[WorkOrderTypesData] = []
    var priorityTypeArr:[PriorityTypesData] = []
    
    var statusArr:[StatusTypesData] = []
    var categoryArr:[CategoryTypesData] = []
    var contactID:String?
    var lastContentOffset: CGFloat = 0
    var pickerView = UIPickerView()
    
    var id:String?
    var typeID:String?
    var statusID:String?
    var priorityID:String?
    var categoryID:String?
    var acID:String?
    var accountName:String?
    
    var EndDate:String?
    var Ends:String?
    var EndsAfterOccurrences:String?
    var EndsOnDate:String?
    var EndTime:String?
    var IntervalEvery:String?
    var IsRecurring:String?
    var RepeatEvery:String?
    var RepeatOn:String?
    var StartDate:String?
    var StartTime:String?
    var WORecurrenceID:String?
    var parentWO_ID:String?
    var StartOn:String?
    
    var textfieldTag = 0
    var latitude:String?
    var longitude:String?
    var flag = 0
    var menuView:ActionSheetWO?
    
    var chemicalListArr:[GetChemicalsData] = []
    var responseWO_Details:OrderDetailsResponse?
    
    var strMobileNo:String?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    
    //UITextfield
    @IBOutlet weak var txtWorkOrder: AkiraTextField!
    @IBOutlet weak var txtWorkOwner: UITextField!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtPopupReminder: AkiraTextField!
    @IBOutlet weak var txtPrimaryContact: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: AkiraTextField!
    @IBOutlet weak var txtState: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    @IBOutlet weak var txtCountry: AkiraTextField!
    @IBOutlet weak var txtOrderType: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPriority: UITextField!
    @IBOutlet weak var txtStartDateTime: AkiraTextField!
    @IBOutlet weak var txtEndDateTime: AkiraTextField!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtSubTotal: AkiraTextField!
    @IBOutlet weak var txtDiscount: AkiraTextField!
    @IBOutlet weak var txtTax: AkiraTextField!
    @IBOutlet weak var txtTotalPrice: AkiraTextField!
    @IBOutlet weak var txtGrandTotal: AkiraTextField!
    @IBOutlet weak var txtSignature: AkiraTextField!
    @IBOutlet weak var txtLineCountItems: AkiraTextField!
    @IBOutlet weak var txtParentWorkOrder: UITextField!
    @IBOutlet weak var txtCreatedDate: AkiraTextField!
    @IBOutlet weak var txtCreatedBy: AkiraTextField!
    @IBOutlet weak var txtLastModifiedDate: AkiraTextField!
    @IBOutlet weak var txtLastModifiedBy: AkiraTextField!
    
    
    //UIButton
    @IBOutlet weak var btnRelated: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnRecurring: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    
    //UILabel
    @IBOutlet weak var lblHeadetTitle: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSystemInfo: UILabel!
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var view_BottomContainer: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var containerviewLayout: UIView!
    @IBOutlet weak var actionsheetWOView: UIView!
    @IBOutlet weak var topConstraintActionView: NSLayoutConstraint!
    @IBOutlet weak var viewBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        pickerView.delegate = self
        
        txtOrderType.inputView = pickerView
        txtStatus.inputView = pickerView
        txtPriority.inputView = pickerView
        txtCategory.inputView = pickerView
        setupUI()
        webserviceCall()
        
        webserviceCallForWOTypes()
        webserviceCallForCategoryTypes()
        webserviceCallForPriorityTypes()
        webserviceCallForStatusTypes()
        webserviceCallForGetChemicals()
        webserviceCallForCustomFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if flag == 1
        {
            let vc  = self.children[0] as! WorkOrderRelatedViewController
            vc.viewWillAppear(true)
            flag = 0
        }
    }
    
    //MARK: Functions
    
    func setupUI()
    {
        
        if let actionsheetView = Bundle.main.loadNibNamed("ActionSheetWOView", owner: self, options: nil)?.first as? ActionSheetWO
        {
            
            menuView = actionsheetView
            actionsheetView.btnSignature.addTarget(self, action: #selector(btnSignature), for: .touchUpInside)
            actionsheetView.btnNewFile.addTarget(self, action: #selector(btnNewFile), for: .touchUpInside)
            actionsheetView.btnClose.addTarget(self, action: #selector(btnCloseMenu), for: .touchUpInside)
            actionsheetView.btnNewLines.addTarget(self, action: #selector(btnNewLineItems), for: .touchUpInside)
            actionsheetView.btnCall.addTarget(self, action: #selector(btnCall1), for: .touchUpInside)
            actionsheetView.btnText.addTarget(self, action: #selector(btnMessage1), for: .touchUpInside)
            actionsheetView.btnEdit.addTarget(self, action: #selector(btnEdit1), for: .touchUpInside)
            actionsheetView.btnEditLines.addTarget(self, action: #selector(btnEditLineItems), for: .touchUpInside)
            actionsheetView.btnNewChemicals.addTarget(self, action: #selector(btnNewChemical), for: .touchUpInside)
            actionsheetView.btnEditChemical.addTarget(self, action: #selector(btnEditChemical), for: .touchUpInside)
            actionsheetView.btnNewInvoice.addTarget(self, action: #selector(btnNewInvoice), for: .touchUpInside)
            actionsheetView.btnNewFile.addTarget(self, action: #selector(btnNewFile), for: .touchUpInside)
            actionsheetView.btnNewTask.addTarget(self, action: #selector(btnNewTask), for: .touchUpInside)
            actionsheetView.btnEmail.addTarget(self, action: #selector(btnEmail), for: .touchUpInside)
            actionsheetView.btnNewNote.addTarget(self, action: #selector(btnNewNote), for: .touchUpInside)
            actionsheetView.btnCloneOrder.addTarget(self, action: #selector(btnCloneOrder), for: .touchUpInside)
            actionsheetView.btnDeleteOrder.addTarget(self, action: #selector(btnDeleteOrder), for: .touchUpInside)
            actionsheetView.btnNewEvent.addTarget(self, action: #selector(btnNewEvent), for: .touchUpInside)
            actionsheetView.btnConvertInvoice.addTarget(self, action: #selector(btnConvertInvoice), for: .touchUpInside)
            actionsheetView.btnGenerateDoc.addTarget(self, action: #selector(btnGenerateDoc), for: .touchUpInside)
            actionsheetView.scrollview.giveBorderToView()
            actionsheetView.frame = self.actionsheetWOView.bounds
            self.actionsheetWOView.addSubview(actionsheetView)
        }
        txtviewDescription.giveBorder()
        
        btnDetails.giveCornerRadius()
        btnRelated.giveCornerRadius()
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnRelated.giveBorderToButton()
        btnDetails.giveBorderToButton()
        btnLocation.giveBorderToButton()
        
        txtOrderType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtPriority.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtAccount.setRightImage(name: "search_small", placeholder: "--None--")
        txtCategory.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtWorkOwner.setRightImage(name: "search_small", placeholder: "--None--")
        txtPrimaryContact.setRightImage(name: "search_small", placeholder: "--None--")
        txtParentWorkOrder.setRightImage(name: "search_small", placeholder: "--None--")
        
        txtStatus.layer.borderColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    func loadNIB()
    {
        if let customView = Bundle.main.loadNibNamed("Browse", owner: self, options: nil)?.first as? Browse
        {
            customView.tag = 101
            customView.btnAttachFile.addTarget(self, action: #selector(btnAttachFile), for: .touchUpInside)
            customView.btnGallery.addTarget(self, action: #selector(btnGallery), for: .touchUpInside)
            customView.btnTakePhotos.addTarget(self, action: #selector(btnOpenCamera
                ), for: .touchUpInside)
            customView.frame = CGRect(x: 0, y: 64, width: User.instance.screenWidth, height: User.instance.screenHeight-64)
            self.view.addSubview(customView)
        }
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        
        if textfieldTag == 2 {
            
            txtWorkOwner.text = searchVC.fullName
            id = searchVC.userID
        } else if textfieldTag == 3  {
            
            txtAccount.text = searchVC.accountName
            acID = searchVC.accountID
            User.instance.acID = acID!
            webserviceCallForAccountDetails()
            
        } else if textfieldTag == 18 {
            txtPrimaryContact.text = searchVC.contactName
            contactID = searchVC.contactID
            
        } else if textfieldTag == 19 {
            txtParentWorkOrder.text = searchVC.parentWO_Name
            parentWO_ID = searchVC.parentWO_ID
        }
    }
    
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @objc func btnSignature()
    {
        actionsheetWOView.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignatureViewController") as? SignatureViewController
        vc?.flag = "0"
        self.present(vc!, animated: true, completion: nil)
    }
    @objc func btnEmail()
    {
        let emailVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailViewController") as! EmailViewController
        emailVC.strRelatedTo = "WorkOrder"
        emailVC.strWhat = User.instance.workorderID
        self.navigationController?.pushViewController(emailVC, animated: true)
    }
    @objc func btnNewLineItems()
    {
        actionsheetWOView.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        
        let vc = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProductViewController") as? SearchProductViewController
        vc?.selectedProducts = User.instance.productNameArr
        vc?.selectedQuantity = User.instance.quantityArr
        vc?.selectedListPriceArr = User.instance.listPriceArr
        vc?.selectedTaxableArr = User.instance.taxableArr
        vc?.selectedTaxArr = User.instance.taxArr
        vc?.selectedQuantityEditableArr = User.instance.quantityEditableArr
        vc?.selectedListPriceEditableArr = User.instance.listPriceEditableArr
        vc?.selectedProductIDArr = User.instance.productIDArr
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnEditLineItems()
    {
        actionsheetWOView.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        
        let vc = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProductViewController") as? SearchProductViewController
       
        vc?.selectedProducts = User.instance.productNameArr
        vc?.selectedQuantity = User.instance.quantityArr
        vc?.selectedListPriceArr = User.instance.listPriceArr
        vc?.selectedTaxableArr = User.instance.taxableArr
        vc?.selectedTaxArr = User.instance.taxArr
        vc?.selectedQuantityEditableArr = User.instance.quantityEditableArr
        vc?.selectedListPriceEditableArr = User.instance.listPriceEditableArr
        vc?.selectedProductIDArr = User.instance.productIDArr
        User.instance.flagSkipProduct = "1"

        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @objc func btnEditChemical()
    {
        actionsheetWOView.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        
        User.instance.flagForEditChemical = "0"
        
        let vc = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditChemicalViewController") as? EditChemicalViewController
        vc?.chemicalArr.removeAll()
        vc?.chemicalArr = User.instance.chemicalArr
        vc?.app1Arr = User.instance.app1Arr
        vc?.app2Arr = User.instance.app2Arr
        vc?.tested1Arr = User.instance.tested1Arr
        vc?.tested2Arr = User.instance.tested2Arr
        vc?.appAreaArr = User.instance.appAreaArr
        vc?.appNoteArr = User.instance.appNoteArr
        vc?.chemicalListArr.removeAll()
        vc?.chemicalListArr = chemicalListArr
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @objc func btnNewChemical()
    {
        actionsheetWOView.isHidden = true
        view_BottomContainer.isHidden = false
        view_back.isHidden = true
        
        let vc = UIStoryboard.init(name: "WorkOrder", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchChemicalViewController") as? SearchChemicalViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnNewFile()
    {
        loadNIB()
    }
    
    @objc func btnCloseMenu()
    {
        self.actionsheetWOView.isHidden = true
        self.view_BottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    
    @objc func btnEdit1()
    {
        self.btnCancel.isHidden = false
        self.btnSave.isHidden = false
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        self.actionsheetWOView.isHidden = true
        self.view_BottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    
    @objc func btnCall1()
    {
        if let number = strMobileNo {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    
    @objc func btnMessage1()
    {
        if let number = strMobileNo {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
        
    }
    @objc func btnGenerateDoc()
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenerateDocumentViewController") as? GenerateDocumentViewController
        vc?.object = "WorkOrder"
        vc?.objectID = User.instance.workorderID
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @objc func btnConvertInvoice() {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "WorkOrderID":User.instance.workorderID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.convertToInvoice(urlString: API.convertToInvoiceURL, parameters: parameters , headers: headers, vc: self) { (response:ConvertToInvoice) in
            
            if response.Result == "True"
            {
                let vc = UIStoryboard(name: "Invoice", bundle: Bundle.main).instantiateViewController(withIdentifier: "InvoiceDetailsViewController") as! InvoiceDetailsViewController
                User.instance.invoiceID = String(response.InvoiceID!)
                self.navigationController?.pushViewController(vc, animated: true)
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    @objc func btnNewEvent()
    {
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "WorkOrder"
        createEventVC.objectID = User.instance.workorderID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    @objc func btnNewNote()
    {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateNoteViewController") as! CreateNoteViewController
        vc.relatedTo = "WorkOrder"
        vc.objectID = User.instance.workorderID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnCloneOrder()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateWOViewController") as! CreateWOViewController
        vc.flagResponse = "1"
        vc.response = responseWO_Details
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnDeleteOrder()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "RelatedTo":"WorkOrder",
                          "What":User.instance.workorderID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteObjectURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                
                self.performSegue(withIdentifier: "id", sender: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnNewInvoice()
    {
        let vc = UIStoryboard(name: "Invoice", bundle: nil).instantiateViewController(withIdentifier: "CreateInvoiceViewController") as? CreateInvoiceViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnNewTask()
    {
        let vc = UIStoryboard(name: "Task", bundle: nil).instantiateViewController(withIdentifier: "CreateTaskViewController") as? CreateTaskViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnAttachFile()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        self.present(documentPicker, animated: false) {
            if #available(iOS 11.0, *) {
                documentPicker.allowsMultipleSelection = true
            }
        }
    }
    
    @objc func btnGallery()
    {
        self.view.viewWithTag(101)?.removeFromSuperview()
        //imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //self.present(imagePicker, animated: true, completion: nil)
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        //fusuma.hasVideo = true //To allow for video capturing with .library and .camera available by default
        fusuma.cropHeightRatio = 0.6 // Height-to-width ratio. The default value is 1, which means a squared-size photo.
        fusuma.allowMultipleSelection = true // You can select multiple photos from the camera roll. The default value is false.
        self.present(fusuma, animated: true, completion: nil)
    }
    
    @objc func btnOpenCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            present(imagePicker, animated: true, completion: nil)
            
        }
        
        self.view.viewWithTag(101)?.removeFromSuperview()
    }
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":acID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtAddress.text = response.data?.BillingAddress
                self.txtCity.text = response.data?.BillingCity
                self.txtState.text = response.data?.BillingState
                self.txtCountry.text = response.data?.BillingCountry
                self.txtPostalCode.text = response.data?.BillingPostalCode
                self.latitude = response.data?.BillingLatitude
                self.longitude = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCall()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderID":User.instance.workorderID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.orderDetails(urlString: API.orderDetailsURL, parameters: parameters, headers: headers, vc: self) { (response:OrderDetailsResponse) in
            
            if response.Result == "True"
            {
                self.responseWO_Details = response
                self.txtWorkOwner.text  = response.data?.AssignedToName
                self.txtPrimaryContact.text = response.data?.PrimaryContactName
                self.txtPopupReminder.text = response.data?.PopUpReminder
                self.txtSubject.text = response.data?.Subject
                self.txtWorkOrder.text = response.data?.WorkOrderNo
                self.txtviewDescription.text = response.data?.Description
                self.txtOrderType.text =  response.data?.WorkOrderTypeName
                self.txtPriority.text = response.data?.Priority
                self.txtAddress.text = (response.data?.Address) ?? ""
                self.txtCity.text = (response.data?.City) ?? ""
                self.txtState.text = (response.data?.State)!
                self.txtCountry.text = (response.data?.Country)!
                self.txtPostalCode.text = (response.data?.PostalCode) ?? ""
                self.txtStatus.text = response.data?.Status
                self.lblStatus.text = "Status: " + (response.data?.Status ?? "")
                self.lblPriority.text = "Priority: " + (response.data?.Priority ?? "")
                self.lblHeadetTitle.text = response.data?.Subject
                self.txtStartDateTime.text = response.data?.StartDate
                self.txtEndDateTime.text = response.data?.EndDate
                self.StartTime = response.data?.StartTime
                self.EndDate = response.data?.EndDate
                self.Ends = response.data?.Ends
                self.EndsAfterOccurrences = response.data?.EndsAfterOccurrences
                self.EndsOnDate = response.data?.EndsOnDate
                self.EndTime = response.data?.EndTime
                self.IntervalEvery = response.data?.IntervalEvery
                self.StartOn = response.data?.StartOn
                self.IsRecurring = response.data?.IsRecurring
                self.RepeatEvery = response.data?.RepeatEvery
                self.RepeatOn = response.data?.RepeatOn
                self.StartDate = response.data?.StartDate
                self.StartTime = response.data?.StartTime
                self.WORecurrenceID = response.data?.WORecurrenceID
                self.id = response.data?.AssignedTo
                self.statusID = response.data?.WOStatus
                self.priorityID = response.data?.WOPriority
                self.categoryID = response.data?.WOCategory
                self.typeID = response.data?.WorkOrderType
                self.acID = response.data?.Account
                User.instance.acID = self.acID ?? ""
                self.contactID = response.data?.PrimaryContact
                self.txtAccount.text = response.data?.AccountName
                self.txtCategory.text = response.data?.CategoryName
                self.txtTotalPrice.text = response.data?.TotalPrice
                self.txtTax.text = response.data?.Tax
                self.txtSubTotal.text = response.data?.SubTotal
                self.txtLineCountItems.text = response.data?.LineItemCount
                self.txtGrandTotal.text = response.data?.GrandTotal
                self.txtDiscount.text = response.data?.Discount
                self.txtSignature.text = response.data?.Signature
                self.txtParentWorkOrder.text = response.data?.ParentWorkOrderName
                self.parentWO_ID = response.data?.ParentWorkOrder
                self.latitude = response.data?.Latitude
                self.longitude = response.data?.Longitude
                self.txtCreatedBy.text = response.data?.CreatedBy
                self.txtCreatedDate.text = response.data?.CreatedDate
                self.txtLastModifiedBy.text = response.data?.LastModifiedBy
                self.txtLastModifiedDate.text = response.data?.LastModifiedDate
                self.strMobileNo = response.data?.MobileNo
                
                self.lblHeadetTitle.text = response.data?.Subject
                if self.IsRecurring == "0"
                {
                    self.btnRecurring.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
                    
                }
                else{
                    self.btnRecurring.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
                }
                if response.data?.PopUpReminder != "" {
                    Helper.instance.showAlertView(message: (response.data?.PopUpReminder)!, vc: self)
                }
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForWOTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.workOrderTypes(urlString: API.woTypesURL, parameters: parameters , headers: headers, vc: self) { (response:WorkOrderTypesResponse) in
            
            if response.Result == "True"
            {
                
                self.woTypesArr = response.WorkOrderTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForPriorityTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.priorityTypes(urlString: API.priorityTypesURL, parameters: parameters , headers: headers, vc: self) { (response:PriorityTypesResponse) in
            
            if response.Result == "True"
            {
                
                self.priorityTypeArr = response.PriorityTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForStatusTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.statusTypes(urlString: API.statusTypesURL, parameters: parameters , headers: headers, vc: self) { (response:StatusTypesResponse) in
            
            if response.Result == "True"
            {
                
                self.statusArr = response.StatusTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCategoryTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.categoryTypes(urlString: API.categoryTypesURL, parameters: parameters , headers: headers, vc: self) { (response:CategoryTypesResponse) in
            
            if response.Result == "True"
            {
                
                self.categoryArr = response.CategoryTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForGetChemicals()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getChemicals(urlString: API.getChemicals, parameters: parameters , headers: headers, vc: self) { (response:GetChemicals) in
            
            if response.Result == "True"
            {
                
                self.chemicalListArr = response.data!
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "ObjectID":User.instance.workorderID,
                          "Object":"WorkOrder",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                var arrayFieldValue:[String]?
                
                var viewFromConstrain:Any = self.txtLineCountItems
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            arrayFieldValue = (response.data?[i].FieldValue)?.components(separatedBy: ",")
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtLineCountItems)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtLineCountItems, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtLineCountItems, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            //
                            customView.txtLabelField.text = response.data?[i].FieldValue
                            
                            //
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtLabelField.text ?? "")
                            
                            //
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            
                            if response.data?[i].IsRequired == "1" {
                                
                                if response.data?[i].FieldValue == "" {
                                    customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                    customView.txtviewLabelField.textColor = UIColor.lightGray
                                } else {
                                    customView.txtviewLabelField.text = response.data?[i].FieldValue
                                    
                                }
                                
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            //Edit
                            
                            self.customFieldKeyArr.append("CustomFieldIDs[\(customView.txtviewLabelField.tag/10)]")
                            self.customFieldValueArr.append(customView.txtviewLabelField.text ?? "")
                            
                            //
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            //Edit
                            
                            if response.data?[i].FieldValue == "" {
                                self.checkboxValueArr.append("")
                            } else{
                                self.checkboxValueArr.append("\((response.data?[i].FieldValue ?? "")),")
                            }
                            
                            //
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                if ((arrayFieldValue?.contains((arrayCheckboxValues[j])))!) {
                                    button.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                                } else {
                                    button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                }
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.lblSystemInfo, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                
                //Edit
                if value == "," {
                    checkboxValueArr.insert("\(sender.accessibilityLabel!),", at: index)
                } else {
                    checkboxValueArr.insert(value + "\(sender.accessibilityLabel!),", at: index)
                }
                //
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: "\(sender.accessibilityLabel!),", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < scrollView.contentOffset.y)
        {
            // moved to top
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                self.view_BottomContainer.alpha = 0 // Here you will get the animation you want
            }, completion: { _ in
                self.view_BottomContainer.isHidden = true
                self.actionsheetWOView.isHidden = true
                // Here you hide it when animation done
            })
            
        }
        else if (self.lastContentOffset > scrollView.contentOffset.y)
        {
            // moved to bottom
            UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                self.view_BottomContainer.alpha = 1 // Here you will get the animation you want
            }, completion: { _ in
                self.view_BottomContainer.isHidden = false // Here you hide it when animation done
            })
            
        }
        else
        {
            // didn't move
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == SEGUE_RECURRING
        {
            let settingsVC = segue.destination as! RecurrenceDetailsViewController
            
            settingsVC.EndDate = EndDate
            settingsVC.Ends = Ends
            settingsVC.EndsAfterOccurrences = EndsAfterOccurrences
            settingsVC.EndsOnDate = EndsOnDate
            settingsVC.EndTime = EndTime
            settingsVC.IntervalEvery = IntervalEvery
            settingsVC.StartOn = StartOn
            settingsVC.IsRecurring = IsRecurring
            settingsVC.RepeatEvery = RepeatEvery
            settingsVC.RepeatOn = RepeatOn
            settingsVC.StartDate = StartDate
            settingsVC.StartTime = StartTime
            settingsVC.WORecurrenceID = WORecurrenceID
        }
    }
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        if let RecurrenceDetailsVC = sender.source as? RecurrenceDetailsViewController {
            
            Ends = RecurrenceDetailsVC.ends ?? ""
            IntervalEvery = RecurrenceDetailsVC.txtInterval.text ?? ""
            RepeatEvery = RecurrenceDetailsVC.txtRepeats.text ?? ""
            EndsOnDate = RecurrenceDetailsVC.txtOn.text ?? ""
            EndsAfterOccurrences = RecurrenceDetailsVC.txtAfter.text ?? ""
            StartTime = RecurrenceDetailsVC.txtStartTime.text ?? ""
            EndTime = RecurrenceDetailsVC.txtEndTime.text ?? ""
            StartOn = RecurrenceDetailsVC.txtStartsOn.text ?? ""
            RepeatOn = RecurrenceDetailsVC.weekArr.joined(separator: ",")
            
        }
        else if let SelectLocationVC = sender.source as? SelectLocationViewController {
            
             txtAddress.text = SelectLocationVC.address
             txtCity.text = SelectLocationVC.city
             txtState.text = SelectLocationVC.state
             txtCountry.text = SelectLocationVC.country
             txtPostalCode.text = SelectLocationVC.postalCode
             self.latitude = SelectLocationVC.latitude
             self.longitude = SelectLocationVC.longitude
        }
        else if let _ = sender.source as? EditLinesViewController
        {
            self.flag = 1
        }
        else if let _ = sender.source as? EditChemicalViewController
        {
            self.flag = 1
            User.instance.flagForEditChemical = "1"
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 12
        {
            return woTypesArr.count + 1
        }
        else if textfieldTag == 13
        {
            return statusArr.count + 1
        }
        else if textfieldTag == 14
        {
            return priorityTypeArr.count + 1
        }
        else if textfieldTag == 15
        {
            return categoryArr.count + 1
        }
        return 0
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 12
        {
            return row == 0 ? "--None--":woTypesArr[row-1].WorkOrderType
        }
        else if textfieldTag == 13
        {
            return row == 0 ? "--None--":statusArr[row-1].Status
        }
        else if textfieldTag == 14
        {
            return row == 0 ? "--None--":priorityTypeArr[row-1].Priority
        }
        else if textfieldTag == 15
        {
            return row == 0 ? "--None--":categoryArr[row-1].CategoryName
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 12
        {
            if row == 0
            {
                txtOrderType.text = ""
            }
            else
            {
                txtOrderType.text = woTypesArr[row-1].WorkOrderType
                typeID = woTypesArr[row-1].WorkOrderTypeID
            }
        }
        else if textfieldTag == 13
        {
            if row == 0
            {
                txtStatus.text = ""
            }
            else
            {
                txtStatus.text = statusArr[row-1].Status
                statusID = statusArr[row-1].WOStatusID
            }
        }
        else if textfieldTag == 14
        {
            if row == 0
            {
                txtPriority.text = ""
            }
            else
            {
                txtPriority.text = priorityTypeArr[row-1].Priority
                priorityID = priorityTypeArr[row-1].WOPriorityID
            }
        }
        else if textfieldTag == 15
        {
            if row == 0
            {
                txtCategory.text = ""
            }
            else
            {
                txtCategory.text = categoryArr[row-1].CategoryName
                categoryID = categoryArr[row-1].WOCategoryID
            }
        }
        
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: UITextview Delegate Methods
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            txtAddress.becomeFirstResponder()
            
            return false
        }
        return true
    }
    var lastTappedTextView:UITextView!
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    //MARK: UITextfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        //
        if textField == txtWorkOwner || textField == txtOrderType || textField == txtPrimaryContact || textField == txtStatus || textField == txtPriority || textField == txtCategory || textField == txtOrderType || textField == txtStartDateTime || textField == txtEndDateTime || textField == txtParentWorkOrder
        {
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        if textField.tag == 2
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 3
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Account")
        }
        if textField.tag == 12
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 13
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 14
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 15
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 18
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 19
        {
            textField.resignFirstResponder()
            textfieldTag = textField.tag
            textfieldTapped(object: "WorkOrder")
        }
        if textField.tag == 5
        {
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtWorkOwner.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtAccount.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtSubject.becomeFirstResponder()
        }
        else if textField.tag == 4
        {
            txtviewDescription.becomeFirstResponder()
            return false
        }
        else if textField.tag == 6
        {
            txtCity.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtState.becomeFirstResponder()
        }
        else if textField.tag == 8
        {
            txtCountry.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 10
        {
            txtPopupReminder.becomeFirstResponder()
        }
        else if textField.tag == 12
        {
            txtOrderType.becomeFirstResponder()
        }
        else if textField.tag == 13
        {
            txtStatus.becomeFirstResponder()
        }
        else if textField.tag == 14
        {
            txtPriority.becomeFirstResponder()
        }
        else if textField.tag == 15
        {
            txtCategory.becomeFirstResponder()
        }
        else if textField.tag == 16
        {
            txtStartDateTime.becomeFirstResponder()
        }
        else if textField.tag == 17
        {
            txtEndDateTime.becomeFirstResponder()
        }
        else if textField.tag == 18
        {
            txtPrimaryContact.becomeFirstResponder()
        }
        else if textField.tag == 19
        {
            txtParentWorkOrder.becomeFirstResponder()
        }
        else if textField.tag == 20
        {
            txtSubTotal.becomeFirstResponder()
        }
        else if textField.tag == 21
        {
            txtDiscount.becomeFirstResponder()
        }
        else if textField.tag == 22
        {
            txtTax.becomeFirstResponder()
        }
        else if textField.tag == 23
        {
            txtTotalPrice.becomeFirstResponder()
        }
        else if textField.tag == 24
        {
            txtGrandTotal.becomeFirstResponder()
        }
        else if textField.tag == 25
        {
            txtSignature.becomeFirstResponder()
        }
        else if textField.tag == 26
        {
            txtLineCountItems.becomeFirstResponder()
        }
        else if textField.tag == 26
        {
            txtLineCountItems.resignFirstResponder()
        }
        return true
    }
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    //MARK: IBActions
    
    @IBAction func btnDetails(_ sender: Any) {
        
        containerviewLayout.isHidden = false
        self.containerview.isHidden = true
        self.scrollview.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    @IBAction func btnRelated(_ sender: Any) {
        
        containerviewLayout.isHidden = true
        self.containerview.isHidden = false
        self.scrollview.isHidden = true
        self.btnDetails.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.btnRelated.backgroundColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.view.viewWithTag(101)?.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCall(_ sender: Any) {
        if let number = strMobileNo {
            if let url = URL(string: "tel://\(number.removeSpecialCharacters())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
    }
    @IBAction func btnClose(_ sender: Any) {
        self.actionsheetWOView.isHidden = true
        self.view_BottomContainer.isHidden = false
        self.view_back.isHidden = true
    }
    
    @IBAction func btnMessage(_ sender: Any) {
        
        if let number = strMobileNo {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                controller.recipients = [number.removeSpecialCharacters()]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnCalender(_ sender: Any) {
        
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventViewController") as! CreateEventViewController
        createEventVC.relatedTo = "WorkOrder"
        createEventVC.objectID = User.instance.workorderID
        self.navigationController?.pushViewController(createEventVC, animated: true)
    }
    
    func editWebserviceCall() {
        if acID == nil || id == nil || typeID == nil || statusID == nil || priorityID == nil  || categoryID == nil || txtviewDescription.text! == "Description*" || txtSubject.text! == "" || txtStartDateTime.text! == "" || txtEndDateTime.text! == "" || txtAddress.text! == "" || txtCity.text! == "" || txtState.text! == "" || txtCountry.text! == "" || txtPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        StartTime = Helper.instance.convertDateFormat(date: txtStartDateTime.text!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "hh:mm a")
        EndTime = Helper.instance.convertDateFormat(date: txtEndDateTime.text!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "hh:mm a")
        
        self.showHUD()
        
        var parameters:[String:String] = [:]
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["WorkOrderID"] = User.instance.workorderID
        parameters["Account"] = acID ?? ""
        parameters["AssignedTo"] = id ?? ""
        parameters["Subject"] = self.txtSubject.text!
        parameters["Description"] = self.txtviewDescription.text!
        parameters["Address"] = self.txtAddress.text!
        parameters["City"] = self.txtCity.text!
        parameters["State"] = self.txtState.text!
        parameters["PostalCode"] = self.txtPostalCode.text!
        parameters["Country"] = self.txtCountry.text!
        parameters["PopUpReminder"] = self.txtPopupReminder.text!
        parameters["WorkOrderType"] = typeID ?? ""
        parameters["WOStatus"] = statusID ?? ""
        parameters["WOPriority"] = priorityID ?? ""
        parameters["PrimaryContact"] = contactID ?? ""
        parameters["WOStartTime"] = StartTime ?? ""
        parameters["WOEndTime"] = EndTime ?? ""
        parameters["StartDate"] = StartDate ?? ""
        parameters["EndDate"] = EndDate ?? ""
        parameters["IsRecurring"] = IsRecurring ?? ""
        parameters["RepeatEvery"] = RepeatEvery ?? ""
        parameters["IntervalEvery"] = IntervalEvery ?? ""
        parameters["StartOn"] = StartOn ?? ""
        parameters["RepeatOn"] = RepeatOn ?? ""
        parameters["Ends"] = Ends ?? ""
        parameters["EndsOnDate"] = EndsOnDate ?? ""
        parameters["EndsAfterOccurrences"] = EndsAfterOccurrences ?? ""
        parameters["StartTime"] = StartTime ?? ""
        parameters["EndTime"] = EndTime ?? ""
        parameters["WORecurrenceID"] = WORecurrenceID ?? ""
        parameters["WOCategory"] = categoryID ?? ""
        parameters["ParentWorkOrder"] = parentWO_ID
        parameters["Latitude"] = latitude
        parameters["Longitude"] = longitude
        
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.editOrderURL, parameters: parameters , headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                self.webserviceCall()
                self.containerviewLayout.isHidden = false
                self.scrollview.isHidden = true
                
                let vc  = self.children[1] as! WorkOrderLayoutViewController
                vc.viewWillAppear(true)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    @IBAction func btnSave(_ sender: Any) {
        
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        if customFieldKeyArr.count != 0 {
            
            if lastTextfieldTapped != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                    customFieldValueArr.append(lastTextfieldTapped.text ?? "")
                }
            }
            
        }
        
        
        //CustomTextView
        if customFieldKeyArr.count != 0 {
            
            if lastTappedTextView != nil {
                if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                    
                    if customFieldValueArr.count != 0 {
                        customFieldValueArr.remove(at: index)
                        customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                    }
                    
                } else {
                    
                    customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                    customFieldValueArr.append(lastTappedTextView.text ?? "")
                }
            }
            
        }
        
        checkboxEditedValueArr.removeAll()
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropLast()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        editWebserviceCall()
    }
    @IBAction func btnMore(_ sender: Any) {
        
        //self.btnClose.isHidden = false
        self.actionsheetWOView.isHidden = false
        self.view_BottomContainer.isHidden = true
        self.view_back.isHidden = false
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        
        txtviewDescription.isUserInteractionEnabled = true
        containerviewLayout.isHidden = true
        scrollview.isHidden = false
        btnCancel.isHidden = false
        btnSave.isHidden = false
        txtStatus.layer.borderColor = #colorLiteral(red: 0, green: 0.6657338738, blue: 0.06580683589, alpha: 1)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        txtviewDescription.isUserInteractionEnabled = false
        containerviewLayout.isHidden = false
        scrollview.isHidden = true
        btnCancel.isHidden = true
        btnSave.isHidden = true
        
        let vc  = self.children[1] as! WorkOrderLayoutViewController
        vc.viewWillAppear(true)
    }
    
    
    
    @IBAction func btnRecurring(_ sender: Any) {
        
        if (btnRecurring.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnRecurring.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            IsRecurring = "0"
        }
        else{
            btnRecurring.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            self.performSegue(withIdentifier: SEGUE_RECURRING, sender: self)
            IsRecurring = "1"
        }
    }
    
    @IBAction func txtStartTime(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 101
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        (sender as! UITextField).inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtEndTime(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 102
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        (sender as! UITextField).inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func btnLocation(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        vc.isUnwindTo = "orderDetails"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        if sender.tag == 101 {
            txtStartDateTime.text = dateFormatter.string(from: sender.date)
        } else {
            txtEndDateTime.text = dateFormatter.string(from: sender.date)
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view == view_back
        {
            view_back.isHidden = true
            actionsheetWOView.isHidden = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension WorkOrdersDetailsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Coordinates:\(place.coordinate)")
        latitude = String(place.coordinate.latitude)
        longitude = String(place.coordinate.longitude)
        var area:String!
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                area = component.name
            }
            if component.type == "sublocality_level_1" {
                print(component.name)
                txtAddress.text = "\(place.name) \(area ?? "") \(component.name)"
            }
            if component.type == "postal_code"
            {
                print("Code: \(component.name)")
                txtPostalCode.text = component.name
            }
            if component.type == "administrative_area_level_2" {
                print("City: \(component.name)")
                txtCity.text = component.name
            }
            if component.type == "administrative_area_level_1"
            {
                print("State: \(component.name)")
                txtState.text = component.name
            }
            if component.type == "country"
            {
                print("Country: \(component.name)")
                txtCountry.text = component.name
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension WorkOrdersDetailsViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)

        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtSubject.text ?? "",
                          "AssignedTo":self.id!,
                          "RelatedTo":"WorkOrder",
                          "What":User.instance.workorderID] as [String : Any]

        let headers = ["key":User.instance.key,
                       "token":User.instance.token]

        NetworkManager.sharedInstance.uploadDocs(urlString: API.createNewFileAllURL, fileName: "FileName", URLs: urls as [NSURL], parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in

            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }

        }
    }
    
}

extension WorkOrdersDetailsViewController:FusumaDelegate {
    
    // Return the image which is selected from camera roll or is taken via the camera.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
        
        print("Image selected")
    }
    
    // Return the image but called after is dismissed.
    func fusumaDismissedWithImage(image: UIImage, source: FusumaMode) {
        
        print("Called just after FusumaViewController is dismissed.")
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
        print("Called just after a video has been selected.")
    }
    
    // When camera roll is not authorized, this method is called.
    func fusumaCameraRollUnauthorized() {
        
        print("Camera roll unauthorized")
    }
    
    // Return selected images when you allow to select multiple photos.
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
        
        
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Subject":txtSubject.text ?? "",
                          "AssignedTo":self.id!,
                          "RelatedTo":"WorkOrder",
                          "What":User.instance.workorderID] as [String : Any]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createNewFileAll(urlString: API.createNewFileAllURL, pickedImages: images, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                print("Success")
                //Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    // Return an image and the detailed information.
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
        
    }
}
