//
//  ChemicalSearchViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 11/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

//protocol searchChemicalDelegate {
//    
//    func sendData(productID:String, productName:String)
//}
class ChemicalSearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    //MARK: Variables
    var filteredArray:[GetChemicalsData] = []
    var chemicalArr:[GetChemicalsData] = []
    var isSeaching = false
    var productID = ""
    var productName = ""
    var delegate:searchChemicalDelegate!
    
    //MARK: IBOutlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webserviceCallForGetChemicals()
    }

    //MARK: Function
    
    func webserviceCallForGetChemicals()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getChemicals(urlString: API.getChemicals, parameters: parameters , headers: headers, vc: self) { (response:GetChemicals) in
            
            if response.Result == "True"
            {
                
                self.chemicalArr = response.data!
                self.table.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    //MARK: UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSeaching
        {
            return filteredArray.count
        }
        return chemicalArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "id")
        if isSeaching
        {
            cell.textLabel?.text = filteredArray[indexPath.row].ProductName
        }
        else
        {
            cell.textLabel?.text = chemicalArr[indexPath.row].ProductName
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSeaching
        {
            productID = filteredArray[indexPath.row].ProductID!
            productName = filteredArray[indexPath.row].ProductName!
            
        }
        else
        {
            productID = chemicalArr[indexPath.row].ProductID!
            productName = chemicalArr[indexPath.row].ProductName!
        }
        
        self.navigationController?.popViewController(animated: true)
        delegate.sendData(productID: productID, productName: productName)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredArray = chemicalArr.filter({ (text) -> Bool in
            let tmp: NSString = text.ProductName! as NSString
            let range = tmp.range(of: searchText, options: String.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(filteredArray.count == 0){
            isSeaching = false;
        } else {
            isSeaching = true;
        }
        self.table.reloadData()
    }
    
    
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
