//
//  RecentOrderListViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 22/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class RecentOrderListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, sendingDataDelegate, UIScrollViewDelegate {
    
    

    //MARK: Variables
    private let CELL_LIST_WORK_ORDERS = "cell_subOrders"
    
    private let SEGUE_CREATE_ORDER = "segueCreateOrder"
    private let SEGUE_ORDER_DETAILS = "segue_details"
    
    var WorkOrderViewID:String = ""
    var WorkOrderViewName:String = ""
    
    var viewWOListArr:[[String:String]]? = []
    var woGetViewsArr:[WOGetViewsData]? = []
    
    var count:Int?
    var city:String?
    var state:String?
    var flag = 0
    var sortingFlag = 0
    var lastContentOffset: CGFloat = 0
    var addressLabel:PaddingLabel?
    var flagForDelegate = 0
    
    //MARK: IBOutlets
    
    @IBOutlet weak var table_listOrders: UITableView!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var viewDropdown: UIView!
    
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var tableDropdown: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSorting: UIButton!
    @IBOutlet weak var btnMoreMenu: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       customViewWillAppear()
    }

    func customViewWillAppear() {
        if flagForDelegate == 0 {
            webserviceCallForWOGetViews()
            webserviceCallForViewWOList()
        } else {
            flagForDelegate = 0
        }
        
        lblDropdown.text = "  " + WorkOrderViewName + "  "
        if WorkOrderViewID == "MyOpenWorkOrdersToday" || WorkOrderViewID == "MyOpenWorkOrdersThisWeek" || WorkOrderViewID == "AllOpenWorkOrdersToday" || WorkOrderViewID == "AllOpenWorkOrdersThisWeek" || WorkOrderViewID == "WorkOrdersCreatedToday" || WorkOrderViewID == "WorkOrdersCreatedThisWeek" || WorkOrderViewID == "UnscheduledWorkOrders" || WorkOrderViewID == "UnassignedWorkOrders"
        {
            btnMoreMenu.isEnabled = false
            btnSorting.isEnabled = false
        }
        else
        {
            btnMoreMenu.isEnabled = true
            btnSorting.isEnabled = true
        }
    }
    //MARK: Functions
    
    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        table_listOrders.tableFooterView = UIView()
        lblDropdown.giveBorderToLabel()
        btnDropdown.giveBorderToButton()
        viewDropdown.dropShadow()
    }
    
    func sendData(array: [[String : String]], flag: Int) {
    
        viewWOListArr = array
        flagForDelegate = flag
        self.table_listOrders.reloadData()
    }
    
    func  webserviceCallForViewWOList()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "WorkOrderViewID":WorkOrderViewID,
                          "OrganizationID":User.instance.OrganizationID]
        print(parameters)
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        print(API.viewWOListURL)
        NetworkManager.sharedInstance.viewWOList(urlString: API.viewWOListURL, parameters: parameters, headers: headers, vc: self) { (response:ViewWOList) in
            
            if response.Result == "True"
            {
                self.viewWOListArr = response.data as? [[String : String]]
                
                self.table_listOrders.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForWOGetViews()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.woGetViews(urlString: API.woGetViewsURL, parameters: parameters, headers: headers, vc: self) { (response:WOGetViews) in
            
            if response.Result == "True"
            {
                
                self.woGetViewsArr = response.data
                self.tableDropdown.reloadData()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func  webserviceCallForCopyCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"WorkOrder",
                          "ViewID":WorkOrderViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.copyCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    
    func  webserviceCallForDeleteCustomView()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID,
                          "Object":"WorkOrder",
                          "ViewID":WorkOrderViewID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.webserviceCall(urlString: API.deleteCustomViewURL, parameters: parameters, headers: headers, vc: self) { (response:ResponseDict) in
            
            if response.Result == "True"
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
                self.view.viewWithTag(101)?.removeFromSuperview()
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView(self.tableDropdown, didSelectRowAt: indexPath)
                self.customViewWillAppear()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_ORDER_DETAILS
        {
            let detailVC = segue.destination as! WorkOrdersDetailsViewController
            
            let wo = sender as? [String:String]
            User.instance.workorderID = wo?["WorkOrderID"] ?? ""
            User.instance.workorderSubject = wo?["Subject"] ?? ""
            
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == table_listOrders
        {
            if (self.lastContentOffset < scrollView.contentOffset.y)
            {
                // moved to top
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 0 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = true
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
            }
            else if (self.lastContentOffset > scrollView.contentOffset.y)
            {
                // moved to bottom
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.viewBottomContainer.alpha = 1 // Here you will get the animation you want
                }, completion: { _ in
                    self.viewBottomContainer.isHidden = false
                    //self.containerViewMenu.isHidden = true
                    //self.btnCancelActionMenu.isHidden = true// Here you hide it when animation done
                })
            }
            else
            {
                // didn't move
            }
        }
    }
    @objc func btnCreateView() {
        
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "WorkOrder"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    @objc func btnRenameView() {
        
        let renameViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RenameView_ViewController") as!  RenameView_ViewController
        renameViewVC.object = "WorkOrder"
        renameViewVC.ViewID = WorkOrderViewID
        self.navigationController?.pushViewController(renameViewVC, animated: true)
    }
    @objc func btnEditSharing() {
        
        let editSharingVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditSharingViewController") as!  EditSharingViewController
        editSharingVC.object = "WorkOrder"
        editSharingVC.ViewID = WorkOrderViewID
        self.navigationController?.pushViewController(editSharingVC, animated: true)
    }
    @objc func btnCopy() {
        
        webserviceCallForCopyCustomView()
    }
    @objc func btnDeleteRow() {
        
        webserviceCallForDeleteCustomView()
    }
    @objc func btnEditColumns() {
        
        let editColumnsVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DisplayedColumnsViewController") as!  DisplayedColumnsViewController
        editColumnsVC.flagSpecifyAction = "Edit"
        editColumnsVC.object = "WorkOrder"
        editColumnsVC.ViewID = WorkOrderViewID
        self.navigationController?.pushViewController(editColumnsVC, animated: true)
    }
    @objc func btnEditFilter() {
        
        let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
        editFilterVC.object = "WorkOrder"
        editFilterVC.ViewID = WorkOrderViewID
        self.navigationController?.pushViewController(editFilterVC, animated: true)
    }
    //MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table_listOrders
        {
            return viewWOListArr!.count
        }
        else
        {
            return woGetViewsArr!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table_listOrders
        {
            count = 0
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_LIST_WORK_ORDERS) as! SubWorkOrdersTableViewCell

            for subview in cell.stackview.subviews {
                subview.removeFromSuperview()
            }
            
            cell.viewBack.dropShadow()
            
            for (key, value) in viewWOListArr![indexPath.row] {
                // here use key and value
                
                print(key)
                print(value)
                
                if key == "WorkOrderNo" || key == "Account" || key == "AssignedTo" || key == "PrimaryContact" || key == "ParentWorkOrder" || key == "WorkOrderType" || key == "Description" || key == "WOPriority" || key == "WOStatus"  || key == "WOCategory" || key == "Subject" || key == "PopUpReminder" || key == "CreatedDate" 
                {
                    
                    let label = PaddingLabel()
                    label.textAlignment = .left
                    label.text = key.camelCaseToWords() + ": " + value
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        count = count! + 20
                        label.font = label.font.withSize(15)
                    } else { //IPAD
                        count = count! + 25
                        label.font = label.font.withSize(20)
                    }
                    
                    label.textColor = UIColor.darkGray
                    cell.stackview.addArrangedSubview(label)
                }
                
                if key == "City"
                {
                    if addressLabel?.text != nil
                    {
                        addressLabel?.text = ""
                        addressLabel?.text = " Address: " + value + ", " + (state ?? "")
                    }
                    else
                    {
                        city = value
                    }
                }
                if key == "State"
                {
                    state = value
                    addressLabel = PaddingLabel()
                    addressLabel?.textAlignment = .left
                    addressLabel?.text = " Address: " + (city ?? "") + ", " + value
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        count = count! + 20
                        addressLabel?.font = addressLabel?.font.withSize(15)
                    } else { //IPAD
                        count = count! + 25
                        addressLabel?.font = addressLabel?.font.withSize(20)
                    }
                    addressLabel?.textColor = UIColor.darkGray
                    cell.stackview.addArrangedSubview(addressLabel!)
                    
                }
            }
            return cell
        }
        else
        {
            let cell1 = UITableViewCell(style: .default, reuseIdentifier: "id")
            
            cell1.textLabel?.text = woGetViewsArr?[indexPath.row].WorkOrderViewName
            cell1.textLabel?.numberOfLines = 0
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(12)
            } else { //IPAD
                cell1.textLabel?.font = cell1.textLabel?.font.withSize(17)
            }
            return cell1
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == table_listOrders
        {
            self.performSegue(withIdentifier: SEGUE_ORDER_DETAILS, sender: viewWOListArr?[indexPath.row])
        }
        else
        {
            WorkOrderViewID = (woGetViewsArr?[indexPath.row].WorkOrderViewID)!
            WorkOrderViewName = (woGetViewsArr?[indexPath.row].WorkOrderViewName)!
            flag = 0
            webserviceCallForViewWOList()
            viewDropdown.isHidden = true
            lblDropdown.text = "  " + WorkOrderViewName + "  "
            
            if WorkOrderViewID == "MyOpenWorkOrdersToday" || WorkOrderViewID == "MyOpenWorkOrdersThisWeek" || WorkOrderViewID == "AllOpenWorkOrdersToday" || WorkOrderViewID == "AllOpenWorkOrdersThisWeek" || WorkOrderViewID == "WorkOrdersCreatedToday" || WorkOrderViewID == "WorkOrdersCreatedThisWeek" || WorkOrderViewID == "UnscheduledWorkOrders" || WorkOrderViewID == "UnassignedWorkOrders"
            {
                btnMoreMenu.isEnabled = false
                btnSorting.isEnabled = false
            }
            else
            {
                btnMoreMenu.isEnabled = true
                btnSorting.isEnabled = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == table_listOrders
        {
            
            return CGFloat(count! + 10)
        }
        else
        {
            if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                return 40
            } else { //IPAD
                return 50
            }
        }
    }
    
    //MARK: IBActions
    
    @IBAction func btnNewOrder(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_CREATE_ORDER, sender: self)
    }
    
    @IBAction func btnDropdown(_ sender: Any) {
        if viewDropdown.isHidden == true
        {
            viewDropdown.isHidden = false
        }
        else
        {
            viewDropdown.isHidden = true
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        if WorkOrderViewID != "MyOpenWorkOrdersToday" && WorkOrderViewID != "MyOpenWorkOrdersThisWeek" && WorkOrderViewID != "AllOpenWorkOrdersToday" && WorkOrderViewID != "AllOpenWorkOrdersThisWeek" && WorkOrderViewID != "WorkOrdersCreatedToday" && WorkOrderViewID != "WorkOrdersCreatedThisWeek" && WorkOrderViewID != "UnscheduledWorkOrders" && WorkOrderViewID != "UnassignedWorkOrders" {
            
            let editFilterVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditFilterForCustomViewViewController") as!  EditFilterForCustomViewViewController
            editFilterVC.object = "WorkOrder"
            editFilterVC.ViewID = WorkOrderViewID
            self.navigationController?.pushViewController(editFilterVC, animated: true)
        }
        else{
            let filterVC = self.storyboard?.instantiateViewController(withIdentifier: "WOFilterViewController") as! WOFilterViewController
            filterVC.WorkOrderViewID = WorkOrderViewID
            filterVC.delegate = self
            self.navigationController?.pushViewController(filterVC, animated: true)
        }
        
    }
    
    @IBAction func btnSorting(_ sender: Any) {
        
        let sortVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SortViewController") as! SortViewController
        sortVC.viewID = WorkOrderViewID
        sortVC.object = "WorkOrder"
        self.navigationController?.pushViewController(sortVC, animated: true)
    }
    @IBAction func btnOpenMenu(_ sender: Any) {
        
        if WorkOrderViewID != "MyOpenWorkOrdersToday" && WorkOrderViewID != "MyOpenWorkOrdersThisWeek" && WorkOrderViewID != "AllOpenWorkOrdersToday" && WorkOrderViewID != "AllOpenWorkOrdersThisWeek" && WorkOrderViewID != "WorkOrdersCreatedToday" && WorkOrderViewID != "WorkOrdersCreatedThisWeek" && WorkOrderViewID != "UnscheduledWorkOrders" && WorkOrderViewID != "UnassignedWorkOrders" {
            if let customView = Bundle.main.loadNibNamed("CustomViewMenu", owner: self, options: nil)?.first as? CustomViewMenu {
                
                customView.tag = 101
                customView.btnCreateView.addTarget(self, action: #selector(btnCreateView), for: .touchUpInside)
                customView.btnRename.addTarget(self, action: #selector(btnRenameView), for: .touchUpInside)
                customView.btnEditSharing.addTarget(self, action: #selector(btnEditSharing), for: .touchUpInside)
                customView.btnCopy.addTarget(self, action: #selector(btnCopy), for: .touchUpInside)
                customView.btnDeleteRow.addTarget(self, action: #selector(btnDeleteRow), for: .touchUpInside)
                customView.btnEditColumns.addTarget(self, action: #selector(btnEditColumns), for: .touchUpInside)
                customView.btnEditFilter.addTarget(self, action: #selector(btnEditFilter), for: .touchUpInside)
                customView.frame = self.view.frame
                self.view.addSubview(customView)
            }
        }
        
    }
    
    @IBAction func btnCreateNewView(_ sender: Any) {
        let createViewVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateView_ViewController") as!  CreateView_ViewController
        createViewVC.object = "WorkOrder"
        self.navigationController?.pushViewController(createViewVC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.viewWithTag(101)?.removeFromSuperview()
        viewDropdown.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

}
