//
//  CreateWOViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class CreateWOViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate, searchDelegate{
    
    //MARK: Variables
    
    private let SEGUE_RECURRING = "segue_recurring"
    private let SEGUE_SEARCH = "segueSearchAC"
    
    var pickerView = UIPickerView()
    var textfieldTag = 0
    var woTypesArr:[WorkOrderTypesData] = []
    var priorityTypeArr:[PriorityTypesData] = []
    var statusArr:[StatusTypesData] = []
    var categoryArr:[CategoryTypesData] = []
    var priorityID:String?
    var woTypeID:String?
    var acTypeID:String?
    var statusID:String?
    var categoryID:String?
    var recurring:String = "0"
    var assignedToID:String?
    var contactID:String?
    var startTime:String?
    var endTime:String?
    var parentWO_ID:String?
    
    var Ends:String?
    var EndsAfterOccurrences:String?
    var EndsOnDate:String?
    var EndTime:String?
    var IntervalEvery:String?
    var RepeatEvery:String?
    var RepeatOn:String?
    var StartTime:String?
    var StartOn:String?
    
    var latitude:String?
    var longitude:String?
    var flagResponse = "0"
    var response:OrderDetailsResponse?
    var contactName:String?
    
    var accountName:String?
    
    //CustomFields Variables
    var fieldName = ""
    var fieldID = ""
    
    var requiredCustomFieldsArr:[Any] = []
    var customFieldKeyArr :[String] = []
    var customFieldValueArr :[String] = []
    var checkboxKeyArr:[String] = []
    var checkboxValueArr:[String] = []
    var checkboxEditedValueArr:[String] = []
    var requiredCheckBoxFieldArr:[String] = []
    
    //MARK: IBOutlets
    @IBOutlet weak var txtOrderOwner: UITextField!
    @IBOutlet weak var txtOrderType: UITextField!
    @IBOutlet weak var txtSubject: AkiraTextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var txtCity: AkiraTextField!
    @IBOutlet weak var txtState: AkiraTextField!
    @IBOutlet weak var txtCountry: AkiraTextField!
    @IBOutlet weak var txtPostalCode: AkiraTextField!
    @IBOutlet weak var txtWOType: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPriority: UITextField!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtPopupReminder: AkiraTextField!
    @IBOutlet weak var txtStartDateTime: AkiraTextField!
    @IBOutlet weak var txtEndDateTime: AkiraTextField!
    @IBOutlet weak var txtPrimaryConstact: UITextField!
    @IBOutlet weak var btnRecurring: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var txtParentWorkOrder: UITextField!
    
    @IBOutlet weak var viewBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        pickerView.delegate = self
        txtWOType.inputView = pickerView
        txtStatus.inputView = pickerView
        txtPriority.inputView = pickerView
        txtCategory.inputView = pickerView
        webserviceCallForWOTypes()
        webserviceCallForCategoryTypes()
        webserviceCallForPriorityTypes()
        webserviceCallForStatusTypes()
        webserviceCallForCustomFields()
    }
    
    func setupUI()
    {
        btnSave.giveCornerRadius()
        btnCancel.giveCornerRadius()
        btnLocation.giveBorderToButton()
        txtOrderOwner.setRightImage(name: "search_small", placeholder: "--None--")
        txtOrderType.setRightImage(name: "search_small", placeholder: "--None--")
        txtWOType.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtStatus.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtPriority.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtCategory.setRightImage(name: "down-arrow_black", placeholder: "--None--")
        txtPrimaryConstact.setRightImage(name: "search_small", placeholder: "--None--")
        txtParentWorkOrder.setRightImage(name: "search_small", placeholder: "--None--")
        self.txtAddress.layer.borderWidth = 1.5
        self.txtAddress.layer.borderColor = UIColor.darkGray.cgColor
        self.txtviewDescription.layer.borderWidth = 1.5
        self.txtviewDescription.layer.borderColor = UIColor.darkGray.cgColor
       
        if flagResponse == "1" {
            responseWODetails()
            flagResponse = "0"
        }
        if accountName != nil {
            self.txtOrderType.text = accountName
        }
        if contactName != nil {
            self.txtPrimaryConstact.text = contactName
        }
    }
    
    func sendData(searchVC:SearchAccountViewController) {
        if textfieldTag == 1 {
            
            txtOrderOwner.text = searchVC.fullName
            assignedToID = searchVC.userID
        } else if textfieldTag == 2 {
            
            txtOrderType.text = searchVC.accountName
            acTypeID = searchVC.accountID
            webserviceCallForAccountDetails()
            
        } else if textfieldTag == 17 {
            txtPrimaryConstact.text = searchVC.contactName
            contactID = searchVC.contactID
            
        } else if textfieldTag == 18 {
            txtParentWorkOrder.text = searchVC.parentWO_Name
            parentWO_ID = searchVC.parentWO_ID
        }
    }
    
    func webserviceCallForAccountDetails()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "AccountID":acTypeID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.accountDetails(urlString: API.accountDetailsURL, parameters: parameters as? [String : String], headers: headers, vc: self) { (response:accountDetailsResponse) in
            
            if response.Result == "True"
            {
                self.txtAddress.text = response.data?.BillingAddress
                self.txtCity.text = response.data?.BillingCity
                self.txtState.text = response.data?.BillingState
                self.txtCountry.text = response.data?.BillingCountry
                self.txtPostalCode.text = response.data?.BillingPostalCode
                self.latitude = response.data?.BillingLatitude
                self.longitude = response.data?.BillingLongitude
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    func textfieldTapped(object:String)
    {
        let searchVC = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAccountViewController") as! SearchAccountViewController
        searchVC.delegate = self
        searchVC.objectFlag = object
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func webserviceCallForWOTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.workOrderTypes(urlString: API.woTypesURL, parameters: parameters , headers: headers, vc: self) { (response:WorkOrderTypesResponse) in
            
            if response.Result == "True"
            {
                self.woTypesArr = response.WorkOrderTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForPriorityTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.priorityTypes(urlString: API.priorityTypesURL, parameters: parameters , headers: headers, vc: self) { (response:PriorityTypesResponse) in
            
            if response.Result == "True"
            {
                self.priorityTypeArr = response.PriorityTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForStatusTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.statusTypes(urlString: API.statusTypesURL, parameters: parameters , headers: headers, vc: self) { (response:StatusTypesResponse) in
            
            if response.Result == "True"
            {
                self.statusArr = response.StatusTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCategoryTypes()
    {
        self.showHUD()
        let parameters = ["UserID":User.instance.UserID,
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.categoryTypes(urlString: API.categoryTypesURL, parameters: parameters , headers: headers, vc: self) { (response:CategoryTypesResponse) in
            
            if response.Result == "True"
            {
                self.categoryArr = response.CategoryTypesData!
                self.pickerView.reloadAllComponents()
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func webserviceCallForCreateWO()
    {
        
        if acTypeID == nil || assignedToID == nil || woTypeID == nil || statusID == nil || priorityID == nil  || categoryID == nil || txtviewDescription.text! == "Description*" || txtSubject.text! == "" || txtAddress.text! == "" || txtCity.text! == "" || txtState.text! == "" || txtCountry.text! == "" || txtPostalCode.text! == ""
        {
            Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
            return
        }
        
        if txtStartDateTime.text != "" {
            startTime = Helper.instance.convertDateFormat(date: txtStartDateTime.text!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "hh:mm a")
        }
        if txtEndDateTime.text != "" {
            endTime = Helper.instance.convertDateFormat(date: txtEndDateTime.text!, fromFormat: "MM/dd/YYYY hh:mm a", toFormat: "hh:mm a")
        }
        
        var parameters:[String:String] = [:]
        self.showHUD()
        ///////
        if customFieldKeyArr.count != 0 {
            
            for i in 0..<customFieldKeyArr.count {
                
                parameters["\(customFieldKeyArr[i])"] = customFieldValueArr[i]
            }
        }
        
        if checkboxKeyArr.count != 0 {
            for i in 0..<checkboxKeyArr.count {
                
                parameters["\(checkboxKeyArr[i])"] = checkboxEditedValueArr[i]
            }
        }
        ///////
        
        
        parameters["UserID"] = User.instance.UserID
        parameters["OrganizationID"] = User.instance.OrganizationID
        parameters["Account"] = acTypeID!
        parameters["AssignedTo"] = assignedToID!
        parameters["Subject"] = self.txtSubject.text ?? ""
        parameters["Description"] = self.txtviewDescription.text ?? ""
        parameters["Address"] = (self.txtAddress.text ?? "")
        parameters["City"] = self.txtCity.text ?? ""
        parameters["State"] = self.txtState.text ?? ""
        parameters["PostalCode"] = self.txtPostalCode.text ?? ""
        parameters["PopUpReminder"] = self.txtPopupReminder.text ?? ""
        parameters["Country"] = self.txtCountry.text ?? ""
        parameters["WorkOrderType"] = woTypeID!
        parameters["WOStatus"] = statusID!
        parameters["WOPriority"] = self.priorityID!
        parameters["PrimaryContact"] = contactID ?? ""
        parameters["WOStartTime"] = startTime ?? ""
        parameters["WOEndTime"] = endTime ?? ""
        parameters["StartDate"] = txtStartDateTime.text ?? ""
        parameters["EndDate"] = txtEndDateTime.text ?? ""
        parameters["IsRecurring"] = recurring
        parameters["RepeatEvery"] = RepeatEvery
        parameters["IntervalEvery"] = IntervalEvery
        parameters["RepeatOn"] = RepeatOn
        parameters["Ends"] = Ends
        parameters["StartOn"] = StartOn ?? ""
        parameters["EndsOnDate"] = EndsOnDate
        parameters["EndsAfterOccurrences"] = EndsAfterOccurrences
        parameters["StartTime"] = StartTime
        parameters["EndTime"] = EndTime
        parameters["ParentWorkOrder"] = parentWO_ID
        parameters["Latitude"] = latitude
        parameters["Longitude"] = longitude
        parameters["WOCategory"] = categoryID
        
        print(parameters)
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.createWO(urlString: API.createWOURL, parameters: parameters , headers: headers, vc: self) { (response:CreateWOResponse) in
            
            if response.Result == "True"
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WorkOrdersDetailsViewController") as! WorkOrdersDetailsViewController
                User.instance.workorderID = String(response.WorkOrderID!)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    func responseWODetails()
    {
        
        self.txtOrderOwner.text  = response?.data?.AssignedToName
        self.txtPrimaryConstact.text = response?.data?.PrimaryContactName
        self.txtPopupReminder.text = response?.data?.PopUpReminder
        self.txtSubject.text = response?.data?.Subject
        self.txtviewDescription.text = response?.data?.Description
        self.txtWOType.text =  response?.data?.WorkOrderTypeName
        self.txtPriority.text = response?.data?.Priority
        self.txtAddress.text = (response?.data?.Address) ?? ""
        self.txtCity.text = (response?.data?.City) ?? ""
        self.txtState.text = (response?.data?.State) ?? ""
        self.txtCountry.text = (response?.data?.Country) ?? ""
        self.txtPostalCode.text = (response?.data?.PostalCode) ?? ""
        self.txtStatus.text = response?.data?.Status
                
        self.txtStartDateTime.text = response?.data?.StartDate
        self.txtEndDateTime.text = response?.data?.EndDate
        self.recurring = (response?.data?.IsRecurring ?? "0")!
        self.txtStartDateTime.text = response?.data?.StartDate
                
        self.assignedToID = response?.data?.AssignedTo
        self.statusID = response?.data?.WOStatus
        self.priorityID = response?.data?.WOPriority
        self.categoryID = response?.data?.WOCategory
        self.woTypeID = response?.data?.WorkOrderType
        self.acTypeID = response?.data?.Account
        self.contactID = response?.data?.PrimaryContact
        self.txtOrderType.text = response?.data?.AccountName
        self.txtCategory.text = response?.data?.CategoryName
                
        self.txtParentWorkOrder.text = response?.data?.ParentWorkOrderName
        self.parentWO_ID = response?.data?.ParentWorkOrder
        self.latitude = response?.data?.Latitude
        self.longitude = response?.data?.Longitude
                
        if self.recurring == "0"
        {
            self.btnRecurring.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
                    
        }
        else {
            self.btnRecurring.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
        }
    }
    
    func webserviceCallForCustomFields()
    {
        self.showHUD()
        
        let parameters = ["UserID":User.instance.UserID,
                          "Object":"WorkOrder",
                          "OrganizationID":User.instance.OrganizationID]
        
        let headers = ["key":User.instance.key,
                       "token":User.instance.token]
        
        NetworkManager.sharedInstance.getCustomFields(urlString: API.getCustomFieldsURL, parameters: parameters, headers: headers, vc: self) { (response:GetCustomFields) in
            
            if response.Result == "True"
            {
                
                
                var constant = 0
                var constantHeight = 0
                var arrayCheckboxLabels:[String]?
                var arrayCheckboxValues:[String] = []
                
                var viewFromConstrain:Any = self.txtPostalCode
                for i in 0..<(response.data?.count)!  {
                    
                    if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                        if i == 0 {
                            constant = 40
                        } else {
                            constant = 10
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 45
                        case "LongText":
                            constantHeight = 90
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            arrayCheckboxValues = ((response.data?[i].OptionValues)?.components(separatedBy: ","))!
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 20)
                            print(constantHeight)
                        default:
                            print("no match")
                        }
                    } else { //IPAD
                        if i == 0 {
                            constant = 76
                        } else {
                            constant = 20
                        }
                        
                        switch response.data?[i].FieldType {
                            
                        case "DateTime","Date","Number","Text","Currency","URL":
                            constantHeight = 55
                        case "LongText":
                            constantHeight = 130
                        case "Checkbox":
                            arrayCheckboxLabels = (response.data?[i].OptionLabels)?.components(separatedBy: ",")
                            
                            
                            constantHeight = 40 + (arrayCheckboxLabels!.count * 30)
                        default:
                            print("no match")
                        }
                    }
                    if i == 0 {
                        Helper.instance.addLabel(viewBackground: self.viewBackground, upperView: self.txtPostalCode)
                    }
                    
                    let myView = UIView()
                    self.viewBackground.addSubview(myView)
                    
                    myView.translatesAutoresizingMaskIntoConstraints = false
                    
                    let topConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                    let heightConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(constantHeight))
                    
                    let trailingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPostalCode, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
                    let leadingConstraint = NSLayoutConstraint(item: myView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.txtPostalCode, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
                    self.view.addConstraints([topConstraint,heightConstraint,trailingConstraint,leadingConstraint])
                    
                    switch response.data?[i].FieldType {
                        
                    case "DateTime","Date","Number","Text","Currency","URL":
                        
                        if let customView = Bundle.main.loadNibNamed("TextfieldView", owner: self, options: nil)?.first as? TextfieldView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtLabelField.placeholder = (response.data?[i].FieldLabel)! + "*"
                                self.requiredCustomFieldsArr.append(customView.txtLabelField)
                            } else {
                                customView.txtLabelField.placeholder = response.data?[i].FieldLabel
                            }
                            customView.txtLabelField.accessibilityLabel = response.data?[i].FieldName
                            customView.txtLabelField.accessibilityValue = response.data?[i].FieldType
                            customView.txtLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtLabelField.delegate = self
                            customView.txtLabelField.accessibilityHint = "CustomTextField"
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "LongText":
                        if let customView = Bundle.main.loadNibNamed("LongTextView", owner: self, options: nil)?.first as? LongTextView {
                            
                            if response.data?[i].IsRequired == "1" {
                                customView.txtviewLabelField.text = (response.data?[i].FieldLabel)! + "*"
                                customView.txtviewLabelField.textColor = UIColor.lightGray
                                self.requiredCustomFieldsArr.append(customView.txtviewLabelField)
                            } else {
                                customView.txtviewLabelField.text = response.data?[i].FieldValue
                            }
                            customView.txtviewLabelField.accessibilityLabel = response.data?[i].FieldName
                            
                            customView.txtviewLabelField.tag = Int((response.data?[i].CustomFieldID)!)! * 10
                            
                            customView.txtviewLabelField.delegate = self
                            customView.txtviewLabelField.accessibilityHint = "CustomTextView"
                            
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                            
                        }
                        
                    case "Checkbox":
                        if let customView = Bundle.main.loadNibNamed("CheckboxView", owner: self, options: nil)?.first as? CheckboxView {
                            
                            self.checkboxKeyArr.append("CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]")
                            self.checkboxValueArr.append("")
                            self.fieldID = (response.data?[i].CustomFieldID)!
                            self.fieldName = (response.data?[i].FieldName)!
                            if response.data?[i].IsRequired == "1" {
                                self.requiredCheckBoxFieldArr.append("1")
                                customView.lblLabelField.text = (response.data?[i].FieldLabel)!  + "*"
                            } else {
                                self.requiredCheckBoxFieldArr.append("0")
                                customView.lblLabelField.text = response.data?[i].FieldLabel
                            }
                            for j in 0..<arrayCheckboxLabels!.count {
                                let button = UIButton()
                                button.tag = j
                                button.addTarget(self, action: #selector(self.btnCheckboxExtraFields(sender:)), for: .touchUpInside)
                                button.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                                button.accessibilityLabel = arrayCheckboxValues[j]
                                button.accessibilityHint = "CustomFieldIDs[\((response.data?[i].CustomFieldID) ?? "")]"
                                
                                let label = UILabel()
                                label.text = arrayCheckboxLabels![j]
                                
                                customView.stackviewButton.addArrangedSubview(button)
                                customView.stackviewLabel.addArrangedSubview(label)
                            }
                            
                            customView.frame = myView.frame
                            myView.addSubview(customView)
                        }
                    default:
                        print("no match")
                    }
                    
                    viewFromConstrain  = myView
                    
                }
                if UIDevice.current.userInterfaceIdiom == .phone { //IPHONE
                    constant = 20
                } else { //IPAD
                    constant = 40
                }
                let lblTopConstraint = NSLayoutConstraint(item: self.btnSave, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: viewFromConstrain, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: CGFloat(constant))
                
                self.view.addConstraints([lblTopConstraint])
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response.ResponseMsg)!, vc: self)
            }
        }
    }
    
    
    
    @objc func btnCheckboxExtraFields(sender:UIButton) {
        
        
        if let index = checkboxKeyArr.index(of: sender.accessibilityHint!) {
            
            if sender.currentImage == #imageLiteral(resourceName: "uncheck-box_black") {
                sender.setImage(#imageLiteral(resourceName: "check-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(value + "," + sender.accessibilityLabel!, at: index)
                
                
            } else {
                sender.setImage(#imageLiteral(resourceName: "uncheck-box_black"), for: .normal)
                
                let value = checkboxValueArr[index]
                
                let str = value.replacingOccurrences(of: ",\(sender.accessibilityLabel!)", with: "")
                
                
                checkboxValueArr.remove(at: index)
                checkboxValueArr.insert(str, at: index)
                
                
            }
            
            
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxKey:\(checkboxValueArr)")
        
        
    }
    
    func txtDate(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDate(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDate(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    func txtDateTime(txtfield: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        txtfield.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChangedForDateTime(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChangedForDateTime(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        lastTextfieldTapped.text = dateFormatter.string(from: sender.date)
        
    }
    
    
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        if let RecurrenceDetailsVC = sender.source as? RecurrenceSettingsViewController {
            
            Ends = RecurrenceDetailsVC.ends ?? ""
            IntervalEvery = RecurrenceDetailsVC.txtInterval.text ?? ""
            RepeatEvery = RecurrenceDetailsVC.txtRepeats.text ?? ""
            EndsOnDate = RecurrenceDetailsVC.txtOn.text ?? ""
            EndsAfterOccurrences = RecurrenceDetailsVC.txtAfter.text ?? ""
            StartTime = RecurrenceDetailsVC.txtStartTime.text ?? ""
            EndTime = RecurrenceDetailsVC.txtEndTime.text ?? ""
            RepeatOn = RecurrenceDetailsVC.weekArr.joined(separator: ",")
            StartOn = RecurrenceDetailsVC.txtStartsOn.text ?? ""
            
        }
        else if let SelectLocationVC = sender.source as? SelectLocationViewController {
            
            txtAddress.text = SelectLocationVC.address
            txtCity.text = SelectLocationVC.city
            txtState.text = SelectLocationVC.state
            txtCountry.text = SelectLocationVC.country
            txtPostalCode.text = SelectLocationVC.postalCode
            self.latitude = SelectLocationVC.latitude
            self.longitude = SelectLocationVC.longitude
        }
    }
    
    //MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if textfieldTag == 10
        {
            return woTypesArr.count + 1
        }
        else if textfieldTag == 11
        {
            return statusArr.count + 1
        }
        else if textfieldTag == 12
        {
            return priorityTypeArr.count + 1
        }
        else if textfieldTag == 13
        {
            return categoryArr.count + 1
        }
        
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if textfieldTag == 10
        {
            return row == 0 ? "--None--":woTypesArr[row-1].WorkOrderType
        }
        if textfieldTag == 11
        {
            return row == 0 ? "--None--":statusArr[row-1].Status
        }
        else if textfieldTag == 12
        {
            return row == 0 ? "--None--":priorityTypeArr[row-1].Priority
        }
        else if textfieldTag == 13
        {
            return row == 0 ? "--None--":categoryArr[row-1].CategoryName
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if textfieldTag == 10
        {
            if row == 0
            {
                txtWOType.text = ""
            }
            else{
                txtWOType.text = woTypesArr[row-1].WorkOrderType
                woTypeID = woTypesArr[row-1].WorkOrderTypeID
            }
        }
        else if textfieldTag == 11
        {
            if row == 0
            {
                txtStatus.text = ""
            }
            else{
                txtStatus.text = statusArr[row-1].Status
                statusID = statusArr[row-1].WOStatusID
            }
        }
        else if textfieldTag == 12
        {
            if row == 0
            {
                txtPriority.text = ""
            }
            else{
                txtPriority.text = priorityTypeArr[row-1].Priority
                priorityID = priorityTypeArr[row-1].WOPriorityID
            }
        }
        else if textfieldTag == 13
        {
            if row == 0
            {
                txtCategory.text = ""
            }
            else{
                txtCategory.text = categoryArr[row-1].CategoryName
                categoryID = categoryArr[row-1].WOCategoryID
            }
        }
        self.view.endEditing(true)
        pickerView.selectRow(0, inComponent:0, animated:true)
    }
    
    //MARK: Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //CustomField
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if textField.accessibilityValue == "Date"  {
                
                return false
            }
            if textField.accessibilityValue == "DateTime"  {
                
                return false
            }
        }
        
        
        //
        if textField == txtOrderOwner || textField == txtOrderType || textField == txtPrimaryConstact || textField == txtStatus || textField == txtPriority || textField == txtCategory || textField == txtWOType 
        {
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtOrderType.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtSubject.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtAddress.becomeFirstResponder()
            return false
        }
        else if textField.tag == 5
        {
            txtCity.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtState.becomeFirstResponder()
        }
        else if textField.tag == 7
        {
            txtCountry.becomeFirstResponder()
        }
        else if textField.tag == 8
        {
            txtPostalCode.becomeFirstResponder()
        }
        else if textField.tag == 9
        {
            txtWOType.becomeFirstResponder()
        }
        else if textField.tag == 10
        {
            txtStatus.becomeFirstResponder()
        }
        else if textField.tag == 11
        {
            txtPriority.becomeFirstResponder()
        }
        else if textField.tag == 12
        {
            txtCategory.becomeFirstResponder()
        }
        else if textField.tag == 13
        {
            txtPopupReminder.becomeFirstResponder()
        }
        else if textField.tag == 14
        {
            txtStartDateTime.becomeFirstResponder()
        }
        else if textField.tag == 15
        {
            txtEndDateTime.becomeFirstResponder()
        }
        else if textField.tag == 16
        {
            txtPrimaryConstact.becomeFirstResponder()
        }
        else if textField.tag == 17
        {
            txtPrimaryConstact.resignFirstResponder()
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 1
        {
            textfieldTag = textField.tag
            textField.resignFirstResponder()
            textfieldTapped(object: "Owner")
        }
        if textField.tag == 2
        {
            textfieldTag = textField.tag
            textField.resignFirstResponder()
            textfieldTapped(object: "Account")
        }
        if textField.tag == 10
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 11
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 12
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 13
        {
            textfieldTag = textField.tag
        }
        if textField.tag == 17
        {
            textfieldTag = textField.tag
            textField.resignFirstResponder()
            textfieldTapped(object: "Contact")
        }
        if textField.tag == 18
        {
            textfieldTag = textField.tag
            textField.resignFirstResponder()
            textfieldTapped(object: "WorkOrder")
        }
        if textField.tag == 4
        {
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            self.present(acController, animated: true, completion: nil)
        }
        pickerView.reloadAllComponents()
        
        // For CustomTextFields
        
        if textField.accessibilityHint == "CustomTextField" {
            
            lastTextfieldTapped = textField
            
            if textField.accessibilityValue == "Date"  {
                
                txtDate(txtfield: textField)
            }
            if textField.accessibilityValue == "DateTime"  {
                
                txtDateTime(txtfield: textField)
            }
            if textField.accessibilityValue == "Number" {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        
    }
    var lastTextfieldTapped:UITextField!
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "CustomTextField" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textField.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textField.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textField.tag/10)]")
                customFieldValueArr.append(textField.text ?? "")
            }
        }
    }
    //MARK: Textview Delegate Methods
    
    var lastTappedTextView:UITextView!
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description*"
        {
            textView.text = ""
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            lastTappedTextView = textView
            
            //Placeholder
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = "Description*"
        }
        
        //CustomField
        if textView.accessibilityHint == "CustomTextView" {
            
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(textView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(textView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(textView.tag/10)]")
                customFieldValueArr.append(textView.text ?? "")
            }
            //Placeholder
            if textView.text.isEmpty {
                textView.text = "Required Field*"
                textView.textColor = UIColor.lightGray
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: IBAction
    
    @IBAction func btnLocation(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        vc.isUnwindTo = "createWO"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnRecurring(_ sender: Any) {
        
        if (btnRecurring.currentImage?.isEqual(UIImage(named: "check-box")))!
        {
            btnRecurring.setImage(#imageLiteral(resourceName: "uncheck-box"), for: .normal)
            recurring = "0"
        }
        else{
            btnRecurring.setImage(#imageLiteral(resourceName: "check-box"), for: .normal)
            
            self.performSegue(withIdentifier: SEGUE_RECURRING, sender: self)
            recurring = "1"
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
    }
    @IBAction func btnSave(_ sender: Any) {
        if requiredCheckBoxFieldArr.count != 0 {
            
            for i in 0..<requiredCheckBoxFieldArr.count {
                
                if requiredCheckBoxFieldArr[i] == "1"
                {
                    if checkboxValueArr[i] == "" {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                    }
                }
            }
        }
        
        
        if self.requiredCustomFieldsArr.count != 0 {
            
            for i in 0..<self.requiredCustomFieldsArr.count {
                
                
                if self.requiredCustomFieldsArr[i] is UITextView {
                    let charset = CharacterSet(charactersIn: "*")
                    if ((self.requiredCustomFieldsArr[i] as! UITextView).text).rangeOfCharacter(from: charset) != nil {
                        
                        Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                        return
                        
                    }
                }
                
                
                if (self.requiredCustomFieldsArr[i] as AnyObject).text == "" {
                    
                    Helper.instance.showAlertNotification(message: Message.fillNecessaryField, vc: self)
                    return
                }
            }
        }
        
        //CustomTextField
        
        
        if lastTextfieldTapped != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTextfieldTapped.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTextfieldTapped.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTextfieldTapped.tag/10)]")
                customFieldValueArr.append(lastTextfieldTapped.text ?? "")
            }
        }
        
        
        
        //CustomTextView
        
        if lastTappedTextView != nil {
            if let index = customFieldKeyArr.index(of: "CustomFieldIDs[\(lastTappedTextView.tag/10)]") {
                
                if customFieldValueArr.count != 0 {
                    customFieldValueArr.remove(at: index)
                    customFieldValueArr.insert(lastTappedTextView.text ?? "", at: index)
                }
                
            } else {
                
                customFieldKeyArr.append("CustomFieldIDs[\(lastTappedTextView.tag/10)]")
                customFieldValueArr.append(lastTappedTextView.text ?? "")
            }
        }
        
        
        if checkboxValueArr.count != 0 {
            
            for i in 0..<checkboxValueArr.count {
                
                checkboxEditedValueArr.append(checkboxValueArr[i])
                let str = checkboxValueArr[i]
                checkboxEditedValueArr.remove(at: i)
                checkboxEditedValueArr.insert(String(str.dropFirst()), at: i)
            }
        }
        
        print("CheckBoxKey:\(checkboxKeyArr)")
        print("CheckBoxValue:\(checkboxValueArr)")
        print("CheckBoxValue:\(checkboxEditedValueArr)")
        
        print("TextKey:\(customFieldKeyArr)")
        print("TextValue:\(customFieldValueArr)")
        webserviceCallForCreateWO()
    }
    
    @IBAction func txtStartTime(_ sender: Any) {
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 101
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @IBAction func txtEndTime(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 102
        datePickerView.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM/dd/YYYY hh:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        if sender.tag == 101 {
            txtStartDateTime.text = dateFormatter.string(from: sender.date)
        } else {
            txtEndDateTime.text = dateFormatter.string(from: sender.date)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}

extension CreateWOViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress ?? "")")
        print("Coordinates:\(place.coordinate)")
        latitude = String(place.coordinate.latitude)
        longitude = String(place.coordinate.longitude)
        var area:String?
        for component in place.addressComponents! {
            
            if component.type == "route" {
                print(component.name)
                
                area = component.name
            }
            
            if component.type == "sublocality_level_1" {
                print(component.name)
                
                txtAddress.text = "\(place.name) \(area ?? "") \(component.name)"
            }
            if component.type == "postal_code"
            {
                print("Code: \(component.name)")
                txtPostalCode.text = component.name
            }
            if component.type == "administrative_area_level_2" {
                print("City: \(component.name)")
                txtCity.text = component.name
            }
            if component.type == "administrative_area_level_1"
            {
                print("State: \(component.name)")
                txtState.text = component.name
                
            }
            if component.type == "country"
            {
                print("Country: \(component.name)")
                txtCountry.text = component.name
            }
            
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

