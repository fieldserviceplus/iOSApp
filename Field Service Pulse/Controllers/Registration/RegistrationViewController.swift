//
//  RegistrationViewController.swift
//  Field Service Pulse
//
//  Created by Apple on 05/06/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ObjectMapper
import SVProgressHUD

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    //MARK: IBOutlets
    
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var txtFirstName: AkiraTextField!
    @IBOutlet weak var txtLastName: AkiraTextField!
    @IBOutlet weak var txtCompany: AkiraTextField!
    @IBOutlet weak var txtEmail: AkiraTextField!
    @IBOutlet weak var txtPhone: AkiraTextField!
    @IBOutlet weak var txtPassword: AkiraTextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    func setupUI()
    {
        btnSignup.giveCornerRadius()
    }
    
    //MARK: UITextfield Delegate Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1
        {
            txtLastName.becomeFirstResponder()
        }
        else if textField.tag == 2
        {
            txtCompany.becomeFirstResponder()
        }
        else if textField.tag == 3
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField.tag == 4
        {
            txtPhone.becomeFirstResponder()
        }
        else if textField.tag == 5
        {
            txtPassword.becomeFirstResponder()
        }
        else if textField.tag == 6
        {
            txtPassword.resignFirstResponder()
        }
        return true
    }
    
    //MARK: IBActions
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        
        if (txtFirstName?.text?.isBlank())!{
            
            Helper.instance.showAlertNotification(message: Message.validName, vc: self)
            return
        }
        if (txtLastName?.text?.isBlank())!{
             Helper.instance.showAlertNotification(message: Message.validName, vc: self)
            return
        }
        if (txtEmail?.text?.isValidEmail() == false){
             Helper.instance.showAlertNotification(message: Message.validEmail, vc: self)
            return
        }
        
        if (txtPassword?.text?.isPasswordValid() == false){
             Helper.instance.showAlertNotification(message: Message.validPassword, vc: self)
            return
        }
        
        if  !(txtPhone?.text?.isPhoneNumber)! {
             Helper.instance.showAlertNotification(message: Message.validPhone, vc: self)
            return
        }
        self.showHUD()
        let parameters = ["FirstName":txtFirstName.text!,
                          "LastName":txtLastName.text!,
                          "Email":txtEmail.text!,
                          "Password":txtPassword.text!,
                          "CompanyName":txtCompany.text!,
                          "PhoneNo":txtPhone.text!,
                          "City":"1",
                          "DeviceUDID":User.instance.deviceID,
                          "DeviceType":"iOS"]
        let headers = ["key":User.instance.key]
        
        NetworkManager.sharedInstance.signUp(urlString: API.signUpURL, parameters: parameters, headers: headers, vc: self) {(response:SignUpResponse?) in
            
            if response?.Result == "True"
            {
                self.txtFirstName.text = ""
                self.txtLastName.text = ""
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                self.txtCompany.text = ""
                self.txtPhone.text = ""
                
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            else
            {
                Helper.instance.showAlertNotification(message: (response?.ResponseMsg)!, vc: self)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
